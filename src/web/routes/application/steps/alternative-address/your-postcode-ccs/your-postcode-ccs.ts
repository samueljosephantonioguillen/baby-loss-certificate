import { validationResult } from "express-validator";

import * as logger from "../../../../../logger/logger";
import { ADDRESS_CCS_KEYS } from "../../../constants";
import { stateMachine } from "../../../flow-control/state-machine";
import { SET_NEXT_ALLOWED_PATH } from "../../../flow-control/state-machine/actions";
import { contextPath } from "../../../paths/context-path";
import {
  transformOsPlacesApiResponse,
  getAddressLookupResults,
} from "./os-places";
import { validate } from "./validate";
import { sanitise } from "../sanitise";
import { userIsAgent, userIsBackOffice } from "../../../../azure-routes/auth";
import { getFlowDataObjectName } from "../../../tools/data-object";

const pageContent = ({ translate }) => ({
  title: translate("altPostcode.title"),
  sectionTitle: translate("section.yourDetails"),
  heading: translate("altPostcode.heading"),
  body: translate("altPostcode.body"),
  postcode: translate("altPostcode.postcode"),
  enterAddressManually: translate("altPostcode.enterAddressManually"),
  backLinkText: translate("buttons:back"),
  continueButtonText: translate("buttons:continue"),
});

const isNavigable = (req) =>
  (userIsAgent(req) &&
    req.session[getFlowDataObjectName(req.session.journeyName)][
      "confirm-your-address"
    ] === "alt") ||
  (userIsBackOffice(req) &&
    !!req.session[getFlowDataObjectName(req.session.journeyName)][
      "reference-number"
    ]);

const resetAddressKey = (address, key) => ({ ...address, [key]: "" });

const resetAddress = (claim) => ({
  ...claim,
  ...ADDRESS_CCS_KEYS.reduce(resetAddressKey, {}),
});

const behaviourForGet = (config, journey) => (req, res, next) => {
  if (req.session.postcodeLookupError) {
    delete req.session.postcodeLookupError;
  }

  stateMachine.dispatch(
    SET_NEXT_ALLOWED_PATH,
    req,
    journey,
    contextPath("/enter-your-address", req.session.journeyName)
  );

  next();
};

const behaviourForPost = (config) => async (req, res, next) => {
  if (!validationResult(req).isEmpty()) {
    return next();
  }

  const dataObjectName = getFlowDataObjectName(req.session.journeyName);

  req.session[dataObjectName] = resetAddress(req.session[dataObjectName]);

  try {
    const addressLookupResults = await getAddressLookupResults(
      config,
      req.body["postcode-ccs"]
    );

    const results = transformOsPlacesApiResponse(addressLookupResults.data);

    req.session.postcodeLookupResults = results;

    if (Array.isArray(results) && results.length === 0) {
      res.locals.errorTitleText = req.t("validation:errorTitleText");
      res.locals.errors = [
        {
          path: "postcode-ccs",
          msg: req.t("yourPostcode.noAddressFoundError"),
        },
      ];
    }

    return next();
  } catch (error) {
    logger.logger.error(
      `Error looking up address for postcode-ccs: ${error}`,
      req
    );
    req.session.postcodeLookupResults = [];
    req.session.postcodeLookupError = true;
    res.locals.errorTitleText = req.t("validation:errorTitleText");
    res.locals.errors = [
      {
        path: "postcode-ccs",
        msg: req.t("yourPostcode.addressSearchError"),
      },
    ];
    return next();
  }
};

const yourPostcodeCcs = {
  path: "/lookup-postcode",
  template: "your-postcode-ccs",
  pageContent,
  isNavigable,
  sanitise,
  validate,
  behaviourForGet,
  behaviourForPost,
};

export {
  yourPostcodeCcs,
  pageContent,
  isNavigable,
  sanitise,
  validate,
  behaviourForGet,
  behaviourForPost,
};
