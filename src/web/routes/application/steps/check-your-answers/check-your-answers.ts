import { START } from "../../../start";
import { DECLARATION_URL } from "../../paths/paths";
import { contextPath } from "../../paths/context-path";

import { toMultiLineString } from "../common/formats";
import { toLongLocaleDateString } from "../common/format-date";
import { isNilOrEmpty } from "../../../../../common/predicates";

import { stateMachine } from "../../flow-control/state-machine";
import { IN_PROGRESS, IN_REVIEW } from "../../flow-control/states";
import {
  INCREMENT_NEXT_ALLOWED_PATH,
  SET_NEXT_ALLOWED_PATH,
} from "../../flow-control/state-machine/actions";
import { ADDRESS_CCS_KEYS, ADDRESS_KEYS } from "../../constants";
import { userIsAgent, userIsBackOffice } from "../../../azure-routes/auth";
import { logger } from "../../../../logger/logger";
import { getFlowDataObjectName } from "../../tools/data-object";

const getKeyFromSession = (session) => (key) => session[key];

const getPostalAddress = (req) => {
  const dataObjectName = getFlowDataObjectName(req.session.journeyName);
  if (
    (req.session[dataObjectName]["confirm-your-address"] === "alt" &&
      userIsAgent(req)) ||
    userIsBackOffice(req)
  ) {
    return toMultiLineString(
      ADDRESS_CCS_KEYS.map(getKeyFromSession(req.session[dataObjectName]))
    );
  }

  return toMultiLineString(
    ADDRESS_KEYS.map(getKeyFromSession(req.session[dataObjectName]))
  );
};

const showSecondParentLabel = (translate, req) => {
  const dataObjectName = getFlowDataObjectName(req.session.journeyName);
  if (
    req.session[dataObjectName]["add-another-parent"] === "yes" &&
    !!req.session[dataObjectName]["their-first-name"] &&
    !!req.session[dataObjectName]["their-last-name"] &&
    userIsBackOffice(req)
  ) {
    return `${req.session[dataObjectName]["their-first-name"]} ${req.session[dataObjectName]["their-last-name"]}`;
  }
  if (req.session[dataObjectName]["add-another-parent"] === "yes") {
    return translate("checkYourAnswers.otherParentNameTbc");
  }

  return "";
};

const pageContent = ({ translate, req }) => ({
  title: translate("checkYourAnswers.title"),
  heading: translate("checkYourAnswers.heading"),
  yourDetails: translate("checkYourAnswers.yourDetails"),
  checkYourDetails: translate("checkYourAnswers.checkYourDetails", {
    yourName: `${
      req.session[getFlowDataObjectName(req.session.journeyName)]["first-name"]
    } ${
      req.session[getFlowDataObjectName(req.session.journeyName)]["last-name"]
    }`,
    postalAddress: getPostalAddress(req),
    emailAddress:
      req.session[getFlowDataObjectName(req.session.journeyName)][
        "applicant-email"
      ],
  }),
  theirDetails: translate("checkYourAnswers.theirDetails"),
  checkTheirDetails: translate("checkYourAnswers.checkTheirDetails", {
    doYouWantToAddAnotherParent:
      req.session[getFlowDataObjectName(req.session.journeyName)][
        "add-another-parent"
      ] === "yes"
        ? "Yes"
        : "No",
    theirEmailAddress:
      req.session[getFlowDataObjectName(req.session.journeyName)][
        "their-email"
      ],
    theirDateOfBirth:
      req.session[getFlowDataObjectName(req.session.journeyName)][
        "add-another-parent"
      ] === "yes"
        ? toLongLocaleDateString(
            req.session[getFlowDataObjectName(req.session.journeyName)][
              "their-date-of-birth-day"
            ],
            req.session[getFlowDataObjectName(req.session.journeyName)][
              "their-date-of-birth-month"
            ],
            req.session[getFlowDataObjectName(req.session.journeyName)][
              "their-date-of-birth-year"
            ]
          )
        : "",
  }),
  certificateDetails: translate("checkYourAnswers.certificateDetails"),
  firstParent: translate("checkYourAnswers.firstParent"),
  checkFirstParent: translate(
    "checkYourAnswers.checkCertificateDetails.checkFirstParent",
    {
      yourName: `${
        req.session[getFlowDataObjectName(req.session.journeyName)][
          "first-name"
        ]
      } ${
        req.session[getFlowDataObjectName(req.session.journeyName)]["last-name"]
      }`,
      yourRelation:
        req.session[getFlowDataObjectName(req.session.journeyName)][
          "relation-to-baby"
        ],
    }
  ),
  secondParent: translate("checkYourAnswers.secondParent"),
  checkSecondParent: translate(
    "checkYourAnswers.checkCertificateDetails.checkSecondParent",
    {
      theirName: showSecondParentLabel(translate, req),
      theirRelation:
        req.session[getFlowDataObjectName(req.session.journeyName)][
          "their-relation-to-baby"
        ],
      secondParentAdded:
        req.session[getFlowDataObjectName(req.session.journeyName)][
          "add-another-parent"
        ] === "yes"
          ? "yes"
          : "no",
    }
  ),
  babyDetails: translate("checkYourAnswers.babyDetails"),
  checkBabyDetails: translate(
    "checkYourAnswers.checkCertificateDetails.checkBabyDetails",
    {
      babyName:
        req.session[getFlowDataObjectName(req.session.journeyName)][
          "baby-name"
        ],
      babySex:
        req.session[getFlowDataObjectName(req.session.journeyName)]["baby-sex"],
      babyLossDate: isNilOrEmpty(
        req.session[getFlowDataObjectName(req.session.journeyName)][
          "baby-date-of-loss"
        ]
      )
        ? ""
        : toLongLocaleDateString(
            req.session[getFlowDataObjectName(req.session.journeyName)][
              "baby-date-of-loss-day"
            ],
            req.session[getFlowDataObjectName(req.session.journeyName)][
              "baby-date-of-loss-month"
            ],
            req.session[getFlowDataObjectName(req.session.journeyName)][
              "baby-date-of-loss-year"
            ]
          ),
      babyLossLocation:
        req.session[getFlowDataObjectName(req.session.journeyName)][
          "baby-loss-location"
        ],
    }
  ),
  change: translate("change"),
  button: translate("checkYourAnswers.button"),
  backLinkText: translate("buttons:back"),
  backOfficeCheckYourDetails: translate(
    "checkYourAnswers.backOfficeCheckYourDetails",
    {
      yourName: `${
        req.session[getFlowDataObjectName(req.session.journeyName)][
          "first-name"
        ]
      } ${
        req.session[getFlowDataObjectName(req.session.journeyName)]["last-name"]
      }`,
      postalAddress: getPostalAddress(req),
      emailAddress:
        req.session[getFlowDataObjectName(req.session.journeyName)][
          "applicant-email"
        ],
    }
  ),
  backOfficeCheckTheirDetails: translate(
    "checkYourAnswers.backOfficeCheckTheirDetails",
    {
      theirDateOfBirth:
        req.session[getFlowDataObjectName(req.session.journeyName)][
          "add-another-parent"
        ] === "yes"
          ? toLongLocaleDateString(
              req.session[getFlowDataObjectName(req.session.journeyName)][
                "their-date-of-birth-day"
              ],
              req.session[getFlowDataObjectName(req.session.journeyName)][
                "their-date-of-birth-month"
              ],
              req.session[getFlowDataObjectName(req.session.journeyName)][
                "their-date-of-birth-year"
              ]
            )
          : "",
    }
  ),
  backOfficeCheckFirstParent: translate(
    "checkYourAnswers.checkCertificateDetails.backOfficeCheckFirstParent",
    {
      yourName: `${
        req.session[getFlowDataObjectName(req.session.journeyName)][
          "first-name"
        ]
      } ${
        req.session[getFlowDataObjectName(req.session.journeyName)]["last-name"]
      }`,
      yourRelation:
        req.session[getFlowDataObjectName(req.session.journeyName)][
          "relation-to-baby"
        ],
    }
  ),
  backOfficeCheckSecondParent: translate(
    "checkYourAnswers.checkCertificateDetails.backOfficeCheckSecondParent",
    {
      theirName: showSecondParentLabel(translate, req),
      theirRelation:
        req.session[getFlowDataObjectName(req.session.journeyName)][
          "their-relation-to-baby"
        ],
    }
  ),
});

const citizenRequiredFields = [
  "date-of-birth",
  "postcode",
  "first-name",
  "last-name",
  "relation-to-baby",
];

const agentRequiredFields = [
  "date-of-birth",
  "first-name",
  "last-name",
  "relation-to-baby",
];

const backOfficeRequiredFields = [
  "date-of-birth",
  "postcode-ccs",
  "first-name",
  "last-name",
  "relation-to-baby",
];

function isMissingRequiredFields(data, req) {
  if (userIsBackOffice(req)) {
    return backOfficeRequiredFields.some((field) => isNilOrEmpty(data[field]));
  } else if (userIsAgent(req)) {
    // if has all ccs fields and has postcode or postcode ccs
    const requiredAgentFieldsExist = agentRequiredFields.every(
      (field) => !isNilOrEmpty(data[field])
    );
    const isAgentPoscodeSetWhenNotUsingAltAddress =
      !isNilOrEmpty(data["postcode"]) && data["confirm-your-address"] !== "alt";
    const isAgentPoscodeSetWhenUsingAltAddress =
      !isNilOrEmpty(data["postcode-ccs"]) &&
      data["confirm-your-address"] === "alt";
    return !(
      requiredAgentFieldsExist &&
      (isAgentPoscodeSetWhenNotUsingAltAddress ||
        isAgentPoscodeSetWhenUsingAltAddress)
    );
  }
  return citizenRequiredFields.some((field) => isNilOrEmpty(data[field]));
}

const behaviourForGet = (config, journey) => (req, res, next) => {
  if (
    isMissingRequiredFields(
      req.session[getFlowDataObjectName(req.session.journeyName)],
      req
    )
  ) {
    logger.error("Missing required fields in check-your-answers");
    return res.redirect(contextPath(START, journey.name));
  }

  stateMachine.setState(IN_REVIEW, req, journey);
  stateMachine.dispatch(INCREMENT_NEXT_ALLOWED_PATH, req, journey);

  next();
};

const behaviourForPost = (config, journey) => (req, res) => {
  stateMachine.setState(IN_PROGRESS, req, journey);
  stateMachine.dispatch(
    SET_NEXT_ALLOWED_PATH,
    req,
    journey,
    contextPath(DECLARATION_URL, journey.name)
  );

  return res.redirect(contextPath(DECLARATION_URL, journey.name));
};

const checkYourAnswers = {
  path: "/check-your-answers",
  template: "check-your-answers",
  pageContent,
  behaviourForGet,
  behaviourForPost,
};

export {
  checkYourAnswers,
  pageContent,
  behaviourForGet,
  behaviourForPost,
  getKeyFromSession,
  isMissingRequiredFields,
  getPostalAddress,
};
