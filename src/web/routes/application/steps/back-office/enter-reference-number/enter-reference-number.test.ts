import expect from "expect";

import {
  enterReferenceNumber,
  isNavigable,
  pageContent,
  validate,
} from "./enter-reference-number";
import { acceptableReferenceNumberTypes } from "../proof-of-name/validate";
import { AGENT_ROLE, BACKOFFICE_ROLE } from "../../../constants";
import { config } from "../../../../../../config";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";
import { countryCodeMapper } from "../../../tools/country-code-parser";

test("enterReferenceNumber should match expected outcomes", () => {
  const expectedResults = {
    path: "/reference-number",
    template: "enter-reference-number",
    isNavigable,
    pageContent,
    validate,
  };

  expect(enterReferenceNumber).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const req = { session: { applicant: {}, journeyName: MAINFLOW.name } };
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("enterReferenceNumber.title"),
    sectionTitle: translate("section.yourDetails"),
    heading: translate("enterReferenceNumber.heading"),
    referenceNumber: translate("enterReferenceNumber.referenceNumber"),
    countryOrigin: translate("enterReferenceNumber.countryOrigin"),
    countryCodes: {
      data: countryCodeMapper(
        req.session.applicant["reference-number-country"]
      ),
    },
    continueButtonText: translate("buttons:continue"),
  };
  const result = pageContent({ translate, req });

  expect(result).toEqual(expected);
});

test("pageContent should return expected results when reference-number-country is present", () => {
  const applicant = {
    "reference-number-country": "USA",
  };
  const req = { session: { applicant, journeyName: MAINFLOW.name } };
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("enterReferenceNumber.title"),
    sectionTitle: translate("section.yourDetails"),
    heading: translate("enterReferenceNumber.heading"),
    referenceNumber: translate("enterReferenceNumber.referenceNumber"),
    countryOrigin: translate("enterReferenceNumber.countryOrigin"),
    countryCodes: {
      data: countryCodeMapper(
        req.session.applicant["reference-number-country"]
      ),
    },
    continueButtonText: translate("buttons:continue"),
  };
  const result = pageContent({ translate, req });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  config.environment.USE_AUTHENTICATION = true;

  test.each(acceptableReferenceNumberTypes)(
    "should return true when backoffice_role and reference-number-type type is %s",
    (proof) => {
      const req = {
        session: {
          journeyName: MAINFLOW.name,
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "reference-number-type": proof,
          },
          account: {
            idTokenClaims: {
              roles: [BACKOFFICE_ROLE],
            },
          },
        },
      };
      const result = isNavigable(req);

      expect(result).toEqual(true);
    }
  );

  test("should return false when not backoffice_role and reference-number-type type is accepted", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "reference-number-type": "birthCertificate",
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when agent_role and reference-number-type type is accepted", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "reference-number-type": "birthCertificate",
        },
        account: {
          idTokenClaims: {
            roles: [AGENT_ROLE],
          },
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when backoffice_role and reference-number-type type is not accepted", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "reference-number-type": "invalid",
        },
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});
