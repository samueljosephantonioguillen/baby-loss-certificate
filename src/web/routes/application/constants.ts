const ADDRESS_LINE_1_KEY = "address-line-1";
const ADDRESS_LINE_2_KEY = "address-line-2";
const ADDRESS_LINE_3_KEY = "address-line-3";
const ADDRESS_LINE_4_KEY = "address-line-4";
const ADDRESS_LINE_5_KEY = "address-line-5";
const POSTCODE_KEY = "postcode";

export const ADDRESS_KEYS = [
  ADDRESS_LINE_1_KEY,
  ADDRESS_LINE_2_KEY,
  ADDRESS_LINE_3_KEY,
  ADDRESS_LINE_4_KEY,
  ADDRESS_LINE_5_KEY,
  POSTCODE_KEY,
];

const ADDRESS_LINE_1_CCS_KEY = "address-line-1-ccs";
const ADDRESS_LINE_2_CCS_KEY = "address-line-2-ccs";
const ADDRESS_LINE_3_CCS_KEY = "address-line-3-ccs";
const ADDRESS_LINE_4_CCS_KEY = "address-line-4-ccs";
const ADDRESS_LINE_5_CCS_KEY = "address-line-5-ccs";
const POSTCODE_CCS_KEY = "postcode-ccs";

export const ADDRESS_CCS_KEYS = [
  ADDRESS_LINE_1_CCS_KEY,
  ADDRESS_LINE_2_CCS_KEY,
  ADDRESS_LINE_3_CCS_KEY,
  ADDRESS_LINE_4_CCS_KEY,
  ADDRESS_LINE_5_CCS_KEY,
  POSTCODE_CCS_KEY,
];

export const ADDRESS_LINE_MAX_LENGTH = 50;
export const ADDRESS_LINE_PATTERN =
  /^(?!.*[\u00D7\u00F7])[A-Za-z0-9\u1E00-\u1EFF\u0100-\u017F\u00c0-\u00ff\s\-'.]*$/;

export const TOWN_OR_CITY_MAX_LENGTH = 50;
export const TOWN_OR_CITY_PATTERN =
  /^(?!.*[\u00D7\u00F7])[A-Za-z0-9\u1E00-\u1EFF\u0100-\u017F\u00c0-\u00ff\s\-'.]*$/;

export const COUNTY_MAX_LENGTH = 50;
export const COUNTY_PATTERN =
  /^(?!.*[\u00D7\u00F7])[A-Za-z0-9\u1E00-\u1EFF\u0100-\u017F\u00c0-\u00ff\s\-'.]*$/;

const ukPostcodeLetters = "[A-PR-UWYZ]";
const ukPostcodeDigits = "\\d";
const ukPostcodeAreaCodes = `[A-HK-Y]\\d([0-9ABEHMNPRV-Y])?|\\d[A-HJKPS-UW]`;

export const UK_POSTCODE_PATTERN = new RegExp(
  `^(GIR ?0AA|${ukPostcodeLetters}(${ukPostcodeDigits}{1,2}|${ukPostcodeAreaCodes})( ?\\d[ABD-HJLNP-UW-Z]{2})?)$`
);

export const LOSS_LOCATION_MAX_LENGTH = 100;
export const LOSS_LOCATION_PATTERN =
  /^(?!.*[\u00D7\u00F7])[A-Za-z0-9\u1E00-\u1EFF\u0100-\u017F\u00c0-\u00ff\s\-,'.]*$/;

export const FIRST_NAME_MAX_LENGTH = 35;
export const LAST_NAME_MAX_LENGTH = 35;
export const BABY_NAME_MAX_LENGTH = 75;
export const NAME_PATTERN =
  /^(?!.*[\u00D7\u00F7])(?![-'])[A-Za-z\u1E00-\u1EFF\u0100-\u017F\u00c0-\u00ff\s\-']*$/;

export const NHS_NUMBER_LENGTH = 10;
export const NHS_NUMBER_PATTERN = /^\d{10}$/;
export const NHS_NUMBER_PATTERN_NO_LENGTH = /^\d*$/;
export const SIGN_IN_URL = "/auth/signin";
export const SIGN_OUT_URL = "/auth/signout";
export const LOCAL_USER_CYPHER = "LOCAL_USER_CYPHER";
export const BACKOFFICE_ROLE = "BackOffice";
export const AGENT_ROLE = "Agent";

export const EMAIL_VALID_CHARACTERS_REGEX = /^[a-zA-Z0-9\-_'@.]+$/;
export const EMAIL_REGEX = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;

export const OS_PLACES_ADDRESS_KEYS = [
  "ADDRESS",
  "ORGANISATION_NAME",
  "SUB_BUILDING_NAME",
  "BUILDING_NAME",
  "BUILDING_NUMBER",
  "DEPENDENT_THOROUGHFARE_NAME",
  "THOROUGHFARE_NAME",
  "DOUBLE_DEPENDENT_LOCALITY",
  "DEPENDENT_LOCALITY",
  "POST_TOWN",
  "POSTCODE",
  "LOCAL_CUSTODIAN_CODE_DESCRIPTION",
  "UDPRN",
];

export const MAX_CERT_REFERENCE_LENGTH = 13;
export const CERT_REFERENCE_PATTERN = /^[a-zA-Z0-9-]{1,13}$/;
