import { check } from "express-validator";
import { translateValidationMessage } from "../common/translate-validation-message";

const validate = () => [
  check("declaration")
    .not()
    .isEmpty()
    .bail()
    .withMessage(
      translateValidationMessage("validation:selectDeclarationAgree")
    ),
];

export { validate };
