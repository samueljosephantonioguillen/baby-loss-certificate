import { check } from "express-validator";

import { translateValidationMessage } from "../common/translate-validation-message";

const validate = () => [
  check("are-details-correct")
    .not()
    .isEmpty()
    .bail()
    .withMessage(
      translateValidationMessage("validation:selectAreDetailsCorrect")
    ),
];

export { validate };
