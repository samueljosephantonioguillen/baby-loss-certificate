import { START } from "../../../start";
import { userIsBackOffice } from "../../../azure-routes/auth";
import { contextPath } from "../../paths/context-path";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../tools/data-object";
import { formatReference } from "../common/certificate-reference-generator";

const pageContent = ({ translate, req }) => ({
  title: translate("requestComplete.title"),
  requestComplete: translate("requestComplete.heading"),
  requestPending: translate("requestComplete.pendingHeading"),
  pendingSecondParent: !!(
    req.session[MAINFLOW_DATA_OBJECT_NAME] &&
    req.session[MAINFLOW_DATA_OBJECT_NAME]["add-another-parent"] === "yes" &&
    !userIsBackOffice(req)
  ),
  yourReference: translate("requestComplete.yourReference", {
    certificateReference: formatReference(req.session.certificateReference),
  }),
  whatHappensNextHeading: translate("requestComplete.whatHappensNext.heading"),
  whatHappensNextLineOne: translate(
    "requestComplete.whatHappensNext.firstLine"
  ),
  whatHappensNextLineTwo: translate(
    "requestComplete.whatHappensNext.secondLine",
    { serviceStart: contextPath(START, req.session.journeyName) }
  ),
  pendingWhatHappensNextLineOne: translate(
    "requestComplete.pendingWhatHappensNext.firstLine"
  ),
  pendingWhatHappensNextLineTwo: translate(
    "requestComplete.pendingWhatHappensNext.secondLine"
  ),
  pendingWhatHappensNextLineThree: translate(
    "requestComplete.pendingWhatHappensNext.thirdLine",
    { serviceStart: contextPath(START, req.session.journeyName) }
  ),
  supportOrganisationsHeading: translate("supportOrganisations.heading"),
  supportOrganisationsBody: translate("supportOrganisations.body"),
  supportOrganisationsList: translate("supportOrganisations.listItems"),
  link: translate("requestComplete.link"),
});

const behaviourForGet = () => (req, res, next) => {
  res.set("Clear-Site-Data", '"storage"');
  next();
};

const requestComplete = {
  path: "/confirmation",
  template: "request-complete",
  pageContent,
  behaviourForGet,
};

export { requestComplete, pageContent, behaviourForGet };
