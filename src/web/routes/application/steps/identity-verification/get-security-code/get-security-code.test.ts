import expect from "expect";

const sendOtpEmail = jest.fn();
const sendOtpSms = jest.fn();
jest.mock("../../common/notify-client", () => ({
  sendOtpEmail: sendOtpEmail,
  sendOtpSms: sendOtpSms,
}));

const generateOTP = jest.fn();
jest.mock("./generate-otp", () => ({
  generateOTP: generateOTP,
}));

const sessionDestroyButKeepLoggedIn = jest.fn();
jest.mock("../../common/session-destroy", () => ({
  sessionDestroyButKeepLoggedIn: sessionDestroyButKeepLoggedIn,
}));

import {
  obfuscateEmail,
  obfuscatePhoneNumber,
} from "../../common/obfuscate-contact";

import {
  getSecurityCode,
  pageContent,
  isNavigable,
  validate,
  behaviourForPost,
  MAXIMUM_ALLOWED_OTP_SENDS,
} from "./get-security-code";

import { PATH as tryAgainPath } from "../../../../error-pages/try-again";

import { MAINFLOW, SECOND_PARENT_FLOW } from "../../../journey-definitions";
import { contextPath } from "../../../paths/context-path";
import {
  MAINFLOW_DATA_OBJECT_NAME,
  SECOND_PARENT_DATA_OBJECT_NAME,
} from "../../../tools/data-object";
import { BACKOFFICE_ROLE } from "../../../constants";
import { config } from "../../../../../../config";

test("getSecurityCode should match expected outcomes", () => {
  const expectedResults = {
    path: "/security-code",
    template: "get-security-code",
    pageContent,
    isNavigable,
    validate,
    behaviourForPost,
  };

  expect(getSecurityCode).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const req = {
    session: {
      journeyName: MAINFLOW.name,
      [MAINFLOW_DATA_OBJECT_NAME]: {
        email: "test@email.com",
        "phone-number": "12345678987",
      },
    },
  };

  const expected = {
    title: translate("getSecurityCode.title"),
    sectionTitle: translate("section.yourDetails"),
    heading: translate("getSecurityCode.heading"),
    bodyLineOne: translate("getSecurityCode.body.firstLine"),
    bodyLineTwo: translate("getSecurityCode.body.secondLine"),
    renderEmailOption: true,
    renderTextOption: true,
    email: translate("getSecurityCode.email", {
      emailAddress: obfuscateEmail(
        req.session[MAINFLOW_DATA_OBJECT_NAME].email
      ),
    }),
    text: translate("getSecurityCode.text", {
      phoneNumber: obfuscatePhoneNumber(
        req.session[MAINFLOW_DATA_OBJECT_NAME]["phone-number"]
      ),
    }),
    details: translate("getSecurityCode.details"),
    detailsReveal: translate("getSecurityCode.detailsReveal"),
    backLinkText: translate("buttons:back"),
    continueButtonText: translate("buttons:continue"),
  };
  const result = pageContent({ translate, req });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when email and phone-number are populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          email: "test@email.com",
          "phone-number": "12345678987",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return true when only email is populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          email: "test@email.com",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return true when only phone-number is populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "phone-number": "12345678987",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when no contact methods are populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {},
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when email and phone-number are populated and user is back office", () => {
    config.environment.USE_AUTHENTICATION = true;
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          email: "test@email.com",
          "phone-number": "12345678987",
        },
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});

describe("behaviourForPost", () => {
  test("should send email when selected method is email and resets otp attempts", async () => {
    const req = {
      body: {
        "security-code-method": "email",
      },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          email: "test@email.com",
          "phone-number": "01632 960669",
          otpAttempts: 2,
          otpEmailSends: 1,
          otpTextSends: 0,
        },
      },
    };
    const res = {};
    const next = jest.fn();

    const expectedOtp = "123456";
    const expectedReq = {
      ...req,
      session: {
        ...req.session,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          otp: expectedOtp,
          otpAttempts: 0,
          otpEmailSends: 2,
          otpTextSends: 0,
          email: req.session[MAINFLOW_DATA_OBJECT_NAME].email,
          "phone-number":
            req.session[MAINFLOW_DATA_OBJECT_NAME]["phone-number"],
        },
      },
    };

    sendOtpEmail.mockResolvedValueOnce({ msg: "here" });
    generateOTP.mockReturnValue(expectedOtp);

    await behaviourForPost()(req, res, next);

    expect(req).toEqual(expectedReq);
    expect(next).toBeCalledTimes(1);
  });

  test("should send sms when selected method is text", async () => {
    const req = {
      body: {
        "security-code-method": "text",
      },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          email: "test@email.com",
          "phone-number": "01632 960669",
          otpAttempts: 0,
          otpEmailSends: 0,
          otpTextSends: 1,
        },
      },
    };
    const res = {};
    const next = jest.fn();

    const expectedOtp = "123456";
    const expectedReq = {
      ...req,
      session: {
        ...req.session,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          otp: expectedOtp,
          otpTextSends: 2,
          otpEmailSends: 0,
          otpAttempts: 0,
          email: "test@email.com",
          "phone-number": "01632 960669",
        },
      },
    };

    sendOtpSms.mockResolvedValueOnce({ msg: "here" });
    generateOTP.mockReturnValue(expectedOtp);

    await behaviourForPost()(req, res, next);

    expect(req).toEqual(expectedReq);
    expect(next).toBeCalledTimes(1);
  });

  test("should redirect to try again page when status 429 is returned", async () => {
    const req = {
      body: {
        "security-code-method": "email",
      },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          email: "test@email.com",
          "phone-number": "01632 960669",
          otpEmailSends: 0,
          otpTextSends: 0,
          otpAttempts: 0,
        },
      },
    };
    const res = { redirect: jest.fn() };
    const next = jest.fn();

    const expectedOtp = "123456";
    const expectedReq = {
      ...req,
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          ...req.session[MAINFLOW_DATA_OBJECT_NAME],
          "security-code-method": "email",
          otp: expectedOtp,
          otpEmailSends: 1,
          otpTextSends: 0,
          otpAttempts: 0,
        },
      },
    };

    sendOtpEmail.mockRejectedValue({ response: { status: 429 } });
    generateOTP.mockReturnValue(expectedOtp);

    await behaviourForPost()(req, res, next);

    expect(req).toEqual(expectedReq);
    expect(res.redirect).toBeCalledTimes(1);
    expect(res.redirect).toBeCalledWith(tryAgainPath);
  });

  test("should destroy session and redirect to problem with service page on other errors", async () => {
    const req = {
      body: {
        "security-code-method": "email",
      },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          email: "test@email.com",
          "phone-number": "01632 960669",
          otpEmailSends: 0,
          otpTextSends: 0,
          otpAttempts: 0,
        },
      },
    };
    const res = { clearCookie: jest.fn(), redirect: jest.fn() };
    const next = jest.fn();

    const expectedOtp = "123456";

    sendOtpEmail.mockRejectedValue({ response: { status: 500 } });
    generateOTP.mockReturnValue(expectedOtp);

    await behaviourForPost()(req, res, next);

    expect(sessionDestroyButKeepLoggedIn).toBeCalledWith(req, res);
    expect(res.redirect).toBeCalledTimes(1);
    expect(res.redirect).toBeCalledWith(
      contextPath("/problem-with-the-service", req.session.journeyName)
    );
  });

  test("should destroy session and redirect to problem with service page on other errors", async () => {
    const req = {
      body: {
        "security-code-method": "email",
      },
      session: {
        journeyName: SECOND_PARENT_FLOW.name,
        [SECOND_PARENT_DATA_OBJECT_NAME]: {
          email: "test@email.com",
          "phone-number": "01632 960669",
          otpEmailSends: 0,
          otpTextSends: 0,
          otpAttempts: 0,
        },
      },
    };
    const res = { clearCookie: jest.fn(), redirect: jest.fn() };
    const next = jest.fn();

    const expectedOtp = "123456";

    sendOtpEmail.mockRejectedValue({ response: { status: 500 } });
    generateOTP.mockReturnValue(expectedOtp);

    await behaviourForPost()(req, res, next);

    expect(sessionDestroyButKeepLoggedIn).toBeCalledWith(req, res);
    expect(res.redirect).toBeCalledTimes(1);
    expect(res.redirect).toBeCalledWith(
      contextPath("/problem-with-the-service", req.session.journeyName)
    );
  });

  test("should redirect to error page when exceed max otp sends", async () => {
    const req = {
      body: {
        "security-code-method": "email",
      },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "security-code-method": "email",
          otpEmailSends: MAXIMUM_ALLOWED_OTP_SENDS,
        },
      },
    };
    const res = { redirect: jest.fn() };
    const next = jest.fn();

    await behaviourForPost()(req, res, next);

    expect(res.redirect).toBeCalledTimes(1);
    expect(res.redirect).toBeCalledWith(
      contextPath("/cannot-request-another-code", req.session.journeyName)
    );
  });
});
