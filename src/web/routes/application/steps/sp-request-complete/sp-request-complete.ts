import { SECOND_PARENT_DATA_OBJECT_NAME } from "../../tools/data-object";
import { formatReference } from "../common/certificate-reference-generator";

const firstParentStartUrl = process.env.START_PAGE_URL ?? "";

const pageContent = ({ translate, req }) => ({
  title: translate("spRequestComplete.title"),
  spRequestComplete: translate("spRequestComplete.heading"),
  yourReference: translate("spRequestComplete.yourReference", {
    certificateReference: formatReference(
      req.session[SECOND_PARENT_DATA_OBJECT_NAME][
        "sp-sanitised-reference-number"
      ]
    ),
  }),
  whatHappensNext: translate("spRequestComplete.whatHappensNext", {
    serviceStart: firstParentStartUrl,
  }),
  supportOrganisations: translate("supportOrganisations"),
  link: translate("spRequestComplete.link"),
});

const behaviourForGet = () => (req, res, next) => {
  res.set("Clear-Site-Data", '"storage"');
  next();
};

const spRequestComplete = {
  path: "/confirmation",
  template: "sp-request-complete",
  pageContent,
  behaviourForGet,
};

export { spRequestComplete, pageContent, behaviourForGet };
