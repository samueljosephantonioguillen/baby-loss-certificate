import { StatusCodes } from "http-status-codes";
import { logger } from "../logger/logger";
import { contextPath } from "../routes/application/paths/context-path";
import { MAINFLOW } from "../routes/application/journey-definitions";
import { START } from "../routes/start";
import { SP_START } from "../routes/sp-start";

const toErrorLogMessage = (error) => `${error.toString()}\n${error.stack}`;

const logErrors = (err, req, res, next) => {
  logger.error(toErrorLogMessage(err), req);
  next(err);
};

const errorHandler = (err, req, res, next) => {
  const CONTEXT_PATH = process.env.CONTEXT_PATH ?? "";
  const SP_CONTEXT_PATH = process.env.SP_CONTEXT_PATH ?? "";
  const statusCode = err.statusCode || StatusCodes.INTERNAL_SERVER_ERROR;
  const journeyName = req.session.journeyName || MAINFLOW.name;

  res.status(statusCode);

  switch (statusCode) {
    case StatusCodes.REQUEST_TIMEOUT:
      res.redirect(`${CONTEXT_PATH}/timed-out`);
      break;
    case StatusCodes.FORBIDDEN:
      switch (res?.route?.path ?? "") {
        case CONTEXT_PATH + START:
          res.redirect(CONTEXT_PATH + START);
          break;
        case SP_CONTEXT_PATH + SP_START:
          res.redirect(SP_CONTEXT_PATH + SP_START);
          break;
        default:
          res.redirect(contextPath("/session-ended", journeyName));
      }
      break;
    default:
      res.redirect(contextPath("/problem-with-the-service", journeyName));
  }
  next();
};

const registerErrorHandlers = (app) => {
  app.use(logErrors);
  app.use(errorHandler);
};

export { errorHandler, registerErrorHandlers };
