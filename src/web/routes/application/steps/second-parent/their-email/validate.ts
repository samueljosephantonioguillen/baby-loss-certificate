import { check } from "express-validator";
import { translateValidationMessage } from "../../common/translate-validation-message";
import { EMAIL_REGEX, EMAIL_VALID_CHARACTERS_REGEX } from "../../../constants";

const isEmailsEqual = (email, { req }) => {
  if (email !== req.body["their-email"]) {
    return false;
  }
  return true;
};

const validate = () => [
  check("their-email")
    .notEmpty()
    .bail()
    .withMessage(
      translateValidationMessage("validation:missingOtherParentEmail")
    )
    .isLength({ max: 255 })
    .bail()
    .withMessage(
      translateValidationMessage("validation:otherParentEmailTooLong")
    )
    .matches(EMAIL_VALID_CHARACTERS_REGEX)
    .bail()
    .withMessage(
      translateValidationMessage("validation:otherParentEmailInvalidCharacters")
    )
    .matches(EMAIL_REGEX)
    .bail()
    .withMessage(
      translateValidationMessage("validation:otherParentEmailInvalidFormat")
    ),
  check("their-email-confirm")
    .custom(isEmailsEqual)
    .withMessage(
      translateValidationMessage("validation:otherParentEmailsNotEqual")
    ),
];

export { validate };
