import expect from "expect";

import {
  notAllowedRelationship,
  pageContent,
  isNavigable,
} from "./not-allowed-relationship";
import { MAINFLOW } from "../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../tools/data-object";

test("notAllowedRelationship should match expected outcomes", () => {
  const expectedResults = {
    path: "/cannot-request-relationship",
    template: "not-eligible",
    pageContent,
    isNavigable,
  };

  expect(notAllowedRelationship).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("notAllowedRelationship.title"),
    heading: translate("notAllowedRelationship.heading"),
    supportOrganisationsHeading: translate("supportOrganisations.heading"),
    supportOrganisationsBody: translate("supportOrganisations.body"),
    supportOrganisationsList: translate("supportOrganisations.listItems"),
    backLinkText: translate("buttons:back"),
  };

  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when relation-to-baby is none", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "relation-to-baby": "None",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when relation-to-baby isn't none", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "relation-to-baby": "Mother",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});
