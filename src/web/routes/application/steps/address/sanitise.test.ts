import expect from "expect";

import { sanitise } from "./sanitise";

const next = jest.fn();

describe("postcode", () => {
  test("sanitise replaces multiple whitespace with one, converts to uppercase and saves to a new variable", () => {
    const req = {
      body: {
        postcode: "bs1     4tb",
      },
    };
    const expectedsanitisedPostcode = "BS1 4TB";
    const expectedPostcode = "bs1     4tb";

    sanitise()(req, {}, next);
    expect(req.body["sanitisedPostcode"]).toEqual(expectedsanitisedPostcode);
    expect(req.body.postcode).toEqual(expectedPostcode);
    expect(next).toBeCalled();
  });

  test("sanitise adds a single space before the last 3 ending characters, and saves to a new variable for postcode CB30QB", () => {
    const req = {
      body: {
        postcode: "cb3  0qb",
      },
    };
    const expectedsanitisedPostcode = "CB3 0QB";
    const expectedPostcode = "cb3  0qb";

    sanitise()(req, {}, next);

    expect(req.body["sanitisedPostcode"]).toEqual(expectedsanitisedPostcode);
    expect(req.body.postcode).toEqual(expectedPostcode);
    expect(next).toBeCalled();
  });

  test("sanitise adds a single space before the last 3 ending characters, and saves to a new variable for postcode N12NL", () => {
    const req = {
      body: {
        postcode: "n1  2nl",
      },
    };
    const expectedsanitisedPostcode = "N1 2NL";
    const expectedPostcode = "n1  2nl";

    sanitise()(req, {}, next);

    expect(req.body["sanitisedPostcode"]).toEqual(expectedsanitisedPostcode);
    expect(req.body.postcode).toEqual(expectedPostcode);
    expect(next).toBeCalled();
  });

  test("sanitise adds a single space before the last 3 ending characters, and saves to a new variable for postcode CB249LQ", () => {
    const req = {
      body: {
        postcode: "cb24  9lq",
      },
    };
    const expectedsanitisedPostcode = "CB24 9LQ";
    const expectedPostcode = "cb24  9lq";

    sanitise()(req, {}, next);

    expect(req.body["sanitisedPostcode"]).toEqual(expectedsanitisedPostcode);
    expect(req.body.postcode).toEqual(expectedPostcode);
    expect(next).toBeCalled();
  });

  test("sanitise adds a single space before the last 3 ending characters, and saves to a new variable for postcode OX145FB", () => {
    const req = {
      body: {
        postcode: "ox14  5fb",
      },
    };
    const expectedsanitisedPostcode = "OX14 5FB";
    const expectedPostcode = "ox14  5fb";

    sanitise()(req, {}, next);

    expect(req.body["sanitisedPostcode"]).toEqual(expectedsanitisedPostcode);
    expect(req.body.postcode).toEqual(expectedPostcode);
    expect(next).toBeCalled();
  });

  test("sanitise adds a single space before the last 3 ending characters, and saves to a new variable for postcode OX145FC", () => {
    const req = {
      body: {
        postcode: "ox145fc",
      },
    };
    const expectedsanitisedPostcode = "OX14 5FC";
    const expectedPostcode = "ox145fc";

    sanitise()(req, {}, next);

    expect(req.body["sanitisedPostcode"]).toEqual(expectedsanitisedPostcode);
    expect(req.body.postcode).toEqual(expectedPostcode);
    expect(next).toBeCalled();
  });
});

test("sanitise should trim starting and ending spaces for address fields", () => {
  const req = {
    body: {
      "address-line-1": "        Alan Jeffery Engineering     ",
      "address-line-2": "  1 Valley Road     ",
      "town-or-city": "    Plymouth    ",
      county: "    Devon",
      postcode: "PL7 1RF",
    },
  };

  const expectedResult = {
    body: {
      "address-line-1": "Alan Jeffery Engineering",
      "address-line-2": "1 Valley Road",
      "town-or-city": "Plymouth",
      county: "Devon",
      postcode: "PL7 1RF",
      sanitisedPostcode: "PL7 1RF",
    },
  };

  sanitise()(req, {}, next);
  expect(req).toEqual(expectedResult);
  expect(next).toBeCalled();
});
