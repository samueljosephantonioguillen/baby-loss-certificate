import expect from "expect";
import { CONTEXT_PATH } from "../../../static-pages/paths";
import { SECOND_PARENT_FLOW } from "../../journey-definitions";
import { SECOND_PARENT_DATA_OBJECT_NAME } from "../../tools/data-object";
import { mockClient } from "aws-sdk-client-mock";
import { SQSClient, SendMessageCommand } from "@aws-sdk/client-sqs";
import { secondParentContextPath } from "../../paths/context-path";
import { REQUEST_COMPLETE_URL } from "../../paths/paths";
import { COMPLETED, IN_PROGRESS } from "../../flow-control/states";

const isEmpty = jest.fn();
jest.mock("express-validator", () => ({
  validationResult: () => ({
    isEmpty: isEmpty,
  }),
}));

jest.mock("uuid", () => ({
  v4: jest.fn(() => "mocked-uuid"),
}));

import { logger } from "../../../../logger/logger";
jest.mock("../../../../logger/logger", () => ({
  logger: {
    info: jest.fn(),
    error: jest.fn(),
  },
}));

jest.useFakeTimers().setSystemTime(new Date("2023-01-01"));

import {
  spDeclaration,
  pageContent,
  validate,
  behaviourForPost,
  isNavigable,
} from "./sp-declaration";
import { buildSessionForJourney } from "../../flow-control/test-utils/test-utils";
import { AGENT_ROLE } from "../../constants";

const sqsMock = mockClient(SQSClient);

test("declaration should match expected outcomes", () => {
  const expectedResults = {
    path: "/declaration",
    template: "sp-declaration",
    pageContent,
    validate,
    behaviourForPost,
    isNavigable,
  };

  expect(spDeclaration).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("spDeclaration.title"),
    body: translate("spDeclaration.body", {
      contextPath: CONTEXT_PATH,
    }),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

test("isNavigable should return true when are-details-correct is yes", () => {
  const req = {
    session: {
      journeyName: SECOND_PARENT_FLOW.name,
      secondParent: {
        "are-details-correct": "yes",
      },
    },
  };

  const result = isNavigable(req);

  expect(result).toEqual(true);
});

test("isNavigable should return false when are-details-correct is no", () => {
  const req = {
    session: {
      journeyName: SECOND_PARENT_FLOW.name,
      secondParent: {
        "are-details-correct": "no",
      },
    },
  };

  const result = isNavigable(req);

  expect(result).toEqual(false);
});

test("isNavigable should return false when are-details-correct is empty", () => {
  const req = {
    session: {
      journeyName: SECOND_PARENT_FLOW.name,
      secondParent: {
        "are-details-correct": "",
      },
    },
  };

  const result = isNavigable(req);

  expect(result).toEqual(false);
});

describe(`behaviourForPost`, () => {
  const journey = { name: SECOND_PARENT_FLOW.name };

  const config = {
    environment: {
      XEROX_QUEUE_URL: "test-xerox-queue-url",
      INCOMPLETE_APPLICATION_QUEUE_URL: "test-incomplete-application-queue-url",
    },
  };

  test(`should send user request to xerox SQS with reference-number`, async () => {
    const req = {
      session: {
        journeyName: SECOND_PARENT_FLOW.name,
        certificateReference: "C52A1999D8F",
        [SECOND_PARENT_DATA_OBJECT_NAME]: {
          "application-id": "123",
          "sp-parent-id": "mock-uuid",
          "reference-number": "9000000001",
          "reference-number-type": "nhsNumber",
          "first-name": "testFirstName",
          "last-name": "testLastName",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };

    const secondParentApplicationRequest = {
      applicationId:
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["application-id"],
      applicationUpdatedBy: "citizen",
    };

    const secondParentRequest = {
      parentId: req.session[SECOND_PARENT_DATA_OBJECT_NAME]["sp-parent-id"],
      parentReferenceNumber:
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["reference-number"] ||
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["pds-nhs-number"] ||
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["nhs-number"],
      parentReferenceType: 1,
      parentReferenceNumberIssuingCountry: "GBR",
      parentGivenName:
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["first-name"],
      parentFamilyName:
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["last-name"],
      applicationCreatedBy: "citizen",
    };

    const expectedBody = {
      secondParentApplicationRequest,
      secondParentRequest,
    };

    const res = { redirect: jest.fn() };
    const next = jest.fn();
    isEmpty.mockReturnValue(true);

    sqsMock
      .on(SendMessageCommand, {
        QueueUrl: config.environment.XEROX_QUEUE_URL,
        MessageBody: JSON.stringify(expectedBody),
        MessageGroupId: "mocked-uuid",
      })
      .resolves({
        MessageId: "test-message-id-for-jane",
        MD5OfMessageBody: "test-md5-value-for-jane",
      });
    await behaviourForPost(config, journey)(req, res, next);

    expect(next).not.toBeCalled();
    expect(logger.info).toBeCalledTimes(2);
    expect(logger.error).toBeCalledTimes(0);
    expect(req.session.journeys[journey.name].state).toEqual(COMPLETED);
    expect(req.session.certificateReference).toBeTruthy();
    expect(res.redirect).toBeCalledWith(
      secondParentContextPath(REQUEST_COMPLETE_URL)
    );
  });

  test(`should send user request to xerox SQS with nhs-number`, async () => {
    const req = {
      session: {
        account: {
          idTokenClaims: {
            roles: [AGENT_ROLE],
          },
        },
        journeyName: SECOND_PARENT_FLOW.name,
        certificateReference: "C52A1999D8F",
        [SECOND_PARENT_DATA_OBJECT_NAME]: {
          "application-id": "123",
          "sp-parent-id": "mock-uuid",
          "nhs-number": "9000000001",
          "first-name": "testFirstName",
          "last-name": "testLastName",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };

    const secondParentApplicationRequest = {
      applicationId:
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["application-id"],
      applicationUpdatedBy: "agent",
    };

    const secondParentRequest = {
      parentId: req.session[SECOND_PARENT_DATA_OBJECT_NAME]["sp-parent-id"],
      parentReferenceNumber:
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["reference-number"] ||
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["pds-nhs-number"] ||
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["nhs-number"],
      parentReferenceType: 1,
      parentReferenceNumberIssuingCountry: "GBR",
      parentGivenName:
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["first-name"],
      parentFamilyName:
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["last-name"],
      applicationCreatedBy: "agent",
    };

    const expectedBody = {
      secondParentApplicationRequest,
      secondParentRequest,
    };

    const res = { redirect: jest.fn() };
    const next = jest.fn();
    isEmpty.mockReturnValue(true);

    sqsMock
      .on(SendMessageCommand, {
        QueueUrl: config.environment.XEROX_QUEUE_URL,
        MessageBody: JSON.stringify(expectedBody),
        MessageGroupId: "mocked-uuid",
      })
      .resolves({
        MessageId: "test-message-id-for-jane",
        MD5OfMessageBody: "test-md5-value-for-jane",
      });
    await behaviourForPost(config, journey)(req, res, next);

    expect(next).not.toBeCalled();
    expect(logger.info).toBeCalledTimes(2);
    expect(logger.error).toBeCalledTimes(0);
    expect(req.session.journeys[journey.name].state).toEqual(COMPLETED);
    expect(req.session.certificateReference).toBeTruthy();
    expect(res.redirect).toBeCalledWith(
      secondParentContextPath(REQUEST_COMPLETE_URL)
    );
  });

  test(`should send user request to xerox SQS with pds-nhs-number`, async () => {
    const req = {
      session: {
        journeyName: SECOND_PARENT_FLOW.name,
        certificateReference: "C52A1999D8F",
        [SECOND_PARENT_DATA_OBJECT_NAME]: {
          "application-id": "123",
          "sp-parent-id": "mock-uuid",
          "pds-nhs-number": "9000000001",
          "first-name": "testFirstName",
          "last-name": "testLastName",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };

    const secondParentApplicationRequest = {
      applicationId:
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["application-id"],
      applicationUpdatedBy: "citizen",
    };

    const secondParentRequest = {
      parentId: req.session[SECOND_PARENT_DATA_OBJECT_NAME]["sp-parent-id"],
      parentReferenceNumber:
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["reference-number"] ||
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["pds-nhs-number"] ||
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["nhs-number"],
      parentReferenceType: 1,
      parentReferenceNumberIssuingCountry: "GBR",
      parentGivenName:
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["first-name"],
      parentFamilyName:
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["last-name"],
      applicationCreatedBy: "citizen",
    };

    const expectedBody = {
      secondParentApplicationRequest,
      secondParentRequest,
    };

    const res = { redirect: jest.fn() };
    const next = jest.fn();
    isEmpty.mockReturnValue(true);

    sqsMock
      .on(SendMessageCommand, {
        QueueUrl: config.environment.XEROX_QUEUE_URL,
        MessageBody: JSON.stringify(expectedBody),
        MessageGroupId: "mocked-uuid",
      })
      .resolves({
        MessageId: "test-message-id-for-jane",
        MD5OfMessageBody: "test-md5-value-for-jane",
      });
    await behaviourForPost(config, journey)(req, res, next);

    expect(next).not.toBeCalled();
    expect(logger.info).toBeCalledTimes(2);
    expect(logger.error).toBeCalledTimes(0);
    expect(req.session.journeys[journey.name].state).toEqual(COMPLETED);
    expect(req.session.certificateReference).toBeTruthy();
    expect(res.redirect).toBeCalledWith(
      secondParentContextPath(REQUEST_COMPLETE_URL)
    );
  });
});
