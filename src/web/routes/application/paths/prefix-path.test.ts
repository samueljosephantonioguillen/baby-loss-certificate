import expect from "expect";
import { prefixPath } from "./prefix-path";

test("prefixPath() does not prefix path if no prefix defined", () => {
  expect(prefixPath(undefined, "/first")).toBe("/first");
});

test("prefixPath() adds prefix to path", () => {
  expect(prefixPath("/my-journey", "/first")).toBe("/my-journey/first");
});

test('prefixPath() throws an error if segment does not start with "/"', () => {
  const prefixResult = () => prefixPath("my-journey", "/first");
  const pathResult = () => prefixPath("/my-journey", "first");
  expect(prefixResult).toThrowError(
    /Invalid prefix "my-journey". Prefix must be a string starting with "\/"/
  );
  expect(pathResult).toThrowError(
    /Invalid path "first". Path must be a string starting with "\/"/
  );
});

test("prefixPath() throws an error if segment is not a string", () => {
  const prefixResult = () => prefixPath(true, "/first");
  const pathResult = () => prefixPath("/my-journey", true);
  expect(prefixResult).toThrowError(
    /Invalid prefix "true". Prefix must be a string starting with "\/"/
  );
  expect(pathResult).toThrowError(
    /Invalid path "true". Path must be a string starting with "\/"/
  );
});

test("prefixPath() throws an error if path is undefined", () => {
  const result = () => prefixPath("/my-journey", undefined);
  expect(result).toThrowError(
    /Invalid path "undefined". Path must be a string starting with "\/"/
  );
});
