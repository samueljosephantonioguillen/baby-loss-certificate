import { toUpper, compose } from "ramda";
import { SQSClient, SendMessageCommand } from "@aws-sdk/client-sqs";
import { v4 as uuidv4 } from "uuid";
import { validationResult } from "express-validator";
import { validate } from "./validate";

import { logger } from "../../../../logger/logger";
import { stateMachine } from "../../flow-control/state-machine";
import { COMPLETED } from "../../flow-control/states";
import { contextPath } from "../../paths/context-path";
import { REQUEST_COMPLETE_URL } from "../../paths/paths";
import {
  ParentRequest,
  IncompleteParentRequest,
  ApplicationRequest,
} from "../common/types/types";
import { toLongLocaleDateString } from "../common/format-date";
import {
  isNilOrEmpty,
  notIsNilOrEmpty,
} from "../../../../../common/predicates";
import { generateReference } from "../common/certificate-reference-generator";
import { CONTEXT_PATH } from "../../../static-pages/paths";
import { userIsAgent, userIsBackOffice } from "../../../azure-routes/auth";
import {
  MAINFLOW_DATA_OBJECT_NAME,
  getFlowDataObjectName,
} from "../../tools/data-object";
import { convertReferenceTypeToNumber } from "../../tools/object-mappers";

const pageContent = ({ translate, req }) => ({
  title: translate("declaration.title"),
  firstHeading: translate("declaration.firstHeading"),
  firstBody: userIsAgent(req)
    ? translate("declaration.ccsFirstBody")
    : translate("declaration.firstBody", { contextPath: CONTEXT_PATH }),
  thirdHeading: translate("declaration.thirdHeading"),
  secondBody: userIsAgent(req)
    ? translate("declaration.ccsSecondBody")
    : translate("declaration.secondBody", { contextPath: CONTEXT_PATH }),
  agree: userIsAgent(req)
    ? translate("declaration.ccsCheckbox")
    : translate("declaration.checkbox"),
  button: userIsAgent(req)
    ? translate("declaration.ccsButton")
    : translate("declaration.button"),
});

const replaceMultipleSpacesWithOne = (value) => {
  return value.replace(/\s{2,}/g, " ");
};

const addSpaceBeforeLastThreeCharacters = (value) => {
  return value.replace(/^(.*)(\d)/, "$1 $2");
};

const replaceMultipleSpacesAndAddSingleSpaceThenUppercase = compose(
  replaceMultipleSpacesWithOne,
  addSpaceBeforeLastThreeCharacters,
  toUpper
);

const mapToApplicationRequest = (req): ApplicationRequest => {
  const dataObjectName = getFlowDataObjectName(req.session.journeyName);
  const sessionDataObject = req.session[dataObjectName];
  const hasLossYear = notIsNilOrEmpty(
    req.session[dataObjectName]["baby-date-of-loss-year"]
  );
  const hasSecondParent =
    req.session[dataObjectName]["add-another-parent"] === "yes";

  return {
    applicationId: req.session.applicationId,
    certificateReference: req.session.certificateReference,
    applicantName: `${sessionDataObject["first-name"]} ${sessionDataObject["last-name"]}`,
    applicantParentId: sessionDataObject["parent-id"],
    secondParentId: hasSecondParent
      ? sessionDataObject["second-parent-id"]
      : null,
    addressLine1: isAltAddressAndAgentOrBackOffice(sessionDataObject, req)
      ? sessionDataObject["address-line-1-ccs"]
      : sessionDataObject["address-line-1"],
    addressLine2: getValueBasedOnRole(
      sessionDataObject,
      req,
      "address-line-2-ccs",
      "address-line-2"
    ),
    addressLine3: getValueBasedOnRole(
      sessionDataObject,
      req,
      "address-line-3-ccs",
      "address-line-3"
    ),
    addressLine4: getValueBasedOnRole(
      sessionDataObject,
      req,
      "address-line-4-ccs",
      "address-line-4"
    ),
    addressLine5: getValueBasedOnRole(
      sessionDataObject,
      req,
      "address-line-5-ccs",
      "address-line-5"
    ),
    postcode: isAltAddressAndAgentOrBackOffice(sessionDataObject, req)
      ? replaceMultipleSpacesAndAddSingleSpaceThenUppercase(
          sessionDataObject["postcode-ccs"]
        )
      : replaceMultipleSpacesAndAddSingleSpaceThenUppercase(
          sessionDataObject["postcode"]
        ),
    lossDateDay: sessionDataObject["baby-date-of-loss-day"] || null,
    lossDateMonth: sessionDataObject["baby-date-of-loss-month"] || null,
    lossDateYear: sessionDataObject["year-of-loss"],
    lossDateFull: hasLossYear
      ? toLongLocaleDateString(
          sessionDataObject["baby-date-of-loss-day"],
          sessionDataObject["baby-date-of-loss-month"],
          sessionDataObject["baby-date-of-loss-year"]
        )
      : null,
    printDateOfLoss: !!hasLossYear,
    lossPlaceName: sessionDataObject["baby-loss-location"] || null,
    lossCountry: "GB-ENG",
    babyName: sessionDataObject["baby-name"] || null,
    babySex: sessionDataObject["baby-sex"]
      ? sessionDataObject["baby-sex"]
      : null,
    applicationCompleteDateTime: new Date(),
    applicationType: sessionDataObject["application-type"],
    version: 1,
  };
};

function isAltAddressAndAgentOrBackOffice(sessionDataObject, req) {
  return (
    (sessionDataObject["confirm-your-address"] === "alt" && userIsAgent(req)) ||
    userIsBackOffice(req)
  );
}

function getValueBasedOnRole(sessionDataObject, req, keyCCS, key) {
  return isAltAddressAndAgentOrBackOffice(sessionDataObject, req)
    ? sessionDataObject[keyCCS] || null
    : sessionDataObject[key] || null;
}

const mapToParentRequest = (req): ParentRequest => {
  const dataObjectName = getFlowDataObjectName(req.session.journeyName);
  const sessionDataObject = req.session[dataObjectName];
  return {
    parentId: sessionDataObject["parent-id"],
    parentReferenceNumber:
      sessionDataObject["reference-number"] ||
      sessionDataObject["pds-nhs-number"] ||
      sessionDataObject["nhs-number"],
    parentReferenceType: sessionDataObject["reference-number-type"]
      ? convertReferenceTypeToNumber(sessionDataObject["reference-number-type"])
      : 1,
    parentReferenceNumberIssuingCountry:
      sessionDataObject["reference-number-country"] || "GBR",
    parentGivenName: sessionDataObject["first-name"],
    parentFamilyName: sessionDataObject["last-name"],
    parentRelationship: sessionDataObject["relation-to-baby"],
    parentDateOfBirth: sessionDataObject["date-of-birth"],
    parentEmailAddress: sessionDataObject["applicant-email"] || null,
    applicationCompleteDateTime: sessionDataObject["application-time-sent"],
    applicationCreatedBy: sessionDataObject["application-type"],
    applicationId: sessionDataObject["application-id"],
  };
};

const mapToIncompleteSecondParentRequest = (req): IncompleteParentRequest => {
  const dataObjectName = getFlowDataObjectName(req.session.journeyName);
  return {
    parentId: req.session[dataObjectName]["second-parent-id"],
    parentRelationship: req.session[dataObjectName]["their-relation-to-baby"],
    parentDateOfBirth: req.session[dataObjectName]["their-date-of-birth"],
    parentEmailAddress: req.session[dataObjectName]["their-email"] || null,
    applicationCompleteDateTime:
      req.session[dataObjectName]["application-time-sent"],
    applicationCreatedBy: req.session[dataObjectName]["application-type"],
    applicationId: req.session[dataObjectName]["application-id"],
  };
};

const mapToSecondParentRequestForBackOffice = (req): ParentRequest => {
  const dataObjectName = getFlowDataObjectName(req.session.journeyName);
  const sessionDataObject = req.session[dataObjectName];
  return {
    parentId: sessionDataObject["second-parent-id"],
    parentReferenceNumber: sessionDataObject["their-reference-number"],
    parentReferenceType: convertReferenceTypeToNumber(
      sessionDataObject["their-reference-number-type"]
    ),
    parentReferenceNumberIssuingCountry:
      sessionDataObject["their-reference-number-country"] || "GBR",
    parentGivenName: sessionDataObject["their-first-name"],
    parentFamilyName: sessionDataObject["their-last-name"],
    parentRelationship: sessionDataObject["their-relation-to-baby"],
    parentDateOfBirth: sessionDataObject["their-date-of-birth"],
    parentEmailAddress: req.session[dataObjectName]["their-email"] || null,
    applicationCompleteDateTime: sessionDataObject["application-time-sent"],
    applicationCreatedBy: sessionDataObject["application-type"],
    applicationId: sessionDataObject["application-id"],
  };
};

const behaviourForPost = (config, journey) => async (req, res, next) => {
  const dataObjectName = getFlowDataObjectName(req.session.journeyName);
  const sessionDataObject = req.session[dataObjectName];
  if (!validationResult(req).isEmpty()) {
    return next();
  }
  const hasSecondParent = sessionDataObject["add-another-parent"] === "yes";

  const firstParentJourney = dataObjectName == MAINFLOW_DATA_OBJECT_NAME;

  if (isNilOrEmpty(req.session.certificateReference)) {
    req.session.certificateReference = generateReference();
  }

  if (isNilOrEmpty(sessionDataObject.submitted)) {
    sessionDataObject.submitted = true;
    sessionDataObject["parent-id"] = uuidv4();
    sessionDataObject["second-parent-id"] = uuidv4();
    sessionDataObject["application-time-sent"] = new Date();
    sessionDataObject["application-id"] = req.session.applicationId;
    const applicationType = setApplicationType(req);
    sessionDataObject["application-type"] = applicationType;

    const applicationRequest = mapToApplicationRequest(req);
    const parentRequest = mapToParentRequest(req);
    let secondParentRequest = populateSecondParentRequest(
      req,
      firstParentJourney,
      hasSecondParent
    );

    logIfUsingAlternativeAddressForPostalAddress(sessionDataObject, req);

    const client = new SQSClient({
      region: "eu-west-2",
      endpoint: config.environment.AWS_CLIENT_ENDPOINT,
    });

    let queueToSend = determineQueueToSend(
      firstParentJourney,
      hasSecondParent,
      config
    );

    if (isBackOfficeTwoParentApplication(req, hasSecondParent)) {
      secondParentRequest = mapToSecondParentRequestForBackOffice(req);
      queueToSend = config.environment.XEROX_QUEUE_URL;
    }

    const messageBody = {
      applicationRequest,
      parentRequest,
      secondParentRequest,
    };

    await sendToQueue(
      client,
      messageBody,
      applicationRequest,
      queueToSend,
      req
    );

    stateMachine.setState(COMPLETED, req, journey);
    return res.redirect(contextPath(REQUEST_COMPLETE_URL, journey.name));
  }
};

async function sendToQueue(
  client: SQSClient,
  messageBody: {
    applicationRequest: ApplicationRequest;
    parentRequest: ParentRequest;
    secondParentRequest: IncompleteParentRequest | ParentRequest | null;
  },
  applicationRequest: ApplicationRequest,
  queueUrl,
  req
) {
  try {
    const response = await client.send(
      new SendMessageCommand({
        QueueUrl: queueUrl,
        MessageBody: JSON.stringify(messageBody),
        MessageGroupId: uuidv4(),
      })
    );
    logger.info(
      `SQS message sent successfully: { id: ${response.MessageId}, hash: ${response.MD5OfMessageBody}, correlation-id: ${applicationRequest.applicationId} }`,
      req
    );
  } catch (error) {
    console.error(error);
    logger.error(error, req);
  }
}

function isBackOfficeTwoParentApplication(req, hasSecondParent) {
  if (userIsBackOffice(req) && hasSecondParent) {
    return true;
  }
  return false;
}

function populateSecondParentRequest(req, firstParentJourney, hasSecondParent) {
  return firstParentJourney && hasSecondParent
    ? mapToIncompleteSecondParentRequest(req)
    : null;
}

function setApplicationType(req) {
  let applicationType = "citizen";
  if (userIsAgent(req)) {
    applicationType = "agent";
  } else if (userIsBackOffice(req)) {
    applicationType = "backoffice";
  }
  return applicationType;
}

function determineQueueToSend(
  firstParentJourney: boolean,
  hasSecondParent: boolean,
  config
) {
  return firstParentJourney && hasSecondParent
    ? config.environment.INCOMPLETE_APPLICATION_QUEUE_URL
    : config.environment.XEROX_QUEUE_URL;
}

function logIfUsingAlternativeAddressForPostalAddress(sessionDataObject, req) {
  if (sessionDataObject["confirm-your-address"] === "alt") {
    logger.info("Using alternative address for postal address", req);
  }
}
const declaration = {
  path: "/declaration",
  template: "declaration",
  pageContent,
  validate,
  behaviourForPost,
};

export {
  declaration,
  pageContent,
  validate,
  behaviourForPost,
  setApplicationType,
};
