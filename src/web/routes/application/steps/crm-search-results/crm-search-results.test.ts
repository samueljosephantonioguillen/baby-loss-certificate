import expect from "expect";

import {
  crmSearchResults,
  pageContent,
  behaviourForGet,
} from "./crm-search-results";
import { CRM_FLOW } from "../../journey-definitions";
import { CRM_DATA_OBJECT_NAME } from "../../tools/data-object";

test("crmSearchResults should match expected outcomes", () => {
  const expectedResults = {
    path: "/search-results",
    template: "crm-search-results",
    pageContent,
    behaviourForGet,
  };

  expect(crmSearchResults).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const req = {
    session: {
      journeyName: CRM_FLOW.name,
      [CRM_DATA_OBJECT_NAME]: {
        "crm-search-details": {
          firstName: "John",
          lastName: "Smith",
          dateOfBirth: "01/01/1970",
          postcode: "AA1 1AA",
        },
        "crm-search-results": [
          {
            applicationReference: "E10BE325539",
            firstName: "John",
            lastName: "Smith",
            dateOfBirth: "01/01/1970",
            postcode: "AA1 1AA",
          },
        ],
      },
    },
  };
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("crmSearchResults.title"),
    heading: translate("crmSearchResults.heading"),
    results: req.session[CRM_DATA_OBJECT_NAME]["crm-search-results"],
    searchDetails: req.session[CRM_DATA_OBJECT_NAME]["crm-search-details"],
    backLinkText: translate("crmSearchResults.back"),
    resultsPreamble:
      req.session[CRM_DATA_OBJECT_NAME]["crm-search-results"].length > 0
        ? translate("crmSearchResults.resultsFor")
        : translate("crmSearchResults.noResultsFor"),
    and: translate("and"),
    note: translate("crmSearchResults.note"),
    noteLineOne: translate("crmSearchResults.noteLineOne"),
  };
  const result = pageContent({ translate, req });

  expect(result).toEqual(expected);
});
