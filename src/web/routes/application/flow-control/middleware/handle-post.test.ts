/* eslint-disable @typescript-eslint/no-var-requires */

const isEmpty = jest.fn();
const array = jest.fn();

jest.mock("express-validator", () => ({
  validationResult: () => ({
    isEmpty: isEmpty,
    array: array,
  }),
}));

import expect from "expect";
import * as sinon from "sinon";
import * as states from "../states";
import {
  buildSessionForJourney,
  getNextAllowedPathForJourney,
} from "../test-utils/test-utils";
import { MAINFLOW } from "../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../tools/data-object";

describe("handlePost() function", () => {
  afterEach(() => {
    jest.resetAllMocks();
    jest.resetModules();
  });

  test("should add errors and [MAINFLOW_DATA_OBJECT_NAME]: to locals if errors exist", () => {
    const errors = ["expected error message"];
    isEmpty.mockReturnValue(false);
    array.mockReturnValue(errors);

    const { handlePost } = require("./handle-post");

    const steps = [
      { path: "/first", next: () => "/second" },
      { path: "/second" },
    ];
    const journey = { name: MAINFLOW.name, steps };
    const req = { body: {}, t: (string) => string };
    const res = {
      locals: {
        [MAINFLOW_DATA_OBJECT_NAME]: {},
        errors: {},
        errorTitleText: {},
      },
    };
    const next = sinon.spy();

    handlePost(journey)(req, res, next);

    expect(res.locals.errorTitleText).toEqual(
      req.t("validation:errorTitleText")
    );
    expect(res.locals.errors).toEqual(errors);
    expect(next.called).toBe(true);
  });

  test("adds body to the session if no errors exist", () => {
    isEmpty.mockReturnValue(true);
    const { handlePost } = require("./handle-post");

    const steps = [
      { path: "/first", next: () => "/second" },
      { path: "/second" },
    ];
    const journey = { name: MAINFLOW.name, steps };
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          other: "[MAINFLOW_DATA_OBJECT_NAME]: data",
        },
      },
      body: {
        new: "[MAINFLOW_DATA_OBJECT_NAME]: data",
      },
    };

    const next = sinon.spy();

    const expected = {
      other: "[MAINFLOW_DATA_OBJECT_NAME]: data",
      new: "[MAINFLOW_DATA_OBJECT_NAME]: data",
    };

    handlePost(journey)(req, {}, next);
    expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(expected);
    expect(next.called).toBe(true);
  });

  test("calls next() with error on error", () => {
    isEmpty.mockRejectedValue("error");
    array.mockReturnValue([]);
    const { handlePost } = require("./handle-post");

    const steps = [
      { path: "/first", next: () => "/second" },
      { path: "/second" },
    ];

    const journey = { steps };
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {},
      },
      body: {},
    };

    const res = { locals: {} };
    const next = sinon.spy();

    handlePost(journey)(req, res, next);

    expect(next.calledWith(sinon.match.instanceOf(Error))).toBe(true);
  });

  test("adds next allowed step to session", () => {
    isEmpty.mockReturnValue(true);
    const { handlePost } = require("./handle-post");

    const steps = [
      { path: "/first", next: () => "/second" },
      { path: "/second" },
    ];
    const journey = {
      name: MAINFLOW.name,
      steps,
      pathsInSequence: ["/first", "/second"],
    };

    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW.name,
          state: states.IN_PROGRESS,
          nextAllowedPath: "/first",
        }),
      },
      path: "/first",
    };

    const res = {};
    const next = () => {
      /* explicit empty function */
    };

    handlePost(journey)(req, res, next);

    expect(getNextAllowedPathForJourney(MAINFLOW.name, req)).toBe("/second");
  });
});
