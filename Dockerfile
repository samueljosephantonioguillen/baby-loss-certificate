FROM node:hydrogen-alpine

# variables
ARG port
ARG config_file

WORKDIR /app
COPY . /app
COPY $config_file /app/.env

# Set environment variables - to add datadog

RUN npm install --ignore-scripts && npm run build

EXPOSE $port

VOLUME ["/app"]

ENTRYPOINT ["npm", "start"]


