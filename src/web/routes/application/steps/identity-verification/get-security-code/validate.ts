import { check } from "express-validator";

import { translateValidationMessage } from "../../common/translate-validation-message";

const validate = () => [
  check("security-code-method")
    .not()
    .isEmpty()
    .bail()
    .withMessage(
      translateValidationMessage("validation:missingSecurityCodeContactMethod")
    ),
];

export { validate };
