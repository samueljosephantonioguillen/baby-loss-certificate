import { userIsBackOffice } from "../../../../azure-routes/auth";
import { stateMachine } from "../../../flow-control/state-machine";
import { IN_REVIEW, IN_REVIEW_PROGRESS } from "../../../flow-control/states";
import { getFlowDataObjectName } from "../../../tools/data-object";
import { validate } from "./validate";

const pageContent = ({ translate }) => ({
  title: translate("theirRelationToBaby.title"),
  sectionTitle: translate("section.otherParentsDetails"),
  heading: translate("theirRelationToBaby.heading"),
  hint: translate("theirRelationToBaby.hint"),
  mother: translate("relationToBaby.radios.mother"),
  father: translate("relationToBaby.radios.father"),
  parent: translate("relationToBaby.radios.parent"),
  surrogate: translate("relationToBaby.radios.surrogate"),
  backLinkText: translate("buttons:back"),
  continueButtonText: translate("buttons:continue"),
});

const behaviourForPost = (config, journey) => (req, res, next) => {
  if (stateMachine.getState(req, journey) === IN_REVIEW_PROGRESS) {
    stateMachine.setState(IN_REVIEW, req, journey);
  }

  next();
};

const isNavigable = (req) =>
  req.session[getFlowDataObjectName(req.session.journeyName)][
    "add-another-parent"
  ] === "yes" && !userIsBackOffice(req);

const theirRelationToBaby = {
  path: "/other-parents-relationship-to-baby",
  template: "their-relation-to-baby",
  pageContent,
  isNavigable,
  validate,
  behaviourForPost,
};

export {
  theirRelationToBaby,
  pageContent,
  isNavigable,
  validate,
  behaviourForPost,
};
