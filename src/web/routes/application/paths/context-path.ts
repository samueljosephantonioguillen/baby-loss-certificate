import { isUndefined, isString } from "../../../../common/predicates";
import { environment } from "../../../../config/environment";
import { isUsingAuthentication } from "../../azure-routes/auth";
import { START } from "../../start";
import { CRM_FLOW, MAINFLOW, SECOND_PARENT_FLOW } from "../journey-definitions";

const isInvalidSegment = (segment) =>
  !isString(segment) || !segment.startsWith("/");

const externalStartUrl = process.env.START_PAGE_URL ?? "";

const contextPath = (path, journeyName) => {
  if (isInvalidSegment(path)) {
    throw new Error(
      `Invalid path "${path}". Path must be a string starting with "/"`
    );
  }
  switch (journeyName) {
    case MAINFLOW.name:
      if (redirectToExternalUrl(path)) return externalStartUrl;
      return firstParentContextPath(path);
    case SECOND_PARENT_FLOW.name:
      return secondParentContextPath(path);
    case CRM_FLOW.name:
      return crmContextPath(path);
    default:
      throw new Error(`Invalid journey name "${journeyName}"`);
  }
};

const blankContextPath = (journeyName) => {
  switch (journeyName) {
    case MAINFLOW.name:
      return firstParentContextPath("");
    case SECOND_PARENT_FLOW.name:
      return secondParentContextPath("");
    case CRM_FLOW.name:
      return crmContextPath("");
    default:
      throw new Error(`Invalid journey name "${journeyName}"`);
  }
};

const firstParentContextPath = (path) => {
  const CONTEXT_PATH = environment.CONTEXT_PATH;

  if (isUndefined(CONTEXT_PATH)) {
    throw new Error(`Undefined context path "${CONTEXT_PATH}"`);
  }

  if (redirectToExternalUrl(path)) return externalStartUrl;

  return `${CONTEXT_PATH}${path}`;
};

const secondParentContextPath = (path) => {
  const SP_CONTEXT_PATH = environment.SP_CONTEXT_PATH;

  if (isUndefined(SP_CONTEXT_PATH)) {
    throw new Error(`Undefined context path "${SP_CONTEXT_PATH}"`);
  }

  return `${SP_CONTEXT_PATH}${path}`;
};

const crmContextPath = (path) => {
  const CRM_CONTEXT_PATH = environment.CRM_CONTEXT_PATH;

  if (isUndefined(CRM_CONTEXT_PATH)) {
    throw new Error(`Undefined context path "${CRM_CONTEXT_PATH}"`);
  }

  return `${CRM_CONTEXT_PATH}${path}`;
};

const redirectToExternalUrl = (path) => {
  if (path === START && !isUsingAuthentication()) {
    return true;
  }
  return false;
};

export {
  contextPath,
  secondParentContextPath,
  firstParentContextPath,
  blankContextPath,
  crmContextPath,
};
