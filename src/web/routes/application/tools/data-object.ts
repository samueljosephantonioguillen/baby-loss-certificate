import { CRM_FLOW, MAINFLOW, SECOND_PARENT_FLOW } from "../journey-definitions";

export const MAINFLOW_DATA_OBJECT_NAME = "applicant";
export const SECOND_PARENT_DATA_OBJECT_NAME = "secondParent";
export const CRM_DATA_OBJECT_NAME = "crm";

export function getFlowDataObjectName(journeyName: string) {
  if (journeyName === MAINFLOW.name) {
    return MAINFLOW_DATA_OBJECT_NAME;
  } else if (journeyName === SECOND_PARENT_FLOW.name) {
    return SECOND_PARENT_DATA_OBJECT_NAME;
  } else if (journeyName === CRM_FLOW.name) {
    return CRM_DATA_OBJECT_NAME;
  } else {
    throw new Error(`Unknown journey name ${journeyName}`);
  }
}
