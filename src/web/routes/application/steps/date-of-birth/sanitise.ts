const trimLeadingZero = (input) => input.replace(/\b0+/g, "");

const sanitise = () => (req, res, next) => {
  req.body["date-of-birth-day"] = trimLeadingZero(
    req.body["date-of-birth-day"]
  );
  req.body["date-of-birth-month"] = trimLeadingZero(
    req.body["date-of-birth-month"]
  );
  req.body["date-of-birth-year"] = trimLeadingZero(
    req.body["date-of-birth-year"]
  );

  next();
};

export { sanitise, trimLeadingZero };
