import * as dotEnv from "dotenv";
import { toBoolean } from "./to-boolean";

dotEnv.config();

const ONE_HOUR = 60 * 60;

const defaultConfig = {
  password: process.env.REDIS_AUTH ?? "",
  socket: {
    port: process.env.REDIS_PORT ?? "6379",
    host: process.env.REDIS_HOST ?? "127.0.0.1",
    tls: toBoolean(process.env.REDIS_TLS ?? "true"),
  },
  ttl: ONE_HOUR,
  legacyMode: true,
};

export const redis = defaultConfig;
