const sanitise = () => (req, res, next) => {
  req.body["baby-name"] = req.body["baby-name"].trim();

  req.body["baby-name"] = req.body["baby-name"].replace(/`/g, "'");

  req.body["baby-name"] = req.body["baby-name"].replace(/’/g, "'");

  // capitalise first letter of each word
  req.body["baby-name"] = req.body["baby-name"]
    .split(" ")
    .map((word) => {
      return word.charAt(0).toUpperCase() + word.slice(1);
    })
    .join(" ");

  next();
};

export { sanitise };
