import axios from "axios";
import { DateTime } from "luxon";
import { compose, map, pick, prop, propOr } from "ramda";

import { logger } from "../../../../../logger/logger";
import { OS_PLACES_ADDRESS_KEYS } from "../../../constants";
import { toTitleCase } from "../../common/formats";

const REQUEST_TIMEOUT = 5000;
const OS_PLACES_API_PATH = "/search/places/v1/postcode";
const RESULTS_PROP = "results";
const DELIVERY_POINT_ADDRESS_PROP = "DPA";

const transformAddressField = (address) => ({
  ...address,
  ADDRESS: toTitleCase(address.ADDRESS.replace(`, ${address.POSTCODE}`, "")),
});

const transformAddress = compose(
  transformAddressField,
  pick(OS_PLACES_ADDRESS_KEYS),
  prop(DELIVERY_POINT_ADDRESS_PROP)
);

const transformOsPlacesApiResponse = compose(
  map(transformAddress),
  propOr([], RESULTS_PROP)
);

const standardisePostcode = (postcode) =>
  postcode.toUpperCase().replace(/\s/g, "");

const getAddressLookupResults = async (config, postcode) => {
  const { OS_PLACES_URI, OS_PLACES_API_KEY } = config.environment;
  const standardisedPostcode = standardisePostcode(postcode);
  const requestStartTime = DateTime.now();

  const response = await axios.get(
    `${OS_PLACES_URI}${OS_PLACES_API_PATH}?postcode=${standardisedPostcode}&lr=en`,
    {
      headers: {
        key: OS_PLACES_API_KEY,
      },
      timeout: REQUEST_TIMEOUT,
    }
  );

  const requestEndTime = DateTime.now();
  const requestTimeInSeconds =
    requestEndTime.diff(requestStartTime).milliseconds;
  logger.debug(
    `OS Places API request time in milliseconds: ${requestTimeInSeconds}`
  );

  return response;
};

export {
  getAddressLookupResults,
  standardisePostcode,
  transformAddress,
  transformOsPlacesApiResponse,
};
