import expect from "expect";

const getNextForStep = jest.fn();

jest.mock("./get-next-for-step", () => ({
  getNextForStep: getNextForStep,
}));

const isNextPathNavigable = jest.fn();

jest.mock("../predicates", () => ({
  isNextPathNavigable: isNextPathNavigable,
}));

import {
  getStepForPath,
  getNextInDetailReviewPath,
  getNextInReviewPath,
  getNextNavigablePath,
} from "./selectors";

import {
  CHECK_ANSWERS_URL,
  CHECK_DETAILS_URL,
  SP_CONFIRM_DETAILS_URL,
} from "../../../paths/paths";
import { MAINFLOW, SECOND_PARENT_FLOW } from "../../../journey-definitions";

const step1 = {
  path: "/first",
};

const step2 = {
  path: "/second",
};

const step3 = {
  path: "/third",
};

const steps = [step1, step2, step3];

const journey = { steps };

describe("getStepForPath", () => {
  test("gets the next step in sequence of steps", () => {
    const result = getStepForPath("/first", steps);

    expect(result).toBe(step1);
  });

  test("returns undefined when no matching path in steps", () => {
    const steps = [step1, step2, step3];
    const result = getStepForPath("/fourth", steps);

    expect(result).toBe(undefined);
  });
});

test(`getNextInDetailsReviewPath should return ${CHECK_DETAILS_URL} url`, () => {
  const req = {};
  const prefix = "/context-path";
  const result = getNextInDetailReviewPath(req, prefix);

  expect(result).toBe(`${prefix}${CHECK_DETAILS_URL}`);
});

test(`getNextInReviewPath should return ${CHECK_ANSWERS_URL} url`, () => {
  const req = {
    session: {
      journeyName: MAINFLOW.name,
    },
  };
  const prefix = "/context-path";
  const result = getNextInReviewPath(req, prefix);

  expect(result).toBe(`${prefix}${CHECK_ANSWERS_URL}`);
});

test(`getNextInReviewPath should return ${SP_CONFIRM_DETAILS_URL} url for second parent journey`, () => {
  const req = {
    session: {
      journeyName: SECOND_PARENT_FLOW.name,
    },
  };
  const prefix = "/context-path";
  const result = getNextInReviewPath(req, prefix);

  expect(result).toBe(`${prefix}${SP_CONFIRM_DETAILS_URL}`);
});

test("getNextNavigablePath should return next path when it is navigable", () => {
  getNextForStep.mockReturnValue(step2.path);
  isNextPathNavigable.mockReturnValue(true);

  const req = {};

  const result = getNextNavigablePath(step1.path, req, journey);

  expect(result).toEqual(step2.path);
});
