import fs from "fs";

export interface Country {
  name: string;
  "alpha-3": string;
}

export function getCountryCodes(): Country[] {
  try {
    const data = fs.readFileSync("./country-codes.json", "utf8");
    const parsedData: Country[] = JSON.parse(data);
    return parsedData;
  } catch (error) {
    throw new Error("Error parsing country codes");
  }
}

export function countryCodeMapper(inputCountry) {
  // return country codes in form of text, value
  return getCountryCodes().map((country) => {
    if (inputCountry && country["alpha-3"] === inputCountry) {
      return {
        text: country.name,
        value: country["alpha-3"],
        selected: true,
      };
    }
    if (!inputCountry && country["alpha-3"] === "GBR") {
      return {
        text: country.name,
        value: country["alpha-3"],
        selected: true,
      };
    }
    return {
      text: country.name,
      value: country["alpha-3"],
    };
  });
}
