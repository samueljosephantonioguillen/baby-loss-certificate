import expect from "expect";

import { validate } from "./validate";
import { applyExpressValidation } from "../../common/test/apply-express-validation";

test("returns no validation errors when reference-number and reference-number-country field is valid", async () => {
  const req = {
    body: {
      "reference-number": "123456",
      "reference-number-country": "GBR",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("returns expected validation error when reference-number field is empty", async () => {
  const req = {
    body: {
      "reference-number": "",
      "reference-number-country": "GBR",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "",
    msg: "validation:missingReferenceId",
    path: "reference-number",
    location: "body",
  };

  expect(result.array().length).toBe(1);
  expect(error).toEqual(expectedError);
});

test("returns expected validation error when reference-number field is invalid", async () => {
  const req = {
    body: {
      "reference-number": "123456",
      "reference-number-country": "ZZZZZZZZ",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "ZZZZZZZZ",
    msg: "validation:invalidCountry",
    path: "reference-number-country",
    location: "body",
  };

  expect(result.array().length).toBe(1);
  expect(error).toEqual(expectedError);
});

test("returns expected validation error when reference-number field is empty", async () => {
  const req = {
    body: {
      "reference-number": "123456",
      "reference-number-country": "ZZZZZZZZ",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "ZZZZZZZZ",
    msg: "validation:invalidCountry",
    path: "reference-number-country",
    location: "body",
  };

  expect(result.array().length).toBe(1);
  expect(error).toEqual(expectedError);
});

test("returns expected validation error when reference-number field is too long", async () => {
  const req = {
    body: {
      "reference-number": "1".repeat(65),
      "reference-number-country": "GBR",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "1".repeat(65),
    msg: "validation:referenceIdTooLong",
    path: "reference-number",
    location: "body",
  };

  expect(result.array().length).toBe(1);
  expect(error).toEqual(expectedError);
});
