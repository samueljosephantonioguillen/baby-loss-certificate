import expect from "expect";

import {
  spEnterReferenceNumber,
  isNavigable,
  pageContent,
  validate,
  behaviourForPost,
} from "./sp-enter-reference-number";
import { acceptableReferenceNumberTypes } from "../proof-of-name/validate";
import { AGENT_ROLE, BACKOFFICE_ROLE } from "../../../constants";
import { config } from "../../../../../../config";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";
import { countryCodeMapper } from "../../../tools/country-code-parser";
import {
  IN_REVIEW_PROGRESS,
  IN_REVIEW,
  IN_PROGRESS,
} from "../../../flow-control/states";
import { buildSessionForJourney } from "../../../flow-control/test-utils/test-utils";

test("enterReferenceNumber should match expected outcomes", () => {
  const expectedResults = {
    path: "/other-parent-reference-number",
    template: "sp-enter-reference-number",
    isNavigable,
    pageContent,
    validate,
    behaviourForPost,
  };

  expect(spEnterReferenceNumber).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const req = { session: { applicant: {}, journeyName: MAINFLOW.name } };
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("enterReferenceNumber.title"),
    sectionTitle: translate("section.otherParentsDetails"),
    heading: translate("enterReferenceNumber.heading"),
    referenceNumber: translate("enterReferenceNumber.referenceNumber"),
    countryOrigin: translate("enterReferenceNumber.countryOrigin"),
    countryCodes: {
      data: countryCodeMapper(
        req.session.applicant["their-reference-number-country"]
      ),
    },
    continueButtonText: translate("buttons:continue"),
  };
  const result = pageContent({ translate, req });

  expect(result).toEqual(expected);
});

test("pageContent should return expected results when reference-number-country is present", () => {
  const applicant = {
    "their-reference-number-country": "USA",
  };
  const req = { session: { applicant, journeyName: MAINFLOW.name } };
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("enterReferenceNumber.title"),
    sectionTitle: translate("section.otherParentsDetails"),
    heading: translate("enterReferenceNumber.heading"),
    referenceNumber: translate("enterReferenceNumber.referenceNumber"),
    countryOrigin: translate("enterReferenceNumber.countryOrigin"),
    countryCodes: {
      data: countryCodeMapper(
        req.session.applicant["their-reference-number-country"]
      ),
    },
    continueButtonText: translate("buttons:continue"),
  };
  const result = pageContent({ translate, req });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  config.environment.USE_AUTHENTICATION = true;

  test.each(acceptableReferenceNumberTypes)(
    "should return true when backoffice_role and their-reference-number-type type is %s",
    (proof) => {
      const req = {
        session: {
          journeyName: MAINFLOW.name,
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "their-reference-number-type": proof,
          },
          account: {
            idTokenClaims: {
              roles: [BACKOFFICE_ROLE],
            },
          },
        },
      };
      const result = isNavigable(req);

      expect(result).toEqual(true);
    }
  );

  test("should return false when not backoffice_role and their-reference-number-type type is accepted", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "their-reference-number-type": "birthCertificate",
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when agent_role and their-reference-number-type type is accepted", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "their-reference-number-type": "birthCertificate",
        },
        account: {
          idTokenClaims: {
            roles: [AGENT_ROLE],
          },
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when backoffice_role and their-reference-number-type type is not accepted", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "their-reference-number-type": "invalid",
        },
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});

describe("behaviourForPost", () => {
  test("should set state to IN_REVIEW when current journey state is IN_REVIEW_PROGRESS", () => {
    const config = {};
    const journey = { name: MAINFLOW.name };

    const req = {
      session: {
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "their-reference-number": "123",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_REVIEW_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = {};
    const next = jest.fn();

    behaviourForPost(config, journey)(req, res, next);

    const expectedSession = {
      ...req.session,
      ...buildSessionForJourney({
        journeyName: journey.name,
        state: IN_REVIEW,
        nextAllowedPath: undefined,
      }),
    };

    expect(req.session).toEqual(expectedSession);
    expect(next).toBeCalled();
  });

  test("should call next and not change state when state isn't IN_REVIEW_PROGRESS", () => {
    const config = {};
    const journey = { name: MAINFLOW.name };

    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "their-reference-number": "123",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = {};
    const next = jest.fn();

    behaviourForPost(config, journey)(req, res, next);

    const expectedReq = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "their-reference-number": "123",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };

    expect(req).toEqual(expectedReq);
    expect(next).toBeCalled();
  });
});
