import expect from "expect";
import { partial } from "ramda";

import {
  IN_PROGRESS,
  IN_REVIEW,
  IN_REVIEW_PROGRESS,
  COMPLETED,
  IN_DETAILS_REVIEW,
} from "../states";
import {
  GET_NEXT_PATH,
  IS_PATH_ALLOWED,
  GET_NEXT_ALLOWED_PATH,
  SET_NEXT_ALLOWED_PATH,
  INCREMENT_NEXT_ALLOWED_PATH,
  INVALIDATE_REVIEW,
} from "./actions";
import {
  setCurrentPathInSession,
  getCurrentPathFromSession,
} from "../session-accessors";
import {
  buildSessionForJourney,
  getStateForJourney,
  getNextAllowedPathForJourney,
} from "../test-utils/test-utils";

const info = jest.fn();
const logger = { info };

jest.mock("../../../../logger/logger", () => ({
  logger,
}));

import { stateMachine } from "./state-machine";
import {
  CHECK_ANSWERS_URL,
  CHECK_DETAILS_URL,
  REQUEST_COMPLETE_URL,
} from "../../paths/paths";

jest.mock("../session-accessors", () => ({
  ...jest.requireActual("../session-accessors"),
  getCurrentPathFromSession: jest.fn(() => {
    /*do nothing*/
  }),
  setCurrentPathInSession: jest.fn(),
}));

const MAINFLOW = "mainflow";

const MAINFLOW_JOURNEY = {
  name: MAINFLOW,
  steps: [
    { path: "/first", next: () => "/second" },
    { path: "/second", next: () => "/third" },
    { path: "/third" },
  ],
  pathsInSequence: ["/first", "/second", "/third"],
};

const getStateForMainflowJourney = partial(getStateForJourney, [MAINFLOW]);

const getNextAllowedPathForMainflowJourney = partial(
  getNextAllowedPathForJourney,
  [MAINFLOW]
);

describe("IN_PROGRESS", () => {
  test(`${GET_NEXT_PATH} should return next property of associated step when state of ${IN_PROGRESS} defined in session`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
      path: "/first",
    };

    expect(stateMachine.dispatch(GET_NEXT_PATH, req, MAINFLOW_JOURNEY)).toBe(
      "/second"
    );
    expect(setCurrentPathInSession).toBeCalledWith(
      req,
      MAINFLOW_JOURNEY,
      req.path
    );
  });

  test(`${GET_NEXT_PATH} should return next navigable path when state of ${IN_PROGRESS} defined in session and next step is not navigable`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
      path: "/second",
    };

    expect(stateMachine.dispatch(GET_NEXT_PATH, req, MAINFLOW_JOURNEY)).toBe(
      "/third"
    );
    expect(setCurrentPathInSession).toBeCalledWith(
      req,
      MAINFLOW_JOURNEY,
      req.path
    );
  });

  test(`${IS_PATH_ALLOWED} should return true when given next allowed path is a next step`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_PROGRESS,
          nextAllowedPath: "/third",
        }),
      },
      path: "/second",
    };

    expect(
      stateMachine.dispatch(IS_PATH_ALLOWED, req, MAINFLOW_JOURNEY)
    ).toEqual(true);
  });

  test(`${IS_PATH_ALLOWED} should return false when given next allowed path is a previous step`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_PROGRESS,
          nextAllowedPath: "/first",
        }),
      },
      path: "/second",
    };

    expect(
      stateMachine.dispatch(IS_PATH_ALLOWED, req, MAINFLOW_JOURNEY)
    ).toEqual(false);
  });

  test(`${GET_NEXT_ALLOWED_PATH} should return the next allowed path in session`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_PROGRESS,
          nextAllowedPath: "/third",
        }),
      },
      path: "/second",
    };

    expect(
      stateMachine.dispatch(GET_NEXT_ALLOWED_PATH, req, MAINFLOW_JOURNEY)
    ).toEqual("/third");
  });

  test(`${SET_NEXT_ALLOWED_PATH} sets next allowed path in session`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_PROGRESS,
          nextAllowedPath: "/third",
        }),
      },
      path: "/second",
    };

    stateMachine.dispatch(
      SET_NEXT_ALLOWED_PATH,
      req,
      MAINFLOW_JOURNEY,
      req.path
    );

    expect(getNextAllowedPathForMainflowJourney(req)).toBe(req.path);
    expect(setCurrentPathInSession).toBeCalledWith(
      req,
      MAINFLOW_JOURNEY,
      req.path
    );
  });

  test(`${INCREMENT_NEXT_ALLOWED_PATH} updates next allowed path in session with the next allowed path in sequence`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
      path: "/first",
    };

    stateMachine.dispatch(INCREMENT_NEXT_ALLOWED_PATH, req, MAINFLOW_JOURNEY);
    expect(getNextAllowedPathForMainflowJourney(req)).toBe("/second");
    expect(setCurrentPathInSession).toBeCalledWith(
      req,
      MAINFLOW_JOURNEY,
      req.path
    );
  });

  test(`Dispatching an invalid action should return null when state is ${IN_PROGRESS}`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
      path: "/first",
    };

    expect(stateMachine.dispatch("INVALID_ACTION", req, MAINFLOW_JOURNEY)).toBe(
      null
    );
    expect(setCurrentPathInSession).toBeCalledTimes(0);
  });
});

describe("IN_DETAILS_REVIEW", () => {
  test(`${GET_NEXT_PATH} should return ${CHECK_DETAILS_URL} path when state of ${IN_DETAILS_REVIEW} defined in session`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_DETAILS_REVIEW,
          nextAllowedPath: undefined,
        }),
      },
      path: "/first",
    };

    expect(stateMachine.dispatch(GET_NEXT_PATH, req, MAINFLOW_JOURNEY)).toBe(
      CHECK_DETAILS_URL
    );
    expect(setCurrentPathInSession).toBeCalledWith(
      req,
      MAINFLOW_JOURNEY,
      req.path
    );
  });

  test(`${IS_PATH_ALLOWED} should return true when given next allowed path is a next step`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_DETAILS_REVIEW,
          nextAllowedPath: "/third",
        }),
      },
      path: "/second",
    };

    expect(
      stateMachine.dispatch(IS_PATH_ALLOWED, req, MAINFLOW_JOURNEY)
    ).toEqual(true);
  });

  test(`${IS_PATH_ALLOWED} should return true when given next allowed path is ${CHECK_DETAILS_URL}`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_DETAILS_REVIEW,
          nextAllowedPath: "/third",
        }),
      },
      path: CHECK_DETAILS_URL,
    };

    expect(
      stateMachine.dispatch(IS_PATH_ALLOWED, req, MAINFLOW_JOURNEY)
    ).toEqual(true);
  });

  test(`${IS_PATH_ALLOWED} should return false when given next allowed path is a previous step`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_DETAILS_REVIEW,
          nextAllowedPath: "/first",
        }),
      },
      path: "/second",
    };

    expect(
      stateMachine.dispatch(IS_PATH_ALLOWED, req, MAINFLOW_JOURNEY)
    ).toEqual(false);
  });

  test(`${GET_NEXT_ALLOWED_PATH} should return the next allowed path in session`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_DETAILS_REVIEW,
          nextAllowedPath: "/third",
        }),
      },
      path: "/second",
    };

    expect(
      stateMachine.dispatch(GET_NEXT_ALLOWED_PATH, req, MAINFLOW_JOURNEY)
    ).toEqual("/third");
  });

  test(`${SET_NEXT_ALLOWED_PATH} sets next allowed path in session`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_DETAILS_REVIEW,
          nextAllowedPath: "/third",
        }),
      },
      path: "/second",
    };

    stateMachine.dispatch(
      SET_NEXT_ALLOWED_PATH,
      req,
      MAINFLOW_JOURNEY,
      req.path
    );

    expect(getNextAllowedPathForMainflowJourney(req)).toBe(req.path);
    expect(setCurrentPathInSession).toBeCalledWith(
      req,
      MAINFLOW_JOURNEY,
      req.path
    );
  });

  test(`${INCREMENT_NEXT_ALLOWED_PATH} updates next allowed path in session with the next allowed path in sequence`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_DETAILS_REVIEW,
          nextAllowedPath: undefined,
        }),
      },
      path: "/first",
    };

    stateMachine.dispatch(INCREMENT_NEXT_ALLOWED_PATH, req, MAINFLOW_JOURNEY);
    expect(getNextAllowedPathForMainflowJourney(req)).toBe(CHECK_DETAILS_URL);
    expect(setCurrentPathInSession).toBeCalledWith(
      req,
      MAINFLOW_JOURNEY,
      req.path
    );
  });

  test(`Dispatching an invalid action should return null when state is ${IN_DETAILS_REVIEW}`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_DETAILS_REVIEW,
          nextAllowedPath: undefined,
        }),
      },
      path: "/first",
    };

    expect(stateMachine.dispatch("INVALID_ACTION", req, MAINFLOW_JOURNEY)).toBe(
      null
    );
    expect(setCurrentPathInSession).toBeCalledTimes(0);
  });
});

describe("IN_DETAILS_REVIEW_PROGRESS", () => {
  test(`${GET_NEXT_PATH} should return next property of associated step when state of ${IN_REVIEW_PROGRESS} defined in session`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_REVIEW_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
      path: "/first",
    };

    expect(stateMachine.dispatch(GET_NEXT_PATH, req, MAINFLOW_JOURNEY)).toBe(
      "/second"
    );
    expect(setCurrentPathInSession).toBeCalledWith(
      req,
      MAINFLOW_JOURNEY,
      req.path
    );
  });

  test(`${IS_PATH_ALLOWED} should return true when given next allowed path is a next step`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_REVIEW_PROGRESS,
          nextAllowedPath: "/third",
        }),
      },
      path: "/second",
    };

    expect(
      stateMachine.dispatch(IS_PATH_ALLOWED, req, MAINFLOW_JOURNEY)
    ).toEqual(true);
  });

  test(`${IS_PATH_ALLOWED} should return false when given next allowed path is a previous step`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_REVIEW_PROGRESS,
          nextAllowedPath: "/first",
        }),
      },
      path: "/second",
    };

    expect(
      stateMachine.dispatch(IS_PATH_ALLOWED, req, MAINFLOW_JOURNEY)
    ).toEqual(false);
  });

  test(`${GET_NEXT_ALLOWED_PATH} should return the next allowed path in session`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_REVIEW_PROGRESS,
          nextAllowedPath: "/third",
        }),
      },
      path: "/second",
    };

    expect(
      stateMachine.dispatch(GET_NEXT_ALLOWED_PATH, req, MAINFLOW_JOURNEY)
    ).toEqual("/third");
  });

  test(`${SET_NEXT_ALLOWED_PATH} sets next allowed path in session`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_REVIEW_PROGRESS,
          nextAllowedPath: "/third",
        }),
      },
      path: "/second",
    };

    stateMachine.dispatch(
      SET_NEXT_ALLOWED_PATH,
      req,
      MAINFLOW_JOURNEY,
      req.path
    );

    expect(getNextAllowedPathForMainflowJourney(req)).toBe(req.path);
    expect(setCurrentPathInSession).toBeCalledWith(
      req,
      MAINFLOW_JOURNEY,
      req.path
    );
  });

  test(`${INCREMENT_NEXT_ALLOWED_PATH} updates next allowed path in session with the next allowed path in sequence`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_REVIEW_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
      path: "/first",
    };

    stateMachine.dispatch(INCREMENT_NEXT_ALLOWED_PATH, req, MAINFLOW_JOURNEY);
    expect(getNextAllowedPathForMainflowJourney(req)).toBe("/second");
    expect(setCurrentPathInSession).toBeCalledWith(
      req,
      MAINFLOW_JOURNEY,
      req.path
    );
  });

  test(`Dispatching an invalid action should return null when state is ${IN_REVIEW_PROGRESS}`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_REVIEW_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
      path: "/first",
    };

    expect(stateMachine.dispatch("INVALID_ACTION", req, MAINFLOW_JOURNEY)).toBe(
      null
    );
    expect(setCurrentPathInSession).toBeCalledTimes(0);
  });
});

describe("IN_REVIEW", () => {
  test(`${GET_NEXT_PATH} should return ${CHECK_ANSWERS_URL} path when state of ${IN_REVIEW} defined in session`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_REVIEW,
          nextAllowedPath: undefined,
        }),
      },
      path: "/first",
    };

    expect(stateMachine.dispatch(GET_NEXT_PATH, req, MAINFLOW_JOURNEY)).toBe(
      CHECK_ANSWERS_URL
    );
    expect(setCurrentPathInSession).toBeCalledWith(
      req,
      MAINFLOW_JOURNEY,
      req.path
    );
  });

  test(`${IS_PATH_ALLOWED} should return true when given next allowed path is a next step`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_REVIEW,
          nextAllowedPath: "/third",
        }),
      },
      path: "/second",
    };

    expect(
      stateMachine.dispatch(IS_PATH_ALLOWED, req, MAINFLOW_JOURNEY)
    ).toEqual(true);
  });

  test(`${IS_PATH_ALLOWED} should return true when given next allowed path is ${CHECK_ANSWERS_URL}`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_REVIEW,
          nextAllowedPath: "/third",
        }),
      },
      path: CHECK_ANSWERS_URL,
    };

    expect(
      stateMachine.dispatch(IS_PATH_ALLOWED, req, MAINFLOW_JOURNEY)
    ).toEqual(true);
  });

  test(`${IS_PATH_ALLOWED} should return false when given next allowed path is a previous step`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_REVIEW,
          nextAllowedPath: "/first",
        }),
      },
      path: "/second",
    };

    expect(
      stateMachine.dispatch(IS_PATH_ALLOWED, req, MAINFLOW_JOURNEY)
    ).toEqual(false);
  });

  test(`${GET_NEXT_ALLOWED_PATH} should return the next allowed path in session`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_REVIEW,
          nextAllowedPath: "/third",
        }),
      },
      path: "/second",
    };

    expect(
      stateMachine.dispatch(GET_NEXT_ALLOWED_PATH, req, MAINFLOW_JOURNEY)
    ).toEqual("/third");
  });

  test(`${SET_NEXT_ALLOWED_PATH} sets next allowed path in session`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_REVIEW,
          nextAllowedPath: "/third",
        }),
      },
      path: "/second",
    };

    stateMachine.dispatch(
      SET_NEXT_ALLOWED_PATH,
      req,
      MAINFLOW_JOURNEY,
      req.path
    );

    expect(getNextAllowedPathForMainflowJourney(req)).toBe(req.path);
    expect(setCurrentPathInSession).toBeCalledWith(
      req,
      MAINFLOW_JOURNEY,
      req.path
    );
  });

  test(`${INCREMENT_NEXT_ALLOWED_PATH} updates next allowed path in session with the next allowed path in sequence`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_REVIEW,
          nextAllowedPath: undefined,
        }),
      },
      path: "/first",
    };

    stateMachine.dispatch(INCREMENT_NEXT_ALLOWED_PATH, req, MAINFLOW_JOURNEY);
    expect(getNextAllowedPathForMainflowJourney(req)).toBe(CHECK_ANSWERS_URL);
    expect(setCurrentPathInSession).toBeCalledWith(
      req,
      MAINFLOW_JOURNEY,
      req.path
    );
  });

  test(`${INVALIDATE_REVIEW} should set state to ${IN_PROGRESS}`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_REVIEW,
          nextAllowedPath: undefined,
        }),
      },
      path: "/first",
    };

    stateMachine.dispatch(INVALIDATE_REVIEW, req, MAINFLOW_JOURNEY);
    expect(getStateForMainflowJourney(req)).toBe(IN_PROGRESS);
  });

  test(`Dispatching an invalid action should return null when state is ${IN_REVIEW}`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_REVIEW,
          nextAllowedPath: undefined,
        }),
      },
      path: "/first",
    };

    expect(stateMachine.dispatch("INVALID_ACTION", req, MAINFLOW_JOURNEY)).toBe(
      null
    );
    expect(setCurrentPathInSession).toBeCalledTimes(0);
  });
});

describe("IN_REVIEW_PROGRESS", () => {
  test(`${GET_NEXT_PATH} should return next property of associated step when state of ${IN_REVIEW_PROGRESS} defined in session`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_REVIEW_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
      path: "/first",
    };

    expect(stateMachine.dispatch(GET_NEXT_PATH, req, MAINFLOW_JOURNEY)).toBe(
      "/second"
    );
    expect(setCurrentPathInSession).toBeCalledWith(
      req,
      MAINFLOW_JOURNEY,
      req.path
    );
  });

  test(`${IS_PATH_ALLOWED} should return true when given next allowed path is a next step`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_REVIEW_PROGRESS,
          nextAllowedPath: "/third",
        }),
      },
      path: "/second",
    };

    expect(
      stateMachine.dispatch(IS_PATH_ALLOWED, req, MAINFLOW_JOURNEY)
    ).toEqual(true);
  });

  test(`${IS_PATH_ALLOWED} should return false when given next allowed path is a previous step`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_REVIEW_PROGRESS,
          nextAllowedPath: "/first",
        }),
      },
      path: "/second",
    };

    expect(
      stateMachine.dispatch(IS_PATH_ALLOWED, req, MAINFLOW_JOURNEY)
    ).toEqual(false);
  });

  test(`${GET_NEXT_ALLOWED_PATH} should return the next allowed path in session`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_REVIEW_PROGRESS,
          nextAllowedPath: "/third",
        }),
      },
      path: "/second",
    };

    expect(
      stateMachine.dispatch(GET_NEXT_ALLOWED_PATH, req, MAINFLOW_JOURNEY)
    ).toEqual("/third");
  });

  test(`${SET_NEXT_ALLOWED_PATH} sets next allowed path in session`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_REVIEW_PROGRESS,
          nextAllowedPath: "/third",
        }),
      },
      path: "/second",
    };

    stateMachine.dispatch(
      SET_NEXT_ALLOWED_PATH,
      req,
      MAINFLOW_JOURNEY,
      req.path
    );

    expect(getNextAllowedPathForMainflowJourney(req)).toBe(req.path);
    expect(setCurrentPathInSession).toBeCalledWith(
      req,
      MAINFLOW_JOURNEY,
      req.path
    );
  });

  test(`${INCREMENT_NEXT_ALLOWED_PATH} updates next allowed path in session with the next allowed path in sequence`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_REVIEW_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
      path: "/first",
    };

    stateMachine.dispatch(INCREMENT_NEXT_ALLOWED_PATH, req, MAINFLOW_JOURNEY);
    expect(getNextAllowedPathForMainflowJourney(req)).toBe("/second");
    expect(setCurrentPathInSession).toBeCalledWith(
      req,
      MAINFLOW_JOURNEY,
      req.path
    );
  });

  test(`Dispatching an invalid action should return null when state is ${IN_REVIEW_PROGRESS}`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_REVIEW_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
      path: "/first",
    };

    expect(stateMachine.dispatch("INVALID_ACTION", req, MAINFLOW_JOURNEY)).toBe(
      null
    );
    expect(setCurrentPathInSession).toBeCalledTimes(0);
  });
});

describe("COMPLETED", () => {
  test(`${GET_NEXT_PATH} should return ${REQUEST_COMPLETE_URL} path when state of ${COMPLETED} defined in session`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: COMPLETED,
          nextAllowedPath: undefined,
        }),
      },
      path: "/first",
    };

    expect(stateMachine.dispatch(GET_NEXT_PATH, req, MAINFLOW_JOURNEY)).toBe(
      REQUEST_COMPLETE_URL
    );
    expect(setCurrentPathInSession).toBeCalledWith(
      req,
      MAINFLOW_JOURNEY,
      req.path
    );
  });

  test(`${IS_PATH_ALLOWED} should return true when given next allowed path is ${REQUEST_COMPLETE_URL}`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: COMPLETED,
          nextAllowedPath: undefined,
        }),
      },
      path: REQUEST_COMPLETE_URL,
    };

    expect(
      stateMachine.dispatch(IS_PATH_ALLOWED, req, MAINFLOW_JOURNEY)
    ).toEqual(true);
  });

  test(`${IS_PATH_ALLOWED} should return false when given next allowed path is not ${REQUEST_COMPLETE_URL}`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: COMPLETED,
          nextAllowedPath: undefined,
        }),
      },
      path: "/second",
    };

    expect(
      stateMachine.dispatch(IS_PATH_ALLOWED, req, MAINFLOW_JOURNEY)
    ).toEqual(false);
  });

  test(`${GET_NEXT_ALLOWED_PATH} should return ${REQUEST_COMPLETE_URL}`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: COMPLETED,
          nextAllowedPath: undefined,
        }),
      },
      path: "/second",
    };

    expect(
      stateMachine.dispatch(GET_NEXT_ALLOWED_PATH, req, MAINFLOW_JOURNEY)
    ).toEqual(REQUEST_COMPLETE_URL);
  });

  test(`${SET_NEXT_ALLOWED_PATH} sets next allowed path in session`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: IN_REVIEW,
          nextAllowedPath: "/third",
        }),
      },
      path: "/second",
    };

    stateMachine.dispatch(
      SET_NEXT_ALLOWED_PATH,
      req,
      MAINFLOW_JOURNEY,
      req.path
    );

    expect(getNextAllowedPathForMainflowJourney(req)).toBe(req.path);
    expect(setCurrentPathInSession).toBeCalledWith(
      req,
      MAINFLOW_JOURNEY,
      req.path
    );
  });

  test(`${INCREMENT_NEXT_ALLOWED_PATH} updates next allowed path in session with ${REQUEST_COMPLETE_URL}`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: COMPLETED,
          nextAllowedPath: undefined,
        }),
      },
      path: "/first",
    };

    stateMachine.dispatch(INCREMENT_NEXT_ALLOWED_PATH, req, MAINFLOW_JOURNEY);
    expect(getNextAllowedPathForMainflowJourney(req)).toBe(
      REQUEST_COMPLETE_URL
    );
    expect(setCurrentPathInSession).toBeCalledWith(
      req,
      MAINFLOW_JOURNEY,
      req.path
    );
  });

  test(`Dispatching an invalid action should return null when state is ${IN_REVIEW}`, () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW,
          state: COMPLETED,
          nextAllowedPath: undefined,
        }),
      },
      path: "/first",
    };

    expect(stateMachine.dispatch("INVALID_ACTION", req, MAINFLOW_JOURNEY)).toBe(
      null
    );
    expect(setCurrentPathInSession).toBeCalledTimes(0);
  });
});

test("setState does not log a change in state if the new state is the same as the current state", () => {
  info.mockClear();
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: MAINFLOW,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
  };

  stateMachine.setState(IN_PROGRESS, req, MAINFLOW_JOURNEY);

  expect(info).toBeCalledTimes(0);
});

test("setState sets the state if the new state is different from the current state", () => {
  info.mockClear();
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: MAINFLOW,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
    headers: {
      REQUEST_ID_HEADER: "123456",
    },
  };

  stateMachine.setState(COMPLETED, req, MAINFLOW_JOURNEY);
  expect(getStateForMainflowJourney(req)).toBe(COMPLETED);
  expect(info).toBeCalledTimes(1);
});

test("getCurrentPath calls the function getCurrentPathFromSession", () => {
  info.mockClear();
  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: MAINFLOW,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
  };

  stateMachine.getCurrentPath(req, MAINFLOW_JOURNEY);

  expect(getCurrentPathFromSession).toBeCalledTimes(1);
});
