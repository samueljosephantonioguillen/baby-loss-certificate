import { check } from "express-validator";
import { translateValidationMessage } from "../../common/translate-validation-message";

const validate = () => [
  check("selected-address")
    .not()
    .isEmpty()
    .withMessage(translateValidationMessage("validation:selectAddress")),
];

export { validate };
