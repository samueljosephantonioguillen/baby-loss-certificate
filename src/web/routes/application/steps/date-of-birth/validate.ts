import { body } from "express-validator";

import { isNilOrEmpty } from "../../../../../common/predicates";
import { toDateString } from "../common/format-date";
import {
  isDateRealDate,
  isDateInPast,
  isDateSixteenOrMoreYearsInThePast,
  isDateAHundredTwentyOrMoreYearsInThePast,
} from "../common/validate-date";
import { getFlowDataObjectName } from "../../tools/data-object";

const addDobToBody = (req, res, next) => {
  req.body["date-of-birth"] = toDateString(
    req.body["date-of-birth-day"],
    req.body["date-of-birth-month"],
    req.body["date-of-birth-year"]
  );
  next();
};

const validateDateOfBirth = (dob, { req }) => {
  const dataObjectName = getFlowDataObjectName(req.session.journeyName);
  if (isNilOrEmpty(dob)) {
    throw new Error(req.t("validation:missingDateOfBirth"));
  }

  //  return true if any individual field is empty
  if (
    isNilOrEmpty(req.body["date-of-birth-day"]) ||
    isNilOrEmpty(req.body["date-of-birth-month"]) ||
    isNilOrEmpty(req.body["date-of-birth-year"])
  ) {
    return true;
  }

  // if day contains non number characters
  if (
    /\D/.test(req.body["date-of-birth-day"]) ||
    /\D/.test(req.body["date-of-birth-month"]) ||
    /\D/.test(req.body["date-of-birth-year"])
  ) {
    return true;
  }

  if (!isDateRealDate(dob)) {
    throw new Error(req.t("validation:dateOfBirthNotReal"));
  }

  if (!isDateInPast(dob)) {
    throw new Error(req.t("validation:dateOfBirthInFuture"));
  }

  if (isDateAHundredTwentyOrMoreYearsInThePast(dob)) {
    const oldestAge = 120;
    const today = new Date();
    const oldestDob = new Date();
    oldestDob.setFullYear(today.getFullYear() - oldestAge);
    const dd = oldestDob.getDate().toString().padStart(2, "0");
    const mm = (oldestDob.getMonth() + 1).toString().padStart(2, "0");
    const yyyy = oldestDob.getFullYear().toString();
    const dateString = `${dd}-${mm}-${yyyy}`;

    throw new Error(
      req.t("validation:dateOfBirthTooOld", { oldestDob: dateString })
    );
  }

  req.session[dataObjectName].olderThanAgeRequirement =
    isDateSixteenOrMoreYearsInThePast(dob);

  return true;
};

const validateDateOfBirthDay = (dob, { req }) => {
  // if day contains non number characters
  if (/\D/.test(req.body["date-of-birth-day"])) {
    throw new Error(req.t("validation:dateOfBirthDayInvalid"));
  }
  if (
    isNilOrEmpty(req.body["date-of-birth-day"]) &&
    (!isNilOrEmpty(req.body["date-of-birth-month"]) ||
      !isNilOrEmpty(req.body["date-of-birth-year"]))
  ) {
    throw new Error(req.t("validation:missingDateOfBirthDay"));
  }
  return true;
};

const validateDateOfBirthMonth = (dob, { req }) => {
  // if month contains non number characters
  if (/\D/.test(req.body["date-of-birth-month"])) {
    throw new Error(req.t("validation:dateOfBirthMonthInvalid"));
  }
  if (
    isNilOrEmpty(req.body["date-of-birth-month"]) &&
    (!isNilOrEmpty(req.body["date-of-birth-day"]) ||
      !isNilOrEmpty(req.body["date-of-birth-year"]))
  ) {
    throw new Error(req.t("validation:missingDateOfBirthMonth"));
  }
  return true;
};

const validateDateOfBirthYear = (dob, { req }) => {
  // if year contains non number characters
  if (/\D/.test(req.body["date-of-birth-year"])) {
    throw new Error(req.t("validation:dateOfBirthYearInvalid"));
  }
  if (
    isNilOrEmpty(req.body["date-of-birth-year"]) &&
    (!isNilOrEmpty(req.body["date-of-birth-day"]) ||
      !isNilOrEmpty(req.body["date-of-birth-month"]))
  ) {
    throw new Error(req.t("validation:missingDateOfBirthYear"));
  }
  return true;
};

const validate = () => [
  addDobToBody,

  body("date-of-birth").custom(validateDateOfBirth),

  body("date-of-birth-day").custom(validateDateOfBirthDay),
  body("date-of-birth-month").custom(validateDateOfBirthMonth),
  body("date-of-birth-year").custom(validateDateOfBirthYear),
];

export {
  validate,
  validateDateOfBirthDay,
  validateDateOfBirthMonth,
  validateDateOfBirthYear,
};
