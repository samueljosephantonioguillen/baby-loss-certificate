import expect from "expect";

import { applyExpressValidation } from "../../common/test/apply-express-validation";
import { validate } from "./validate";

test("'live-in-england' field should be 'yes', otherwise no errors are returned", async () => {
  const req = {
    body: {
      "live-in-england": "no",
      postcode: "B3 2DX",
      sanitisedPostcode: "B3 2DX",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const error = result.array()[0];

  expect(result.array().length).toBe(0);
  expect(error).toBeFalsy();
});

test("return no errors when postcode is valid", async () => {
  const req = {
    body: {
      "live-in-england": "yes",
      postcode: "B3 2DX",
      sanitisedPostcode: "B3 2DX",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const error = result.array()[0];

  expect(result.array().length).toBe(0);
  expect(error).toBeFalsy();
});

test("when missing field, return expected error", async () => {
  const req = {
    body: {
      "live-in-england": "yes",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const expectedError = {
    type: "field",
    value: undefined,
    msg: "validation:missingPostcode",
    path: "postcode",
    location: "body",
  };

  const error = result.array()[0];

  expect(result.array().length).toBe(1);
  expect(error).toEqual(expectedError);
});

test("when invalid field, return expected error", async () => {
  const req = {
    body: {
      "live-in-england": "yes",
      postcode: "invalid postcode",
      sanitisedPostcode: "invalid postcode",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const expectedError = {
    type: "field",
    value: req.body["postcode"],
    msg: "validation:invalidPostcode",
    path: "postcode",
    location: "body",
  };

  const error = result.array()[0];

  expect(result.array().length).toBe(1);
  expect(error).toEqual(expectedError);
});
