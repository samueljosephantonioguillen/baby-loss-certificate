import expect from "expect";
import { validate } from "./validate";
import { applyExpressValidation } from "../../common/test/apply-express-validation";

test("return no validation errors when and other-parent-email is valid email and their-email-confirm is same and confirm-told-other-parent is true", async () => {
  const req = {
    body: {
      "their-email": "te-st@email.com",
      "their-email-confirm": "te-st@email.com",
      "confirm-told-other-parent": "true",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("return expected validation error when their-email is invalid email", async () => {
  const req = {
    body: {
      "their-email": "test@emailcom",
      "their-email-confirm": "test@emailcom",
      "confirm-told-other-parent": "true",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "test@emailcom",
    msg: "validation:otherParentEmailInvalidFormat",
    path: "their-email",
    location: "body",
  };

  expect(error).toEqual(expectedError);
});

test("return expected validation error when their-email is empty", async () => {
  const req = {
    body: {
      "their-email": "",
      "their-email-confirm": "",
      "confirm-told-other-parent": "true",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "",
    msg: "validation:missingOtherParentEmail",
    path: "their-email",
    location: "body",
  };

  expect(error).toEqual(expectedError);
});

test("return expected validation error when their-email is too long", async () => {
  const req = {
    body: {
      "their-email": "a".repeat(256),
      "their-email-confirm": "a".repeat(256),
      "confirm-told-other-parent": "true",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "a".repeat(256),
    msg: "validation:otherParentEmailTooLong",
    path: "their-email",
    location: "body",
  };

  expect(error).toEqual(expectedError);
});

test("return expected validation error when their-email-confirm is not same as their-email", async () => {
  const req = {
    body: {
      "their-email": "test@email.com",
      "their-email-confirm": "waesrv",
      "confirm-told-other-parent": "true",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "waesrv",
    msg: "validation:otherParentEmailsNotEqual",
    path: "their-email-confirm",
    location: "body",
  };

  expect(error).toEqual(expectedError);
});

test("return expected validation error when their-email has invalid characters", async () => {
  const req = {
    body: {
      "their-email": "test@£$%^&*()_+{}:@~<>?|",
      "their-email-confirm": "test@£$%^&*()_+{}:@~<>?|",
      "confirm-told-other-parent": "true",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }
  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "test@£$%^&*()_+{}:@~<>?|",
    msg: "validation:otherParentEmailInvalidCharacters",
    path: "their-email",
    location: "body",
  };

  expect(error).toEqual(expectedError);
});
