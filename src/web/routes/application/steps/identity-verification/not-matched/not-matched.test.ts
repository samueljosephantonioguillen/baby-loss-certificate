import { MAINFLOW } from "../../../journey-definitions";
import { contextPath } from "../../../paths/context-path";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";
import { checkYourDetails } from "../../check-your-details";
import {
  notMatched,
  pageContent,
  isNavigable,
  behaviourForPost,
} from "./not-matched";

const req = {
  session: {
    journeyName: MAINFLOW.name,
    [MAINFLOW_DATA_OBJECT_NAME]: {
      notMatched: true,
    },
  },
};
const res = {
  redirect: jest.fn(),
};

test("notMatched should match expected outcomes", () => {
  const expectedResults = {
    path: "/cannot-request-cannot-find-match",
    template: "not-matched",
    pageContent,
    isNavigable,
    behaviourForPost,
  };

  expect(notMatched).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("notMatched.title"),
    heading: translate("notMatched.heading"),
    bodyLineOne: translate("notMatched.body.firstLine"),
    bodyLineTwo: translate("notMatched.body.secondLine"),
    bodyLineThree: translate("notMatched.body.thirdLine"),
    tryAgainButton: translate("notMatched.button"),
    backLinkText: translate("buttons:back"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when notMatched is true", () => {
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when notMatched is false", () => {
    req.session[MAINFLOW_DATA_OBJECT_NAME].notMatched = false;
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});

test("behaviourForPost should redirect to check-your-details", () => {
  behaviourForPost()(req, res);

  expect(res.redirect).toBeCalled();
  expect(res.redirect).toBeCalledWith(
    contextPath(checkYourDetails.path, MAINFLOW.name)
  );
});
