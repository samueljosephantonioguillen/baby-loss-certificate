import { check } from "express-validator";

import { translateValidationMessage } from "../../common/translate-validation-message";
import { UK_POSTCODE_PATTERN } from "../../../constants";

const checkPostcodeIsValid = (_, { req }) =>
  UK_POSTCODE_PATTERN.test(req.body.sanitisedPostcode);

const validate = () => [
  check("postcode")
    .not()
    .isEmpty()
    .bail()
    .withMessage(translateValidationMessage("validation:missingPostcode"))
    .custom(checkPostcodeIsValid)
    .withMessage(translateValidationMessage("validation:invalidPostcode")),
];

export { validate };
