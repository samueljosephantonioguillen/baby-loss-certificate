import expect from "expect";
import { dateAsString, toDateString } from "./format-date";
import {
  isValidDateString,
  isDateRealDate,
  isDateInPast,
  isDateAfterDateOfLoss,
  isDateSixteenOrMoreYearsInThePast,
  isDateAHundredTwentyOrMoreYearsInThePast,
} from "./validate-date";

test("isValidDateString", () => {
  expect(isValidDateString("1999-12-12")).toBe(true);
  expect(isValidDateString("1900-01-01")).toBe(true);
  expect(isValidDateString("2999-12-31")).toBe(true);

  expect(isValidDateString("")).toBe(false);
  expect(isValidDateString(null)).toBe(false);
  expect(isValidDateString("abcd-ab-ab")).toBe(false);
  expect(isValidDateString("2007-04-05T14:30")).toBe(false);
});

test("isDateInPast", () => {
  const today = new Date();
  const dateToday = toDateString(
    today.getDate(),
    today.getMonth() + 1,
    today.getFullYear()
  );
  const yyyy = today.getFullYear() + 1;

  const thisYear = today.getFullYear() + "-01-01";
  const nextYear = yyyy + "-01-01";

  expect(isDateInPast("1999-12-12")).toBe(true);
  expect(isDateInPast(dateToday)).toBe(false);
  expect(isDateInPast(thisYear)).toBe(true);
  expect(isDateInPast(nextYear)).toBe(false);
});

test("isDateAfterDateOfLoss", () => {
  const today = new Date();
  const thisYear = today.getFullYear();
  const nextYear = today.getFullYear() + 1;
  const lastYear = today.getFullYear() - 1;

  expect(isDateAfterDateOfLoss(dateAsString(), thisYear)).toBe(true);
  expect(isDateAfterDateOfLoss(dateAsString(), nextYear)).toBe(true);
  expect(isDateAfterDateOfLoss(dateAsString(), lastYear)).toBe(false);
});

test("isDateRealDate", () => {
  expect(isDateRealDate("2019-12-11")).toBe(true);
  expect(isDateRealDate("2019-13-11")).toBe(false);
  expect(isDateRealDate("2019-02-30")).toBe(false);
  expect(isDateRealDate("2019-02-300")).toBe(false);
  expect(isDateRealDate("20191-02-300")).toBe(false);
  expect(isDateRealDate("2019-022-30")).toBe(false);
});

test("isDateSixteenOrMoreYearsInThePast", () => {
  const sixteenYearsAgo = dateAsString({ yearAdjustment: -16 });
  const sixteenYearsOneMonthAgo = dateAsString({
    yearAdjustment: -16,
    monthAdjustment: -1,
  });
  const fifteenYearsElevenMonthsAgo = dateAsString({
    yearAdjustment: -15,
    monthAdjustment: -11,
  });

  expect(isDateSixteenOrMoreYearsInThePast("1999-12-12")).toBe(true);
  expect(isDateSixteenOrMoreYearsInThePast("9999-12-12")).toBe(false);
  expect(isDateSixteenOrMoreYearsInThePast(sixteenYearsAgo)).toBe(true);
  expect(isDateSixteenOrMoreYearsInThePast(sixteenYearsOneMonthAgo)).toBe(true);
  expect(isDateSixteenOrMoreYearsInThePast(fifteenYearsElevenMonthsAgo)).toBe(
    false
  );
});

test("isDateAHundredTwentyOrMoreYearsInThePast", () => {
  const hundredTwentyYearsAgo = dateAsString({ yearAdjustment: -120 });
  const hundredTwentyYearsOneMonthAgo = dateAsString({
    yearAdjustment: -120,
    monthAdjustment: -1,
  });
  const hundredNineteenYearsElevenMonthsAgo = dateAsString({
    yearAdjustment: -119,
    monthAdjustment: -11,
  });

  expect(isDateAHundredTwentyOrMoreYearsInThePast("1900-12-12")).toBe(true);
  expect(isDateAHundredTwentyOrMoreYearsInThePast("9999-12-12")).toBe(false);
  expect(isDateAHundredTwentyOrMoreYearsInThePast(hundredTwentyYearsAgo)).toBe(
    true
  );
  expect(
    isDateAHundredTwentyOrMoreYearsInThePast(hundredTwentyYearsOneMonthAgo)
  ).toBe(true);
  expect(
    isDateAHundredTwentyOrMoreYearsInThePast(
      hundredNineteenYearsElevenMonthsAgo
    )
  ).toBe(false);
});
