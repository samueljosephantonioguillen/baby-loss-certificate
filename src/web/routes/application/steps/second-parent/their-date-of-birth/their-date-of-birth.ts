import { userIsBackOffice } from "../../../../azure-routes/auth";
import { getFlowDataObjectName } from "../../../tools/data-object";
import { sanitise } from "./sanitise";
import { validate } from "./validate";

const pageContent = ({ translate }) => ({
  title: translate("theirDateOfBirth.title"),
  sectionTitle: translate("section.otherParentsDetails"),
  heading: translate("theirDateOfBirth.heading"),
  hint: translate("theirDateOfBirth.hint"),
  day: translate("day"),
  month: translate("month"),
  year: translate("year"),
  backLinkText: translate("buttons:back"),
  continueButtonText: translate("buttons:continue"),
});

const isNavigable = (req) =>
  !!req.session[getFlowDataObjectName(req.session.journeyName)][
    "their-email"
  ] ||
  (userIsBackOffice(req) &&
    !!req.session[getFlowDataObjectName(req.session.journeyName)][
      "their-first-name"
    ] &&
    !!req.session[getFlowDataObjectName(req.session.journeyName)][
      "their-last-name"
    ]);

const theirDateOfBirth = {
  path: "/other-parent-date-of-birth",
  template: "their-date-of-birth",
  pageContent,
  isNavigable,
  sanitise,
  validate,
};

export { theirDateOfBirth, pageContent, isNavigable, sanitise, validate };
