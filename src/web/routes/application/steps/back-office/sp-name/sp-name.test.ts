import expect from "expect";

import {
  spName,
  isNavigable,
  pageContent,
  sanitise,
  validate,
} from "./sp-name";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";
import { config } from "../../../../../../config";
import { BACKOFFICE_ROLE, AGENT_ROLE } from "../../../constants";
import { MAINFLOW } from "../../../journey-definitions";

test("enterReferenceNumber should match expected outcomes", () => {
  const expectedResults = {
    path: "/other-parent-name",
    template: "their-name",
    pageContent,
    isNavigable,
    sanitise,
    validate,
  };

  expect(spName).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("theirName.title"),
    sectionTitle: translate("section.certificateDetails"),
    heading: translate("theirName.heading"),
    hint: translate("theirName.hint"),
    firstName: translate("yourName.firstName"),
    lastName: translate("yourName.lastName"),
    backLinkText: translate("buttons:back"),
    continueButtonText: translate("buttons:continue"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  config.environment.USE_AUTHENTICATION = true;

  test("should return true when backoffice_role and their-relation-to-baby is populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "their-relation-to-baby": "mother",
        },
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when agent_role and their-relation-to-baby is populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "their-relation-to-baby": "mother",
        },
        account: {
          idTokenClaims: {
            roles: [AGENT_ROLE],
          },
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when backoffice_role and their-relation-to-baby is not populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {},
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});
