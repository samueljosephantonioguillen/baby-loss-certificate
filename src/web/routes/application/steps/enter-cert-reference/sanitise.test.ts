import expect from "expect";

import { sanitise } from "./sanitise";

const next = jest.fn();

test("removes hyphens and uppercases reference number", () => {
  const req = {
    body: {
      "sp-reference-number": "9ad-a86-e586d",
    },
  };

  sanitise()(req, {}, next);

  expect(req.body).toEqual({
    "sp-reference-number": "9ad-a86-e586d",
    "sp-sanitised-reference-number": "9ADA86E586D",
  });

  expect(next).toBeCalledTimes(1);
});

test("uppercases reference number", () => {
  const req = {
    body: {
      "sp-reference-number": "9ada86e586d",
    },
  };

  sanitise()(req, {}, next);

  expect(req.body).toEqual({
    "sp-reference-number": "9ada86e586d",
    "sp-sanitised-reference-number": "9ADA86E586D",
  });

  expect(next).toBeCalledTimes(1);
});

test("removes hyphens from reference number", () => {
  const req = {
    body: {
      "sp-reference-number": "9AD-A86-E586D",
    },
  };

  sanitise()(req, {}, next);

  expect(req.body).toEqual({
    "sp-reference-number": "9AD-A86-E586D",
    "sp-sanitised-reference-number": "9ADA86E586D",
  });

  expect(next).toBeCalledTimes(1);
});

test("no transformation when in sanitised form", () => {
  const req = {
    body: {
      "sp-reference-number": "9ADA86E586D",
    },
  };

  sanitise()(req, {}, next);

  expect(req.body).toEqual({
    "sp-reference-number": "9ADA86E586D",
    "sp-sanitised-reference-number": "9ADA86E586D",
  });

  expect(next).toBeCalledTimes(1);
});
