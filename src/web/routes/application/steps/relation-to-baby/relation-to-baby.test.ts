import expect from "expect";

import {
  relationToBaby,
  pageContent,
  isNavigable,
  validate,
  behaviourForGet,
  behaviourForPost,
} from "./relation-to-baby";
import { buildSessionForJourney } from "../../flow-control/test-utils/test-utils";
import {
  IN_PROGRESS,
  IN_REVIEW,
  IN_REVIEW_PROGRESS,
} from "../../flow-control/states";
import { contextPath } from "../../paths/context-path";
import { CHECK_ANSWERS_URL } from "../../paths/paths";
import { MAINFLOW } from "../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../tools/data-object";

test("relationToBaby should match expected outcomes", () => {
  const expectedResults = {
    path: "/relationship-to-baby",
    template: "relation-to-baby",
    pageContent,
    isNavigable,
    validate,
    behaviourForGet,
    behaviourForPost,
  };

  expect(relationToBaby).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("relationToBaby.title"),
    sectionTitle: translate("section.confirmEligibility"),
    heading: translate("relationToBaby.heading"),
    hintLineOne: translate("relationToBaby.hint.firstLine"),
    mother: translate("relationToBaby.radios.mother"),
    father: translate("relationToBaby.radios.father"),
    parent: translate("relationToBaby.radios.parent"),
    surrogate: translate("relationToBaby.radios.surrogate"),
    noneOfTheAbove: translate("relationToBaby.radios.noneOfTheAbove"),
    or: translate("or"),
    backLinkText: translate("buttons:back"),
    continueButtonText: translate("buttons:continue"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when loss-in-five-years is yes and year-of-loss exists", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "loss-in-five-years": "yes",
          "year-of-loss": 2023,
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when loss-in-five-years is yes and year-of-loss doesn't exist", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "loss-in-five-years": "yes",
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when loss-in-five-years is no and year-of-loss exists", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "loss-in-five-years": "no",
          "year-of-loss": 2023,
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});

describe("behaviourForGet", () => {
  const config = {};
  const journey = { name: MAINFLOW.name };

  test("should call set to IN_REVIEW when in IN_REVIEW_PROGRESS state", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "relation-to-baby": "",
        },
        submittedRelation: "mother",
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_REVIEW_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = { locals: { previous: undefined } };
    const next = jest.fn();

    behaviourForGet(config, journey)(req, res, next);

    expect(req.session[MAINFLOW_DATA_OBJECT_NAME]["relation-to-baby"]).toEqual(
      req.session.submittedRelation
    );
    expect(req.session.journeys[journey.name].state).toEqual(IN_REVIEW);
    expect(res.locals.previous).toBe(
      contextPath(CHECK_ANSWERS_URL, MAINFLOW.name)
    );
    expect(next).toBeCalledTimes(1);
  });

  test("should call next when not in IN_REVIEW_PROGRESS state", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "relation-to-baby": "",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = { locals: { previous: undefined } };
    const next = jest.fn();

    behaviourForGet(config, journey)(req, res, next);

    expect(res.locals.previous).toBe(undefined);
    expect(next).toBeCalledTimes(1);
  });
});

describe("behaviourForPost", () => {
  const config = {};
  const journey = { name: MAINFLOW.name };

  test("should set state to IN_REVIEW_PROGRESS when IN_REVIEW and relation is set to none", () => {
    const relation = "mother";

    const req = {
      body: {
        "relation-to-baby": "None",
      },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "relation-to-baby": relation,
        },
        submittedRelation: "",
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_REVIEW,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = {};
    const next = jest.fn();

    behaviourForPost(config, journey)(req, res, next);

    expect(req.session.submittedRelation).toEqual(relation);
    expect(req.session.journeys[journey.name].state).toEqual(
      IN_REVIEW_PROGRESS
    );
  });

  test("when state is IN_PROGRESS, remain IN_PROGRESS", () => {
    const req = {
      body: {
        "relation-to-baby": "None",
      },
      session: {
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = {};
    const next = jest.fn();

    behaviourForPost(config, journey)(req, res, next);

    const expectedSession = {
      ...buildSessionForJourney({
        journeyName: journey.name,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    };

    expect(req.session).toEqual(expectedSession);
  });
});
