import { userIsBackOffice } from "../../../../azure-routes/auth";
import { getFlowDataObjectName } from "../../../tools/data-object";
import { validate } from "./validate";

const pageContent = ({ translate }) => ({
  title: translate("proofOfName.title"),
  sectionTitle: translate("section.yourDetails"),
  heading: translate("proofOfName.heading"),
  help: translate("proofOfName.help"),
  passport: translate("proofOfName.radios.passport"),
  birthCertificate: translate("proofOfName.radios.birthCertificate"),
  eeaMemberCard: translate("proofOfName.radios.eeaMemberCard"),
  drivingLicence: translate("proofOfName.radios.drivingLicence"),
  oldDrivingLicence: translate("proofOfName.radios.oldDrivingLicence"),
  photoRegistration: translate("proofOfName.radios.photoRegistration"),
  benefitBook: translate("proofOfName.radios.benefitBook"),
  residencePermit: translate("proofOfName.radios.residencePermit"),
  nationalIdentity: translate("proofOfName.radios.nationalIdentity"),
  backLinkText: translate("buttons:back"),
  continueButtonText: translate("buttons:continue"),
});

const isNavigable = (req) =>
  userIsBackOffice(req) &&
  !!req.session[getFlowDataObjectName(req.session.journeyName)][
    "date-of-birth"
  ] &&
  !!req.session[getFlowDataObjectName(req.session.journeyName)][
    "date-of-birth-day"
  ] &&
  !!req.session[getFlowDataObjectName(req.session.journeyName)][
    "date-of-birth-month"
  ] &&
  !!req.session[getFlowDataObjectName(req.session.journeyName)][
    "date-of-birth-year"
  ];

const behaviourForPost = () => (req, res, next) => {
  const dataObjectName = getFlowDataObjectName(req.session.journeyName);
  if (
    req.body["reference-number-type"] !==
    req.session[dataObjectName]["reference-number-type"]
  ) {
    req.session[dataObjectName]["reference-number"] = undefined;
  }
  req.session[dataObjectName]["reference-number-type"] =
    req.body["reference-number-type"];
  next();
};

const proofOfName = {
  path: "/proof-of-name",
  template: "proof-of-name",
  isNavigable,
  pageContent,
  validate,
  behaviourForPost,
};

export { proofOfName, isNavigable, pageContent, validate, behaviourForPost };
