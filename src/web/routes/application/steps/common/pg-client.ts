import { Pool } from "pg";

import { config } from "../../../../../config";
import { XeroxStatus } from "./types/xerox-statuses";
import { setApplicationType } from "../declaration/declaration";

const pool = new Pool({
  user: config.environment.DB_USERNAME,
  password: config.environment.DB_PASSWORD,
  host: config.environment.DB_URL,
  database: config.environment.DB_NAME,
  port: config.environment.DB_PORT,
  ssl: config.environment.DB_SSL_CA
    ? {
        rejectUnauthorized: true,
        ca: Buffer.from(config.environment.DB_SSL_CA, "base64").toString(
          "ascii"
        ),
      }
    : false,
});

const lookupCertificateReference = async (certificateReference) => {
  const text = `SELECT 
    application.application_id,
    application.loss_year, 
    application.loss_month, 
    application.loss_day, 
    application.loss_placename, 
    application.baby_sex, 
    application.baby_name,
    application.print_date_of_loss,
    parent1.parent_given_name AS parent1_given_name, 
    parent1.parent_family_name AS parent1_family_name, 
    parent1.parent_relationship AS parent1_relationship, 
    parent1.parent_reference_number AS parent1_reference_number, 
    parent1.parent_reference_type AS parent1_reference_type,
    parent1.parent_email_address AS parent1_email,
    parent1.created_by AS parent1_created_by,

    parent2.parent_id AS parent2_id,
    parent2.parent_relationship AS parent2_relationship,
    parent2.parent_date_of_birth AS parent2_date_of_birth
  FROM 
    application
  JOIN 
    parent AS parent1 ON application.applicant_parent_id = parent1.parent_id
  JOIN 
    parent AS parent2 ON application.second_parent_id = parent2.parent_id
  WHERE application.application_status = '${XeroxStatus.INCOMPLETE}' AND application.certificate_reference = $1`;

  const query = {
    text: text,
    values: [certificateReference],
  };

  return await pool.query(query);
};
const lookupApplicationStatus = async (
  certificateReference,
  firstName,
  lastName,
  dateOfBirth,
  postcode
) => {
  const text = `SELECT application.application_id,
            application.certificate_reference,
            application.application_address_line_1,
            application.application_address_line_2,
            application.application_address_line_3,
            application.application_address_line_4,
            application.application_address_line_5,
            application.application_address_postcode,
            application.loss_year,
            application.application_status,
            application.created_dtm,
            parent1.parent_given_name    AS parent1_given_name,
            parent1.parent_family_name   AS parent1_family_name,
            parent1.parent_email_address AS parent1_email_address,
            parent1.parent_date_of_birth AS parent1_date_of_birth
           FROM application
            JOIN
            parent AS parent1 ON application.applicant_parent_id = parent1.parent_id
            WHERE application.certificate_reference = COALESCE($1, application.certificate_reference) AND
                  (SOUNDEX(parent1.parent_given_name) = SOUNDEX(COALESCE($2, parent1.parent_given_name)) OR
                  METAPHONE(parent1.parent_given_name,10) = METAPHONE(COALESCE($2, parent1.parent_given_name),10))
                  AND
                  (SOUNDEX(parent1.parent_family_name) = SOUNDEX(COALESCE($3, parent1.parent_family_name)) OR
                  METAPHONE(parent1.parent_family_name,10) = METAPHONE(COALESCE($3, parent1.parent_family_name),10))
                  AND
                  application.application_address_postcode = COALESCE($4, application.application_address_postcode) AND 
                  parent1.parent_date_of_birth = COALESCE($5, parent1.parent_date_of_birth)
           ORDER BY application.created_dtm DESC LIMIT 50`;

  const query = {
    text: text,
    values: [certificateReference, firstName, lastName, postcode, dateOfBirth],
  };
  const result = await pool.query(query);
  return result.rows;
};

const rejectCertificate = async (req, certificateReference) => {
  const applicationType = setApplicationType(req);
  const text = `UPDATE application
    SET application_status = '${XeroxStatus.REJECTED}',
    updated_dtm = $2,
    updated_by = $3,
    version = version + 1
    WHERE certificate_reference = $1
    AND application_status = '${XeroxStatus.INCOMPLETE}'`;

  const query = {
    text: text,
    values: [certificateReference, new Date(), applicationType],
  };

  const result = await pool.query(query);

  return result.rowCount > 0;
};

export {
  lookupCertificateReference,
  rejectCertificate,
  lookupApplicationStatus,
};
