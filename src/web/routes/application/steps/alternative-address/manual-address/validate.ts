import { check } from "express-validator";

import { translateValidationMessage } from "../../common/translate-validation-message";
import {
  ADDRESS_LINE_MAX_LENGTH,
  ADDRESS_LINE_PATTERN,
  COUNTY_MAX_LENGTH,
  COUNTY_PATTERN,
  TOWN_OR_CITY_MAX_LENGTH,
  TOWN_OR_CITY_PATTERN,
  UK_POSTCODE_PATTERN,
} from "../../../constants";

const checkPostcodeIsValid = (_, { req }) =>
  UK_POSTCODE_PATTERN.test(req.body.sanitisedPostcodeCcs);

const validate = () => [
  check("address-line-1-ccs")
    .not()
    .isEmpty()
    .bail()
    .withMessage(translateValidationMessage("validation:missingAddressLine1"))
    .matches(ADDRESS_LINE_PATTERN)
    .bail()
    .withMessage(translateValidationMessage("validation:patternAddressLine1"))
    .isLength({ max: ADDRESS_LINE_MAX_LENGTH })
    .withMessage(translateValidationMessage("validation:addressLine1TooLong")),

  check("address-line-2-ccs")
    .matches(ADDRESS_LINE_PATTERN)
    .bail()
    .withMessage(translateValidationMessage("validation:patternAddressLine2"))
    .isLength({ max: ADDRESS_LINE_MAX_LENGTH })
    .withMessage(translateValidationMessage("validation:addressLine2TooLong")),

  check("address-line-3-ccs")
    .not()
    .isEmpty()
    .bail()
    .withMessage(
      translateValidationMessage("validation:missingAddressTownOrCity")
    )
    .matches(TOWN_OR_CITY_PATTERN)
    .bail()
    .withMessage(translateValidationMessage("validation:patternTownOrCity"))
    .isLength({ max: TOWN_OR_CITY_MAX_LENGTH })
    .withMessage(translateValidationMessage("validation:townOrCityTooLong")),

  check("address-line-4-ccs")
    .matches(COUNTY_PATTERN)
    .bail()
    .withMessage(translateValidationMessage("validation:patternCounty"))
    .isLength({ max: COUNTY_MAX_LENGTH })
    .withMessage(translateValidationMessage("validation:countyTooLong")),

  check("postcode-ccs")
    .not()
    .isEmpty()
    .bail()
    .withMessage(translateValidationMessage("validation:missingPostcode"))
    .custom(checkPostcodeIsValid)
    .withMessage(translateValidationMessage("validation:invalidPostcode")),
];

export { validate };
