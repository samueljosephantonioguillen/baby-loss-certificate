import {
  livingInEngland,
  notLivingInEngland,
  enterLocation,
  notInEngland,
  yearOfLoss,
  notEligibleYear,
  relationToBaby,
  notAllowedRelationship,
  knowYourNhsNumber,
  whatIsYourNhsNumber,
  yourName,
  dateOfBirth,
  notOldEnough,
  proofOfName,
  enterReferenceNumber,
  yourPostcode,
  checkYourDetails,
  notOldEnoughPds,
  notMatched,
  noFoundAddress,
  noContactDetails,
  getSecurityCode,
  enterSecurityCode,
  confirmYourAddress,
  notCorrectAddress,
  yourPostcodeCcs,
  selectAddress,
  manualAddress,
  addAnotherParent,
  theirEmail,
  theirRelationToBaby,
  theirDateOfBirth,
  includeBabyLossDate,
  includeBabyLossLocation,
  includeBabySex,
  selectBabySex,
  includeBabyName,
  checkYourAnswers,
  declaration,
  requestComplete,
} from "./steps";
import { enterCertReference } from "./steps/enter-cert-reference";
import { spKnowYourNhsNumber } from "./steps/identity-verification/sp-know-your-nhs-number";
import { choosingToAddOtherParent } from "./steps/second-parent/choosing-to-add-other-parent";
import { spDeclaration } from "./steps/sp-declaration";
import { emailUpdate } from "./steps/email-update";
import { spCheckYourAnswers } from "./steps/sp-check-your-answers";
import { spDetailsAreWrong } from "./steps/address/sp-details-are-wrong";
import { spRequestComplete } from "./steps/sp-request-complete";
import { CRM_CONTEXT_PATH, SP_CONTEXT_PATH } from "../static-pages/paths";
import { spDobNotMatched } from "./steps/sp-dob-not-matched";
import { crmSearchDetails } from "./steps/crm-search-details";
import { crmSearchResults } from "./steps/crm-search-results";
import { spCancelApplication } from "./steps/identity-verification/sp-cancel-application";
import { spEnterReferenceNumber } from "./steps/back-office/sp-enter-reference-number";
import { spProofOfName } from "./steps/back-office/sp-proof-of-name";
import { spRelationToBaby } from "./steps/back-office/sp-relation-to-baby/sp-relation-to-baby";
import { spName } from "./steps/back-office/sp-name/sp-name";

const CONTEXT_PATH = process.env.CONTEXT_PATH || undefined;

export const MAINFLOW = {
  name: "MAINFLOW",
  pathPrefix: CONTEXT_PATH,
  steps: [
    livingInEngland,
    notLivingInEngland,
    enterLocation,
    notInEngland,
    yearOfLoss,
    notEligibleYear,
    relationToBaby,
    notAllowedRelationship,
    knowYourNhsNumber,
    whatIsYourNhsNumber,
    yourName,
    dateOfBirth,
    notOldEnough,
    proofOfName,
    enterReferenceNumber,
    yourPostcode,
    checkYourDetails,
    notOldEnoughPds,
    notMatched,
    noFoundAddress,
    noContactDetails,
    getSecurityCode,
    enterSecurityCode,
    confirmYourAddress,
    notCorrectAddress,
    yourPostcodeCcs,
    selectAddress,
    manualAddress,
    addAnotherParent,
    choosingToAddOtherParent,
    spRelationToBaby,
    spName,
    theirEmail,
    theirDateOfBirth,
    theirRelationToBaby,
    spProofOfName,
    spEnterReferenceNumber,
    includeBabyLossDate,
    includeBabyLossLocation,
    includeBabySex,
    selectBabySex,
    includeBabyName,
    emailUpdate,
    checkYourAnswers,
    declaration,
    requestComplete,
  ],
};

export const SECOND_PARENT_FLOW = {
  name: "SECOND_PARENT_FLOW",
  pathPrefix: SP_CONTEXT_PATH,
  steps: [
    enterCertReference,
    spKnowYourNhsNumber,
    whatIsYourNhsNumber,
    yourName,
    dateOfBirth,
    yourPostcode,
    checkYourDetails,
    spDobNotMatched,
    notMatched,
    noContactDetails,
    getSecurityCode,
    enterSecurityCode,
    spCheckYourAnswers,
    spCancelApplication,
    spDetailsAreWrong,
    spDeclaration,
    spRequestComplete,
  ],
};

export const CRM_FLOW = {
  name: "CRM_FLOW",
  pathPrefix: CRM_CONTEXT_PATH,
  steps: [crmSearchDetails, crmSearchResults],
};

export const JOURNEYS = [MAINFLOW, SECOND_PARENT_FLOW, CRM_FLOW];
