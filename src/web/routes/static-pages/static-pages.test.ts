import { renderCookies } from "./cookies";

jest.mock("./cookies", () => ({
  renderCookies: jest.fn(),
}));

import { renderAccessibilityStatement } from "./accessibility-statement";

jest.mock("./accessibility-statement", () => ({
  renderAccessibilityStatement: jest.fn(),
}));

import { renderTermsOfUse } from "./terms-of-use";

jest.mock("./terms-of-use", () => ({
  renderTermsOfUse: jest.fn(),
}));

import { renderPrivacyNotice } from "./privacy-notice";

jest.mock("./privacy-notice", () => ({
  renderPrivacyNotice: jest.fn(),
}));

import { renderContactUs } from "./contact-us";

jest.mock("./contact-us", () => ({
  renderContactUs: jest.fn(),
}));

import { registerStaticPages } from "./static-pages";

test("registerStaticPages should render for expected pages", () => {
  const app = {};

  registerStaticPages(app);

  expect(renderCookies).toBeCalledTimes(1);
  expect(renderCookies).toBeCalledWith(app);
  expect(renderAccessibilityStatement).toBeCalledTimes(1);
  expect(renderAccessibilityStatement).toBeCalledWith(app);
  expect(renderTermsOfUse).toBeCalledTimes(1);
  expect(renderTermsOfUse).toBeCalledWith(app);
  expect(renderPrivacyNotice).toBeCalledTimes(1);
  expect(renderPrivacyNotice).toBeCalledWith(app);
  expect(renderContactUs).toBeCalledTimes(1);
  expect(renderContactUs).toBeCalledWith(app);
});
