import expect from "expect";

import {
  notCorrectAddress,
  pageContent,
  isNavigable,
} from "./cannot-request-wrong-address";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";

test("notCorrectAddress should match expected outcomes", () => {
  const expectedResults = {
    path: "/cannot-request-wrong-address",
    template: "not-eligible",
    pageContent,
    isNavigable,
  };

  expect(notCorrectAddress).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("notCorrectAddress.title"),
    heading: translate("notCorrectAddress.heading"),
    bodyLineOne: translate("notCorrectAddress.body.firstLine"),
    backLinkText: translate("buttons:back"),
  };

  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when confirm-your-address is no", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "confirm-your-address": "no",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when confirm-your-address is not defined", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "confirm-your-address": undefined,
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when confirm-your-address is yes", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "confirm-your-address": "yes",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});
