import {
  compose,
  defaultTo,
  filter,
  head,
  isEmpty,
  join,
  map,
  not,
  pickAll,
  props,
  propOr,
  split,
  tail,
} from "ramda";
import { toTitleCase } from "../../common/formats";
import { OS_PLACES_ADDRESS_KEYS } from "../../../constants";
import { getFlowDataObjectName } from "../../../tools/data-object";

const DELIMITER = ", ";
const isNotEmpty = compose(not, isEmpty);

const behaviourForPost = () => (req, res, next) => {
  const dataObjectName = getFlowDataObjectName(req.session.journeyName);
  if (req.body["selected-address"]) {
    const address = findAddress(
      req.body["selected-address"],
      req.session.postcodeLookupResults
    );

    req.session[dataObjectName] = {
      ...req.session[dataObjectName],
      ...transformAddress(address),
    };
  }

  next();
};

const findAddress = (selectedAddress, postcodeLookupResults) => {
  const address = postcodeLookupResults.find(
    (result) => result.ADDRESS === selectedAddress
  );

  if (!address) {
    throw new Error("cannot find selected address in postcode lookup results");
  }

  return address;
};

const joinAddressFields = (addressKeys) =>
  compose(join(DELIMITER), filter(isNotEmpty), props(addressKeys));

const constructThoroughfare = joinAddressFields([
  "DEPENDENT_THOROUGHFARE_NAME",
  "THOROUGHFARE_NAME",
]);

const constructBuildingName = joinAddressFields([
  "SUB_BUILDING_NAME",
  "BUILDING_NAME",
]);

const constructLocality = joinAddressFields([
  "DOUBLE_DEPENDENT_LOCALITY",
  "DEPENDENT_LOCALITY",
]);

const constructNumberAndStreet = (addressFields) =>
  `${propOr("", "BUILDING_NUMBER", addressFields)} ${constructThoroughfare(
    addressFields
  )}`.trim();

const isNumberFollowedByLetter = (str) => str.match(/^\d+[a-zA-Z]$/);

const convertNumericBuildingName = (addressFields) => {
  if (
    isNumberFollowedByLetter(addressFields.BUILDING_NAME) &&
    isEmpty(addressFields.BUILDING_NUMBER)
  ) {
    return {
      ...addressFields,
      BUILDING_NUMBER: addressFields.BUILDING_NAME,
      BUILDING_NAME: "",
    };
  }
  return addressFields;
};

const constructAddressParts = (addressFields) => [
  addressFields.ORGANISATION_NAME,
  constructBuildingName(addressFields),
  constructNumberAndStreet(addressFields),
  constructLocality(addressFields),
];

const normaliseAddressFields = compose(
  map(defaultTo("")),
  pickAll(OS_PLACES_ADDRESS_KEYS)
);

const splitSinglePartAddress = compose(split(DELIMITER), head);

const parseSinglePartAddress = (addressParts) =>
  addressParts.length === 1
    ? splitSinglePartAddress(addressParts)
    : addressParts;

const joinTail = compose(join(DELIMITER), tail);

const constructTwoPartAddress = (addressParts) => [
  head(addressParts),
  joinTail(addressParts),
];

const getAddressParts = compose(
  filter(isNotEmpty),
  constructAddressParts,
  convertNumericBuildingName,
  normaliseAddressFields
);

const buildAddressLineOneAndTwo = compose(
  constructTwoPartAddress,
  parseSinglePartAddress,
  getAddressParts
);

const transformAddress = (addressFields) => {
  const addressLineOneAndTwo = buildAddressLineOneAndTwo(addressFields);

  return {
    "address-line-1-ccs": toTitleCase(addressLineOneAndTwo[0]),
    "address-line-2-ccs": toTitleCase(addressLineOneAndTwo[1]),
    "address-line-3-ccs": toTitleCase(addressFields.POST_TOWN),
    "postcode-ccs": addressFields.POSTCODE,
    "address-id": addressFields.UDPRN,
  };
};

export { behaviourForPost, findAddress };
