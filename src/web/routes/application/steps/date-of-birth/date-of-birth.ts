import { stateMachine } from "../../flow-control/state-machine";
import {
  IN_DETAILS_REVIEW,
  IN_DETAILS_REVIEW_PROGRESS,
} from "../../flow-control/states";
import { INCREMENT_NEXT_ALLOWED_PATH } from "../../flow-control/state-machine/actions";
import { contextPath } from "../../paths/context-path";
import { CHECK_DETAILS_URL } from "../../paths/paths";
import { sanitise } from "./sanitise";
import { validate } from "./validate";
import { userIsBackOffice } from "../../../azure-routes/auth";
import { getFlowDataObjectName } from "../../tools/data-object";

const pageContent = ({ translate }) => ({
  title: translate("dateOfBirth.title"),
  sectionTitle: translate("section.yourDetails"),
  heading: translate("dateOfBirth.heading"),
  hint: translate("dateOfBirth.hint"),
  day: translate("day"),
  month: translate("month"),
  year: translate("year"),
  backLinkText: translate("buttons:back"),
  continueButtonText: translate("buttons:continue"),
});

const isNavigable = (req) =>
  (req.session[getFlowDataObjectName(req.session.journeyName)][
    "know-your-nhs-number"
  ] === "no" ||
    userIsBackOffice(req)) &&
  !!req.session[getFlowDataObjectName(req.session.journeyName)]["first-name"] &&
  !!req.session[getFlowDataObjectName(req.session.journeyName)]["last-name"];

const behaviourForGet = (config, journey) => (req, res, next) => {
  const dataObjectName = getFlowDataObjectName(req.session.journeyName);
  if (stateMachine.getState(req, journey) === IN_DETAILS_REVIEW_PROGRESS) {
    req.session[dataObjectName]["date-of-birth-day"] =
      req.session.submittedDobDay;
    req.session[dataObjectName]["date-of-birth-month"] =
      req.session.submittedDobMonth;
    req.session[dataObjectName]["date-of-birth-year"] =
      req.session.submittedDobYear;
    req.session[dataObjectName]["date-of-birth"] = req.session.submittedDob;

    stateMachine.setState(IN_DETAILS_REVIEW, req, journey);
    stateMachine.dispatch(INCREMENT_NEXT_ALLOWED_PATH, req, journey);
    res.locals.previous = contextPath(CHECK_DETAILS_URL, journey.name);
  }

  next();
};

const behaviourForPost = (config, journey) => (req, res, next) => {
  const dataObjectName = getFlowDataObjectName(req.session.journeyName);
  if (
    stateMachine.getState(req, journey) === IN_DETAILS_REVIEW &&
    req.session[dataObjectName].olderThanAgeRequirement === false
  ) {
    req.session.submittedDobDay =
      req.session[dataObjectName]["date-of-birth-day"];
    req.session.submittedDobMonth =
      req.session[dataObjectName]["date-of-birth-month"];
    req.session.submittedDobYear =
      req.session[dataObjectName]["date-of-birth-year"];
    req.session.submittedDob = req.session[dataObjectName]["date-of-birth"];

    stateMachine.setState(IN_DETAILS_REVIEW_PROGRESS, req, journey);
  }

  if (
    stateMachine.getState(req, journey) === IN_DETAILS_REVIEW_PROGRESS &&
    req.session[dataObjectName].olderThanAgeRequirement === true
  ) {
    stateMachine.setState(IN_DETAILS_REVIEW, req, journey);
  }

  next();
};

const dateOfBirth = {
  path: "/date-of-birth",
  template: "date-of-birth",
  pageContent,
  isNavigable,
  sanitise,
  validate,
  behaviourForGet,
  behaviourForPost,
};

export {
  dateOfBirth,
  pageContent,
  isNavigable,
  sanitise,
  validate,
  behaviourForGet,
  behaviourForPost,
};
