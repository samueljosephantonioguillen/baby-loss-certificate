import { livingInEngland } from "./address/living-in-england";
import { enterLocation } from "./enter-location";
import { yearOfLoss } from "./year-of-loss";
import { relationToBaby } from "./relation-to-baby";
import { knowYourNhsNumber } from "./identity-verification/know-your-nhs-number";
import { whatIsYourNhsNumber } from "./identity-verification/what-is-your-nhs-number";
import { yourName } from "./your-name";
import { dateOfBirth } from "./date-of-birth";
import { proofOfName } from "./back-office/proof-of-name";
import { enterReferenceNumber } from "./back-office/enter-reference-number";
import { yourPostcode } from "./address/your-postcode";
import { checkYourDetails } from "./check-your-details";
import { getSecurityCode } from "./identity-verification/get-security-code";
import { enterSecurityCode } from "./identity-verification/enter-security-code";
import { confirmYourAddress } from "./address/confirm-your-address";
import { yourPostcodeCcs } from "./alternative-address/your-postcode-ccs/your-postcode-ccs";
import { selectAddress } from "./alternative-address/select-address";
import { manualAddress } from "./alternative-address/manual-address";
import { addAnotherParent } from "./second-parent/add-another-parent";
import { theirEmail } from "./second-parent/their-email";
import { theirDateOfBirth } from "./second-parent/their-date-of-birth";
import { theirRelationToBaby } from "./second-parent/their-relation-to-baby";
import { includeBabyLossDate } from "./baby-details/include-baby-loss-date";
import { includeBabyLossLocation } from "./baby-details/include-baby-loss-location";
import { includeBabySex } from "./baby-details/include-baby-sex";
import { selectBabySex } from "./baby-details/select-baby-sex";
import { includeBabyName } from "./baby-details/include-baby-name";
import { checkYourAnswers } from "./check-your-answers";
import { declaration } from "./declaration";
import { requestComplete } from "./request-complete";
import { notLivingInEngland } from "./address/not-living-in-england";
import { notInEngland } from "./not-in-england";
import { notEligibleYear } from "./not-eligible-year";
import { notAllowedRelationship } from "./not-allowed-relationship";
import { notOldEnough } from "./not-old-enough";
import { notOldEnoughPds } from "./not-old-enough-pds";
import { notMatched } from "./identity-verification/not-matched";
import { notCorrectAddress } from "./address/not-correct-address";
import { noFoundAddress } from "./identity-verification/no-found-address";
import { noContactDetails } from "./identity-verification/no-contact-details";

export {
  livingInEngland,
  notLivingInEngland,
  enterLocation,
  notInEngland,
  yearOfLoss,
  notEligibleYear,
  relationToBaby,
  notAllowedRelationship,
  knowYourNhsNumber,
  whatIsYourNhsNumber,
  yourName,
  dateOfBirth,
  notOldEnough,
  proofOfName,
  enterReferenceNumber,
  yourPostcode,
  checkYourDetails,
  notOldEnoughPds,
  notMatched,
  noFoundAddress,
  noContactDetails,
  getSecurityCode,
  enterSecurityCode,
  confirmYourAddress,
  notCorrectAddress,
  yourPostcodeCcs,
  selectAddress,
  manualAddress,
  addAnotherParent,
  theirEmail,
  theirDateOfBirth,
  theirRelationToBaby,
  includeBabyLossDate,
  includeBabyLossLocation,
  includeBabySex,
  selectBabySex,
  includeBabyName,
  checkYourAnswers,
  declaration,
  requestComplete,
};
