import expect from "expect";

const mockDoubleCsrf = jest.fn();
jest.mock("csrf-csrf", () => ({
  doubleCsrf: mockDoubleCsrf,
}));

import {
  getPathsInSequence,
  registerJourney,
  doubleCsrfOptions,
} from "./register-journeys";
import { REQUEST_COMPLETE_URL } from "./paths/paths";

const steps = [
  {
    path: "/first",
  },
  {
    path: "/second",
    toggle: "STEP_TWO_ENABLED",
  },
  {
    path: "/third",
  },
];

describe("getPathsInSequence", () => {
  test("should return the correct sequence of paths when no prefix is defined", () => {
    const expected = ["/first", "/second", "/third", REQUEST_COMPLETE_URL];
    const result = getPathsInSequence(undefined, steps);

    expect(result).toEqual(expected);
  });

  test("should return the correct sequence of paths when a prefix is defined", () => {
    const prefix = "/my-journey";
    const expected = [
      "/my-journey/first",
      "/my-journey/second",
      "/my-journey/third",
      `/my-journey${REQUEST_COMPLETE_URL}`,
    ];
    const result = getPathsInSequence(prefix, steps);

    expect(result).toEqual(expected);
  });
});

describe("registerJourney", () => {
  test("should register journeys with correct properties when no path prefix is defined", () => {
    const features = {
      STEP_TWO_ENABLED: false,
    };

    const journey = {
      steps,
    };

    const result = registerJourney(features)(journey);

    const expected = {
      steps: [{ path: "/first" }, { path: "/third" }],
      pathsInSequence: ["/first", "/third", REQUEST_COMPLETE_URL],
    };

    expect(result).toEqual(expected);
  });

  test("should register journeys with correct properties when path prefix is defined", () => {
    const features = {
      STEP_TWO_ENABLED: false,
    };

    const pathPrefix = "/my-journey";

    const journey = {
      steps,
      pathPrefix,
    };

    const result = registerJourney(features)(journey);

    const expected = {
      steps: [{ path: "/my-journey/first" }, { path: "/my-journey/third" }],
      pathPrefix: "/my-journey",
      pathsInSequence: [
        "/my-journey/first",
        "/my-journey/third",
        `/my-journey${REQUEST_COMPLETE_URL}`,
      ],
    };

    expect(result).toEqual(expected);
  });
});

describe("doubleCsrfOptions()", () => {
  test("sets options when secure cookies set", () => {
    const config = {
      environment: {
        USE_UNSECURE_COOKIE: false,
      },
    };

    doubleCsrfOptions(config);

    expect(mockDoubleCsrf).toBeCalledWith({
      cookieName: "__Host-nhsbsa.x-csrf-token",
      cookieOptions: { secure: true },
      getSecret: expect.any(Function),
      getTokenFromRequest: expect.any(Function),
    });
  });

  test("sets options when secure cookies not set", () => {
    const config = {
      environment: {
        USE_UNSECURE_COOKIE: true,
      },
    };

    doubleCsrfOptions(config);

    expect(mockDoubleCsrf).toBeCalledWith({
      cookieName: "nhsbsa.x-csrf-token",
      cookieOptions: { secure: false },
      getSecret: expect.any(Function),
      getTokenFromRequest: expect.any(Function),
    });
  });
});
