import expect from "expect";

import {
  choosingToAddOtherParent,
  pageContent,
  isNavigable,
} from "./choosing-to-add-other-parent";
import { BACKOFFICE_ROLE } from "../../../constants";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";
import { config } from "../../../../../../config";

test("choosingToAddOtherParent should match expected outcomes", () => {
  const expectedResults = {
    path: "/what-happens-next",
    template: "choosing-to-add-other-parent",
    pageContent,
    isNavigable,
  };

  expect(choosingToAddOtherParent).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("choosingToAddOtherParent.title"),
    sectionTitle: translate("section.otherParentsDetails"),
    heading: translate("choosingToAddOtherParent.heading"),
    insetText: translate("choosingToAddOtherParent.insetText"),
    body: translate("choosingToAddOtherParent.body"),
    continueButtonText: translate("buttons:continue"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when add-another-parent is yes and is user is not back office", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "add-another-parent": "yes",
        },
        account: {},
      },
    };

    const result = isNavigable(req);
    expect(result).toBe(true);
  });

  test("should return false when add-another-parent is no and is user is not back office", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "add-another-parent": "no",
        },
        account: {},
      },
    };

    const result = isNavigable(req);
    expect(result).toBe(false);
  });

  test("should return false when user is back office", () => {
    config.environment.USE_AUTHENTICATION = true;

    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "add-another-parent": "yes",
        },
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };

    const result = isNavigable(req);
    expect(result).toBe(false);
  });
});
