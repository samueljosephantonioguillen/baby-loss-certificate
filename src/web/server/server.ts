import compression from "compression";
import * as express from "express";
import * as bodyParser from "body-parser";
import * as path from "path";
import cookieParser from "cookie-parser";
import { logger } from "../logger/logger";
import { configureSecurity } from "./configure-security";
import { registerRoutes } from "../routes";
import { initialiseSession } from "./session";
import { registerErrorHandlers } from "./error-handlers";
import { setViewEngine } from "./view-engine";
import { internationalisation } from "./internationalisation";
import { requestID } from "./headers/request-id";
import { v4 } from "uuid";

const configureStaticPaths = (app) => {
  /**
   * 1. Gov UK fonts and images from node_modules/govuk-frontend
   * 2. Gov UK Javascript
   * 3. Gov UK local CSS compiled from SASS
   */
  app.use(
    "/assets",
    express.static(
      path.resolve("node_modules/govuk-frontend/dist/govuk/assets")
    )
  ); /* 1 */
  app.use(
    process.env.ASSET_PATH + "/assets",
    express.static(path.resolve("node_modules/govuk-frontend/dist/govuk"))
  );
  app.use(
    process.env.ASSET_PATH + "/assets",
    express.static(path.resolve("src/web/public"))
  ); /* 3 */
};

const listen = async (config, app) => {
  app.listen(config.server.PORT, () => {
    logger.info(`App listening on port ${config.server.PORT}`);
    logger.info(
      `App applicant parent context path is ${config.server.CONTEXT_PATH}`
    );
    logger.info(
      `App second parent context path is ${config.server.SP_CONTEXT_PATH}`
    );
    logger.info(`App CRM context path is ${config.server.CRM_CONTEXT_PATH}`);
    logger.info(`App asset path path is ${config.server.ASSET_PATH}`);
    logger.info(`App version is ${config.server.APP_VERSION}`);
    logger.info(`Logging level set to ${config.environment.LOG_LEVEL}`);
    logger.info(
      `Authentication enabled is ${config.environment.USE_AUTHENTICATION}`
    );
  });
};

const start = (config, app) => () => {
  app.use(requestID(v4()));
  app.use(compression());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(cookieParser());

  configureSecurity(app);
  setViewEngine(config, app);
  registerRoutes(config, app);
  registerErrorHandlers(app);
  listen(config, app);
};

const initialise = (config, app) => {
  // Configure static paths before registering any middleware to ensure
  // assets are available to all middleware
  configureStaticPaths(app);

  // Apply internationalisation before initialising session to ensure
  // translation function is available to session middleware
  internationalisation(config, app);
  initialiseSession(start(config, app), config, app);
};

export { initialise };
