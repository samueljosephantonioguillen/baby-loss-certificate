import axios from "axios";

const getPdsOauthTokenMock = jest.fn();
jest.mock("./get-pds-oauth-token", () => ({
  getPdsOauthToken: getPdsOauthTokenMock,
}));

import { requestPdsSearch } from "./request-pds-search";
import { MAINFLOW } from "../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../tools/data-object";

jest.mock("axios");

const config = {
  environment: {
    PDS_URI: "https://mock.pds.uri",
  },
};

describe("requestPdsSearch", () => {
  const req = {
    session: {
      journeyName: MAINFLOW.name,
      [MAINFLOW_DATA_OBJECT_NAME]: {
        "first-name": "Jane",
        "last-name": "Smith",
        "date-of-birth": "2010-10-22",
        postcode: "LS1 6AE",
      },
    },
  };
  getPdsOauthTokenMock.mockImplementation(() => {
    return "pds_oauth_token";
  });
  test("should return valid response with all data present", async () => {
    const responseData = {
      data: {
        resourceType: "Bundle",
        entry: [
          {
            resource: {
              resourceType: "Patient",
              id: "9000000009",
              birthDate: "2010-10-22",
              name: [
                {
                  use: "usual",
                  period: {
                    start: "2020-01-01",
                  },
                  given: ["Jane"],
                  family: "Smith",
                },
              ],
              telecom: [
                {
                  system: "phone",
                  use: "mobile",
                  value: "01632960587",
                  period: {
                    start: "2020-01-01",
                  },
                },
                {
                  system: "email",
                  value: "jane.smith@example.com",
                  period: {
                    start: "2020-01-01",
                  },
                },
              ],
              address: [
                {
                  id: "456",
                  period: {
                    start: "2020-01-01",
                  },
                  use: "home",
                  line: [
                    "1 Trevelyan Square",
                    "Boar Lane",
                    "City Centre",
                    "Leeds",
                    "West Yorkshire",
                  ],
                  postalCode: "LS1 6AE",
                },
              ],
              deceasedDateTime: "2023-07-07T09:34:40+00:00",
            },
          },
        ],
      },
    };
    const expectedResult = {
      firstName: "Jane",
      lastName: "Smith",
      email: "jane.smith@example.com",
      phoneNumber: "01632960587",
      addressLines: [
        "1 Trevelyan Square",
        "Boar Lane",
        "City Centre",
        "Leeds",
        "West Yorkshire",
      ],
      postcode: "LS1 6AE",
      hasDeceasedDateTime: true,
      nhsNumber: "9000000009",
      dateOfBirth: "2010-10-22",
    };
    const mock = jest.fn();
    axios.get = mock.mockResolvedValue(responseData);

    const result = await requestPdsSearch(req, config);
    expect(result).toEqual(expectedResult);
  });

  test("should return valid response with missing data", async () => {
    const responseData = {
      data: {
        resourceType: "Bundle",
        entry: [
          {
            resource: {
              resourceType: "Patient",
              id: "9000000009",
              telecom: [
                {
                  system: "phone",
                  use: "mobile",
                  value: "01632960587",
                  period: {
                    start: "2020-01-01",
                    end: "2021-01-01",
                  },
                },
                {
                  system: "email",
                  value: "jane.smith@example.com",
                  period: {
                    start: "2020-01-01",
                    end: "2021-01-01",
                  },
                },
              ],
              name: [
                {
                  use: "usual",
                  period: {
                    start: "2020-01-01",
                    end: "2021-01-01",
                  },
                  given: ["Expired"],
                  family: "Expired",
                },
              ],
              address: [
                {
                  id: "456",
                  period: {
                    start: "2020-01-01",
                    end: "2021-01-01",
                  },
                  use: "home",
                  line: ["Expired"],
                  postalCode: "Expired",
                },
              ],
            },
          },
        ],
      },
    };
    const expectedResult = {
      firstName: null,
      lastName: null,
      email: null,
      phoneNumber: null,
      addressLines: [],
      postcode: null,
      hasDeceasedDateTime: false,
      nhsNumber: "9000000009",
      dateOfBirth: null,
    };
    const mock = jest.fn();
    axios.get = mock.mockResolvedValue(responseData);

    const result = await requestPdsSearch(req, config);
    expect(result).toEqual(expectedResult);
  });

  test("should throw error when PDS returns empty bundle", async () => {
    const emptyBundleError = new TypeError(
      "PDS search (non-NHS number) response returned no results"
    );

    const responseData = {
      resourceType: "Bundle",
      type: "searchset",
      timestamp: "2023-08-01T10:23:38+00:00",
      total: 0,
    };

    const mock = jest.fn();
    axios.get = mock.mockResolvedValue(responseData);

    try {
      await requestPdsSearch(req, config);
    } catch (error) {
      expect(error).toEqual(emptyBundleError);
    }
  });

  test("should throw error when PDS call fails", async () => {
    const pdsError = {
      response: {
        status: 400,
        message: "400 Bad Request",
      },
    };
    const mock = jest.fn();
    axios.get = mock.mockImplementation(() => Promise.reject(pdsError));

    try {
      await requestPdsSearch(req, config);
    } catch (error) {
      expect(error).toEqual(pdsError);
    }
  });

  test("should throw error when PDS call fails", async () => {
    const pdsError = {
      response: {
        status: 400,
        message: "400 Bad Request",
      },
    };
    const mock = jest.fn();
    axios.get = mock.mockImplementation(() => Promise.reject(pdsError));

    try {
      await requestPdsSearch(req, config);
    } catch (error) {
      expect(error).toEqual(pdsError);
    }
  });

  test("should throw error when PDS call returns too many patients issue", async () => {
    const responseData = {
      data: {
        issue: [
          {
            code: "multiple-matches",
            details: {
              coding: [
                {
                  code: "TOO_MANY_MATCHES",
                  display: "Too Many Matches",
                  system:
                    "https://fhir.nhs.uk/R4/CodeSystem/Spine-ErrorOrWarningCode",
                  version: "1",
                },
              ],
            },
            severity: "information",
          },
        ],
        resourceType: "OperationOutcome",
      },
    };
    const pdsError = new TypeError(
      "PDS search (non-NHS number) response returned too many results"
    );
    const mock = jest.fn();
    axios.get = mock.mockResolvedValue(responseData);

    try {
      await requestPdsSearch(req, config);
    } catch (error) {
      expect(error).toEqual(pdsError);
    }
  });

  test("should change PDS response First and Second names to Title Case and postcode to UPPERCASE", async () => {
    const responseData = {
      data: {
        resourceType: "Bundle",
        entry: [
          {
            resource: {
              resourceType: "Patient",
              id: "9000000009",
              birthDate: "2010-10-22",
              name: [
                {
                  use: "usual",
                  period: {
                    start: "2020-01-01",
                  },
                  given: ["jane"],
                  family: "SMITH",
                },
              ],
              telecom: [
                {
                  system: "phone",
                  use: "mobile",
                  value: "01632960587",
                  period: {
                    start: "2020-01-01",
                  },
                },
                {
                  system: "email",
                  value: "jane.smith@example.com",
                  period: {
                    start: "2020-01-01",
                  },
                },
              ],
              address: [
                {
                  id: "456",
                  period: {
                    start: "2020-01-01",
                  },
                  use: "home",
                  line: [
                    "1 Trevelyan Square",
                    "Boar Lane",
                    "City Centre",
                    "Leeds",
                    "West Yorkshire",
                  ],
                  postalCode: "ls1 6ae",
                },
              ],
            },
          },
        ],
      },
    };
    const expectedResult = {
      firstName: "Jane",
      lastName: "Smith",
      email: "jane.smith@example.com",
      phoneNumber: "01632960587",
      addressLines: [
        "1 Trevelyan Square",
        "Boar Lane",
        "City Centre",
        "Leeds",
        "West Yorkshire",
      ],
      postcode: "LS1 6AE",
      hasDeceasedDateTime: false,
      nhsNumber: "9000000009",
      dateOfBirth: "2010-10-22",
    };
    const mock = jest.fn();
    axios.get = mock.mockResolvedValue(responseData);

    const result = await requestPdsSearch(req, config);
    expect(result).toEqual(expectedResult);
  });
});
