import { registerRoutes, setCommonTemplateValues } from "./register-routes";

jest.mock("./azure-routes/auth-routes", () => ({
  registerAuthRoutes: jest.fn(),
}));
jest.mock("../../common/azure-utils", () => ({
  getUserName: jest.fn(),
  isUserAuthenticated: jest.fn().mockReturnValueOnce(false),
  checkAuthenticationSet: jest.fn().mockReturnValueOnce(false),
}));

beforeEach(() => {
  jest.clearAllMocks();
});

const req = {
  originalUrl: "/test-context",
  language: "en",
  t: jest.fn((key) => key),
  session: {
    user: {
      name: "Alice",
    },
  },
};
const res = {
  locals: {},
  next: jest.fn(),
};

describe("setCommonTemplateValues", () => {
  test("should set the expected properties on res.locals", () => {
    setCommonTemplateValues(req, res, res.next);
    expect(res.locals["htmlLang"]).toBe(req.language);
    expect(res.locals["language"]).toBe("en");
    expect(res.locals["back"]).toBe("back");
    expect(res.locals["serviceName"]).toBe("header.serviceName");
    expect(res.locals["footerLinks"]).toBe("footer");
    expect(res.locals["skipLink"]).toBe("header.skipLink");
    expect(res.locals["serviceStart"]).toBe(process.env.START_PAGE_URL);
    expect(res.locals["isSecondParentJourney"]).toBe(false);
    expect(res.locals["isUserBackOffice"]).toBe(false);
    expect(res.locals["hideCookieBanner"]).toBe(false);
  });
});

describe("registerRoutes", () => {
  test("should set templates and register expected routes", () => {
    const config = {
      environment: {
        USE_UNSECURE_COOKIE: true,
      },
    };
    const app = {
      all: jest.fn(),
      use: jest.fn(),
      get: jest.fn(),
      post: jest.fn(),
    };

    registerRoutes(config, app);

    expect(app.use).toBeCalledWith(setCommonTemplateValues);
  });
});
