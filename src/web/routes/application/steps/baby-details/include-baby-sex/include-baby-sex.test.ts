const isEmpty = jest.fn();

jest.mock("express-validator", () => ({
  validationResult: () => ({
    isEmpty: isEmpty,
  }),
}));

import expect from "expect";

import {
  includeBabySex,
  pageContent,
  validate,
  behaviourForGet,
  behaviourForPost,
} from "./include-baby-sex";
import { buildSessionForJourney } from "../../../flow-control/test-utils/test-utils";
import {
  IN_PROGRESS,
  IN_REVIEW,
  IN_REVIEW_PROGRESS,
} from "../../../flow-control/states";
import { CHECK_ANSWERS_URL } from "../../../paths/paths";
import { contextPath } from "../../../paths/context-path";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";

test("includeBabySex should match expected outcomes", () => {
  const expectedResults = {
    path: "/sex-of-baby",
    template: "include-baby-sex",
    pageContent,
    validate,
    behaviourForGet,
    behaviourForPost,
  };

  expect(includeBabySex).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("includeBabySex.title"),
    sectionTitle: translate("section.certificateDetails"),
    heading: translate("includeBabySex.heading"),
    bodyLineOne: translate("includeBabySex.body.firstLine"),
    hintLineOne: translate("includeBabySex.hint.firstLine"),
    yes: translate("yes"),
    no: translate("no"),
    or: translate("or"),
    unknown: translate("includeBabySex.unknown"),
    continueButtonText: translate("buttons:continue"),
    backLinkText: translate("buttons:back"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("behaviourForGet", () => {
  const config = {};
  const journey = { name: MAINFLOW.name };

  test("should call set to IN_REVIEW when in IN_REVIEW_PROGRESS state", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "include-baby-sex": "",
          "baby-sex": "",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_REVIEW_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = { locals: { previous: undefined } };
    const next = jest.fn();

    behaviourForGet(config, journey)(req, res, next);

    expect(req.session.journeys[journey.name].state).toEqual(IN_REVIEW);
    expect(res.locals.previous).toBe(
      contextPath(CHECK_ANSWERS_URL, MAINFLOW.name)
    );
    expect(next).toBeCalledTimes(1);
  });

  test("should set option to no when IN_REVIEW_PROGRESS state and no baby sex was selected", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "include-baby-sex": "yes",
          "baby-sex": "",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_REVIEW_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = { locals: { previous: undefined } };
    const next = jest.fn();

    behaviourForGet(config, journey)(req, res, next);

    const expectedData = {
      "include-baby-sex": "no",
      "baby-sex": "",
    };
    expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(expectedData);
    expect(req.session.journeys[journey.name].state).toEqual(IN_REVIEW);
    expect(res.locals.previous).toBe(
      contextPath(CHECK_ANSWERS_URL, MAINFLOW.name)
    );
    expect(next).toBeCalledTimes(1);
  });

  test("should call next when not in IN_REVIEW_PROGRESS state", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "include-baby-sex": "",
          "baby-sex": "",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = { locals: { previous: undefined } };
    const next = jest.fn();

    behaviourForGet(config, journey)(req, res, next);

    expect(res.locals.previous).toBe(undefined);
    expect(next).toBeCalledTimes(1);
  });
});

describe("behaviourForPost", () => {
  const config = {};
  const journey = { name: MAINFLOW.name };

  test("should return expected data when no option is selected", () => {
    isEmpty.mockReturnValue(true);

    const req = {
      body: { "include-baby-sex": "no" },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "baby-sex": "male",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = {};
    const next = jest.fn();

    const expectedSession = {
      ...req.session,
      [MAINFLOW_DATA_OBJECT_NAME]: {},
    };

    behaviourForPost(config, journey)(req, res, next);
    expect(req.session).toEqual(expectedSession);
    expect(next).toBeCalledTimes(1);
  });

  test("should return expected data when unknown option is selected", () => {
    isEmpty.mockReturnValue(true);

    const req = {
      body: { "include-baby-sex": "unknown" },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "baby-sex": "male",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = {};
    const next = jest.fn();

    const expectedSession = {
      ...req.session,
      [MAINFLOW_DATA_OBJECT_NAME]: {},
    };

    behaviourForPost(config, journey)(req, res, next);
    expect(req.session).toEqual(expectedSession);
    expect(next).toBeCalledTimes(1);
  });

  test("should set state to IN_REVIEW_PROGRESS when current state is IN_REVIEW and yes option is selected", () => {
    isEmpty.mockReturnValue(true);

    const req = {
      body: { "include-baby-sex": "yes" },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "baby-sex": "male",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_REVIEW,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = {};
    const next = jest.fn();

    const expectedSession = {
      ...req.session,
      ...buildSessionForJourney({
        journeyName: journey.name,
        state: IN_REVIEW_PROGRESS,
        nextAllowedPath: undefined,
      }),
    };

    behaviourForPost(config, journey)(req, res, next);
    expect(req.session).toEqual(expectedSession);
    expect(next).toBeCalledTimes(1);
  });

  test("should call next when validation error", () => {
    isEmpty.mockReturnValue(false);

    const req = {};
    const res = {};
    const next = jest.fn();

    behaviourForPost(config, journey)(req, res, next);
    expect(next).toBeCalledTimes(1);
  });
});
