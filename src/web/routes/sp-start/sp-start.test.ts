import expect from "expect";
import express from "express";

import { configureAuthentication } from "../application/flow-control/middleware/configure-authentication";

const mockSessionDestroy = jest.fn();
jest.mock("../application/steps/common/session-destroy", () => ({
  sessionDestroyButKeepLoggedIn: mockSessionDestroy,
}));

import { registerSecondParentStartRoute, SP_START } from "./sp-start";

const router = {
  ...jest.requireActual("express"),
  get: jest.fn(),
};
jest.spyOn(express, "Router").mockImplementationOnce(() => router);

const res = {
  redirect: jest.fn(),
  cookie: jest.fn(),
  clearCookie: jest.fn(),
  status: jest.fn().mockReturnThis(),
  render: jest.fn(),
};
const next = jest.fn();
const app = {
  use: jest.fn(),
};

test("registerSecondParentStartRoute should destroy session and return page content", () => {
  const req = {
    t: (string) => string,
    session: {
      destroy: jest.fn(),
      isAuthenticated: true,
      account: {
        username: "test@mail.com",
      },
    },
  };

  const expectedPageContent = {
    title: "spStart.title",
    heading: "spStart.heading",
    bodyLineOne: "spStart.body.firstLine",
    bodyLineTwo: "spStart.body.secondLine",
    requestACertificateHeading: "spStart.requestACertificate.heading",
    requestACertificateLineOne: "spStart.requestACertificate.firstLine",
    requestACertificateLineTwo: "spStart.requestACertificate.secondLine",
    requestACertificateLineThree: "spStart.requestACertificate.thirdLine",
    requestACertificateLineFour: "spStart.requestACertificate.fourthLine",
    requestACertificateLineFive: "spStart.requestACertificate.fifthLine",
    aboutTheCertificateHeading: "spStart.aboutTheCertificate.heading",
    aboutTheCertificateInsetText: "spStart.aboutTheCertificate.insetText",
    aboutTheCertificateLineOne: "spStart.aboutTheCertificate.firstLine",
    aboutTheCertificateListItemOne: "spStart.aboutTheCertificate.firstListItem",
    aboutTheCertificateListItemTwo:
      "spStart.aboutTheCertificate.secondListItem",
    aboutTheCertificateListItemThree:
      "spStart.aboutTheCertificate.thirdListItem",
    aboutTheCertificateListItemFour:
      "spStart.aboutTheCertificate.fourthListItem",
    aboutTheCertificateListItemFive:
      "spStart.aboutTheCertificate.fifthListItem",
    aboutTheCertificateListItemSix: "spStart.aboutTheCertificate.sixthListItem",
    aboutTheCertificateLineTwo: "spStart.aboutTheCertificate.secondLine",
    aboutTheCertificateLineThree: "spStart.aboutTheCertificate.thirdLine",
    beforeYouStartHeading: "spStart.beforeYouStart.heading",
    beforeYouStartLineOne: "spStart.beforeYouStart.firstLine",
    beforeYouStartListItemOne: "spStart.beforeYouStart.firstListItem",
    beforeYouStartListItemTwo: "spStart.beforeYouStart.secondListItem",
    beforeYouStartListItemThree: "spStart.beforeYouStart.thirdListItem",
    startNow: "spStart.startNow",
    startPath: "/sp-test-context/enter-reference-number",
  };

  router.get.mockImplementation((path, configureAuthentication, callback) => {
    callback(req, res, next);
  });

  registerSecondParentStartRoute(app);

  expect(app.use).toBeCalledTimes(2);
  expect(router.get).toBeCalledTimes(2);
  expect(router.get).toHaveBeenNthCalledWith(
    1,
    "/sp-test-context",
    configureAuthentication,
    expect.anything()
  );
  expect(res.redirect).toBeCalledWith("/sp-test-context" + SP_START);
  expect(router.get).toHaveBeenNthCalledWith(
    2,
    "/sp-test-context" + SP_START,
    configureAuthentication,
    expect.anything()
  );
  expect(mockSessionDestroy).toBeCalled();
  expect(mockSessionDestroy).toBeCalledWith(req, res);
  expect(res.redirect).toBeCalledWith(`/sp-test-context${SP_START}`);
  expect(res.clearCookie).toBeCalledWith("lang");
  expect(res.render).toBeCalledWith("sp-start", expectedPageContent);
});
