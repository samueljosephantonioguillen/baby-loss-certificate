import { CRM_DATA_OBJECT_NAME } from "../../tools/data-object";

const pageContent = ({ translate, req }) => ({
  title: translate("crmSearchResults.title"),
  heading: translate("crmSearchResults.heading"),
  results: req.session[CRM_DATA_OBJECT_NAME]["crm-search-results"],
  searchDetails: req.session[CRM_DATA_OBJECT_NAME]["crm-search-details"],
  backLinkText: translate("crmSearchResults.back"),
  resultsPreamble:
    req.session[CRM_DATA_OBJECT_NAME]["crm-search-results"].length > 0
      ? translate("crmSearchResults.resultsFor")
      : translate("crmSearchResults.noResultsFor"),
  and: translate("and"),
  note: translate("crmSearchResults.note"),
  noteLineOne: translate("crmSearchResults.noteLineOne"),
});

const behaviourForGet = () => (req, res, next) => {
  res.locals.useFullScreen = true;
  return next();
};

const crmSearchResults = {
  path: "/search-results",
  template: "crm-search-results",
  pageContent,
  behaviourForGet,
};

export { crmSearchResults, pageContent, behaviourForGet };
