const CONTEXT_PATH = process.env.CONTEXT_PATH ?? "";
import { sessionDestroyButKeepLoggedIn } from "../application/steps/common/session-destroy";
import { injectAdditionalHeaders } from "../application/flow-control/middleware/inject-headers";
import { getPageNotFoundContent } from "./error-page-content";

const getPageNotFoundErrorPage = (req, res) => {
  sessionDestroyButKeepLoggedIn(req, res);
  injectAdditionalHeaders(req, res);

  res
    .status(404)
    .render("error-pages/page-not-found", getPageNotFoundContent(req));
};

const registerPageNotFoundRoute = (app) => {
  app.use((req, res, next) => {
    res.set("Clear-Site-Data", '"storage"');
    next();
  });
  app.get(`${CONTEXT_PATH}/page-not-found`, getPageNotFoundErrorPage);
  app.all("*", (req, res) => {
    res.status(404).redirect(`${CONTEXT_PATH}/page-not-found`);
  });
};

export { registerPageNotFoundRoute, getPageNotFoundErrorPage };
