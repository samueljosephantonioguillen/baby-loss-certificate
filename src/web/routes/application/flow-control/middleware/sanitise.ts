import { map, trim, compose } from "ramda";
import escapeHtml from "escape-html";

const sanitiseInput = compose(escapeHtml, trim);
const sanitiseBody = map(sanitiseInput);

const sanitise = (req, res, next) => {
  req.body = sanitiseBody(req.body);
  next();
};

export { sanitise, sanitiseBody };
