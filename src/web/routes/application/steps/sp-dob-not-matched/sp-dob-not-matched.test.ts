import expect from "expect";

import {
  spDobNotMatched,
  pageContent,
  isNavigable,
} from "./sp-dob-not-matched";
import { MAINFLOW, SECOND_PARENT_FLOW } from "../../journey-definitions";
import {
  MAINFLOW_DATA_OBJECT_NAME,
  SECOND_PARENT_DATA_OBJECT_NAME,
} from "../../tools/data-object";

test("notAllowedRelationship should match expected outcomes", () => {
  const expectedResults = {
    path: "/cannot-request-certificate-date-of-birth-not-matched",
    template: "not-eligible",
    pageContent,
    isNavigable,
  };

  expect(spDobNotMatched).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("spDobNotMatched.title"),
    heading: translate("spDobNotMatched.heading"),
    bodyLineOne: translate("spDobNotMatched.body.firstLine"),
    supportOrganisationsHeading: translate("supportOrganisations.heading"),
    supportOrganisationsBody: translate("supportOrganisations.body"),
    supportOrganisationsList: translate("supportOrganisations.listItems"),
    backLinkText: translate("buttons:back"),
  };

  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("return true when spDobNotMatched is true", () => {
    const req = {
      session: {
        journeyName: SECOND_PARENT_FLOW.name,
        [SECOND_PARENT_DATA_OBJECT_NAME]: {
          spDobNotMatched: true,
        },
      },
    };
    const result = isNavigable(req);
    expect(result).toEqual(true);
  });

  test("return false when spDobNotMatched is false", () => {
    const req = {
      session: {
        journeyName: SECOND_PARENT_FLOW.name,
        [SECOND_PARENT_DATA_OBJECT_NAME]: {
          spDobNotMatched: false,
        },
      },
    };
    const result = isNavigable(req);
    expect(result).toEqual(false);
  });

  test("return false when main applicant journey", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          spDobNotMatched: true,
        },
      },
    };
    const result = isNavigable(req);
    expect(result).toEqual(false);
  });
});
