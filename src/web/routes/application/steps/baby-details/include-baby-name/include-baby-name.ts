import { validationResult } from "express-validator";
import { validate } from "./validate";
import { sanitise } from "./sanitise";
import { getFlowDataObjectName } from "../../../tools/data-object";

const pageContent = ({ translate }) => ({
  title: translate("includeBabyName.title"),
  sectionTitle: translate("section.certificateDetails"),
  heading: translate("includeBabyName.heading"),
  bodyLineOne: translate("includeBabyName.body.firstLine"),
  bodyLineTwo: translate("includeBabyName.body.secondLine"),
  hintLineOne: translate("includeBabyName.hint.firstLine"),
  yes: translate("yes"),
  no: translate("no"),
  label: translate("includeBabyName.label"),
  continueButtonText: translate("buttons:continue"),
  backLinkText: translate("buttons:back"),
});

const behaviourForPost = () => (req, res, next) => {
  if (!validationResult(req).isEmpty()) {
    return next();
  }

  if (req.body["include-baby-name"] === "no") {
    delete req.body["baby-name"];
    delete req.session[getFlowDataObjectName(req.session.journeyName)][
      "baby-name"
    ];
  }

  next();
};

const includeBabyName = {
  path: "/name-of-baby",
  template: "include-baby-name",
  pageContent,
  sanitise,
  validate,
  behaviourForPost,
};

export { includeBabyName, pageContent, sanitise, validate, behaviourForPost };
