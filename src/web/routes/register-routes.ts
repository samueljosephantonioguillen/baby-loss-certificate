import { getLanguageBase } from "./language";
import { START, registerStartRoute } from "./start";
import { SP_START, registerSecondParentStartRoute } from "./sp-start";
import { registerJourneys } from "./application/register-journeys";
import { registerStaticPages } from "./static-pages";
import { registerTryAgain } from "./error-pages/try-again";
import { registerTooManyAttempts } from "./error-pages/too-many-attempts";
import { registerTooManyOTPSends } from "./error-pages/too-many-otp-sends";
import { registerProblemWithServiceError } from "./error-pages/problem-with-service";
import { registerSessionEndedError } from "./error-pages/session-ended";
import { registerPageNotFoundRoute } from "./error-pages/page-not-found";
import {
  JOURNEYS,
  SECOND_PARENT_FLOW,
} from "./application/journey-definitions";
import { registerAuthRoutes } from "./azure-routes/auth-routes";
import { SIGN_OUT_URL } from "./application/constants";
import { getJourneyFromUrl } from "./application/tools/journey-from-url";
import {
  firstParentContextPath,
  secondParentContextPath,
} from "./application/paths/context-path";
import { CRM_FIRST_STEP } from "./application/register-journey-routes";
import { CERTIFICATE_MANAGER_HOME } from "./static-pages/paths";
import { registerCertificateManagerHomeRoute } from "./static-pages/certificate-manager-home";
import {
  checkAuthenticationSet,
  isUserAuthenticated,
} from "../../common/azure-utils";
import { isUsingAuthentication, userIsBackOffice } from "./azure-routes/auth";
const CONTEXT_PATH = process.env.CONTEXT_PATH ?? "";
const START_PAGE_URL = process.env.START_PAGE_URL ?? "";

const setCommonTemplateValues = (req, res, next) => {
  res.locals.htmlLang = req.language;
  res.locals.language = getLanguageBase(req.language);
  res.locals.back = req.t("back");
  res.locals.serviceName = req.t("header.serviceName");
  res.locals.footerLinks = req.t("footer");
  res.locals.skipLink = req.t("header.skipLink");
  res.locals.showServiceHeader =
    isUsingAuthentication() && isUserAuthenticated(req);
  res.locals.isUserBackOffice = userIsBackOffice(req);
  res.locals.signoutUrl = CONTEXT_PATH + SIGN_OUT_URL;

  res.locals.serviceStart = setServiceStart(req);
  res.locals.applicationStart = setApplicationStartURL(req);

  res.locals.firstParentStartUrl = setFirstParentStartUrl();
  res.locals.secondParentServiceStart = secondParentContextPath(SP_START);
  res.locals.crmServiceStart = CRM_FIRST_STEP;
  res.locals.certificateManagerHome = firstParentContextPath(
    CERTIFICATE_MANAGER_HOME
  );

  res.locals.isSecondParentJourney =
    getJourneyFromUrl(req.originalUrl) === SECOND_PARENT_FLOW.name;

  res.locals.hideCookieBanner = !!req.cookies?.cookieConsent;

  next();
};
const setServiceStart = (req) => {
  if (checkAuthenticationSet()) {
    return firstParentContextPath(CERTIFICATE_MANAGER_HOME);
  } else if (getJourneyFromUrl(req.originalUrl) === SECOND_PARENT_FLOW.name) {
    return secondParentContextPath(SP_START);
  } else {
    return START_PAGE_URL;
  }
};

const setFirstParentStartUrl = () => {
  if (checkAuthenticationSet()) {
    return firstParentContextPath(START);
  } else {
    return START_PAGE_URL;
  }
};

const setApplicationStartURL = (req) => {
  if (checkAuthenticationSet()) {
    return firstParentContextPath(CERTIFICATE_MANAGER_HOME);
  } else if (getJourneyFromUrl(req.originalUrl) === SECOND_PARENT_FLOW.name) {
    return secondParentContextPath(SP_START);
  } else {
    return START_PAGE_URL;
  }
};

const registerRoutes = (config, app) => {
  app.use(setCommonTemplateValues);
  registerAuthRoutes(app);
  registerJourneys(JOURNEYS)(config, app);
  registerStartRoute(app);
  registerSecondParentStartRoute(app);
  registerCertificateManagerHomeRoute(app);
  registerStaticPages(app);
  registerTryAgain(app);
  registerTooManyAttempts(app);
  registerTooManyOTPSends(app);
  registerProblemWithServiceError(app);
  registerSessionEndedError(app);
  // Page not found route should always be registered last as it is a catch all route
  registerPageNotFoundRoute(app);
};

export { registerRoutes, setCommonTemplateValues };
