import expect from "expect";

import {
  crmSearchDetails,
  pageContent,
  sanitise,
  validate,
  isNavigable,
  behaviourForPost,
  pushFieldIfPresent,
} from "./crm-search-details";
import { CRM_FLOW } from "../../journey-definitions";
import { CRM_DATA_OBJECT_NAME } from "../../tools/data-object";

const res = {
  locals: {
    errors: {},
    errorTitleText: {},
  },
  redirect: jest.fn(),
  cookie: jest.fn(),
  clearCookie: jest.fn(),
  status: jest.fn().mockReturnThis(),
  render: jest.fn(),
};
const next = jest.fn();
test("crmSearchDetails should match expected outcomes", () => {
  const expectedResults = {
    path: "/search-details",
    template: "crm-search-details",
    pageContent,
    sanitise,
    validate,
    isNavigable,
    behaviourForPost,
  };

  expect(crmSearchDetails).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("crmSearchDetails.title"),
    heading: translate("crmSearchDetails.heading"),
    certificateNumber: translate("crmSearchDetails.certificateNumber"),
    firstName: translate("crmSearchDetails.firstName"),
    lastName: translate("crmSearchDetails.lastName"),
    postcode: translate("crmSearchDetails.postcode"),
    dob: translate("crmSearchDetails.dob"),
    day: translate("day"),
    month: translate("month"),
    year: translate("year"),
    searchButton: translate("crmSearchDetails.searchButton"),
    hintDOB: translate("dateOfBirth.hint"),
    resetFormButton: translate("crmSearchDetails.resetFormButton"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

test("behaviourForPost  should redirect to crm", () => {
  const req = {
    t: (string) => string,
    session: {
      journeyName: CRM_FLOW.name,
      [CRM_DATA_OBJECT_NAME]: {
        test: "test",
      },
    },
    body: {
      "is-cleared": "true",
    },
  };

  behaviourForPost()(req, res, next);
  expect(req.session[CRM_DATA_OBJECT_NAME]).toEqual(undefined);
  expect(res.redirect).toHaveBeenCalledWith("/crm-test-context/search-details");
});
test("behaviourForPost  shouldn't redirect to crm", () => {
  const req = {
    t: (string) => string,
    session: {
      journeyName: CRM_FLOW.name,
      [CRM_DATA_OBJECT_NAME]: {
        test: "test",
      },
    },
    body: {
      "is-cleared": "false",
    },
  };
  const expectedData = {
    test: "test",
  };

  behaviourForPost()(req, res, next);
  expect(req.session[CRM_DATA_OBJECT_NAME]).toEqual(expectedData);
});

describe("pushFieldIfPresent function", () => {
  let printableFields;
  let searchFieldsUsed;

  beforeEach(() => {
    printableFields = [];
    searchFieldsUsed = [];
  });

  it("should add field and value to searchFieldsUsed and printableFields if field has value", () => {
    const fieldName = "testField";
    const fieldValue = "testValue";
    const field = true;

    pushFieldIfPresent(
      field,
      fieldName,
      fieldValue,
      printableFields,
      searchFieldsUsed
    );

    expect(searchFieldsUsed).toEqual([fieldName]);
    expect(printableFields).toEqual([fieldValue]);
  });

  it("should not add anything if field has no value", () => {
    const fieldName = "testField";
    const fieldValue = "testValue";
    const field = false;

    pushFieldIfPresent(
      field,
      fieldName,
      fieldValue,
      printableFields,
      searchFieldsUsed
    );

    expect(searchFieldsUsed).toEqual([]);
    expect(printableFields).toEqual([]);
  });
});
