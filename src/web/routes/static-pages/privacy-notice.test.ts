import expect from "expect";

import { renderPrivacyNotice, pageContent } from "./privacy-notice";
import { PRIVACY_NOTICE } from "./paths";

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const req = {
    session: {
      hideServiceLink: true,
    },
  };

  const expected = {
    title: translate("privacyNotice.title"),
    firstHeading: translate("privacyNotice.firstHeading"),
    firstBody: translate("privacyNotice.firstBody"),
    secondHeading: translate("privacyNotice.secondHeading"),
    secondBody: translate("privacyNotice.secondBody"),
    thirdHeading: translate("privacyNotice.thirdHeading"),
    thirdBody: translate("privacyNotice.thirdBody"),
    fourthHeading: translate("privacyNotice.fourthHeading"),
    fourthBody: translate("privacyNotice.fourthBody"),
    fifthHeading: translate("privacyNotice.fifthHeading"),
    fifthBody: translate("privacyNotice.fifthBody"),
    sixthHeading: translate("privacyNotice.sixthHeading"),
    sixthBody: translate("privacyNotice.sixthBody"),
    hideServiceLink: req.session.hideServiceLink,
  };
  const result = pageContent({ translate, req });

  expect(result).toEqual(expected);
});

test("renderPrivacyNotice should render page with expected content", () => {
  const app = {
    get: jest.fn(),
  };

  const req = {
    t: (string) => string,
    session: {
      hideServiceLink: true,
    },
  };

  const res = {
    render: jest.fn(),
  };

  app.get.mockImplementation((path, callback) => {
    callback(req, res);
  });

  renderPrivacyNotice(app);

  expect(app.get).toBeCalledTimes(1);
  expect(app.get).toBeCalledWith(
    process.env.CONTEXT_PATH + PRIVACY_NOTICE,
    expect.anything()
  );
  expect(res.render).toBeCalled();
  expect(res.render).toBeCalledWith(
    "static-pages/privacy-notice",
    pageContent({ translate: req.t, req })
  );
});
