import { userIsBackOffice } from "../../../../azure-routes/auth";
import { getFlowDataObjectName } from "../../../tools/data-object";
import { sanitise } from "./sanitise";
import { validate } from "./validate";

const pageContent = ({ translate }) => ({
  title: translate("theirName.title"),
  sectionTitle: translate("section.certificateDetails"),
  heading: translate("theirName.heading"),
  hint: translate("theirName.hint"),
  firstName: translate("yourName.firstName"),
  lastName: translate("yourName.lastName"),
  backLinkText: translate("buttons:back"),
  continueButtonText: translate("buttons:continue"),
});

const isNavigable = (req) =>
  !!req.session[getFlowDataObjectName(req.session.journeyName)][
    "their-relation-to-baby"
  ] && userIsBackOffice(req);

const spName = {
  path: "/other-parent-name",
  template: "their-name",
  pageContent,
  isNavigable,
  sanitise,
  validate,
};

export { spName, pageContent, isNavigable, sanitise, validate };
