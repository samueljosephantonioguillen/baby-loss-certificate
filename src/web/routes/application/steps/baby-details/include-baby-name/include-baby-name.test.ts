const isEmpty = jest.fn();

jest.mock("express-validator", () => ({
  validationResult: () => ({
    isEmpty: isEmpty,
  }),
}));

import expect from "expect";

import {
  includeBabyName,
  pageContent,
  validate,
  behaviourForPost,
  sanitise,
} from "./include-baby-name";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";

test("includeBabyName should match expected outcomes", () => {
  const expectedResults = {
    path: "/name-of-baby",
    template: "include-baby-name",
    pageContent,
    sanitise,
    validate,
    behaviourForPost,
  };

  expect(includeBabyName).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("includeBabyName.title"),
    sectionTitle: translate("section.certificateDetails"),
    heading: translate("includeBabyName.heading"),
    bodyLineOne: translate("includeBabyName.body.firstLine"),
    bodyLineTwo: translate("includeBabyName.body.secondLine"),
    hintLineOne: translate("includeBabyName.hint.firstLine"),
    yes: translate("yes"),
    no: translate("no"),
    label: translate("includeBabyName.label"),
    continueButtonText: translate("buttons:continue"),
    backLinkText: translate("buttons:back"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("behaviourForPost", () => {
  test("should return expected data when no option is selected", () => {
    isEmpty.mockReturnValue(true);

    const req = {
      body: { "include-baby-name": "no", "baby-name": "John Smith" },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "baby-name": "John Smith",
        },
      },
    };
    const res = {};
    const next = jest.fn();

    const expectedReq = {
      body: { "include-baby-name": "no" },
      session: { [MAINFLOW_DATA_OBJECT_NAME]: {}, journeyName: MAINFLOW.name },
    };

    behaviourForPost()(req, res, next);
    expect(req).toEqual(expectedReq);
    expect(next).toBeCalledTimes(1);
  });

  test("should call when validation error", () => {
    isEmpty.mockReturnValue(false);

    const req = {};
    const res = {};
    const next = jest.fn();

    behaviourForPost()(req, res, next);
    expect(next).toBeCalledTimes(1);
  });
});
