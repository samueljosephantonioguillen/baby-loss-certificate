import {
  firstParentContextPath,
  secondParentContextPath,
} from "../application/paths/context-path";

const PATH = "/cannot-request-try-again";

const journeyName = "MAINFLOW";

const pageContent = ({ translate }) => ({
  title: translate("errors:tryAgain.title"),
  heading: translate("errors:tryAgain.heading"),
  body: translate("errors:tryAgain.body"),
  tryAgain: translate("errors:tryAgain.button"),
  backLinkText: translate("buttons:back"),
});

const get = (req, res) => {
  res.render("try-again", pageContent({ translate: req.t }));
};

const post = (req, res) => {
  res.redirect(req.session.journeys[journeyName].currentStep);
};

const registerTryAgain = (app) => {
  app.get([firstParentContextPath(PATH), secondParentContextPath(PATH)], get);
  app.post([firstParentContextPath(PATH), secondParentContextPath(PATH)], post);
};

export { registerTryAgain, pageContent, get, post, PATH };
