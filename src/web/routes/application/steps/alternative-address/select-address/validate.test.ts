import expect from "expect";

import { validate } from "./validate";
import { applyExpressValidation } from "../../common/test/apply-express-validation";

test("returns no validation errors when selected-address field is valid", async () => {
  const req = {
    body: {
      "selected-address": "13 Lorem Ipsum Road, London, W1 0AA",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  expect(result.array().length).toBe(0);
});

test("returns validation errors when selected-address field is empty", async () => {
  const req = {
    body: {
      "selected-address": "",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "",
    msg: "validation:selectAddress",
    path: "selected-address",
    location: "body",
  };

  expect(result.array().length).toBe(1);
  expect(error).toEqual(expectedError);
});
