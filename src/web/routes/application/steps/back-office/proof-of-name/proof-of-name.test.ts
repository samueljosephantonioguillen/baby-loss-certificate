import expect from "expect";

import {
  proofOfName,
  isNavigable,
  pageContent,
  validate,
  behaviourForPost,
} from "./proof-of-name";
import { AGENT_ROLE, BACKOFFICE_ROLE } from "../../../constants";
import { config } from "../../../../../../config";
import { environment } from "../../../../../../config/environment";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";

test("proofOfName should match expected outcomes", () => {
  const expectedResults = {
    path: "/proof-of-name",
    template: "proof-of-name",
    isNavigable,
    pageContent,
    validate,
    behaviourForPost,
  };

  expect(proofOfName).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("proofOfName.title"),
    sectionTitle: translate("section.yourDetails"),
    heading: translate("proofOfName.heading"),
    help: translate("proofOfName.help"),
    passport: translate("proofOfName.radios.passport"),
    birthCertificate: translate("proofOfName.radios.birthCertificate"),
    eeaMemberCard: translate("proofOfName.radios.eeaMemberCard"),
    drivingLicence: translate("proofOfName.radios.drivingLicence"),
    oldDrivingLicence: translate("proofOfName.radios.oldDrivingLicence"),
    photoRegistration: translate("proofOfName.radios.photoRegistration"),
    benefitBook: translate("proofOfName.radios.benefitBook"),
    residencePermit: translate("proofOfName.radios.residencePermit"),
    nationalIdentity: translate("proofOfName.radios.nationalIdentity"),
    backLinkText: translate("buttons:back"),
    continueButtonText: translate("buttons:continue"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when backoffice_role and dob is populated", () => {
    config.environment.USE_AUTHENTICATION = true;

    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "date-of-birth": "1/1/1999",
          "date-of-birth-day": "1",
          "date-of-birth-month": "1",
          "date-of-birth-year": "1999",
        },
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when backoffice_role and dob is not populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {},
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when agent_role and dob is populated", () => {
    environment.USE_AUTHENTICATION = true;
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "date-of-birth": "1/1/1999",
          "date-of-birth-day": "1",
          "date-of-birth-month": "1",
          "date-of-birth-year": "1999",
        },
        account: {
          idTokenClaims: {
            roles: [AGENT_ROLE],
          },
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when no role and dob is populated", () => {
    environment.USE_AUTHENTICATION = true;

    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "date-of-birth": "1/1/1999",
          "date-of-birth-day": "1",
          "date-of-birth-month": "1",
          "date-of-birth-year": "1999",
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});

describe("behaviourForPost", () => {
  test("should set reference-number-type to session", () => {
    const req = {
      body: {
        "reference-number-type": "test",
      },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {},
      },
    };
    const res = {};
    const next = jest.fn();

    behaviourForPost()(req, res, next);

    expect(
      req.session[MAINFLOW_DATA_OBJECT_NAME]["reference-number-type"]
    ).toEqual("test");
  });

  test("should set reference-number to undefined when reference-number-type is different", () => {
    const req = {
      body: {
        "reference-number-type": "foo",
      },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "reference-number": "num",
          "reference-number-type": "bar",
        },
      },
    };
    const res = {};
    const next = jest.fn();

    behaviourForPost()(req, res, next);

    expect(req.session[MAINFLOW_DATA_OBJECT_NAME]["reference-number"]).toEqual(
      undefined
    );
  });

  test("should not set reference-number to undefined when reference-number-type is same", () => {
    const req = {
      body: {
        "reference-number-type": "foo",
      },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "reference-number": "num",
          "reference-number-type": "foo",
        },
      },
    };
    const res = {};
    const next = jest.fn();

    behaviourForPost()(req, res, next);

    expect(req.session[MAINFLOW_DATA_OBJECT_NAME]["reference-number"]).toEqual(
      "num"
    );
  });
});
