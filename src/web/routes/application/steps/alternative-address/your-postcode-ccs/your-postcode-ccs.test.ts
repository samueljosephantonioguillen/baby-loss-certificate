const addressResults = jest.fn();
const isEmpty = jest.fn();

jest.mock("express-validator", () => ({
  validationResult: () => ({
    isEmpty: isEmpty,
  }),
}));

jest.mock("./os-places", () => ({
  ...jest.requireActual("./os-places"),
  getAddressLookupResults: addressResults,
}));

import expect from "expect";

import { wrapError } from "../../../errors";
import { logger } from "../../../../../logger/logger";
import * as TEST_FIXTURES from "./test-fixtures.json";
import * as TEST_FIXTURES_NO_RESULTS from "./test-fixtures-no-results.json";
import { buildSessionForJourney } from "../../../flow-control/test-utils/test-utils";
import { IN_PROGRESS } from "../../../flow-control/states";
import {
  yourPostcodeCcs,
  pageContent,
  isNavigable,
  sanitise,
  validate,
  behaviourForGet,
  behaviourForPost,
} from "./your-postcode-ccs";
import { AGENT_ROLE, BACKOFFICE_ROLE } from "../../../constants";
import { environment } from "../../../../../../config/environment";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";

const config = {
  environment: {
    OS_PLACES_URI: "https://api.os.uk",
    OS_PLACES_API_KEY: "123",
    USE_AUTHENTICATION: false,
  },
};

const expectedNoAddressFoundError = {
  errorTitleText: "validation:errorTitleText",
  errors: [{ msg: "yourPostcode.noAddressFoundError", path: "postcode-ccs" }],
};
const expectedAddressSearchError = {
  errorTitleText: "validation:errorTitleText",
  errors: [{ msg: "yourPostcode.addressSearchError", path: "postcode-ccs" }],
};
const errorSpy = jest.spyOn(logger, "error");

beforeEach(() => {
  errorSpy.mockReset();
});

test("yourPostcodeCcs should match expected outcomes", () => {
  const expectedResults = {
    path: "/lookup-postcode",
    template: "your-postcode-ccs",
    pageContent,
    isNavigable,
    sanitise,
    validate,
    behaviourForGet,
    behaviourForPost,
  };

  expect(yourPostcodeCcs).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("altPostcode.title"),
    sectionTitle: translate("section.yourDetails"),
    heading: translate("altPostcode.heading"),
    body: translate("altPostcode.body"),
    postcode: translate("altPostcode.postcode"),
    enterAddressManually: translate("altPostcode.enterAddressManually"),
    backLinkText: translate("buttons:back"),
    continueButtonText: translate("buttons:continue"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when agent_role and confirm-your-address is alt", () => {
    environment.USE_AUTHENTICATION = true;

    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "confirm-your-address": "alt",
        },
        account: {
          idTokenClaims: {
            roles: [AGENT_ROLE],
          },
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when agent_role and confirm-your-address is no", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "confirm-your-address": "no",
        },
        account: {
          idTokenClaims: {
            roles: [AGENT_ROLE],
          },
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when agent_role and confirm-your-address is yes", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "confirm-your-address": "yes",
        },
        account: {
          idTokenClaims: {
            roles: [AGENT_ROLE],
          },
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when not agent_role and confirm-your-address is alt", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "confirm-your-address": "alt",
        },
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when no ROLE and confirm-your-address is alt", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "confirm-your-address": "alt",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return true when backoffice_role and reference-number is populated", () => {
    environment.USE_AUTHENTICATION = true;

    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "reference-number": "123456",
        },
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when backoffice_role and reference-number is not populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {},
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when agent_role and reference-number is populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "reference-number": "123456",
        },
        account: {
          idTokenClaims: {
            roles: [AGENT_ROLE],
          },
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when no and reference-number is populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "reference-number": "123456",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});

test("behaviourForGet should reset postcodeLookupError", () => {
  const journey = { name: "apply" };

  const req = {
    session: {
      journeyName: MAINFLOW.name,
      postcodeLookupError: true,
      ...buildSessionForJourney({
        journeyName: journey.name,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    },
  };

  const res = {};
  const next = jest.fn();

  behaviourForGet(config, journey)(req, res, next);

  expect(next).toBeCalled();
  expect(req.session.postcodeLookupError).toBe(undefined);
});

describe("behaviourForPost", () => {
  test("should return next when validationResult is not empty", async () => {
    const config = {};
    const req = {};
    const res = {};
    const next = jest.fn();
    isEmpty.mockReturnValue(false);
    addressResults.mockImplementation(() =>
      Promise.resolve({ data: TEST_FIXTURES })
    );

    await behaviourForPost(config)(req, res, next);

    expect(next).toBeCalledTimes(1);
  });

  test("should handle successful address lookup", async () => {
    isEmpty.mockReturnValue(true);

    const req = {
      body: { "postcode-ccs": "BS7 8EE" },
      session: {
        journeyName: MAINFLOW.name,
        id: "skdjfhs-sdfnks-sdfhbsd",
        postcodeLookupResults: undefined,
        postcodeLookupError: undefined,
      },
    };
    const res = {};
    const next = jest.fn();

    const expectedAddresses = [
      {
        ADDRESS: "Alan Jeffery Engineering, 1, Valley Road, Plymouth",
        ORGANISATION_NAME: "ALAN JEFFERY ENGINEERING",
        BUILDING_NUMBER: "1",
        DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
        THOROUGHFARE_NAME: "VALLEY ROAD",
        POST_TOWN: "PLYMOUTH",
        POSTCODE: "PL7 1RF",
        LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
        UDPRN: "50265368",
      },
      {
        ADDRESS: "Dulux Decorator Centre, 2, Valley Road, Plymouth",
        ORGANISATION_NAME: "DULUX DECORATOR CENTRE",
        BUILDING_NUMBER: "2",
        DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
        THOROUGHFARE_NAME: "VALLEY ROAD",
        POST_TOWN: "PLYMOUTH",
        POSTCODE: "PL7 1RF",
        LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
        UDPRN: "19000955",
      },
      {
        ADDRESS: "Mill Autos, 3, Valley Road, Plymouth",
        ORGANISATION_NAME: "MILL AUTOS",
        BUILDING_NUMBER: "3",
        DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
        THOROUGHFARE_NAME: "VALLEY ROAD",
        POST_TOWN: "PLYMOUTH",
        POSTCODE: "PL7 1RF",
        LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
        UDPRN: "19000927",
      },
      {
        ADDRESS:
          "Goat Hill Farm, 2, Troll Bridge, Goat Hill, Slaithwaite, Slaith, Huddersfield",
        ORGANISATION_NAME: "GOAT HILL FARM",
        BUILDING_NUMBER: "2",
        DEPENDENT_THOROUGHFARE_NAME: "TROLL BRIDGE",
        THOROUGHFARE_NAME: "GOAT HILL",
        DOUBLE_DEPENDENT_LOCALITY: "SLAITHWAITE",
        DEPENDENT_LOCALITY: "SLAITH",
        POST_TOWN: "HUDDERSFIELD",
        POSTCODE: "HD7 5UZ",
        LOCAL_CUSTODIAN_CODE_DESCRIPTION: "KIRKLEES",
        UDPRN: "10668197",
      },
      {
        ADDRESS: "10a, Mayfield Avenue, Weston-Super-Mare",
        BUILDING_NAME: "10A",
        THOROUGHFARE_NAME: "MAYFIELD AVENUE",
        POST_TOWN: "WESTON-SUPER-MARE",
        POSTCODE: "BS22 6AA",
        LOCAL_CUSTODIAN_CODE_DESCRIPTION: "NORTH SOMERSET",
        UDPRN: "2916679",
      },
    ];

    await behaviourForPost(config)(req, res, next);

    expect(req.session.postcodeLookupResults).toEqual(expectedAddresses);
    expect(req.session.postcodeLookupError).toBe(undefined);
    expect(next).toBeCalled();
  });

  test("should handle when no addresses found", async () => {
    isEmpty.mockReturnValue(true);
    addressResults.mockImplementation(() =>
      Promise.resolve({ data: TEST_FIXTURES_NO_RESULTS })
    );

    const req = {
      t: (string) => string,
      body: { "postcode-ccs": "BS7 8EE" },
      session: {
        journeyName: MAINFLOW.name,
        id: "skdjfhs-sdfnks-sdfhbsd",
        postcodeLookupResults: undefined,
        postcodeLookupError: undefined,
      },
    };
    const res = { locals: {} };
    const next = jest.fn();

    const expectedAddresses = [];

    await behaviourForPost(config)(req, res, next);

    expect(req.session.postcodeLookupResults).toEqual(expectedAddresses);
    expect(req.session.postcodeLookupError).toBe(undefined);
    expect(res.locals).toEqual(expectedNoAddressFoundError);
    expect(next).toBeCalled();
  });

  test("should handle 400 response from OS places API", async () => {
    const expectedErrorMessage =
      "Error looking up address for postcode-ccs: Error: 400 Error. mocking Error.osPlaces()";
    const cause = { toString: () => "mocking Error.osPlaces()" };
    const osPlacesError = wrapError({
      cause: cause,
      message: "400 Error",
      statusCode: 400,
    });
    isEmpty.mockReturnValue(true);
    addressResults.mockImplementation(() => Promise.reject(osPlacesError));

    const req = {
      t: (string) => string,
      body: { "postcode-ccs": "BS14TM" },
      session: {
        journeyName: MAINFLOW.name,
        id: "skdjfhs-sdfnks-sdfhbsd",
        postcodeLookupResults: undefined,
        postcodeLookupError: undefined,
      },
    };
    const res = { locals: {} };
    const next = jest.fn();

    await behaviourForPost(config)(req, res, next);

    expect(req.session.postcodeLookupResults).toEqual([]);
    expect(req.session.postcodeLookupError).toBe(true);
    expect(next).toBeCalled();
    expect(errorSpy).toBeCalledWith(expectedErrorMessage, req);
    expect(res.locals).toEqual(expectedAddressSearchError);
  });

  test("should handle address lookup error", async () => {
    isEmpty.mockReturnValue(true);
    addressResults.mockImplementation(() => Promise.reject(new Error("error")));

    const req = {
      t: (string) => string,
      body: { "postcode-ccs": "BS7 8EE" },
      session: {
        journeyName: MAINFLOW.name,
        id: "skdjfhs-sdfnks-sdfhbsd",
        postcodeLookupResults: undefined,
        postcodeLookupError: undefined,
      },
    };
    const res = { locals: {} };
    const next = jest.fn();

    await behaviourForPost(config)(req, res, next);

    expect(req.session.postcodeLookupResults).toEqual([]);
    expect(req.session.postcodeLookupError).toBe(true);
    expect(next).toBeCalled();
    expect(errorSpy).toBeCalledWith(
      "Error looking up address for postcode-ccs: Error: error",
      req
    );
    expect(res.locals).toEqual(expectedAddressSearchError);
  });
});
