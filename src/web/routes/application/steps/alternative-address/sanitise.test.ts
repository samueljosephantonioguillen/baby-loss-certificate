import expect from "expect";

import { sanitise } from "./sanitise";

const next = jest.fn();

describe("postcode", () => {
  test("sanitise replaces multiple whitespace with one, converts to uppercase and saves to a new variable", () => {
    const req = {
      body: {
        "postcode-ccs": "bs1     4tb",
      },
    };
    const expectedsanitisedPostcode = "BS1 4TB";
    const expectedPostcode = "bs1     4tb";

    sanitise()(req, {}, next);
    expect(req.body["sanitisedPostcodeCcs"]).toEqual(expectedsanitisedPostcode);
    expect(req.body["postcode-ccs"]).toEqual(expectedPostcode);
    expect(next).toBeCalled();
  });

  test("sanitise adds a single space before the last 3 ending characters, and saves to a new variable for postcode CB30QB", () => {
    const req = {
      body: {
        "postcode-ccs": "cb3  0qb",
      },
    };
    const expectedsanitisedPostcode = "CB3 0QB";
    const expectedPostcode = "cb3  0qb";

    sanitise()(req, {}, next);

    expect(req.body["sanitisedPostcodeCcs"]).toEqual(expectedsanitisedPostcode);
    expect(req.body["postcode-ccs"]).toEqual(expectedPostcode);
    expect(next).toBeCalled();
  });

  test("sanitise adds a single space before the last 3 ending characters, and saves to a new variable for postcode N12NL", () => {
    const req = {
      body: {
        "postcode-ccs": "n1  2nl",
      },
    };
    const expectedsanitisedPostcode = "N1 2NL";
    const expectedPostcode = "n1  2nl";

    sanitise()(req, {}, next);

    expect(req.body["sanitisedPostcodeCcs"]).toEqual(expectedsanitisedPostcode);
    expect(req.body["postcode-ccs"]).toEqual(expectedPostcode);
    expect(next).toBeCalled();
  });

  test("sanitise adds a single space before the last 3 ending characters, and saves to a new variable for postcode CB249LQ", () => {
    const req = {
      body: {
        "postcode-ccs": "cb24  9lq",
      },
    };
    const expectedsanitisedPostcode = "CB24 9LQ";
    const expectedPostcode = "cb24  9lq";

    sanitise()(req, {}, next);

    expect(req.body["sanitisedPostcodeCcs"]).toEqual(expectedsanitisedPostcode);
    expect(req.body["postcode-ccs"]).toEqual(expectedPostcode);
    expect(next).toBeCalled();
  });

  test("sanitise adds a single space before the last 3 ending characters, and saves to a new variable for postcode OX145FB", () => {
    const req = {
      body: {
        "postcode-ccs": "ox14  5fb",
      },
    };
    const expectedsanitisedPostcode = "OX14 5FB";
    const expectedPostcode = "ox14  5fb";

    sanitise()(req, {}, next);

    expect(req.body["sanitisedPostcodeCcs"]).toEqual(expectedsanitisedPostcode);
    expect(req.body["postcode-ccs"]).toEqual(expectedPostcode);
    expect(next).toBeCalled();
  });

  test("sanitise adds a single space before the last 3 ending characters, and saves to a new variable for postcode OX145FC", () => {
    const req = {
      body: {
        "postcode-ccs": "ox145fc",
      },
    };
    const expectedsanitisedPostcode = "OX14 5FC";
    const expectedPostcode = "ox145fc";

    sanitise()(req, {}, next);

    expect(req.body["sanitisedPostcodeCcs"]).toEqual(expectedsanitisedPostcode);
    expect(req.body["postcode-ccs"]).toEqual(expectedPostcode);
    expect(next).toBeCalled();
  });
});

test("sanitise should trim starting and ending spaces for address fields", () => {
  const req = {
    body: {
      "address-line-1-ccs": "        Alan Jeffery Engineering     ",
      "address-line-2-ccs": "  1 Valley Road     ",
      "address-line-3-ccs": "    Plymouth    ",
      "address-line-4-ccs": "    Devon    ",
      "postcode-ccs": "PL7 1RF",
    },
  };

  const expectedResult = {
    body: {
      "address-line-1-ccs": "Alan Jeffery Engineering",
      "address-line-2-ccs": "1 Valley Road",
      "address-line-3-ccs": "Plymouth",
      "address-line-4-ccs": "Devon",
      "postcode-ccs": "PL7 1RF",
      sanitisedPostcodeCcs: "PL7 1RF",
    },
  };

  sanitise()(req, {}, next);
  expect(req).toEqual(expectedResult);
  expect(next).toBeCalled();
});
