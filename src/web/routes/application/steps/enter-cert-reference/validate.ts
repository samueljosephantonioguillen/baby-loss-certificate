import { check } from "express-validator";

import { translateValidationMessage } from "../common/translate-validation-message";
import {
  CERT_REFERENCE_PATTERN,
  MAX_CERT_REFERENCE_LENGTH,
} from "../../constants";

const validateMaxLength = (ref, { req }) => {
  if (ref.length > MAX_CERT_REFERENCE_LENGTH) {
    throw new Error(
      req.t("validation:referenceNumberTooLong", {
        maxLength: MAX_CERT_REFERENCE_LENGTH,
      })
    );
  }

  return true;
};

const validate = () => [
  check("sp-reference-number")
    .not()
    .isEmpty()
    .bail()
    .withMessage(
      translateValidationMessage("validation:missingReferenceNumber")
    )
    .custom(validateMaxLength)
    .bail()
    .matches(CERT_REFERENCE_PATTERN)
    .bail()
    .withMessage(
      translateValidationMessage("validation:patternReferenceNumber")
    ),
];

export { validate };
