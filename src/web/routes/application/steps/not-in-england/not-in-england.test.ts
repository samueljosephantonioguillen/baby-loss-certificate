import expect from "expect";

import { notInEngland, pageContent, isNavigable } from "./not-in-england";
import { MAINFLOW } from "../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../tools/data-object";

test("notInEngland should match expected outcomes", () => {
  const expectedResults = {
    path: "/cannot-request-not-in-england",
    template: "not-eligible",
    pageContent,
    isNavigable,
  };

  expect(notInEngland).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("notInEngland.title"),
    heading: translate("notInEngland.heading"),
    bodyLineOne: translate("notInEngland.body.firstLine"),
    bodyLineTwo: translate("notInEngland.body.secondLine"),
    supportOrganisationsHeading: translate("supportOrganisations.heading"),
    supportOrganisationsBody: translate("supportOrganisations.body"),
    supportOrganisationsList: translate("supportOrganisations.listItems"),
    backLinkText: translate("buttons:back"),
  };

  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when enter-location is no", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "enter-location": "no",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when enter-location is yes", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "enter-location": "yes",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});
