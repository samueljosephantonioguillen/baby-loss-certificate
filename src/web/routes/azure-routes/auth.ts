import * as msal from "@azure/msal-node";
import { environment } from "../../../config/environment";
import { AGENT_ROLE, BACKOFFICE_ROLE } from "../application/constants";

const msalInstance = new msal.ConfidentialClientApplication(
  environment.MSALCONFIG
);
const cryptoProvider = new msal.CryptoProvider();

declare module "express-session" {
  interface SessionData {
    csrfToken: string;
    accessToken: string;
    authCodeRequest;
    idToken: string;
    account;
    isAuthenticated: boolean;
    pkceCodes;
  }
}

function isUsingAuthentication() {
  return environment.USE_AUTHENTICATION === true;
}

function userIsAgent(req) {
  return (
    isUsingAuthentication() &&
    (req?.session?.account?.idTokenClaims?.roles?.includes(AGENT_ROLE) ?? false)
  );
}

function userIsBackOffice(req) {
  return (
    isUsingAuthentication() &&
    (req?.session?.account?.idTokenClaims?.roles?.includes(BACKOFFICE_ROLE) ??
      false)
  );
}

async function redirectToAuthCodeUrl(
  req,
  res,
  next,
  authCodeUrlRequestParams,
  authCodeRequestParams
) {
  const { verifier, challenge } = await cryptoProvider.generatePkceCodes();

  req.session.pkceCodes = {
    challengeMethod: "S256",
    verifier: verifier,
    challenge: challenge,
  };

  req.session.authCodeUrlRequest = {
    redirectUri: environment.REDIRECT_URI,
    responseMode: "form_post",
    codeChallenge: req.session.pkceCodes.challenge,
    codeChallengeMethod: req.session.pkceCodes.challengeMethod,
    ...authCodeUrlRequestParams,
  };

  req.session.authCodeRequest = {
    redirectUri: environment.REDIRECT_URI,
    code: "",
    ...authCodeRequestParams,
  };

  try {
    const authCodeUrlResponse = await msalInstance.getAuthCodeUrl(
      req.session.authCodeUrlRequest
    );
    res.redirect(authCodeUrlResponse);
  } catch (error) {
    next(error);
  }
}

export {
  redirectToAuthCodeUrl,
  userIsAgent,
  userIsBackOffice,
  isUsingAuthentication,
};
