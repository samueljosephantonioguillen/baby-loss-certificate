import expect from "expect";

import {
  notLivingInEngland,
  pageContent,
  isNavigable,
} from "./not-living-in-england";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";

test("notLivingInEngland should match expected outcomes", () => {
  const expectedResults = {
    path: "/cannot-request-not-living-in-england",
    template: "not-eligible",
    pageContent,
    isNavigable,
  };

  expect(notLivingInEngland).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("notLivingInEngland.title"),
    heading: translate("notLivingInEngland.heading"),
    bodyLineOne: translate("notLivingInEngland.body.firstLine"),
    bodyLineTwo: translate("notLivingInEngland.body.secondLine"),
    supportOrganisationsBody: translate("supportOrganisations.body"),
    supportOrganisationsHeading: translate("supportOrganisations.heading"),
    supportOrganisationsList: translate("supportOrganisations.listItems"),
    backLinkText: translate("buttons:back"),
  };

  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when living-in-england is no", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "living-in-england": "no",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return true when living-in-england is not defined", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "living-in-england": undefined,
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when living-in-england is yes", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "living-in-england": "yes",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});
