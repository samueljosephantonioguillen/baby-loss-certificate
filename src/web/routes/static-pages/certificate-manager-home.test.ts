import expect from "expect";
import express from "express";

import { configureAuthentication } from "../application/flow-control/middleware/configure-authentication";

const mockSessionDestroy = jest.fn();
jest.mock("../application/steps/common/session-destroy", () => ({
  sessionDestroyButKeepLoggedIn: mockSessionDestroy,
}));

import { environment } from "../../../config/environment";
import { CERTIFICATE_MANAGER_HOME } from "./paths";
import {
  pageContent,
  registerCertificateManagerHomeRoute,
} from "./certificate-manager-home";

const router = {
  ...jest.requireActual("express"),
  get: jest.fn(),
};
jest.spyOn(express, "Router").mockImplementationOnce(() => router);
jest.mock("../../../common/azure-utils", () => {
  return {
    checkAuthenticationSet: jest.fn().mockReturnValueOnce(true),
    isUserAuthenticated: jest.fn().mockReturnValueOnce(true),
  };
});

const res = {
  redirect: jest.fn(),
  cookie: jest.fn(),
  clearCookie: jest.fn(),
  status: jest.fn().mockReturnThis(),
  render: jest.fn(),
};
const next = jest.fn();
const app = {
  use: jest.fn(),
};

const expectedPageContent = {
  heading: "certificateManagerHome.heading",
  ccsOptions: "certificateManagerHome.ccsOptions",
  backOfficeFirstServiceTitle:
    "certificateManagerHome.backOfficeOptions.firstOption.serviceTitle",
  backOfficeSecondServiceTitle:
    "certificateManagerHome.backOfficeOptions.secondOption.serviceTitle",
  ccsFirstServiceTitle:
    "certificateManagerHome.ccsOptions.firstOption.serviceTitle",
  ccsSecondServiceTitle:
    "certificateManagerHome.ccsOptions.secondOption.serviceTitle",
  ccsThirdServiceTitle:
    "certificateManagerHome.ccsOptions.thirdOption.serviceTitle",
};

test("registerStartRoute should destroy session and return page content", () => {
  const req = {
    t: (string) => string,
    session: {
      destroy: jest.fn(),
      isAuthenticated: true,
      account: {
        username: "test@mail.com",
      },
    },
  };

  router.get.mockImplementation((path, configureAuthentication, callback) => {
    callback(req, res, next);
  });

  registerCertificateManagerHomeRoute(app);

  expect(app.use).toBeCalledTimes(2);
  expect(router.get).toBeCalledTimes(2);
  expect(router.get).toHaveBeenNthCalledWith(
    1,
    "/test-context",
    configureAuthentication,
    expect.anything()
  );
  expect(router.get).toHaveBeenNthCalledWith(
    2,
    "/test-context" + CERTIFICATE_MANAGER_HOME,
    configureAuthentication,
    expect.anything()
  );
  expect(mockSessionDestroy).toBeCalled();
  expect(mockSessionDestroy).toBeCalledWith(req, res);
  expect(res.redirect).toBeCalledWith(
    `/test-context${CERTIFICATE_MANAGER_HOME}`
  );
  expect(res.clearCookie).toBeCalledWith("lang");
  expect(res.render).toBeCalledWith(
    "static-pages/certificate-manager-home",
    expectedPageContent
  );
});

test("pageContent should return return certificate home content", () => {
  environment.USE_AUTHENTICATION = true;
  const result = pageContent({ translate: (string) => string });

  expect(result).toEqual(expectedPageContent);
});
