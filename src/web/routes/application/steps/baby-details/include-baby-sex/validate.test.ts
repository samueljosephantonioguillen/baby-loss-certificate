import expect from "expect";

import { validate } from "./validate";
import { applyExpressValidation } from "../../common/test/apply-express-validation";

test("return no validation errors when include-baby-sex field is yes", async () => {
  const req = {
    body: {
      "include-baby-sex": "yes",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("return no validation errors when include-baby-sex field is no", async () => {
  const req = {
    body: {
      "include-baby-sex": "no",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("return no validation errors when include-baby-sex field is unknown", async () => {
  const req = {
    body: {
      "include-baby-sex": "unknown",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("return expected validation error when baby-sex field is empty", async () => {
  const req = {
    body: {
      "include-baby-sex": "",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "",
    msg: "validation:selectIncludeBabySexOption",
    path: "include-baby-sex",
    location: "body",
  };

  expect(error).toEqual(expectedError);
});
