import expect from "expect";

const sendMock = jest.fn();
const getSecretValueCommandMock = jest.fn();
jest.mock("@aws-sdk/client-secrets-manager", () => {
  const actualModule = jest.requireActual("@aws-sdk/client-secrets-manager");
  return {
    ...actualModule,
    SecretsManagerClient: function SecretsManagerClient() {
      this.send = sendMock;
    },
    GetSecretValueCommand: getSecretValueCommandMock,
  };
});

import { getPdsOauthToken } from "./get-pds-oauth-token";

describe("getPdsOauthToken", () => {
  test("should return oauth token", async () => {
    const req = {};

    sendMock.mockResolvedValue({
      SecretString: '{"oauth_token":"0001"}',
    });

    const oauth = await getPdsOauthToken(req);

    expect(oauth).not.toBeUndefined();
  });

  test("should throw error when secret retrieval fails", async () => {
    const req = {};
    const oauthError = {
      response: {
        status: 400,
        message: "400 Bad Request",
      },
    };

    sendMock.mockRejectedValue({ oauthError });

    try {
      await getPdsOauthToken(req);
    } catch (error) {
      expect(error.oauthError).toEqual(oauthError);
    }
  });
});
