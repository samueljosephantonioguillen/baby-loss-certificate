import expect from "expect";

import { renderTermsOfUse, pageContent } from "./terms-of-use";
import { TERMS_OF_USE } from "./paths";

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const req = {
    session: {
      hideServiceLink: true,
    },
  };

  const expected = {
    title: translate("termsOfUse.title"),
    firstHeading: translate("termsOfUse.firstHeading"),
    firstBody: translate("termsOfUse.firstBody"),
    secondHeading: translate("termsOfUse.secondHeading"),
    secondBody: translate("termsOfUse.secondBody"),
    thirdHeading: translate("termsOfUse.thirdHeading"),
    thirdBody: translate("termsOfUse.thirdBody"),
    fourthHeading: translate("termsOfUse.fourthHeading"),
    fourthBody: translate("termsOfUse.fourthBody"),
    fifthHeading: translate("termsOfUse.fifthHeading"),
    fifthBody: translate("termsOfUse.fifthBody"),
    sixthHeading: translate("termsOfUse.sixthHeading"),
    sixthBody: translate("termsOfUse.sixthBody"),
    hideServiceLink: req.session.hideServiceLink,
  };
  const result = pageContent({ translate, req });

  expect(result).toEqual(expected);
});

test("renderTermsOfUse should render page with expected content", () => {
  const app = {
    get: jest.fn(),
  };

  const req = {
    t: (string) => string,
    session: {
      hideServiceLink: true,
    },
  };

  const res = {
    render: jest.fn(),
  };

  app.get.mockImplementation((path, callback) => {
    callback(req, res);
  });

  renderTermsOfUse(app);

  expect(app.get).toBeCalledTimes(1);
  expect(app.get).toBeCalledWith(
    process.env.CONTEXT_PATH + TERMS_OF_USE,
    expect.anything()
  );

  expect(res.render).toBeCalled();
  expect(res.render).toBeCalledWith(
    "static-pages/terms-of-use",
    pageContent({ translate: req.t, req })
  );
});
