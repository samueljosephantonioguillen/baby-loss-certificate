import { SP_CONTEXT_PATH } from "../../static-pages/paths";
import { CRM_FLOW, MAINFLOW, SECOND_PARENT_FLOW } from "../journey-definitions";

export const getJourneyFromUrl = (url) => {
  const path = "/" + url.split("/")[1];
  switch (path) {
    case SP_CONTEXT_PATH:
      return SECOND_PARENT_FLOW.name;
    case CRM_FLOW.pathPrefix:
      return CRM_FLOW.name;
    default:
      return MAINFLOW.name;
  }
};
