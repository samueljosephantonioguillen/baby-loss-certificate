import { isNilOrEmpty } from "./predicates";

const getUserName = (session) => {
  if (session?.account?.username) {
    return session.account.username.toLowerCase();
  }
  return undefined;
};

const getUserCypher = (session) => {
  if (session?.account?.username) {
    const cypher = session.account.username.split("@")[0];
    return cypher.toUpperCase();
  }
  throw new Error("Unable to derive cypher from username");
};

const isUserAuthenticated = (req) => {
  if (req?.session?.isAuthenticated) {
    return true;
  }
  return false;
};

const checkAuthenticationSet = () => {
  if (
    isNilOrEmpty(process.env.USE_AUTHENTICATION) ||
    process.env.USE_AUTHENTICATION === "false"
  ) {
    return false;
  }
  return true;
};

export {
  getUserName,
  isUserAuthenticated,
  getUserCypher,
  checkAuthenticationSet,
};
