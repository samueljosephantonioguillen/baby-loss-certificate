import * as states from "../states";
import { JOURNEYS_KEY, STEP_DATA_KEY, LOCATOR, APPLICATION_ID } from "../keys";
import { v4 } from "uuid";
import { START } from "../../../start";
import {
  blankContextPath,
  contextPath,
  secondParentContextPath,
} from "../../paths/context-path";
import { getFlowDataObjectName } from "../../tools/data-object";
import { SP_START } from "../../../sp-start";
import { CRM_FLOW, MAINFLOW } from "../../journey-definitions";
import { CRM_FIRST_STEP } from "../../register-journey-routes";
import { logger } from "../../../../logger/logger";
import { sessionDestroyButKeepLoggedIn } from "../../steps/common/session-destroy";

const initialiseProp = (prop, obj, value = {}) => {
  if (!obj.hasOwnProperty(prop)) {
    obj[prop] = value;
  }

  return obj;
};

const defaultJourneyState = (journey) => ({
  nextAllowedStep: journey.pathsInSequence[0],
  state: states.IN_PROGRESS,
});

const configureSessionDetails = (journey) => (req, res, next) => {
  // check the journey
  // if mismatch then clear session
  if (!req.session.journeyName) req.session.journeyName = journey.name;

  res.locals.journeyName = req.session.journeyName;

  if (journey.name !== req.session.journeyName) {
    // redirect to start
    logger.debug("journey mismatch");
    if (journey.name === CRM_FLOW.name) {
      req.session.journeyName = CRM_FLOW.name;
      return res.redirect(CRM_FIRST_STEP);
    }
    sessionDestroyButKeepLoggedIn(req, res);
    res.clearCookie("lang");
    return res.redirect(contextPath(START, journey.name));
  }
  res.locals[getFlowDataObjectName(journey.name)] =
    req.session[getFlowDataObjectName(journey.name)];
  res.locals.dataObjectName = getFlowDataObjectName(journey.name);
  res.locals.contextPath = blankContextPath(journey.name);

  res.locals.serviceStart =
    journey.name == MAINFLOW.name ? START : secondParentContextPath(SP_START);

  const id = v4();
  req.session = initialiseProp(JOURNEYS_KEY, req.session);
  req.session = initialiseProp(STEP_DATA_KEY, req.session);
  req.session = initialiseProp(LOCATOR, req.session, id);
  if (journey.name === MAINFLOW.name) {
    req.session = initialiseProp(APPLICATION_ID, req.session, id);
  }
  req.session.journeys = initialiseProp(
    journey.name,
    req.session.journeys,
    defaultJourneyState(journey)
  );
  res.locals.locator = req.session.locator;
  next();
};

export { configureSessionDetails };
