import expect from "expect";

const isEmpty = jest.fn();

const sendUnsuccessfulApplicationEmail = jest.fn();
jest.mock("../../common/notify-client", () => ({
  sendUnsuccessfulApplicationEmail: sendUnsuccessfulApplicationEmail,
}));

const rejectCertificate = jest.fn();
jest.mock("../../common/pg-client", () => ({
  rejectCertificate: rejectCertificate,
}));

jest.mock("express-validator", () => ({
  validationResult: () => ({
    isEmpty: isEmpty,
  }),
}));

jest.mock("../../../../../logger/logger", () => ({
  logger: {
    info: jest.fn(),
    warn: jest.fn(),
  },
}));

import {
  pageContent,
  isNavigable,
  validate,
  behaviourForPost,
  spCancelApplication,
} from "./sp-cancel-application";

import { SECOND_PARENT_FLOW } from "../../../journey-definitions";
import { contextPath } from "../../../paths/context-path";
import { spCheckYourAnswers } from "../../sp-check-your-answers";
import { SECOND_PARENT_DATA_OBJECT_NAME } from "../../../tools/data-object";
import { AGENT_ROLE } from "../../../constants";

const req = {
  body: {
    "cancel-application": "yes",
  },
  t: (string) => string,
  session: {
    journeyName: SECOND_PARENT_FLOW.name,
    [SECOND_PARENT_DATA_OBJECT_NAME]: {
      "sp-sanitised-reference-number": "9999999999",
      "are-details-correct": "no",
      "fp-email": "test@test.com",
      "fp-first-name": "test",
      "fp-last-name": "name",
    },
  },
};

const res = {
  locals: {
    errors: {},
    errorTitleText: {},
  },
  redirect: jest.fn(),
};

test("cancelApplication should match expected outcomes", () => {
  const expectedResults = {
    path: "/confirm-certificate-cancellation",
    template: "sp-cancel-application",
    pageContent,
    isNavigable,
    validate,
    behaviourForPost,
  };

  expect(spCancelApplication).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("spCancelApplication.title"),
    heading: translate("spCancelApplication.heading"),
    bodyLineOne: translate("spCancelApplication.body.firstLine"),
    yes: translate("yes"),
    no: translate("no"),
    backLinkText: translate("buttons:back"),
    continueButtonText: translate("buttons:continue"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when are-details-correct is no", () => {
    req.session[SECOND_PARENT_DATA_OBJECT_NAME]["are-details-correct"] = "no";

    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false and continue when are-details-correct is not populated", () => {
    req.session[SECOND_PARENT_DATA_OBJECT_NAME]["are-details-correct"] = "yes";

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});

describe("behaviourForPost", () => {
  test("should redirect to check your answers page if cancel-application is no", async () => {
    req.body["cancel-application"] = "no";
    isEmpty.mockReturnValue(true);

    const next = jest.fn();

    await behaviourForPost()(req, res, next);
    expect(res.redirect).toBeCalledWith(
      contextPath(spCheckYourAnswers.path, SECOND_PARENT_FLOW.name)
    );
  });

  test("should update database and send email then call next", async () => {
    req.body["cancel-application"] = "yes";
    isEmpty.mockReturnValue(true);
    rejectCertificate.mockReturnValueOnce(true);
    sendUnsuccessfulApplicationEmail.mockReturnValueOnce({});

    const next = jest.fn();
    await behaviourForPost()(req, res, next);
    expect(rejectCertificate).toBeCalled();
    expect(sendUnsuccessfulApplicationEmail).toBeCalled();
    expect(res.redirect).not.toBeCalled();
    expect(next).toBeCalled();
  });

  test("should update database and send email then call next (user is agent)", async () => {
    const req = {
      body: {
        "cancel-application": "yes",
      },
      t: (string) => string,
      session: {
        account: {
          idTokenClaims: {
            roles: [AGENT_ROLE],
          },
        },
        journeyName: SECOND_PARENT_FLOW.name,
        [SECOND_PARENT_DATA_OBJECT_NAME]: {
          "sp-sanitised-reference-number": "9999999999",
          "are-details-correct": "no",
          "fp-email": "test@test.com",
          "fp-first-name": "test",
          "fp-last-name": "name",
        },
      },
    };

    isEmpty.mockReturnValue(true);
    rejectCertificate.mockReturnValueOnce(true);
    sendUnsuccessfulApplicationEmail.mockReturnValueOnce({});

    const next = jest.fn();
    await behaviourForPost()(req, res, next);
    expect(rejectCertificate).toBeCalled();
    expect(sendUnsuccessfulApplicationEmail).toBeCalled();
    expect(res.redirect).not.toBeCalled();
    expect(next).toBeCalled();
  });

  test("should fail to update database and not send email then call next", async () => {
    req.body["cancel-application"] = "";
    isEmpty.mockReturnValue(false);

    const next = jest.fn();

    await behaviourForPost()(req, res, next);
    expect(rejectCertificate).not.toBeCalled();
    expect(sendUnsuccessfulApplicationEmail).not.toBeCalled();
    expect(res.redirect).not.toBeCalled();
    expect(next).toBeCalledTimes(1);
  });

  test("should update database and not send email then call next", async () => {
    const req = {
      body: {
        "cancel-application": "yes",
      },
      t: (string) => string,
      session: {
        journeyName: SECOND_PARENT_FLOW.name,
        [SECOND_PARENT_DATA_OBJECT_NAME]: {
          "sp-sanitised-reference-number": "9999999999",
          "are-details-correct": "no",
        },
      },
    };

    isEmpty.mockReturnValue(true);
    rejectCertificate.mockReturnValueOnce(false);

    const next = jest.fn();

    await behaviourForPost()(req, res, next);
    expect(rejectCertificate).toBeCalled();
    expect(sendUnsuccessfulApplicationEmail).not.toBeCalled();
    expect(res.redirect).not.toBeCalled();
    expect(next).toBeCalled();
  });
});
