import expect from "expect";

const isEmpty = jest.fn();
jest.mock("express-validator", () => ({
  validationResult: () => ({
    isEmpty: isEmpty,
  }),
}));

import { mockClient } from "aws-sdk-client-mock";
import { SQSClient, SendMessageCommand } from "@aws-sdk/client-sqs";

const sqsMock = mockClient(SQSClient);

jest.mock("uuid", () => ({
  v4: jest.fn(() => "mocked-uuid"),
}));

import { logger } from "../../../../logger/logger";
jest.mock("../../../../logger/logger", () => ({
  logger: {
    info: jest.fn(),
    error: jest.fn(),
  },
}));

jest.useFakeTimers().setSystemTime(new Date("2023-01-01"));

import {
  declaration,
  pageContent,
  validate,
  behaviourForPost,
} from "./declaration";

import { COMPLETED, IN_PROGRESS } from "../../flow-control/states";
import { buildSessionForJourney } from "../../flow-control/test-utils/test-utils";
import { REQUEST_COMPLETE_URL } from "../../paths/paths";
import { contextPath } from "../../paths/context-path";
import { CONTEXT_PATH } from "../../../static-pages/paths";
import { AGENT_ROLE, BACKOFFICE_ROLE } from "../../constants";
import { environment } from "../../../../../config/environment";
import { MAINFLOW } from "../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../tools/data-object";

test("declaration should match expected outcomes", () => {
  const expectedResults = {
    path: "/declaration",
    template: "declaration",
    pageContent,
    validate,
    behaviourForPost,
  };

  expect(declaration).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const req = {
    session: {
      journeyName: MAINFLOW.name,
      [MAINFLOW_DATA_OBJECT_NAME]: {
        "add-another-parent": "yes",
      },
    },
  };
  const expected = {
    title: translate("declaration.title"),
    firstHeading: translate("declaration.firstHeading"),
    firstBody: translate("declaration.firstBody", {
      contextPath: CONTEXT_PATH,
    }),
    thirdHeading: translate("declaration.thirdHeading"),
    secondBody: translate("declaration.secondBody", {
      contextPath: CONTEXT_PATH,
    }),
    agree: translate("declaration.checkbox"),
    button: translate("declaration.button"),
  };
  const result = pageContent({ translate, req });

  expect(result).toEqual(expected);
});

test("pageContent should return ccs content when user is agent", () => {
  environment.USE_AUTHENTICATION = true;

  const translate = (string, object?) => `${string}${object}`;

  const req = {
    session: {
      journeyName: MAINFLOW.name,
      [MAINFLOW_DATA_OBJECT_NAME]: {
        "add-another-parent": "yes",
      },
      account: {
        username: "test@mail.com",
        idTokenClaims: {
          roles: [AGENT_ROLE],
        },
      },
    },
  };
  const expected = {
    title: translate("declaration.title"),
    firstHeading: translate("declaration.firstHeading"),
    firstBody: translate("declaration.ccsFirstBody"),
    thirdHeading: translate("declaration.thirdHeading"),
    secondBody: translate("declaration.ccsSecondBody"),
    agree: translate("declaration.ccsCheckbox"),
    button: translate("declaration.ccsButton"),
  };
  const result = pageContent({ translate, req });

  expect(result).toEqual(expected);
});

describe("behaviourForPost", () => {
  const journey = { name: MAINFLOW.name };
  const config = {
    environment: {
      XEROX_QUEUE_URL: "test-xerox-queue-url",
      INCOMPLETE_APPLICATION_QUEUE_URL: "test-incomplete-application-queue-url",
    },
  };

  const expectedOptionalFieldsApplication = (
    req,
    secondParentExists = false,
    applicationType = "citizen"
  ) => {
    return {
      applicationId: "application-id",
      certificateReference: req.session.certificateReference,
      applicantName: `${req.session[MAINFLOW_DATA_OBJECT_NAME]["first-name"]} ${req.session[MAINFLOW_DATA_OBJECT_NAME]["last-name"]}`,
      applicantParentId: "mocked-uuid",
      secondParentId: secondParentExists ? "mocked-uuid" : null,
      addressLine1:
        applicationType == "citizen"
          ? req.session[MAINFLOW_DATA_OBJECT_NAME]["address-line-1"]
          : req.session[MAINFLOW_DATA_OBJECT_NAME]["address-line-1-ccs"],
      addressLine2:
        applicationType == "citizen"
          ? req.session[MAINFLOW_DATA_OBJECT_NAME]["address-line-2"]
          : req.session[MAINFLOW_DATA_OBJECT_NAME]["address-line-2-ccs"],
      addressLine3:
        applicationType == "citizen"
          ? req.session[MAINFLOW_DATA_OBJECT_NAME]["address-line-3"]
          : req.session[MAINFLOW_DATA_OBJECT_NAME]["address-line-3-ccs"],
      addressLine4:
        applicationType == "citizen"
          ? req.session[MAINFLOW_DATA_OBJECT_NAME]["address-line-4"]
          : req.session[MAINFLOW_DATA_OBJECT_NAME]["address-line-4-ccs"],
      addressLine5:
        applicationType == "citizen"
          ? req.session[MAINFLOW_DATA_OBJECT_NAME]["address-line-5"]
          : req.session[MAINFLOW_DATA_OBJECT_NAME]["address-line-5-ccs"],
      postcode: "PL7 1RF",
      lossDateDay: "04",
      lossDateMonth: "10",
      lossDateYear: "2020",
      lossDateFull: "4 October 2020",
      printDateOfLoss: true,
      lossPlaceName:
        req.session[MAINFLOW_DATA_OBJECT_NAME]["baby-loss-location"],
      lossCountry: "GB-ENG",
      babyName: req.session[MAINFLOW_DATA_OBJECT_NAME]["baby-name"],
      babySex: req.session[MAINFLOW_DATA_OBJECT_NAME]["baby-sex"],
      applicationCompleteDateTime: new Date(),
      applicationType: applicationType,
      version: 1,
    };
  };

  const expectedNoOptionalFieldsApplication = (
    req,
    secondParentExists = false,
    applicationType = "citizen"
  ) => {
    return {
      applicationId: "application-id",
      certificateReference: req.session.certificateReference,
      applicantName: `${req.session[MAINFLOW_DATA_OBJECT_NAME]["first-name"]} ${req.session[MAINFLOW_DATA_OBJECT_NAME]["last-name"]}`,
      applicantParentId: "mocked-uuid",
      secondParentId: secondParentExists ? "mocked-uuid" : null,
      addressLine1:
        applicationType == "citizen"
          ? req.session[MAINFLOW_DATA_OBJECT_NAME]["address-line-1"]
          : req.session[MAINFLOW_DATA_OBJECT_NAME]["address-line-1-ccs"],
      addressLine2: null,
      addressLine3: null,
      addressLine4: null,
      addressLine5: null,
      postcode: "PL7 1RF",
      lossDateDay: null,
      lossDateMonth: null,
      lossDateYear: "2020",
      lossDateFull: null,
      printDateOfLoss: false,
      lossPlaceName: null,
      lossCountry: "GB-ENG",
      babyName: null,
      babySex: null,
      applicationCompleteDateTime: new Date(),
      applicationType: applicationType,
      version: 1,
    };
  };

  const expectedParentRequest = (
    req,
    hasOptionalFields = false,
    applicationType = "citizen"
  ) => {
    return {
      parentId: "mocked-uuid",
      parentReferenceNumber:
        applicationType == "agent"
          ? req.session[MAINFLOW_DATA_OBJECT_NAME]["nhs-number"]
          : req.session[MAINFLOW_DATA_OBJECT_NAME]["reference-number"],
      parentReferenceType: applicationType == "backoffice" ? 2 : 1,
      parentReferenceNumberIssuingCountry: "GBR",
      parentGivenName: req.session[MAINFLOW_DATA_OBJECT_NAME]["first-name"],
      parentFamilyName: req.session[MAINFLOW_DATA_OBJECT_NAME]["last-name"],
      parentRelationship:
        req.session[MAINFLOW_DATA_OBJECT_NAME]["relation-to-baby"],
      parentDateOfBirth: "2000-01-01",
      parentEmailAddress: hasOptionalFields ? "firstParent@test.com" : null,
      applicationCompleteDateTime: new Date(),
      applicationCreatedBy: applicationType,
      applicationId: "application-id",
    };
  };

  const expectedSecondParentRequest = (req, applicationType = "citizen") => {
    const baseFields = {
      parentId: "mocked-uuid",
    };

    const remainingFields = {
      parentRelationship:
        req.session[MAINFLOW_DATA_OBJECT_NAME]["their-relation-to-baby"],
      parentDateOfBirth: "2000-01-01",
      parentEmailAddress:
        applicationType == "backoffice" ? null : "secondParent@test.com",
      applicationCompleteDateTime: new Date(),
      applicationCreatedBy: applicationType,
      applicationId: "application-id",
    };

    if (applicationType === "backoffice") {
      const backOfficeFields = {
        parentReferenceNumber: "GB-1313131313",
        parentReferenceType: 2,
        parentReferenceNumberIssuingCountry: "GBR",
        parentGivenName: "John",
        parentFamilyName: "Doe",
      };
      return { ...baseFields, ...backOfficeFields, ...remainingFields };
    }

    return { ...baseFields, ...remainingFields };
  };

  let allOptionalFieldsReq;
  let noOptionalFieldsReq;

  beforeEach(() => {
    sqsMock.reset();

    allOptionalFieldsReq = {
      "first-name": "Jane",
      "last-name": "Doe",
      "relation-to-baby": "Father",
      "date-of-birth": "2000-01-01",
      "applicant-email": "firstParent@test.com",
      "address-line-1": "Test Line 1",
      "address-line-2": "Test Line 2",
      "address-line-3": "Test Line 3",
      "address-line-4": "Test Line 4",
      "address-line-5": "Test Line 5",
      postcode: "pl7     1rf",
      "confirm-your-address": "yes",
      "baby-date-of-loss-day": "04",
      "baby-date-of-loss-month": "10",
      "baby-date-of-loss-year": "2020",
      "year-of-loss": "2020",
      "baby-loss-location": "Test Loss Location",
      "baby-name": "Test Name",
      "baby-sex": "Male",
      "reference-number": "9000000001",
      "reference-number-type": "nhsNumber",
      "add-another-parent": "no",
    };

    noOptionalFieldsReq = {
      "first-name": "Jane",
      "last-name": "Doe",
      "relation-to-baby": "Father",
      "date-of-birth": "2000-01-01",
      "applicant-email": "",
      "address-line-1": "Test",
      "address-line-2": "",
      "address-line-3": "",
      "address-line-4": "",
      "address-line-5": "",
      postcode: "pl7     1rf",
      "confirm-your-address": "yes",
      "baby-date-of-loss-day": "",
      "baby-date-of-loss-month": "",
      "baby-date-of-loss-year": "",
      "year-of-loss": "2020",
      "baby-loss-location": "",
      "baby-name": "",
      "baby-sex": "",
      "refrerence-number": "9000000001",
      "reference-number-type": "nhsNumber",
      "add-another-parent": "no",
    };
  });

  afterEach(() => {
    jest.clearAllMocks();

    allOptionalFieldsReq = null;
    noOptionalFieldsReq = null;
  });

  test("should call next when there are validation errors", () => {
    const req = { session: { journeyName: MAINFLOW.name } };
    const res = {};
    const next = jest.fn();

    isEmpty.mockReturnValue(false);

    behaviourForPost(config, journey)(req, res, next);

    expect(next).toBeCalledTimes(1);
  });

  describe("incomplete-application SQS", () => {
    test(`should send user request to incomplete-application SQS with all optional fields if second parent exists, set state to ${COMPLETED} and redirect to ${REQUEST_COMPLETE_URL}`, async () => {
      const req = {
        session: {
          journeyName: MAINFLOW.name,
          certificateReference: "C52A1999D8F",
          [MAINFLOW_DATA_OBJECT_NAME]: allOptionalFieldsReq,
          ...buildSessionForJourney({
            journeyName: MAINFLOW.name,
            state: IN_PROGRESS,
            nextAllowedPath: undefined,
          }),
          applicationId: "application-id",
        },
      };
      addInTwoParentFields(req);
      const res = { redirect: jest.fn() };
      const next = jest.fn();

      const applicationRequest = expectedOptionalFieldsApplication(req, true);

      const parentRequest = expectedParentRequest(req, true);

      const secondParentRequest = expectedSecondParentRequest(req);

      const expectedBody = {
        applicationRequest,
        parentRequest,
        secondParentRequest,
      };

      isEmpty.mockReturnValue(true);

      sqsMock
        .on(SendMessageCommand, {
          QueueUrl: config.environment.INCOMPLETE_APPLICATION_QUEUE_URL,
          MessageBody: JSON.stringify(expectedBody),
          MessageGroupId: "mocked-uuid",
        })
        .resolves({
          MessageId: "test-message-id-for-jane",
          MD5OfMessageBody: "test-md5-value-for-jane",
        });

      await behaviourForPost(config, journey)(req, res, next);

      const expectedLog =
        "SQS message sent successfully: { id: test-message-id-for-jane, hash: test-md5-value-for-jane, correlation-id: application-id }";
      expect(next).toBeCalledTimes(0);
      expect(logger.info).toBeCalledWith(expectedLog, req);
      expect(logger.error).toBeCalledTimes(0);
      expect(req.session.journeys[journey.name].state).toEqual(COMPLETED);
      expect(req.session.certificateReference).toBeTruthy();
      expect(res.redirect).toBeCalledWith(
        contextPath(REQUEST_COMPLETE_URL, MAINFLOW.name)
      );
    });

    test(`should send user request to incomplete-application SQS with no optional fields if second parent exists, set state to ${COMPLETED} and redirect to ${REQUEST_COMPLETE_URL}`, async () => {
      const req = {
        session: {
          journeyName: MAINFLOW.name,
          certificateReference: "C52A1999D8F",
          [MAINFLOW_DATA_OBJECT_NAME]: noOptionalFieldsReq,
          ...buildSessionForJourney({
            journeyName: MAINFLOW.name,
            state: IN_PROGRESS,
            nextAllowedPath: undefined,
          }),
          applicationId: "application-id",
        },
      };
      addInTwoParentFields(req);
      const res = { redirect: jest.fn() };
      const next = jest.fn();

      const applicationRequest = expectedNoOptionalFieldsApplication(req, true);

      const parentRequest = expectedParentRequest(req);

      const secondParentRequest = expectedSecondParentRequest(req);

      const expectedBody = {
        applicationRequest,
        parentRequest,
        secondParentRequest,
      };

      isEmpty.mockReturnValue(true);

      sqsMock
        .on(SendMessageCommand, {
          QueueUrl: config.environment.INCOMPLETE_APPLICATION_QUEUE_URL,
          MessageBody: JSON.stringify(expectedBody),
          MessageGroupId: "mocked-uuid",
        })
        .resolves({
          MessageId: "test-message-id-for-jane",
          MD5OfMessageBody: "test-md5-value-for-jane",
        });

      await behaviourForPost(config, journey)(req, res, next);

      const expectedLog =
        "SQS message sent successfully: { id: test-message-id-for-jane, hash: test-md5-value-for-jane, correlation-id: application-id }";

      expect(next).toBeCalledTimes(0);
      expect(logger.info).toBeCalledWith(expectedLog, req);
      expect(logger.error).toBeCalledTimes(0);
      expect(req.session.journeys[journey.name].state).toEqual(COMPLETED);
      expect(req.session.certificateReference).toBeTruthy();
      expect(res.redirect).toBeCalledWith(
        contextPath(REQUEST_COMPLETE_URL, MAINFLOW.name)
      );
    });

    test(`should send user request to incomplete-application SQS with all optional fields if two parents, set state to ${COMPLETED} and redirect to ${REQUEST_COMPLETE_URL} when confirm-your-address is alt and user is Agent`, async () => {
      environment.USE_AUTHENTICATION = true;
      const req = {
        session: {
          journeyName: MAINFLOW.name,
          certificateReference: "C52A1999D8F",
          account: {
            idTokenClaims: {
              roles: [AGENT_ROLE],
            },
          },
          [MAINFLOW_DATA_OBJECT_NAME]: allOptionalFieldsReq,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_PROGRESS,
            nextAllowedPath: undefined,
          }),
          applicationId: "application-id",
        },
      };
      addInTwoParentFields(req);
      updateToCCSParameters(req, true);
      const res = { redirect: jest.fn() };
      const next = jest.fn();

      const applicationRequest = expectedOptionalFieldsApplication(
        req,
        true,
        "agent"
      );

      const parentRequest = expectedParentRequest(req, true, "agent");

      const secondParentRequest = expectedSecondParentRequest(req, "agent");

      const expectedBody = {
        applicationRequest,
        parentRequest,
        secondParentRequest,
      };

      isEmpty.mockReturnValue(true);

      sqsMock
        .on(SendMessageCommand, {
          QueueUrl: config.environment.INCOMPLETE_APPLICATION_QUEUE_URL,
          MessageBody: JSON.stringify(expectedBody),
          MessageGroupId: "mocked-uuid",
        })
        .resolves({
          MessageId: "test-message-id-for-jane",
          MD5OfMessageBody: "test-md5-value-for-jane",
        });

      await behaviourForPost(config, journey)(req, res, next);

      const expectedLog =
        "SQS message sent successfully: { id: test-message-id-for-jane, hash: test-md5-value-for-jane, correlation-id: application-id }";

      expect(next).toBeCalledTimes(0);
      expect(logger.info).toBeCalledWith(expectedLog, req);
      expect(logger.error).toBeCalledTimes(0);
      expect(req.session.journeys[journey.name].state).toEqual(COMPLETED);
      expect(req.session.certificateReference).toBeTruthy();
      expect(res.redirect).toBeCalledWith(
        contextPath(REQUEST_COMPLETE_URL, MAINFLOW.name)
      );
    });

    test(`should send user request to incomplete-application SQS with no optional fields if two parents, set state to ${COMPLETED} and redirect to ${REQUEST_COMPLETE_URL} when confirm-your-address is alt and user is Agent`, async () => {
      environment.USE_AUTHENTICATION = true;
      const req = {
        session: {
          journeyName: MAINFLOW.name,
          certificateReference: "C52A1999D8F",
          account: {
            idTokenClaims: {
              roles: [AGENT_ROLE],
            },
          },
          [MAINFLOW_DATA_OBJECT_NAME]: noOptionalFieldsReq,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_PROGRESS,
            nextAllowedPath: undefined,
          }),
          applicationId: "application-id",
        },
      };
      addInTwoParentFields(req);
      updateToCCSParameters(req, false);
      const res = { redirect: jest.fn() };
      const next = jest.fn();

      const applicationRequest = expectedNoOptionalFieldsApplication(
        req,
        true,
        "agent"
      );

      const parentRequest = expectedParentRequest(req, false, "agent");

      const secondParentRequest = expectedSecondParentRequest(req, "agent");

      const expectedBody = {
        applicationRequest,
        parentRequest,
        secondParentRequest,
      };

      isEmpty.mockReturnValue(true);

      sqsMock
        .on(SendMessageCommand, {
          QueueUrl: config.environment.INCOMPLETE_APPLICATION_QUEUE_URL,
          MessageBody: JSON.stringify(expectedBody),
          MessageGroupId: "mocked-uuid",
        })
        .resolves({
          MessageId: "test-message-id-for-jane",
          MD5OfMessageBody: "test-md5-value-for-jane",
        });

      await behaviourForPost(config, journey)(req, res, next);

      const expectedLog =
        "SQS message sent successfully: { id: test-message-id-for-jane, hash: test-md5-value-for-jane, correlation-id: application-id }";

      expect(next).toBeCalledTimes(0);
      expect(logger.info).toBeCalledWith(expectedLog, req);
      expect(logger.error).toBeCalledTimes(0);
      expect(req.session.journeys[journey.name].state).toEqual(COMPLETED);
      expect(req.session.certificateReference).toBeTruthy();
      expect(res.redirect).toBeCalledWith(
        contextPath(REQUEST_COMPLETE_URL, MAINFLOW.name)
      );
    });
  });

  describe("Xerox SQS", () => {
    test(`should send user request to xerox SQS with all optional fields if only one parent, set state to ${COMPLETED} and redirect to ${REQUEST_COMPLETE_URL}`, async () => {
      const req = {
        session: {
          journeyName: MAINFLOW.name,
          certificateReference: "C52A1999D8F",
          [MAINFLOW_DATA_OBJECT_NAME]: allOptionalFieldsReq,

          ...buildSessionForJourney({
            journeyName: MAINFLOW.name,
            state: IN_PROGRESS,
            nextAllowedPath: undefined,
          }),
          applicationId: "application-id",
        },
      };
      const res = { redirect: jest.fn() };
      const next = jest.fn();

      const applicationRequest = expectedOptionalFieldsApplication(req);

      const parentRequest = expectedParentRequest(req, true);

      const secondParentRequest = null;

      const expectedBody = {
        applicationRequest,
        parentRequest,
        secondParentRequest,
      };

      isEmpty.mockReturnValue(true);

      sqsMock
        .on(SendMessageCommand, {
          QueueUrl: config.environment.XEROX_QUEUE_URL,
          MessageBody: JSON.stringify(expectedBody),
          MessageGroupId: "mocked-uuid",
        })
        .resolves({
          MessageId: "test-message-id-for-jane",
          MD5OfMessageBody: "test-md5-value-for-jane",
        });

      await behaviourForPost(config, journey)(req, res, next);

      const expectedLog =
        "SQS message sent successfully: { id: test-message-id-for-jane, hash: test-md5-value-for-jane, correlation-id: application-id }";

      expect(next).toBeCalledTimes(0);
      expect(logger.info).toBeCalledWith(expectedLog, req);
      expect(logger.error).toBeCalledTimes(0);
      expect(req.session.journeys[journey.name].state).toEqual(COMPLETED);
      expect(req.session.certificateReference).toBeTruthy();
      expect(res.redirect).toBeCalledWith(
        contextPath(REQUEST_COMPLETE_URL, MAINFLOW.name)
      );
    });

    test(`should send user request to xerox SQS with no optional fields if only one parent, set state to ${COMPLETED} and redirect to ${REQUEST_COMPLETE_URL}`, async () => {
      const req = {
        session: {
          journeyName: MAINFLOW.name,
          certificateReference: "C52A1999D8F",
          [MAINFLOW_DATA_OBJECT_NAME]: noOptionalFieldsReq,

          ...buildSessionForJourney({
            journeyName: MAINFLOW.name,
            state: IN_PROGRESS,
            nextAllowedPath: undefined,
          }),
          applicationId: "application-id",
        },
      };
      const res = { redirect: jest.fn() };
      const next = jest.fn();

      const applicationRequest = expectedNoOptionalFieldsApplication(req);

      const parentRequest = expectedParentRequest(req);

      const secondParentRequest = null;

      const expectedBody = {
        applicationRequest,
        parentRequest,
        secondParentRequest,
      };

      isEmpty.mockReturnValue(true);

      sqsMock
        .on(SendMessageCommand, {
          QueueUrl: config.environment.XEROX_QUEUE_URL,
          MessageBody: JSON.stringify(expectedBody),
          MessageGroupId: "mocked-uuid",
        })
        .resolves({
          MessageId: "test-message-id-for-jane",
          MD5OfMessageBody: "test-md5-value-for-jane",
        });

      await behaviourForPost(config, journey)(req, res, next);

      const expectedLog =
        "SQS message sent successfully: { id: test-message-id-for-jane, hash: test-md5-value-for-jane, correlation-id: application-id }";

      expect(next).toBeCalledTimes(0);
      expect(logger.info).toBeCalledWith(expectedLog, req);
      expect(logger.error).toBeCalledTimes(0);
      expect(req.session.journeys[journey.name].state).toEqual(COMPLETED);
      expect(req.session.certificateReference).toBeTruthy();
      expect(res.redirect).toBeCalledWith(
        contextPath(REQUEST_COMPLETE_URL, MAINFLOW.name)
      );
    });

    test(`should send user request to xerox SQS with all optional fields if one parent, set state to ${COMPLETED} and redirect to ${REQUEST_COMPLETE_URL} when confirm-your-address is alt and user is Agent`, async () => {
      environment.USE_AUTHENTICATION = true;
      const req = {
        session: {
          journeyName: MAINFLOW.name,
          certificateReference: "C52A1999D8F",
          [MAINFLOW_DATA_OBJECT_NAME]: allOptionalFieldsReq,
          ...buildSessionForJourney({
            journeyName: MAINFLOW.name,
            state: IN_PROGRESS,
            nextAllowedPath: undefined,
          }),
          applicationId: "application-id",
          account: {
            idTokenClaims: {
              roles: [AGENT_ROLE],
            },
          },
        },
      };
      updateToCCSParameters(req, true);
      const res = { redirect: jest.fn() };
      const next = jest.fn();

      const applicationRequest = expectedOptionalFieldsApplication(
        req,
        false,
        "agent"
      );

      const parentRequest = expectedParentRequest(req, true, "agent");

      const secondParentRequest = null;

      const expectedBody = {
        applicationRequest,
        parentRequest,
        secondParentRequest,
      };

      isEmpty.mockReturnValue(true);

      sqsMock
        .on(SendMessageCommand, {
          QueueUrl: config.environment.XEROX_QUEUE_URL,
          MessageBody: JSON.stringify(expectedBody),
          MessageGroupId: "mocked-uuid",
        })
        .resolves({
          MessageId: "test-message-id-for-jane",
          MD5OfMessageBody: "test-md5-value-for-jane",
        });

      await behaviourForPost(config, journey)(req, res, next);

      const expectedLog =
        "SQS message sent successfully: { id: test-message-id-for-jane, hash: test-md5-value-for-jane, correlation-id: application-id }";

      expect(next).toBeCalledTimes(0);
      expect(logger.info).toBeCalledWith(expectedLog, req);
      expect(logger.error).toBeCalledTimes(0);
      expect(req.session.journeys[journey.name].state).toEqual(COMPLETED);
      expect(req.session.certificateReference).toBeTruthy();
      expect(res.redirect).toBeCalledWith(
        contextPath(REQUEST_COMPLETE_URL, MAINFLOW.name)
      );
    });

    test(`should send user request to xerox SQS with no optional fields if one parent, set state to ${COMPLETED} and redirect to ${REQUEST_COMPLETE_URL} when confirm-your-address is alt and user is Agent`, async () => {
      environment.USE_AUTHENTICATION = true;
      const req = {
        session: {
          journeyName: MAINFLOW.name,
          certificateReference: "C52A1999D8F",
          [MAINFLOW_DATA_OBJECT_NAME]: noOptionalFieldsReq,
          ...buildSessionForJourney({
            journeyName: MAINFLOW.name,
            state: IN_PROGRESS,
            nextAllowedPath: undefined,
          }),
          applicationId: "application-id",
          account: {
            idTokenClaims: {
              roles: [AGENT_ROLE],
            },
          },
        },
      };
      updateToCCSParameters(req, false);
      const res = { redirect: jest.fn() };
      const next = jest.fn();

      const applicationRequest = expectedNoOptionalFieldsApplication(
        req,
        false,
        "agent"
      );

      const parentRequest = expectedParentRequest(req, false, "agent");

      const secondParentRequest = null;

      const expectedBody = {
        applicationRequest,
        parentRequest,
        secondParentRequest,
      };

      isEmpty.mockReturnValue(true);

      sqsMock
        .on(SendMessageCommand, {
          QueueUrl: config.environment.XEROX_QUEUE_URL,
          MessageBody: JSON.stringify(expectedBody),
          MessageGroupId: "mocked-uuid",
        })
        .resolves({
          MessageId: "test-message-id-for-jane",
          MD5OfMessageBody: "test-md5-value-for-jane",
        });

      await behaviourForPost(config, journey)(req, res, next);

      const expectedLog =
        "SQS message sent successfully: { id: test-message-id-for-jane, hash: test-md5-value-for-jane, correlation-id: application-id }";

      expect(next).toBeCalledTimes(0);
      expect(logger.info).toBeCalledWith(expectedLog, req);
      expect(logger.error).toBeCalledTimes(0);
      expect(req.session.journeys[journey.name].state).toEqual(COMPLETED);
      expect(req.session.certificateReference).toBeTruthy();
      expect(res.redirect).toBeCalledWith(
        contextPath(REQUEST_COMPLETE_URL, MAINFLOW.name)
      );
    });

    test(`should send user request to xerox SQS with all optional fields if one parent, set state to ${COMPLETED} and redirect to ${REQUEST_COMPLETE_URL} when confirm-your-address is alt and user is BackOffice`, async () => {
      environment.USE_AUTHENTICATION = true;

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          certificateReference: "C52A1999D8F",
          account: {
            idTokenClaims: {
              roles: [BACKOFFICE_ROLE],
            },
          },
          [MAINFLOW_DATA_OBJECT_NAME]: allOptionalFieldsReq,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_PROGRESS,
            nextAllowedPath: undefined,
          }),
          applicationId: "application-id",
        },
      };
      updateToBackOfficeParameters(req, true);
      const res = { redirect: jest.fn() };
      const next = jest.fn();

      const applicationRequest = expectedOptionalFieldsApplication(
        req,
        false,
        "backoffice"
      );

      const parentRequest = expectedParentRequest(req, true, "backoffice");

      const secondParentRequest = null;

      const expectedBody = {
        applicationRequest,
        parentRequest,
        secondParentRequest,
      };

      isEmpty.mockReturnValue(true);

      sqsMock
        .on(SendMessageCommand, {
          QueueUrl: config.environment.XEROX_QUEUE_URL,
          MessageBody: JSON.stringify(expectedBody),
          MessageGroupId: "mocked-uuid",
        })
        .resolves({
          MessageId: "test-message-id-for-jane",
          MD5OfMessageBody: "test-md5-value-for-jane",
        });

      await behaviourForPost(config, journey)(req, res, next);

      const expectedLog =
        "SQS message sent successfully: { id: test-message-id-for-jane, hash: test-md5-value-for-jane, correlation-id: application-id }";

      expect(next).toBeCalledTimes(0);
      expect(logger.info).toBeCalledWith(expectedLog, req);
      expect(logger.error).toBeCalledTimes(0);
      expect(req.session.journeys[journey.name].state).toEqual(COMPLETED);
      expect(req.session.certificateReference).toBeTruthy();
      expect(res.redirect).toBeCalledWith(
        contextPath(REQUEST_COMPLETE_URL, MAINFLOW.name)
      );
    });

    test(`should send user request to xerox SQS with no optional fields if one parent, set state to ${COMPLETED} and redirect to ${REQUEST_COMPLETE_URL} when confirm-your-address is alt and user is BackOffice`, async () => {
      environment.USE_AUTHENTICATION = true;

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          certificateReference: "C52A1999D8F",
          account: {
            idTokenClaims: {
              roles: [BACKOFFICE_ROLE],
            },
          },
          [MAINFLOW_DATA_OBJECT_NAME]: noOptionalFieldsReq,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_PROGRESS,
            nextAllowedPath: undefined,
          }),
          applicationId: "application-id",
        },
      };
      updateToBackOfficeParameters(req, false);
      const res = { redirect: jest.fn() };
      const next = jest.fn();

      const applicationRequest = expectedNoOptionalFieldsApplication(
        req,
        false,
        "backoffice"
      );

      const parentRequest = expectedParentRequest(req, false, "backoffice");

      const secondParentRequest = null;

      const expectedBody = {
        applicationRequest,
        parentRequest,
        secondParentRequest,
      };

      isEmpty.mockReturnValue(true);

      sqsMock
        .on(SendMessageCommand, {
          QueueUrl: config.environment.XEROX_QUEUE_URL,
          MessageBody: JSON.stringify(expectedBody),
          MessageGroupId: "mocked-uuid",
        })
        .resolves({
          MessageId: "test-message-id-for-jane",
          MD5OfMessageBody: "test-md5-value-for-jane",
        });

      await behaviourForPost(config, journey)(req, res, next);

      const expectedLog =
        "SQS message sent successfully: { id: test-message-id-for-jane, hash: test-md5-value-for-jane, correlation-id: application-id }";

      expect(next).toBeCalledTimes(0);
      expect(logger.info).toBeCalledWith(expectedLog, req);
      expect(logger.error).toBeCalledTimes(0);
      expect(req.session.journeys[journey.name].state).toEqual(COMPLETED);
      expect(req.session.certificateReference).toBeTruthy();
      expect(res.redirect).toBeCalledWith(
        contextPath(REQUEST_COMPLETE_URL, MAINFLOW.name)
      );
    });

    test(`should send user request to xerox SQS with all optional fields if two parents, set state to ${COMPLETED} and redirect to ${REQUEST_COMPLETE_URL} when confirm-your-address is alt and user is BackOffice`, async () => {
      environment.USE_AUTHENTICATION = true;

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          certificateReference: "C52A1999D8F",
          account: {
            idTokenClaims: {
              roles: [BACKOFFICE_ROLE],
            },
          },
          [MAINFLOW_DATA_OBJECT_NAME]: allOptionalFieldsReq,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_PROGRESS,
            nextAllowedPath: undefined,
          }),
          applicationId: "application-id",
        },
      };
      updateToBackOfficeParameters(req, true);
      addInBackOfficeTwoParentFields(req);
      const res = { redirect: jest.fn() };
      const next = jest.fn();

      const applicationRequest = expectedOptionalFieldsApplication(
        req,
        true,
        "backoffice"
      );

      const parentRequest = expectedParentRequest(req, true, "backoffice");

      const secondParentRequest = expectedSecondParentRequest(
        req,
        "backoffice"
      );

      const expectedBody = {
        applicationRequest,
        parentRequest,
        secondParentRequest,
      };

      isEmpty.mockReturnValue(true);

      sqsMock
        .on(SendMessageCommand, {
          QueueUrl: config.environment.XEROX_QUEUE_URL,
          MessageBody: JSON.stringify(expectedBody),
          MessageGroupId: "mocked-uuid",
        })
        .resolves({
          MessageId: "test-message-id-for-jane",
          MD5OfMessageBody: "test-md5-value-for-jane",
        });

      await behaviourForPost(config, journey)(req, res, next);

      const expectedLog =
        "SQS message sent successfully: { id: test-message-id-for-jane, hash: test-md5-value-for-jane, correlation-id: application-id }";

      expect(next).toBeCalledTimes(0);
      expect(logger.info).toBeCalledWith(expectedLog, req);
      expect(logger.error).toBeCalledTimes(0);
      expect(req.session.journeys[journey.name].state).toEqual(COMPLETED);
      expect(req.session.certificateReference).toBeTruthy();
      expect(res.redirect).toBeCalledWith(
        contextPath(REQUEST_COMPLETE_URL, MAINFLOW.name)
      );
    });

    test(`should send user request to xerox SQS with no optional fields if two parents, set state to ${COMPLETED} and redirect to ${REQUEST_COMPLETE_URL} when confirm-your-address is alt and user is BackOffice`, async () => {
      environment.USE_AUTHENTICATION = true;

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          certificateReference: "C52A1999D8F",
          account: {
            idTokenClaims: {
              roles: [BACKOFFICE_ROLE],
            },
          },
          [MAINFLOW_DATA_OBJECT_NAME]: noOptionalFieldsReq,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_PROGRESS,
            nextAllowedPath: undefined,
          }),
          applicationId: "application-id",
        },
      };
      updateToBackOfficeParameters(req, false);
      addInBackOfficeTwoParentFields(req);
      const res = { redirect: jest.fn() };
      const next = jest.fn();

      const applicationRequest = expectedNoOptionalFieldsApplication(
        req,
        true,
        "backoffice"
      );

      const parentRequest = expectedParentRequest(req, false, "backoffice");

      const secondParentRequest = expectedSecondParentRequest(
        req,
        "backoffice"
      );

      const expectedBody = {
        applicationRequest,
        parentRequest,
        secondParentRequest,
      };

      isEmpty.mockReturnValue(true);

      sqsMock
        .on(SendMessageCommand, {
          QueueUrl: config.environment.XEROX_QUEUE_URL,
          MessageBody: JSON.stringify(expectedBody),
          MessageGroupId: "mocked-uuid",
        })
        .resolves({
          MessageId: "test-message-id-for-jane",
          MD5OfMessageBody: "test-md5-value-for-jane",
        });

      await behaviourForPost(config, journey)(req, res, next);

      const expectedLog =
        "SQS message sent successfully: { id: test-message-id-for-jane, hash: test-md5-value-for-jane, correlation-id: application-id }";

      expect(next).toBeCalledTimes(0);
      expect(logger.info).toBeCalledWith(expectedLog, req);
      expect(logger.error).toBeCalledTimes(0);
      expect(req.session.journeys[journey.name].state).toEqual(COMPLETED);
      expect(req.session.certificateReference).toBeTruthy();
      expect(res.redirect).toBeCalledWith(
        contextPath(REQUEST_COMPLETE_URL, MAINFLOW.name)
      );
    });
  });

  test("should generate a certificateReference when it doesn't exist", async () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        certificateReference: undefined,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "first-name": "Jane",
          "last-name": "Doe",
          "relation-to-baby": "Father",
          "date-of-birth": "2000-01-01",
          "applicant-email": "firstParent@test.com",
          "address-line-1": "Test",
          "address-line-2": "",
          "address-line-3": "",
          "address-line-4": "",
          "address-line-5": "",
          postcode: "pl71rf",
          "baby-date-of-loss-day": "",
          "baby-date-of-loss-month": "",
          "baby-date-of-loss-year": "",
          "year-of-loss": "2020",
          "baby-loss-location": "",
          "baby-name": "",
          "baby-sex": "",
          "nhs-number": "9999999999",
          "pds-nhs-number": "9999999999",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = { redirect: jest.fn() };
    const next = jest.fn();

    isEmpty.mockReturnValue(true);

    sqsMock.on(SendMessageCommand).resolves({});

    await behaviourForPost(config, journey)(req, res, next);

    expect(next).toBeCalledTimes(0);
    expect(req.session.certificateReference).toBeDefined();
  });

  test("should skip adding to queue when already submitted form", async () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        submitted: true,
        certificateReference: undefined,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "first-name": "Jane",
          "last-name": "Doe",
          "relation-to-baby": "Father",
          "date-of-birth": "2000-01-01",
          "applicant-email": "firstParent@test.com",
          "address-line-1": "Test",
          "address-line-2": "",
          "address-line-3": "",
          "address-line-4": "",
          "address-line-5": "",
          postcode: "PL71RF",
          "baby-date-of-loss-day": "",
          "baby-date-of-loss-month": "",
          "baby-date-of-loss-year": "",
          "year-of-loss": "2020",
          "baby-loss-location": "",
          "baby-name": "",
          "baby-sex": "",
          "nhs-number": "9999999999",
          "pds-nhs-number": "9999999999",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = { redirect: jest.fn() };
    const next = jest.fn();

    isEmpty.mockReturnValue(true);

    await behaviourForPost(config, journey)(req, res, next);

    expect(res.redirect).toBeCalledWith(
      contextPath(REQUEST_COMPLETE_URL, MAINFLOW.name)
    );
  });
});

function addInTwoParentFields(req) {
  const additionalFields = {
    "their-relation-to-baby": "Mother",
    "their-date-of-birth": "2000-01-01",
    "their-email": "secondParent@test.com",
    "add-another-parent": "yes",
  };

  for (const key in additionalFields) {
    req.session[MAINFLOW_DATA_OBJECT_NAME][key] = additionalFields[key];
  }
}

function addInBackOfficeTwoParentFields(req) {
  const secondParentFields = {
    "their-first-name": "John",
    "their-last-name": "Doe",
    "their-relation-to-baby": "Father",
    "their-reference-number-country": "GBR",
    "their-date-of-birth": "2000-01-01",
    "their-reference-number": "GB-1313131313",
    "their-reference-number-type": "passport",
    "add-another-parent": "yes",
  };

  for (const key in secondParentFields) {
    req.session[MAINFLOW_DATA_OBJECT_NAME][key] = secondParentFields[key];
  }
}

function updateToCCSParameters(req, addInOptionalFields) {
  const newFields = {
    "address-line-1-ccs": "Test Line 1",
    "address-line-2-ccs": addInOptionalFields ? "Test Line 2" : null,
    "address-line-3-ccs": addInOptionalFields ? "Test Line 3" : null,
    "address-line-4-ccs": addInOptionalFields ? "Test Line 4" : null,
    "address-line-5-ccs": addInOptionalFields ? "Test Line 5" : null,
    "postcode-ccs": "pl7     1rf",
    "confirm-your-address": "alt",
    "nhs-number": "9000000001",
  };

  const oldFields = [
    "address-line-1",
    "address-line-2",
    "address-line-3",
    "address-line-4",
    "address-line-5",
    "postcode",
    "reference-number",
  ];

  for (const key of oldFields) {
    delete req.session[MAINFLOW_DATA_OBJECT_NAME][key];
  }

  for (const key in newFields) {
    req.session[MAINFLOW_DATA_OBJECT_NAME][key] = newFields[key];
  }
}

function updateToBackOfficeParameters(req, addInOptionalFields) {
  const newFields = {
    "address-line-1-ccs": "Test Line 1",
    "address-line-2-ccs": addInOptionalFields ? "Test Line 2" : null,
    "address-line-3-ccs": addInOptionalFields ? "Test Line 3" : null,
    "address-line-4-ccs": addInOptionalFields ? "Test Line 4" : null,
    "address-line-5-ccs": addInOptionalFields ? "Test Line 5" : null,
    "postcode-ccs": "pl7     1rf",
    "confirm-your-address": "alt",
    "reference-number": "GB-1212121212",
    "reference-number-type": "passport",
  };

  const oldFields = [
    "address-line-1",
    "address-line-2",
    "address-line-3",
    "address-line-4",
    "address-line-5",
    "postcode",
  ];

  for (const key of oldFields) {
    delete req.session[MAINFLOW_DATA_OBJECT_NAME][key];
  }

  for (const key in newFields) {
    req.session[MAINFLOW_DATA_OBJECT_NAME][key] = newFields[key];
  }
}
