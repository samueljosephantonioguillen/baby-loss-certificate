import axios from "axios";

const getPdsOauthTokenMock = jest.fn();
jest.mock("./get-pds-oauth-token", () => ({
  getPdsOauthToken: getPdsOauthTokenMock,
}));

import { requestPdsLookup } from "./request-pds-lookup";

jest.mock("axios");

const config = {
  environment: {
    PDS_URI: "https://mock.pds.uri",
  },
};

describe("requestPdsLookup", () => {
  const req = {};
  getPdsOauthTokenMock.mockImplementation(() => {
    return "pds_oauth_token";
  });
  test("should return valid response with all data present and returns first of given names", async () => {
    const responseData = {
      data: {
        resourceType: "Patient",
        id: "9000000009",
        birthDate: "2010-10-22",
        name: [
          {
            use: "usual",
            period: {
              start: "2020-01-01",
            },
            given: ["Jane", "Dough"],
            family: "Smith",
          },
        ],
        telecom: [
          {
            system: "phone",
            use: "mobile",
            value: "01632960587",
            period: {
              start: "2020-01-01",
            },
          },
          {
            system: "email",
            value: "jane.smith@example.com",
            period: {
              start: "2020-01-01",
            },
          },
        ],
        address: [
          {
            id: "456",
            period: {
              start: "2020-01-01",
            },
            use: "home",
            line: [
              "1 Trevelyan Square",
              "Boar Lane",
              "City Centre",
              "Leeds",
              "West Yorkshire",
            ],
            postalCode: "LS1 6AE",
          },
        ],
        deceasedDateTime: "2023-07-07T09:34:40+00:00",
      },
    };
    const expectedResult = {
      firstName: "Jane",
      lastName: "Smith",
      dateOfBirth: "2010-10-22",
      email: "jane.smith@example.com",
      phoneNumber: "01632960587",
      addressLines: [
        "1 Trevelyan Square",
        "Boar Lane",
        "City Centre",
        "Leeds",
        "West Yorkshire",
      ],
      postcode: "LS1 6AE",
      hasDeceasedDateTime: true,
      nhsNumber: "9000000009",
    };
    const mock = jest.fn();
    axios.get = mock.mockResolvedValue(responseData);

    const result = await requestPdsLookup(req, config, "1111111111");
    expect(result).toEqual(expectedResult);
  });

  test("should return valid response with missing data", async () => {
    const responseData = {
      data: {
        resourceType: "Patient",
        id: "9000000009",
        birthDate: null,
        name: [
          {
            use: "usual",
            period: {
              start: "2020-01-01",
              end: "2021-01-01",
            },
            given: ["Expired"],
            family: "Expired",
          },
        ],
        telecom: [
          {
            system: "phone",
            use: "mobile",
            value: "01632960587",
            period: {
              start: "2020-01-01",
              end: "2021-01-01",
            },
          },
          {
            system: "email",
            value: "jane.smith@example.com",
            period: {
              start: "2020-01-01",
              end: "2021-01-01",
            },
          },
        ],
        address: [
          {
            id: "456",
            period: {
              start: "2020-01-01",
              end: "2021-01-01",
            },
            use: "home",
            line: ["Expired"],
            postalCode: "Expired",
          },
        ],
      },
    };
    const expectedResult = {
      firstName: null,
      lastName: null,
      dateOfBirth: null,
      email: null,
      phoneNumber: null,
      addressLines: [],
      postcode: null,
      hasDeceasedDateTime: false,
      nhsNumber: "9000000009",
    };
    const mock = jest.fn();
    axios.get = mock.mockResolvedValue(responseData);

    const result = await requestPdsLookup(req, config, "1111111111");
    expect(result).toEqual(expectedResult);
  });

  test("should throw error when PDS call fails", async () => {
    const pdsError = {
      response: {
        status: 400,
        message: "400 Bad Request",
      },
    };
    const mock = jest.fn();
    axios.get = mock.mockImplementation(() => Promise.reject(pdsError));

    try {
      await requestPdsLookup(req, config, "99999999");
    } catch (error) {
      expect(error).toEqual(pdsError);
    }
  });

  test("should change PDS response First and Second names to Title Case and postcode to UPPERCASE", async () => {
    const responseData = {
      data: {
        resourceType: "Patient",
        id: "9000000009",
        birthDate: "2010-10-22",
        name: [
          {
            use: "usual",
            period: {
              start: "2020-01-01",
            },
            given: ["jane", "Dough"],
            family: "SMITH",
          },
        ],
        telecom: [
          {
            system: "phone",
            use: "mobile",
            value: "01632960587",
            period: {
              start: "2020-01-01",
            },
          },
          {
            system: "email",
            value: "jane.smith@example.com",
            period: {
              start: "2020-01-01",
            },
          },
        ],
        address: [
          {
            id: "456",
            period: {
              start: "2020-01-01",
            },
            use: "home",
            line: [
              "1 Trevelyan Square",
              "Boar Lane",
              "City Centre",
              "Leeds",
              "West Yorkshire",
            ],
            postalCode: "ls1 6ae",
          },
        ],
        deceasedDateTime: "2023-07-07T09:34:40+00:00",
      },
    };
    const expectedResult = {
      firstName: "Jane",
      lastName: "Smith",
      dateOfBirth: "2010-10-22",
      email: "jane.smith@example.com",
      phoneNumber: "01632960587",
      addressLines: [
        "1 Trevelyan Square",
        "Boar Lane",
        "City Centre",
        "Leeds",
        "West Yorkshire",
      ],
      postcode: "LS1 6AE",
      hasDeceasedDateTime: true,
      nhsNumber: "9000000009",
    };

    const mock = jest.fn();
    axios.get = mock.mockResolvedValue(responseData);

    const result = await requestPdsLookup(req, config, "1111111111");
    expect(result).toEqual(expectedResult);
  });
});
