import { replaceMultipleSpacesAndAddSingleSpaceThenUppercase } from "../address/sanitise";
import { toUpper, compose } from "ramda";

const removeHyphens = (value) => {
  return value.replace(/-/g, "");
};

const removeHyphensThenUppercase = compose(removeHyphens, toUpper);

const sanitise = () => (req, res, next) => {
  if (req.body.postcode) {
    req.body.sanitisedPostcode =
      replaceMultipleSpacesAndAddSingleSpaceThenUppercase(req.body.postcode);
  }
  if (req.body["last-name"]) {
    req.body["last-name"] = req.body["last-name"].trim();
  }

  if (req.body["first-name"]) {
    req.body["first-name"] = req.body["first-name"].trim();
    // capitalise first letter
    req.body["first-name"] =
      req.body["first-name"].charAt(0).toUpperCase() +
      req.body["first-name"].slice(1);

    req.body["last-name"] =
      req.body["last-name"].charAt(0).toUpperCase() +
      req.body["last-name"].slice(1);
  }

  if (req.body["certificate-reference-number"]) {
    req.body["sanitised-certificate-reference-number"] =
      removeHyphensThenUppercase(req.body["certificate-reference-number"]);
  }
  next();
};

export { sanitise };
