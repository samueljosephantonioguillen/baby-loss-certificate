import * as nunjucks from "nunjucks";

import { toErrorList, getErrorForField } from "./filters";

const setViewEngine = (config, app) => {
  const env = nunjucks.configure(
    [
      "node_modules/govuk-frontend/dist/",
      "node_modules/hmrc-frontend/",
      "src/web/views",
    ],
    {
      autoescape: true,
      express: app,
      noCache: config.server.NO_CACHE_VIEW_TEMPLATES,
    }
  );

  env.addFilter("toErrorList", toErrorList);
  env.addFilter("getErrorForField", getErrorForField);

  env.addGlobal("assetPath", config.server.ASSET_PATH);
  env.addGlobal("appVersion", config.server.APP_VERSION);
  env.addGlobal("firstParentContextPath", config.environment.CONTEXT_PATH);

  env.addGlobal("gaTrackingId", config.environment.GA_TRACKING_ID);

  app.set("view engine", "njk");
};

export { setViewEngine };
