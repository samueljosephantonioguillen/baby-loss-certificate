import { userIsBackOffice } from "../../../../azure-routes/auth";
import { getFlowDataObjectName } from "../../../tools/data-object";
import { countryCodeMapper } from "../../../tools/country-code-parser";
import { validate } from "./validate";
import { acceptableReferenceNumberTypes } from "../proof-of-name/validate";
import { stateMachine } from "../../../flow-control/state-machine";
import { IN_REVIEW_PROGRESS, IN_REVIEW } from "../../../flow-control/states";

const pageContent = ({ translate, req }) => ({
  title: translate("enterReferenceNumber.title"),
  sectionTitle: translate("section.otherParentsDetails"),
  heading: translate("enterReferenceNumber.heading"),
  referenceNumber: translate("enterReferenceNumber.referenceNumber"),
  countryOrigin: translate("enterReferenceNumber.countryOrigin"),
  countryCodes: {
    data: countryCodeMapper(
      req.session[getFlowDataObjectName(req.session.journeyName)][
        "their-reference-number-country"
      ]
    ),
  },
  continueButtonText: translate("buttons:continue"),
});

const isNavigable = (req) =>
  userIsBackOffice(req) &&
  acceptableReferenceNumberTypes.includes(
    req.session[getFlowDataObjectName(req.session.journeyName)][
      "their-reference-number-type"
    ]
  );

const behaviourForPost = (config, journey) => (req, res, next) => {
  if (stateMachine.getState(req, journey) === IN_REVIEW_PROGRESS) {
    stateMachine.setState(IN_REVIEW, req, journey);
  }

  next();
};

const spEnterReferenceNumber = {
  path: "/other-parent-reference-number",
  template: "sp-enter-reference-number",
  isNavigable,
  pageContent,
  validate,
  behaviourForPost,
};

export {
  spEnterReferenceNumber,
  isNavigable,
  pageContent,
  validate,
  behaviourForPost,
};
