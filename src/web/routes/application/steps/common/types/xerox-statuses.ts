export const XeroxStatus = {
  INCOMPLETE: "incomplete",
  EXPIRED: "expired",
  REJECTED: "rejected",
  FAILED: "failed",
  SENT: "sent",
  COMPLETE: "complete",
};
