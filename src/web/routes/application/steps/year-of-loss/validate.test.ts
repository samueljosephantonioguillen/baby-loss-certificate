import expect from "expect";
import { validate } from "./validate";
import { applyExpressValidation } from "../common/test/apply-express-validation";

describe("validate 'loss in five years' field", () => {
  test("when empty, error should return expected", async () => {
    const req = {};

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const expectedError = {
      type: "field",
      value: undefined,
      msg: "validation:selectYearOfLoss",
      path: "loss-in-five-years",
      location: "body",
    };

    const error = result.array()[0];

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });
});

describe("validate 'year of loss' field", () => {
  test("'loss in five years' field should be 'yes', otherwise no errors are returned", async () => {
    const req = {
      body: {
        "loss-in-five-years": "no",
        "year-of-loss": "invalid year",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    expect(result.array().length).toBe(0);
    expect(error).toBeFalsy();
  });

  test("return no errors when within eligible years", async () => {
    const req = {
      body: {
        "loss-in-five-years": "yes",
        "year-of-loss": new Date().getUTCFullYear(),
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    expect(result.array().length).toBe(0);
    expect(error).toBeFalsy();
  });

  test("when missing field, return expected error", async () => {
    const req = {
      body: {
        "loss-in-five-years": "yes",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const expectedError = {
      type: "field",
      value: undefined,
      msg: "validation:missingYearOfLoss",
      path: "year-of-loss",
      location: "body",
    };

    const error = result.array()[0];

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });

  test("when invalid field, return expected error", async () => {
    const req = {
      body: {
        "loss-in-five-years": "yes",
        "year-of-loss": "invalid year",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const expectedError = {
      type: "field",
      value: req.body["year-of-loss"],
      msg: "validation:invalidYearOfLoss",
      path: "year-of-loss",
      location: "body",
    };

    const error = result.array()[0];

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });

  test("when year is before eligble date, return expected error", async () => {
    const req = {
      body: {
        "loss-in-five-years": "yes",
        "year-of-loss": 2017,
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const expectedError = {
      type: "field",
      value: req.body["year-of-loss"],
      msg: "validation:yearOfLossAfterDate",
      path: "year-of-loss",
      location: "body",
    };

    const error = result.array()[0];

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });

  test("when year in future field, return expected error", async () => {
    const req = {
      body: {
        "loss-in-five-years": "yes",
        "year-of-loss": 9999,
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const expectedError = {
      type: "field",
      value: req.body["year-of-loss"],
      msg: "validation:yearOfLossInFuture",
      path: "year-of-loss",
      location: "body",
    };

    const error = result.array()[0];

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });
});
