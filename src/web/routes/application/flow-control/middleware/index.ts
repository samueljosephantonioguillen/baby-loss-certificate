export default {
  ...require("./configure-get"),
  ...require("./configure-post"),
  ...require("./handle-post"),
  ...require("./handle-post-redirects"),
  ...require("./inject-headers"),
  ...require("./render-view"),
  ...require("./sanitise"),
  ...require("./session-details"),
  ...require("./handle-path-request"),
  ...require("./unescape-specific"),
};
