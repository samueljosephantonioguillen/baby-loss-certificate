import {
  firstParentContextPath,
  secondParentContextPath,
} from "../application/paths/context-path";
import { renderSessionEndedPageContent } from "./error-page-content";

const registerSessionEndedError = (app) => {
  app.get(
    [
      firstParentContextPath("/session-ended"),
      secondParentContextPath("/session-ended"),
    ],
    renderSessionEndedPageContent("session-ended")
  );
};

export { registerSessionEndedError };
