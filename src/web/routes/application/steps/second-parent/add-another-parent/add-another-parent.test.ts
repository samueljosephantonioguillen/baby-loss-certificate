const isEmpty = jest.fn();

jest.mock("express-validator", () => ({
  validationResult: () => ({
    isEmpty: isEmpty,
  }),
}));

import expect from "expect";

import {
  addAnotherParent,
  pageContent,
  validate,
  behaviourForGet,
  behaviourForPost,
} from "./add-another-parent";
import { buildSessionForJourney } from "../../../flow-control/test-utils/test-utils";
import {
  IN_PROGRESS,
  IN_REVIEW,
  IN_REVIEW_PROGRESS,
} from "../../../flow-control/states";
import { CHECK_ANSWERS_URL } from "../../../paths/paths";
import { contextPath } from "../../../paths/context-path";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";

test("addAnotherParent should match expected outcomes", () => {
  const expectedResults = {
    path: "/would-you-like-to-add-another-parent",
    template: "add-another-parent",
    pageContent,
    validate,
    behaviourForGet,
    behaviourForPost,
  };

  expect(addAnotherParent).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("addAnotherParent.title"),
    sectionTitle: translate("section.certificateDetails"),
    heading: translate("addAnotherParent.heading"),
    yes: translate("yes"),
    no: translate("no"),
    continueButtonText: translate("buttons:continue"),
    backLinkText: translate("buttons:back"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("behaviourForGet", () => {
  const config = {};
  const journey = { name: MAINFLOW.name };

  test("should call set to IN_REVIEW when in IN_REVIEW_PROGRESS state", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "add-another-parent": "yes",
          "their-relation-to-baby": "mother",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_REVIEW_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = { locals: { previous: undefined } };
    const next = jest.fn();

    behaviourForGet(config, journey)(req, res, next);

    const expectedData = {
      "add-another-parent": "no",
    };
    expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(expectedData);
    expect(req.session.journeys[journey.name].state).toEqual(IN_REVIEW);
    expect(res.locals.previous).toBe(
      contextPath(CHECK_ANSWERS_URL, MAINFLOW.name)
    );
    expect(next).toBeCalledTimes(1);
  });

  test("should call next when not in IN_REVIEW_PROGRESS state", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "relation-to-baby": "",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = { locals: { previous: undefined } };
    const next = jest.fn();

    behaviourForGet(config, journey)(req, res, next);

    expect(res.locals.previous).toBe(undefined);
    expect(next).toBeCalledTimes(1);
  });
});

describe("behaviourForPost", () => {
  const config = {};
  const journey = { name: MAINFLOW.name };

  test("should return expected data when no option is selected", () => {
    isEmpty.mockReturnValue(true);

    const req = {
      body: { "add-another-parent": "no" },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "their-relation-to-baby": "father",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = {};
    const next = jest.fn();

    const expectedReq = {
      body: { "add-another-parent": "no" },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {},
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };

    behaviourForPost(config, journey)(req, res, next);
    expect(req).toEqual(expectedReq);
    expect(next).toBeCalledTimes(1);
  });

  test("should set state to IN_REVIEW_PROGRESS when IN_REVIEW and second parent is empty", () => {
    const req = {
      body: { "add-another-parent": "yes" },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "their-relation-to-baby": "",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_REVIEW,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = {};
    const next = jest.fn();

    const expectedSession = {
      journeyName: MAINFLOW.name,
      [MAINFLOW_DATA_OBJECT_NAME]: {
        "their-relation-to-baby": "",
      },
      ...buildSessionForJourney({
        journeyName: journey.name,
        state: IN_REVIEW_PROGRESS,
        nextAllowedPath: undefined,
      }),
    };

    behaviourForPost(config, journey)(req, res, next);

    expect(req.session).toEqual(expectedSession);
    expect(next).toBeCalledTimes(1);
  });

  test("should call when validation error", () => {
    isEmpty.mockReturnValue(false);

    const req = {};
    const res = {};
    const next = jest.fn();

    behaviourForPost(config, journey)(req, res, next);
    expect(next).toBeCalledTimes(1);
  });
});
