import { userIsBackOffice } from "../../../../azure-routes/auth";
import { getFlowDataObjectName } from "../../../tools/data-object";

const pageContent = ({ translate }) => ({
  title: translate("noContactDetails.title"),
  heading: translate("noContactDetails.heading"),
  bodyLineOne: translate("noContactDetails.body.firstLine"),
  bodyLineTwo: translate("noContactDetails.body.secondLine"),
  bodyLineThree: translate("noContactDetails.body.thirdLine"),
  backLinkText: translate("buttons:back"),
});

const isNavigable = (req) => {
  return (
    req.session[getFlowDataObjectName(req.session.journeyName)][
      "noContactDetails"
    ] === true && !userIsBackOffice(req)
  );
};

const noContactDetails = {
  path: "/cannot-request-cannot-find-contact-details",
  template: "not-eligible",
  pageContent,
  isNavigable,
};

export { noContactDetails, pageContent, isNavigable };
