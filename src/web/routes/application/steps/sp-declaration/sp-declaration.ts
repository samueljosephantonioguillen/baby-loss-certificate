import { SQSClient, SendMessageCommand } from "@aws-sdk/client-sqs";
import { isNilOrEmpty } from "../../../../../common/predicates";
import { logger } from "../../../../logger/logger";
import {
  blankContextPath,
  secondParentContextPath,
} from "../../paths/context-path";
import { SECOND_PARENT_DATA_OBJECT_NAME } from "../../tools/data-object";
import { v4 as uuidv4 } from "uuid";
import { REQUEST_COMPLETE_URL } from "../../paths/paths";
import { COMPLETED } from "../../flow-control/states";
import { stateMachine } from "../../flow-control/state-machine";
import { validate } from "./validate";
import { validationResult } from "express-validator";
import {
  SecondParentApplicationRequest,
  SecondParentRequest,
} from "../common/types/types";
import { MAINFLOW } from "../../journey-definitions";
import { userIsAgent } from "../../../azure-routes/auth";

const pageContent = ({ translate }) => ({
  title: translate("spDeclaration.title"),
  body: translate("spDeclaration.body", {
    contextPath: blankContextPath(MAINFLOW.name),
  }),
});

const isNavigable = (req) =>
  req.session[SECOND_PARENT_DATA_OBJECT_NAME]["are-details-correct"] === "yes";

function setApplicationType(req) {
  let applicationType = "citizen";
  if (userIsAgent(req)) {
    applicationType = "agent";
  }
  return applicationType;
}

const mapToSecondParentApplicationRequest = (
  req
): SecondParentApplicationRequest => {
  const applicationType = setApplicationType(req);
  return {
    applicationId:
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["application-id"],
    applicationUpdatedBy: applicationType,
  };
};

const mapToSecondParentRequest = (req): SecondParentRequest => {
  const dataObjectName = SECOND_PARENT_DATA_OBJECT_NAME;
  const applicationType = setApplicationType(req);
  return {
    parentId: req.session[dataObjectName]["sp-parent-id"],
    parentReferenceNumber:
      req.session[dataObjectName]["reference-number"] ||
      req.session[dataObjectName]["pds-nhs-number"] ||
      req.session[dataObjectName]["nhs-number"],
    parentReferenceType: 1,
    parentReferenceNumberIssuingCountry: "GBR",
    parentGivenName: req.session[dataObjectName]["first-name"],
    parentFamilyName: req.session[dataObjectName]["last-name"],
    applicationCreatedBy: applicationType,
  };
};

const behaviourForPost = (config, journey) => async (req, res, next) => {
  if (!validationResult(req).isEmpty()) {
    return next();
  }
  if (isNilOrEmpty(req.session[SECOND_PARENT_DATA_OBJECT_NAME].submitted)) {
    req.session[SECOND_PARENT_DATA_OBJECT_NAME].submitted = true;

    const client = new SQSClient({
      region: "eu-west-2",
      endpoint: config.environment.AWS_CLIENT_ENDPOINT,
    });

    const secondParentApplicationRequest =
      mapToSecondParentApplicationRequest(req);
    const secondParentRequest = mapToSecondParentRequest(req);

    const messageBody = {
      secondParentApplicationRequest,
      secondParentRequest,
    };

    try {
      const response = await client.send(
        new SendMessageCommand({
          QueueUrl: config.environment.XEROX_QUEUE_URL,
          MessageBody: JSON.stringify(messageBody),
          MessageGroupId: uuidv4(),
        })
      );
      logger.info(
        `SQS message sent successfully: { id: ${response.MessageId}, hash: ${response.MD5OfMessageBody}, correlation-id: ${secondParentApplicationRequest.applicationId} }`,
        req
      );
    } catch (error) {
      logger.error(error, req);
    }

    stateMachine.setState(COMPLETED, req, journey);
    return res.redirect(secondParentContextPath(REQUEST_COMPLETE_URL));
  }
};

const spDeclaration = {
  path: "/declaration",
  template: "sp-declaration",
  pageContent,
  validate,
  behaviourForPost,
  isNavigable,
};

export { spDeclaration, pageContent, validate, behaviourForPost, isNavigable };
