import expect from "expect";
import { configureSessionDetails } from "./session-details";
import {
  CRM_FLOW,
  MAINFLOW,
  SECOND_PARENT_FLOW,
} from "../../journey-definitions";
import {
  MAINFLOW_DATA_OBJECT_NAME,
  SECOND_PARENT_DATA_OBJECT_NAME,
} from "../../tools/data-object";
import { START } from "../../../start";
import { SP_START } from "../../../sp-start";

const mainflow = {
  name: MAINFLOW.name,
  pathsInSequence: ["/first", "/second"],
};

const secondflow = {
  name: SECOND_PARENT_FLOW.name,
  pathsInSequence: ["/first", "/second"],
};

const crmflow = {
  name: CRM_FLOW.name,
  pathsInSequence: ["/first", "/second"],
};

jest.mock("uuid", () => ({
  v4: jest.fn(() => "mocked-uuid"),
}));

import { logger } from "../../../../logger/logger";
import { contextPath } from "../../paths/context-path";
import { CRM_FIRST_STEP } from "../../register-journey-routes";
jest.mock("../../../../logger/logger", () => ({
  logger: {
    debug: jest.fn(),
  },
}));

test("configureSessionDetails() sets trace id locator, applicationId and [MAINFLOW_DATA_OBJECT_NAME]: on res.locals in mainflow", () => {
  const journey = mainflow;

  const req = {
    session: {
      [MAINFLOW_DATA_OBJECT_NAME]: {
        name: "Lisa",
      },
      locator: "580847510",
    },
  };

  const res = { locals: {} };
  const next = jest.fn();

  const expectedLocals = {
    dataObjectName: MAINFLOW_DATA_OBJECT_NAME,
    [MAINFLOW_DATA_OBJECT_NAME]: {
      name: "Lisa",
    },
    locator: "580847510",
    contextPath: "/test-context",
    serviceStart: START,
    journeyName: MAINFLOW.name,
  };

  configureSessionDetails(journey)(req, res, next);
  expect(res.locals).toEqual(expectedLocals);
  expect(next).toHaveBeenCalled();
});

test("configureSessionDetails() sets trace id locator and [SECOND_PARENT_FLOW_DATA_OBJECT_NAME]: on res.locals in secondflow", () => {
  const journey = secondflow;

  const req = {
    session: {
      [SECOND_PARENT_DATA_OBJECT_NAME]: {
        name: "Lisa",
      },
      locator: "580847510",
    },
  };

  const res = { locals: {} };
  const next = jest.fn();

  const expectedLocals = {
    dataObjectName: SECOND_PARENT_DATA_OBJECT_NAME,
    [SECOND_PARENT_DATA_OBJECT_NAME]: {
      name: "Lisa",
    },
    locator: "580847510",
    contextPath: "/sp-test-context",
    serviceStart: process.env.SP_CONTEXT_PATH + SP_START,
    journeyName: SECOND_PARENT_FLOW.name,
  };

  configureSessionDetails(journey)(req, res, next);
  expect(res.locals).toEqual(expectedLocals);
  expect(next).toHaveBeenCalled();
});

test("configureSessionDetails() initialises journey and applicationId in session for mainflow when session.journeys is undefined", () => {
  const journey = mainflow;
  const req = { session: { journey: undefined } };
  const res = { locals: {} };
  const next = jest.fn();

  const expectedSession = {
    journey: undefined,
    journeyName: "MAINFLOW",
    journeys: { MAINFLOW: { nextAllowedStep: "/first", state: "IN_PROGRESS" } },
    stepData: {},
    locator: "mocked-uuid",
    applicationId: "mocked-uuid",
  };

  configureSessionDetails(journey)(req, res, next);
  expect(req.session).toEqual(expectedSession);
  expect(next).toHaveBeenCalled();
});

test("configureSessionDetails() successfully redirects to mainflow start when session is MAINFLOW and journey is secondflow", () => {
  const journey = mainflow;
  const req = {
    session: {
      journey: SECOND_PARENT_DATA_OBJECT_NAME,
      journeyName: secondflow,
      destroy: jest.fn(),
    },
  };
  const res = { locals: {}, redirect: jest.fn(), clearCookie: jest.fn() };
  const next = jest.fn();

  configureSessionDetails(journey)(req, res, next);
  expect(logger.debug).toHaveBeenCalledWith("journey mismatch");
  expect(res.redirect).toHaveBeenCalledWith(contextPath(START, mainflow.name));
  expect(next).not.toHaveBeenCalled();
});

test("configureSessionDetails() successfully redirects to secondflow start when session is MAINFLOW and journey is secondflow", () => {
  const journey = secondflow;
  const req = {
    session: {
      journey: MAINFLOW_DATA_OBJECT_NAME,
      journeyName: mainflow,
      destroy: jest.fn(),
    },
  };
  const res = { locals: {}, redirect: jest.fn(), clearCookie: jest.fn() };
  const next = jest.fn();

  configureSessionDetails(journey)(req, res, next);
  expect(logger.debug).toHaveBeenCalledWith("journey mismatch");
  expect(res.redirect).toHaveBeenCalledWith(
    contextPath(START, secondflow.name)
  );
  expect(next).not.toHaveBeenCalled();
});

test("configureSessionDetails() successfully redirects to crm start when session is mismatched and journey is crmflow", () => {
  const journey = crmflow;
  const req = {
    session: {
      journey: MAINFLOW_DATA_OBJECT_NAME,
      journeyName: mainflow,
      destroy: jest.fn(),
    },
  };
  const res = { locals: {}, redirect: jest.fn(), clearCookie: jest.fn() };
  const next = jest.fn();

  configureSessionDetails(journey)(req, res, next);
  expect(logger.debug).toHaveBeenCalledWith("journey mismatch");
  expect(res.redirect).toHaveBeenCalledWith(CRM_FIRST_STEP);
  expect(next).not.toHaveBeenCalled();
});

test("configureSessionDetails() initialises journey and does not initialise applicationId in session for second parent flow when session.journeys is undefined", () => {
  const journey = secondflow;
  const req = { session: { journey: undefined } };
  const res = { locals: {} };
  const next = jest.fn();

  const expectedSession = {
    journey: undefined,
    journeyName: "SECOND_PARENT_FLOW",
    journeys: {
      SECOND_PARENT_FLOW: { nextAllowedStep: "/first", state: "IN_PROGRESS" },
    },
    stepData: {},
    locator: "mocked-uuid",
  };

  configureSessionDetails(journey)(req, res, next);
  expect(req.session).toEqual(expectedSession);
  expect(next).toHaveBeenCalled();
});
