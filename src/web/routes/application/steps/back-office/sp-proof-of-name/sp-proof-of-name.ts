import { userIsBackOffice } from "../../../../azure-routes/auth";
import { getFlowDataObjectName } from "../../../tools/data-object";
import { validate } from "./validate";

const pageContent = ({ translate }) => ({
  title: translate("proofOfName.title"),
  sectionTitle: translate("section.otherParentsDetails"),
  heading: translate("proofOfName.heading"),
  help: translate("proofOfName.help"),
  passport: translate("proofOfName.radios.passport"),
  birthCertificate: translate("proofOfName.radios.birthCertificate"),
  eeaMemberCard: translate("proofOfName.radios.eeaMemberCard"),
  drivingLicence: translate("proofOfName.radios.drivingLicence"),
  oldDrivingLicence: translate("proofOfName.radios.oldDrivingLicence"),
  photoRegistration: translate("proofOfName.radios.photoRegistration"),
  benefitBook: translate("proofOfName.radios.benefitBook"),
  residencePermit: translate("proofOfName.radios.residencePermit"),
  nationalIdentity: translate("proofOfName.radios.nationalIdentity"),
  backLinkText: translate("buttons:back"),
  continueButtonText: translate("buttons:continue"),
});

const isNavigable = (req) =>
  userIsBackOffice(req) &&
  !!req.session[getFlowDataObjectName(req.session.journeyName)][
    "their-date-of-birth"
  ] &&
  !!req.session[getFlowDataObjectName(req.session.journeyName)][
    "their-date-of-birth-day"
  ] &&
  !!req.session[getFlowDataObjectName(req.session.journeyName)][
    "their-date-of-birth-month"
  ] &&
  !!req.session[getFlowDataObjectName(req.session.journeyName)][
    "their-date-of-birth-year"
  ];

const behaviourForPost = () => (req, res, next) => {
  const dataObjectName = getFlowDataObjectName(req.session.journeyName);
  if (
    req.body["their-reference-number-type"] !==
    req.session[dataObjectName]["their-reference-number-type"]
  ) {
    req.session[dataObjectName]["their-reference-number"] = undefined;
  }
  req.session[dataObjectName]["their-reference-number-type"] =
    req.body["their-reference-number-type"];
  next();
};

const spProofOfName = {
  path: "/other-parent-proof-of-name",
  template: "sp-proof-of-name",
  isNavigable,
  pageContent,
  validate,
  behaviourForPost,
};

export { spProofOfName, isNavigable, pageContent, validate, behaviourForPost };
