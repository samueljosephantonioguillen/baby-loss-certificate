import { randomInt } from "crypto";

export const generateOTP = () => {
  const otpLength = 6;
  const characters = "0123456789";
  let otp = "";

  for (let i = 0; i < otpLength; i++) {
    const randomIndex = randomInt(0, characters.length);
    otp += characters.charAt(randomIndex);
  }

  return otp;
};
