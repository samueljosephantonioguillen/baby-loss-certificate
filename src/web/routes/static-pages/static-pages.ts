import { renderCookies } from "./cookies";
import { renderAccessibilityStatement } from "./accessibility-statement";
import { renderTermsOfUse } from "./terms-of-use";
import { renderPrivacyNotice } from "./privacy-notice";
import { renderContactUs } from "./contact-us";

export const registerStaticPages = (app) => {
  renderCookies(app);
  renderAccessibilityStatement(app);
  renderTermsOfUse(app);
  renderPrivacyNotice(app);
  renderContactUs(app);
};
