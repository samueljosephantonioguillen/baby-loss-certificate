import expect from "expect";
import { validate } from "./validate";
import { applyExpressValidation } from "../common/test/apply-express-validation";

test("return no errors when reference number is valid", async () => {
  const req = {
    body: {
      "sp-reference-number": "9AD-A86-E586D",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const error = result.array()[0];

  expect(result.array().length).toBe(0);
  expect(error).toBeFalsy();
});

test("when missing field, return expected error", async () => {
  const req = {
    body: {},
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const expectedError = {
    type: "field",
    value: undefined,
    msg: "validation:missingReferenceNumber",
    path: "sp-reference-number",
    location: "body",
  };

  const error = result.array()[0];

  expect(result.array().length).toBe(1);
  expect(error).toEqual(expectedError);
});

test("when field is too long, return expected error", async () => {
  const req = {
    body: {
      "sp-reference-number": "9AD-E586D-9AD-E586D-9AD-E586D-9AD-E586D",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const expectedError = {
    type: "field",
    value: req.body["sp-reference-number"],
    msg: "validation:referenceNumberTooLong",
    path: "sp-reference-number",
    location: "body",
  };

  const error = result.array()[0];

  expect(result.array().length).toBe(1);
  expect(error).toEqual(expectedError);
});

test("when invalid field, return expected error", async () => {
  const req = {
    body: {
      "sp-reference-number": "*$%!-E586D",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const expectedError = {
    type: "field",
    value: req.body["sp-reference-number"],
    msg: "validation:patternReferenceNumber",
    path: "sp-reference-number",
    location: "body",
  };

  const error = result.array()[0];

  expect(result.array().length).toBe(1);
  expect(error).toEqual(expectedError);
});
