import { check } from "express-validator";

import { translateValidationMessage } from "../../common/translate-validation-message";
import { isNilOrEmpty } from "../../../../../../common/predicates";

const acceptableReferenceNumberTypes = [
  "nhsNumber",
  "passport",
  "birthCertificate",
  "eeaMemberCard",
  "drivingLicence",
  "oldDrivingLicence",
  "photoRegistration",
  "benefitBook",
  "residencePermit",
  "nationalIdentity",
];

const isValid = (_, { req }) => {
  if (isNilOrEmpty(req.body["their-reference-number-type"])) {
    return false;
  }
  if (
    !acceptableReferenceNumberTypes.includes(
      req.body["their-reference-number-type"]
    )
  ) {
    return false;
  }
  return true;
};

const validate = () => [
  check("their-reference-number-type")
    .custom(isValid)
    .withMessage(translateValidationMessage("validation:selectProofOfName")),
];

export { isValid, validate, acceptableReferenceNumberTypes };
