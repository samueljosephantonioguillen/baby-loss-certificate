import { body } from "express-validator";

import { isNilOrEmpty } from "../../../../../../common/predicates";
import { toDateString } from "../../common/format-date";
import { translateValidationMessage } from "../../common/translate-validation-message";
import {
  isGivenYearDoesNotMatch,
  isFullDate,
  isMonthYearDate,
  isYearOnlyDate,
  validateFullDateOfLoss,
  validatePartialMonthYearDateOfLoss,
  validatePartialYearOnlyDateOfLoss,
} from "./date-loss";
import { getFlowDataObjectName } from "../../../tools/data-object";

const addLossDateToBody = (req, res, next) => {
  req.body["baby-date-of-loss"] = toDateString(
    req.body["baby-date-of-loss-day"],
    req.body["baby-date-of-loss-month"],
    req.body["baby-date-of-loss-year"]
  );

  next();
};

const validateDateOfLoss = (lossDate, { req }) => {
  if (isNilOrEmpty(lossDate)) {
    throw new Error(req.t("validation:missingDateOfLoss"));
  }

  if (isNilOrEmpty(req.body["baby-date-of-loss-year"])) {
    return true;
  }
  if (
    /\D/.test(req.body["baby-date-of-loss-day"]) ||
    /\D/.test(req.body["baby-date-of-loss-month"]) ||
    /\D/.test(req.body["baby-date-of-loss-year"])
  ) {
    return true;
  }

  if (isFullDate(req)) {
    validateFullDateOfLoss(lossDate, req);
    return true;
  }

  if (isMonthYearDate(req)) {
    validatePartialMonthYearDateOfLoss(lossDate, req);
    return true;
  }

  if (isYearOnlyDate(req)) {
    validatePartialYearOnlyDateOfLoss(lossDate, req);
    return true;
  }

  throw new Error(req.t("validation:dateOfLossNotReal"));
};

const isValidYear = (lossDate, { req }) => {
  // if year contains non number characters
  if (/\D/.test(req.body["baby-date-of-loss-year"])) {
    throw new Error(req.t("validation:dateOfLossNotRealYear"));
  }
  if (
    isNilOrEmpty(req.body["baby-date-of-loss-year"]) &&
    isNilOrEmpty(req.body["baby-date-of-loss-month"]) &&
    isNilOrEmpty(req.body["baby-date-of-loss-day"])
  ) {
    return true;
  }

  // check not empty
  if (isNilOrEmpty(req.body["baby-date-of-loss-year"])) {
    throw new Error(req.t("validation:missingDateOfLossYear"));
  }

  if (isNilOrEmpty(req.body["baby-date-of-loss-year"])) {
    return false;
  }

  // check year is same as year of loss
  if (isGivenYearDoesNotMatch(req)) {
    throw new Error(
      req.t("validation:yearDoesNotMatch", {
        yearOfLoss:
          req.session[getFlowDataObjectName(req.session.journeyName)][
            "year-of-loss"
          ],
      })
    );
  }
  return true;
};

const isValidDay = (lossDate, { req }) => {
  // if day contains non number characters
  if (/\D/.test(req.body["baby-date-of-loss-day"])) {
    throw new Error(req.t("validation:dateOfLossNotRealDay"));
  }
  return true;
};

const isValidMonth = (lossDate, { req }) => {
  // if month contains non number characters
  if (/\D/.test(req.body["baby-date-of-loss-month"])) {
    throw new Error(req.t("validation:dateOfLossNotRealMonth"));
  }
  return true;
};

const validate = () => [
  body("include-baby-date-of-loss")
    .not()
    .isEmpty()
    .bail()
    .withMessage(
      translateValidationMessage("validation:selectInludeBabyDateOfLoss")
    ),

  addLossDateToBody,

  body("baby-date-of-loss")
    .if(body("include-baby-date-of-loss").equals("yes"))
    .custom(validateDateOfLoss),

  body("baby-date-of-loss-year")
    .if(body("include-baby-date-of-loss").equals("yes"))
    .custom(isValidYear),

  body("baby-date-of-loss-month")
    .if(body("include-baby-date-of-loss").equals("yes"))
    .custom(isValidMonth),

  body("baby-date-of-loss-day")
    .if(body("include-baby-date-of-loss").equals("yes"))
    .custom(isValidDay),
];

export { validate };
