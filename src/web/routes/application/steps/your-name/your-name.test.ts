import expect from "expect";

import {
  yourName,
  pageContent,
  isNavigable,
  sanitise,
  validate,
} from "./your-name";
import { config } from "../../../../../config";
import { MAINFLOW } from "../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../tools/data-object";
import { BACKOFFICE_ROLE } from "../../constants";

test("yourName should match expected outcomes", () => {
  const expectedResults = {
    path: "/your-name",
    template: "your-name",
    pageContent,
    isNavigable,
    sanitise,
    validate,
  };

  expect(yourName).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("yourName.title"),
    sectionTitle: translate("section.yourDetails"),
    heading: translate("yourName.heading"),
    body: translate("yourName.body"),
    hint: translate("yourName.hint"),
    firstName: translate("yourName.firstName"),
    lastName: translate("yourName.lastName"),
    insetTextLineOne: translate("yourName.insetTextFirstLine"),
    backLinkText: translate("buttons:back"),
    continueButtonText: translate("buttons:continue"),
  };
  const result = pageContent({
    translate,
  });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when know-your-nhs-number is no", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "know-your-nhs-number": "no",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when know-your-nhs-number is yes", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "know-your-nhs-number": "yes",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when know-your-nhs-number is not defined", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "know-your-nhs-number": undefined,
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return true when know-your-nhs-number is no and user is back office", () => {
    config.environment.USE_AUTHENTICATION = true;
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "know-your-nhs-number": "no",
        },
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return true when know-your-nhs-number is yes and user is back office", () => {
    config.environment.USE_AUTHENTICATION = true;
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "know-your-nhs-number": "yes",
        },
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return true when know-your-nhs-number is not defined and user is back office", () => {
    config.environment.USE_AUTHENTICATION = true;
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "know-your-nhs-number": undefined,
        },
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });
});
