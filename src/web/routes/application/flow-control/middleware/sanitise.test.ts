import expect from "expect";
import { sanitise, sanitiseBody } from "./sanitise";

test("sanitise calls next", () => {
  const req = {
    body: {
      lastName: "    whitespace     ",
    },
  };
  const res = {};
  const next = jest.fn();

  sanitise(req, res, next);

  expect(next).toBeCalledTimes(1);
});

test("sanitiseBody with extra whitespace", () => {
  const body = {
    lastName: "    whitespace     ",
  };
  const expected = {
    lastName: "whitespace",
  };
  const result = sanitiseBody(body);

  expect(result).toEqual(expected);
});

test("sanitiseBody", () => {
  const body = {
    firstName: "<script>window.alert('Boo')</script>",
  };
  const expected = {
    firstName: "&lt;script&gt;window.alert(&#39;Boo&#39;)&lt;/script&gt;",
  };
  const result = sanitiseBody(body);
  expect(result).toEqual(expected);
});

test("does not escape spaces", () => {
  const body = {
    addressLine1: "Flat B",
  };
  const expected = {
    addressLine1: "Flat B",
  };
  const result = sanitiseBody(body);
  expect(result).toEqual(expected);
});
