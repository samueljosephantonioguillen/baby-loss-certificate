// This line must come before importing any instrumented module.
import { config } from "../config";
import express from "express";
import server from "./server";

const app = express();

server.initialise(config, app);
