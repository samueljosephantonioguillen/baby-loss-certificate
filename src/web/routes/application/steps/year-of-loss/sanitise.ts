const trimLeadingZero = (input) => input.replace(/\b0+/g, "");

const sanitise = () => (req, res, next) => {
  req.body["year-of-loss"] = trimLeadingZero(req.body["year-of-loss"]);

  next();
};

export { sanitise, trimLeadingZero };
