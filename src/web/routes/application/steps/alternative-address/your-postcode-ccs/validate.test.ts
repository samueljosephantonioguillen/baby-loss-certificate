import expect from "expect";

import { applyExpressValidation } from "../../common/test/apply-express-validation";
import { validate } from "./validate";
import { AGENT_ROLE } from "../../../constants";

test("return no errors when postcode is valid", async () => {
  const req = {
    account: {
      idTokenClaims: {
        roles: [AGENT_ROLE],
      },
    },
    body: {
      "postcode-ccs": "B3 2DX",
      sanitisedPostcodeCcs: "B3 2DX",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const error = result.array()[0];

  expect(result.array().length).toBe(0);
  expect(error).toBeFalsy();
});

test("when missing field, return expected error", async () => {
  const req = {
    account: {
      idTokenClaims: {
        roles: [AGENT_ROLE],
      },
    },
    body: {},
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const expectedError = {
    type: "field",
    value: undefined,
    msg: "validation:missingPostcode",
    path: "postcode-ccs",
    location: "body",
  };

  const error = result.array()[0];

  expect(result.array().length).toBe(1);
  expect(error).toEqual(expectedError);
});

test("when invalid field, return expected error", async () => {
  const req = {
    account: {
      idTokenClaims: {
        roles: [AGENT_ROLE],
      },
    },
    body: {
      "postcode-ccs": "invalid postcode",
      sanitisedPostcodeCcs: "invalid postcode",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("async result is undefined after waiting");
  }

  const expectedError = {
    type: "field",
    value: req.body["postcode-ccs"],
    msg: "validation:invalidPostcode",
    path: "postcode-ccs",
    location: "body",
  };

  const error = result.array()[0];

  expect(result.array().length).toBe(1);
  expect(error).toEqual(expectedError);
});
