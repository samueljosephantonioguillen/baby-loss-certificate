import { logger } from "../../../../../../web/logger/logger";
import { contextPath } from "../../../paths/context-path";
import { START } from "../../../../start";
import { CRM_SEARCH_URL, REQUEST_COMPLETE_URL } from "../../../paths/paths";
import {
  stepNotNavigable,
  completedJourneyExistsInSession,
} from "./predicates";
import { stateMachine } from "../../state-machine";
import {
  IS_PATH_ALLOWED,
  GET_NEXT_ALLOWED_PATH,
} from "../../state-machine/actions";
import { sessionDestroyButKeepLoggedIn } from "../../../steps/common/session-destroy";
import { CRM_FLOW } from "../../../journey-definitions";
import { userIsAgent, userIsBackOffice } from "../../../../azure-routes/auth";

const handleRequestForPath = (journey, step?) => (req, res, next) => {
  logger.info(`[${req.method}] ${req.path}`, req);

  // Destroy the session (not auth session data) if any COMPLETED journeys exists in session, and path is not a REQUEST_COMPLETE_URL path
  // and if user is agent and requested path is CRM_SEARCH_URL
  if (
    completedJourneyExistsInSession(req) &&
    !req.path.endsWith(REQUEST_COMPLETE_URL)
  ) {
    logger.info("Sanitizing session as state is COMPLETED", req);
    sessionDestroyButKeepLoggedIn(req, res);
    res.clearCookie("lang");

    if (
      (userIsAgent(req) || userIsBackOffice(req)) &&
      req.path.endsWith(CRM_SEARCH_URL)
    ) {
      return res.redirect(contextPath(CRM_SEARCH_URL, CRM_FLOW.name));
    }

    return res.redirect(contextPath(START, journey.name));
  }

  const nextAllowedPath = stateMachine.dispatch(
    GET_NEXT_ALLOWED_PATH,
    req,
    journey
  );

  const isPathAllowed = stateMachine.dispatch(IS_PATH_ALLOWED, req, journey);
  // Redirect to nextAllowedPath on invalid path request
  if (!isPathAllowed) {
    logger.info(`Path is not allowed, redirecting to ${nextAllowedPath}`, req);
    return res.redirect(nextAllowedPath);
  }

  // If step is not navigable then return to the next allowed path
  if (stepNotNavigable(step, req)) {
    logger.info(
      `Step is not navigable, redirecting to ${nextAllowedPath}`,
      req
    );
    return res.redirect(nextAllowedPath);
  }

  next();
};

export { handleRequestForPath };
