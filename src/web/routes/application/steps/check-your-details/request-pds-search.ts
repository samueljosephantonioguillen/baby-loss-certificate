import axios from "axios";
import { DateTime } from "luxon";
import { v4 as uuidv4 } from "uuid";
import { logger } from "../../../../logger/logger";
import {
  deceasedDateTime,
  filterAddresses,
  filterContactMethods,
  filterNames,
  matchSearchName,
} from "./pds-filters";
import { getPdsOauthToken } from "./get-pds-oauth-token";
import axiosRetry from "axios-retry";
import { toTitleCase } from "../common/formats";
import { getFlowDataObjectName } from "../../tools/data-object";

const REQUEST_TIMEOUT = 30 * 1000;

const requestPdsSearch = async (req, config) => {
  const dataObjectName = getFlowDataObjectName(req.session.journeyName);
  const { PDS_URI } = config.environment;
  const lastName = req.session[dataObjectName]["last-name"];
  const firstName = req.session[dataObjectName]["first-name"];
  const dateOfBirth = req.session[dataObjectName]["date-of-birth"];
  const postcode = req.session[dataObjectName].postcode;
  const uri = `${PDS_URI}/Patient?family=${lastName}&given=${firstName}&birthdate=eq${dateOfBirth}&address-postalcode=${postcode}`;

  try {
    const oauthToken = await getPdsOauthToken(req);
    const uuid = uuidv4();
    const requestStartTime = DateTime.now();

    axiosRetry(axios, {
      retryDelay: () => 200,
      retries: 3,
      retryCondition() {
        logger.warn(`Error with Axios Call`);
        return true;
      },
    });
    const response = await axios.get(uri, {
      headers: {
        Authorization: `Bearer ${oauthToken}`,
        "X-Request-ID": uuid,
      },
      timeout: REQUEST_TIMEOUT,
    });

    const requestEndTime = DateTime.now();
    const requestTimeInSeconds =
      requestEndTime.diff(requestStartTime).milliseconds;
    logger.debug(
      `PDS search (non-NHS number) API request time in milliseconds: ${requestTimeInSeconds}`
    );

    if (response.data?.issue) {
      logger.error("PDS search (non-NHS number) returned too many matches");
      throw new TypeError(
        "PDS search (non-NHS number) response returned too many results"
      );
    }

    if (!response?.data?.entry || response.data.entry.length === 0) {
      logger.error("PDS search (non-NHS number) returned no matches");
      throw new TypeError(
        "PDS search (non-NHS number) response returned no results"
      );
    }
    const patient = response.data.entry[0].resource;

    const phoneNumbers = filterContactMethods(patient, "phone");
    const phoneNumber = phoneNumbers?.[0]?.value ?? null;

    const emails = filterContactMethods(patient, "email");
    const email = emails?.[0]?.value ?? null;

    const names = filterNames(patient);
    const matchedName = matchSearchName(
      names,
      req.session[dataObjectName]["first-name"],
      req.session[dataObjectName]["last-name"]
    );

    const homeAddresses = filterAddresses(patient);
    let homeAddressLines = [];
    let postcodeFromResponse = null;
    if (homeAddresses?.length > 0) {
      homeAddressLines = homeAddresses[0].line;
      postcodeFromResponse = homeAddresses[0].postalCode.toUpperCase();
    }

    const hasDeceasedDateTime = deceasedDateTime(patient);

    const nhsNumber = patient.id || null;

    return {
      firstName: matchedName.firstName
        ? toTitleCase(matchedName.firstName)
        : null,
      lastName: matchedName.lastName ? toTitleCase(matchedName.lastName) : null,
      dateOfBirth: patient.birthDate || null,
      email: email,
      phoneNumber: phoneNumber,
      addressLines: homeAddressLines,
      postcode: postcodeFromResponse,
      hasDeceasedDateTime: hasDeceasedDateTime,
      nhsNumber: nhsNumber,
    };
  } catch (error) {
    let errorMsg = "Error caught in non-NHS number PDS search";
    if (error.response) {
      errorMsg += ` - Response Code: ${error.response.status}`;
    }
    if (error.message) {
      errorMsg += ` - ${error.message}`;
    }
    logger.error(errorMsg, req);
    throw error;
  }
};

export { requestPdsSearch };
