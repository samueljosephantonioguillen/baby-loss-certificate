import axios from "axios";

import * as TEST_FIXTURES from "./test-fixtures.json";
import {
  standardisePostcode,
  getAddressLookupResults,
  transformAddress,
  transformOsPlacesApiResponse,
} from "./os-places";

jest.mock("axios");

const config = {
  environment: {
    OS_PLACES_URI: "https://api.os.uk",
    OS_PLACES_API_KEY: "123",
  },
};

test("standardisePostcode standardises the postcode", () => {
  expect(standardisePostcode("AB1 1AB")).toBe("AB11AB");
  expect(standardisePostcode("AB1      1AB")).toBe("AB11AB");
  expect(standardisePostcode("   AB1 1AB ")).toBe("AB11AB");
  expect(standardisePostcode("ab11ab")).toBe("AB11AB");
});

test("transformAddress transforms address correctly", () => {
  const result = transformAddress(TEST_FIXTURES.results[0]);

  const expected = {
    ADDRESS: "Alan Jeffery Engineering, 1, Valley Road, Plymouth",
    ORGANISATION_NAME: "ALAN JEFFERY ENGINEERING",
    BUILDING_NUMBER: "1",
    DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
    THOROUGHFARE_NAME: "VALLEY ROAD",
    POST_TOWN: "PLYMOUTH",
    POSTCODE: "PL7 1RF",
    LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
    UDPRN: "50265368",
  };

  expect(result).toEqual(expected);
});

describe("transformOsPlacesApiResponse", () => {
  test("should transform OS Places API response correctly", () => {
    const result = transformOsPlacesApiResponse(TEST_FIXTURES);

    const expected = [
      {
        ADDRESS: "Alan Jeffery Engineering, 1, Valley Road, Plymouth",
        ORGANISATION_NAME: "ALAN JEFFERY ENGINEERING",
        BUILDING_NUMBER: "1",
        DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
        THOROUGHFARE_NAME: "VALLEY ROAD",
        POST_TOWN: "PLYMOUTH",
        POSTCODE: "PL7 1RF",
        LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
        UDPRN: "50265368",
      },
      {
        ADDRESS: "Dulux Decorator Centre, 2, Valley Road, Plymouth",
        ORGANISATION_NAME: "DULUX DECORATOR CENTRE",
        BUILDING_NUMBER: "2",
        DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
        THOROUGHFARE_NAME: "VALLEY ROAD",
        POST_TOWN: "PLYMOUTH",
        POSTCODE: "PL7 1RF",
        LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
        UDPRN: "19000955",
      },
      {
        ADDRESS: "Mill Autos, 3, Valley Road, Plymouth",
        ORGANISATION_NAME: "MILL AUTOS",
        BUILDING_NUMBER: "3",
        DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
        THOROUGHFARE_NAME: "VALLEY ROAD",
        POST_TOWN: "PLYMOUTH",
        POSTCODE: "PL7 1RF",
        LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
        UDPRN: "19000927",
      },
      {
        ADDRESS:
          "Goat Hill Farm, 2, Troll Bridge, Goat Hill, Slaithwaite, Slaith, Huddersfield",
        ORGANISATION_NAME: "GOAT HILL FARM",
        BUILDING_NUMBER: "2",
        DEPENDENT_THOROUGHFARE_NAME: "TROLL BRIDGE",
        THOROUGHFARE_NAME: "GOAT HILL",
        DOUBLE_DEPENDENT_LOCALITY: "SLAITHWAITE",
        DEPENDENT_LOCALITY: "SLAITH",
        POST_TOWN: "HUDDERSFIELD",
        POSTCODE: "HD7 5UZ",
        LOCAL_CUSTODIAN_CODE_DESCRIPTION: "KIRKLEES",
        UDPRN: "10668197",
      },
      {
        ADDRESS: "10a, Mayfield Avenue, Weston-Super-Mare",
        BUILDING_NAME: "10A",
        THOROUGHFARE_NAME: "MAYFIELD AVENUE",
        POST_TOWN: "WESTON-SUPER-MARE",
        POSTCODE: "BS22 6AA",
        LOCAL_CUSTODIAN_CODE_DESCRIPTION: "NORTH SOMERSET",
        UDPRN: "2916679",
      },
    ];

    expect(result).toEqual(expected);
  });

  test("should return empty array if no results on response", () => {
    const apiResponse = { results: undefined }; // TODO: check this with test-fixtures-no-results.json
    const result = transformOsPlacesApiResponse(apiResponse);
    const expected = [];

    expect(result).toEqual(expected);
  });
});

test("getAddressLookupResults calls os places with the correct arguments", async () => {
  const mock = jest.fn();
  axios.get = mock.mockResolvedValue("{}");
  await getAddressLookupResults(config, "AB1 1AB");
  const uri =
    "https://api.os.uk/search/places/v1/postcode?postcode=AB11AB&lr=en";
  const expectedOSPlacesRequestArgs = {
    timeout: 5000,
    headers: {
      key: "123",
    },
  };

  expect(mock).toBeCalledTimes(1);
  expect(mock).nthCalledWith(1, uri, expectedOSPlacesRequestArgs);
});
