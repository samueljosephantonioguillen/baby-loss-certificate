import expect from "expect";

import { behaviourForPost, findAddress } from "./behaviour-for-post";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";

describe("behaviourForPost", () => {
  test("should add transformed address to session data object", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          test: "test",
        },
        postcodeLookupResults: [
          {
            ADDRESS:
              "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF",
            ORGANISATION_NAME: "ALAN JEFFERY ENGINEERING",
            BUILDING_NUMBER: "1",
            THOROUGHFARE_NAME: "VALLEY ROAD",
            DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
            POST_TOWN: "PLYMOUTH",
            POSTCODE: "PL7 1RF",
            LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
            UDPRN: "50265368",
          },
          {
            ADDRESS:
              "DULUX DECORATOR CENTRE, 2, VALLEY ROAD, PLYMOUTH, PL7 1RF",
            ORGANISATION_NAME: "DULUX DECORATOR CENTRE",
            BUILDING_NUMBER: "2",
            THOROUGHFARE_NAME: "VALLEY ROAD",
            DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
            POST_TOWN: "PLYMOUTH",
            POSTCODE: "PL7 1RF",
            LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
            UDPRN: "19000955",
          },
        ],
      },
      body: {
        "selected-address":
          "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF",
      },
    };
    const res = {};
    const next = jest.fn();

    const expectedData = {
      test: "test",
      "address-line-1-ccs": "Alan Jeffery Engineering",
      "address-line-2-ccs": "1 Upper Valley Road, Valley Road",
      "address-line-3-ccs": "Plymouth",
      "postcode-ccs": "PL7 1RF",
      "address-id": "50265368",
    };

    behaviourForPost()(req, res, next);

    expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(expectedData);
    expect(next).toBeCalled();
  });

  test("should call next if no address is selected", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          test: "test",
        },
        postcodeLookupResults: [
          {
            ADDRESS:
              "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF",
            ORGANISATION_NAME: "ALAN JEFFERY ENGINEERING",
            BUILDING_NUMBER: "1",
            THOROUGHFARE_NAME: "VALLEY ROAD",
            DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
            POST_TOWN: "PLYMOUTH",
            POSTCODE: "PL7 1RF",
            LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
          },
          {
            ADDRESS:
              "DULUX DECORATOR CENTRE, 2, VALLEY ROAD, PLYMOUTH, PL7 1RF",
            ORGANISATION_NAME: "DULUX DECORATOR CENTRE",
            BUILDING_NUMBER: "2",
            THOROUGHFARE_NAME: "VALLEY ROAD",
            DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
            POST_TOWN: "PLYMOUTH",
            POSTCODE: "PL7 1RF",
            LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
          },
        ],
      },
      body: {},
    };
    const res = {};
    const next = jest.fn();

    behaviourForPost()(req, res, next);

    expect(next).toBeCalled();
  });
});

describe("findAddress", () => {
  test("should find the address matching the selected address", () => {
    const postcodeLookupResults = [
      {
        ADDRESS: "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF",
        ORGANISATION_NAME: "ALAN JEFFERY ENGINEERING",
        BUILDING_NUMBER: "1",
        THOROUGHFARE_NAME: "VALLEY ROAD",
        DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
        POST_TOWN: "PLYMOUTH",
        POSTCODE: "PL7 1RF",
        LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
      },
      {
        ADDRESS: "DULUX DECORATOR CENTRE, 2, VALLEY ROAD, PLYMOUTH, PL7 1RF",
        ORGANISATION_NAME: "DULUX DECORATOR CENTRE",
        BUILDING_NUMBER: "2",
        THOROUGHFARE_NAME: "VALLEY ROAD",
        DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
        POST_TOWN: "PLYMOUTH",
        POSTCODE: "PL7 1RF",
        LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
      },
    ];
    const selectedAddress =
      "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF";

    const expectedAddress = {
      ADDRESS: "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF",
      ORGANISATION_NAME: "ALAN JEFFERY ENGINEERING",
      BUILDING_NUMBER: "1",
      THOROUGHFARE_NAME: "VALLEY ROAD",
      DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
      POST_TOWN: "PLYMOUTH",
      POSTCODE: "PL7 1RF",
      LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
    };

    const address = findAddress(selectedAddress, postcodeLookupResults);

    expect(address).toEqual(expectedAddress);
  });

  test("should throw an error when the address is not found", () => {
    const postcodeLookupResults = [];
    const selectedAddress =
      "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF";

    expect(
      findAddress.bind(null, selectedAddress, postcodeLookupResults)
    ).toThrow(/cannot find selected address in postcode lookup results/);
  });
});
