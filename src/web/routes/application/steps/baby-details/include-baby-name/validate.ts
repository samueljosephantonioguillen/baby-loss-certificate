import { body, check } from "express-validator";
import { translateValidationMessage } from "../../common/translate-validation-message";

import { BABY_NAME_MAX_LENGTH, NAME_PATTERN } from "../../../constants";

const validate = () => [
  check("include-baby-name")
    .not()
    .isEmpty()
    .bail()
    .withMessage(
      translateValidationMessage("validation:selectIncludeBabyNameOption")
    ),

  check("baby-name")
    .if(body("include-baby-name").equals("yes"))
    .not()
    .isEmpty()
    .bail()
    .withMessage(translateValidationMessage("validation:missingBabyName"))
    .isLength({ max: BABY_NAME_MAX_LENGTH })
    .bail()
    .withMessage(translateValidationMessage("validation:babyNameTooLong"))
    .matches(NAME_PATTERN)
    .bail()
    .withMessage(translateValidationMessage("validation:patternBabyName")),
];

export { validate };
