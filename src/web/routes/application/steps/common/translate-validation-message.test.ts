import expect from "expect";
import * as sinon from "sinon";
import { translateValidationMessage } from "./translate-validation-message";

test("translateValidationMessage()", () => {
  const message = "this is a message for an invalid input";
  const value = "value";
  const translate = sinon.spy();
  const req = { t: translate };

  translateValidationMessage(message)(value, { req });
  expect(translate.calledWith(message)).toBe(true);
});
