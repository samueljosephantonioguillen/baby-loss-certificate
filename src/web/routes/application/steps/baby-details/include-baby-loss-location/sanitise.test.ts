import expect from "expect";

import { sanitise } from "./sanitise";

const next = jest.fn();

test("should trim whitespace from baby-loss-location", () => {
  const req = {
    body: {
      "baby-loss-location": "  Kent  ",
    },
  };
  const res = {};

  const expected = {
    body: {
      "baby-loss-location": "Kent",
    },
  };

  sanitise()(req, res, next);
  expect(req).toEqual(expected);
  expect(next).toBeCalledTimes(1);
});

test("should replace backtick with apostrophe from baby-loss-location", () => {
  const req = {
    body: {
      "baby-loss-location": "Cliff`s Hill",
    },
  };
  const res = {};

  const expected = {
    body: {
      "baby-loss-location": "Cliff's Hill",
    },
  };

  sanitise()(req, res, next);
  expect(req).toEqual(expected);
  expect(next).toBeCalledTimes(1);
});

test("should replace autofilled apostrophe with standard apostrophe from baby-loss-location", () => {
  const req = {
    body: {
      "baby-loss-location": "Cliff’s Hill",
    },
  };
  const res = {};

  const expected = {
    body: {
      "baby-loss-location": "Cliff's Hill",
    },
  };

  sanitise()(req, res, next);
  expect(req).toEqual(expected);
  expect(next).toBeCalledTimes(1);
});

test("should capitalise first letter of each word", () => {
  const req = {
    body: {
      "baby-loss-location": "32 kent street drive avenue",
    },
  };
  const res = {};

  const expected = {
    body: {
      "baby-loss-location": "32 Kent Street Drive Avenue",
    },
  };

  sanitise()(req, res, next);
  expect(req).toEqual(expected);
  expect(next).toBeCalledTimes(1);
});
