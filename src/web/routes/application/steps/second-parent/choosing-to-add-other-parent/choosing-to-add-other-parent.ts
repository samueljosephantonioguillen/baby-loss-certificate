import { userIsBackOffice } from "../../../../azure-routes/auth";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";

const pageContent = ({ translate }) => ({
  title: translate("choosingToAddOtherParent.title"),
  sectionTitle: translate("section.otherParentsDetails"),
  heading: translate("choosingToAddOtherParent.heading"),
  insetText: translate("choosingToAddOtherParent.insetText"),
  body: translate("choosingToAddOtherParent.body"),
  continueButtonText: translate("buttons:continue"),
});

const isNavigable = (req) =>
  req.session[MAINFLOW_DATA_OBJECT_NAME]["add-another-parent"] === "yes" &&
  !userIsBackOffice(req);

const choosingToAddOtherParent = {
  path: "/what-happens-next",
  template: "choosing-to-add-other-parent",
  pageContent,
  isNavigable,
};

export { choosingToAddOtherParent, pageContent, isNavigable };
