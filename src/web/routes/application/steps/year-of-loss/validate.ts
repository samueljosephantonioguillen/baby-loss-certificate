import { body } from "express-validator";
import { translateValidationMessage } from "../common/translate-validation-message";

const yearPattern = /^\d{4}$/;

const inLastFiveYears = (_, { req }) => {
  const givenYear = new Date(req.body["year-of-loss"], 0).getUTCFullYear();

  const eligibleYear = 2018;
  const pastDiff = givenYear - eligibleYear;

  const currentYear = new Date().getFullYear();
  const yearDiff = currentYear - givenYear;

  if (pastDiff < 0) {
    throw new Error(req.t("validation:yearOfLossAfterDate"));
  }

  if (yearDiff < 0) {
    throw new Error(req.t("validation:yearOfLossInFuture"));
  }

  return true;
};

const validate = () => [
  body("loss-in-five-years")
    .not()
    .isEmpty()
    .bail()
    .withMessage(translateValidationMessage("validation:selectYearOfLoss")),

  body("year-of-loss")
    .if(body("loss-in-five-years").equals("yes"))
    .not()
    .isEmpty()
    .bail()
    .withMessage(translateValidationMessage("validation:missingYearOfLoss"))
    .matches(yearPattern)
    .bail()
    .withMessage(translateValidationMessage("validation:invalidYearOfLoss"))
    .custom(inLastFiveYears),
];

export { validate };
