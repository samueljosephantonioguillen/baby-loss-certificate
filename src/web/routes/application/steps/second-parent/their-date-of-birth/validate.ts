import { body } from "express-validator";

import { isNilOrEmpty } from "../../../../../../common/predicates";
import { toDateString } from "../../common/format-date";
import {
  isDateRealDate,
  isDateInPast,
  isDateSixteenOrMoreYearsInThePast,
  isDateAHundredTwentyOrMoreYearsInThePast,
} from "../../common/validate-date";

const addDobToBody = (req, res, next) => {
  req.body["their-date-of-birth"] = toDateString(
    req.body["their-date-of-birth-day"],
    req.body["their-date-of-birth-month"],
    req.body["their-date-of-birth-year"]
  );
  next();
};

const validateDateOfBirth = (dob, { req }) => {
  if (isNilOrEmpty(dob)) {
    throw new Error(req.t("validation:otherParentMissingDateOfBirth"));
  }

  //  return true if any individual field is empty
  if (
    isNilOrEmpty(req.body["their-date-of-birth-day"]) ||
    isNilOrEmpty(req.body["their-date-of-birth-month"]) ||
    isNilOrEmpty(req.body["their-date-of-birth-year"])
  ) {
    return true;
  }

  // if day contains non number characters
  if (
    /\D/.test(req.body["their-date-of-birth-day"]) ||
    /\D/.test(req.body["their-date-of-birth-month"]) ||
    /\D/.test(req.body["their-date-of-birth-year"])
  ) {
    return true;
  }

  if (!isDateRealDate(dob)) {
    throw new Error(req.t("validation:otherParentDateOfBirthNotReal"));
  }

  if (!isDateInPast(dob)) {
    throw new Error(req.t("validation:otherParentDateOfBirthInFuture"));
  }

  if (isDateAHundredTwentyOrMoreYearsInThePast(dob)) {
    const oldestAge = 120;
    const today = new Date();
    const oldestDob = new Date();
    oldestDob.setFullYear(today.getFullYear() - oldestAge);
    const dd = oldestDob.getDate().toString().padStart(2, "0");
    const mm = (oldestDob.getMonth() + 1).toString().padStart(2, "0");
    const yyyy = oldestDob.getFullYear().toString();
    const dateString = `${dd}-${mm}-${yyyy}`;

    throw new Error(
      req.t("validation:otherParentDateOfBirthTooOld", {
        oldestDob: dateString,
      })
    );
  }

  if (!isDateSixteenOrMoreYearsInThePast(dob)) {
    throw new Error(req.t("validation:otherParentDateOfBirthTooYoung"));
  }

  return true;
};

const validateDateOfBirthDay = (dob, { req }) => {
  // if day contains non number characters
  if (/\D/.test(req.body["their-date-of-birth-day"])) {
    throw new Error(req.t("validation:dateOfBirthDayInvalid"));
  }
  if (
    isNilOrEmpty(req.body["their-date-of-birth-day"]) &&
    (!isNilOrEmpty(req.body["their-date-of-birth-month"]) ||
      !isNilOrEmpty(req.body["their-date-of-birth-year"]))
  ) {
    throw new Error(req.t("validation:otherParentMissingDateOfBirthDay"));
  }
  return true;
};

const validateDateOfBirthMonth = (dob, { req }) => {
  // if month contains non number characters
  if (/\D/.test(req.body["their-date-of-birth-month"])) {
    throw new Error(req.t("validation:dateOfBirthMonthInvalid"));
  }
  if (
    isNilOrEmpty(req.body["their-date-of-birth-month"]) &&
    (!isNilOrEmpty(req.body["their-date-of-birth-day"]) ||
      !isNilOrEmpty(req.body["their-date-of-birth-year"]))
  ) {
    throw new Error(req.t("validation:otherParentMissingDateOfBirthMonth"));
  }
  return true;
};

const validateDateOfBirthYear = (dob, { req }) => {
  // if year contains non number characters
  if (/\D/.test(req.body["their-date-of-birth-year"])) {
    throw new Error(req.t("validation:dateOfBirthYearInvalid"));
  }
  if (
    isNilOrEmpty(req.body["their-date-of-birth-year"]) &&
    (!isNilOrEmpty(req.body["their-date-of-birth-day"]) ||
      !isNilOrEmpty(req.body["their-date-of-birth-month"]))
  ) {
    throw new Error(req.t("validation:otherParentMissingDateOfBirthYear"));
  }
  return true;
};

const validate = () => [
  addDobToBody,

  body("their-date-of-birth").custom(validateDateOfBirth),

  body("their-date-of-birth-day").custom(validateDateOfBirthDay),
  body("their-date-of-birth-month").custom(validateDateOfBirthMonth),
  body("their-date-of-birth-year").custom(validateDateOfBirthYear),
];

export { validate };
