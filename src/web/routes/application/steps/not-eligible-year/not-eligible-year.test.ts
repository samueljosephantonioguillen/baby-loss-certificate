import expect from "expect";

import { notEligibleYear, pageContent, isNavigable } from "./not-eligible-year";
import { MAINFLOW } from "../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../tools/data-object";

test("notEligibleYear should match expected outcomes", () => {
  const expectedResults = {
    path: "/cannot-request-year-of-baby-loss",
    template: "not-eligible",
    pageContent,
    isNavigable,
  };

  expect(notEligibleYear).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("notEligibleYear.title"),
    heading: translate("notEligibleYear.heading"),
    supportOrganisationsHeading: translate("supportOrganisations.heading"),
    supportOrganisationsBody: translate("supportOrganisations.body"),
    supportOrganisationsList: translate("supportOrganisations.listItems"),
    backLinkText: translate("buttons:back"),
  };

  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when loss-in-five-years is no", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "loss-in-five-years": "no",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when loss-in-five-years is yes", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "loss-in-five-years": "yes",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});
