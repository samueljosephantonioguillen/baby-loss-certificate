import expect from "expect";

import {
  requestComplete,
  pageContent,
  behaviourForGet,
} from "./request-complete";
import { formatReference } from "../common/certificate-reference-generator";
import { contextPath } from "../../paths/context-path";
import { START } from "../../../start";
import { MAINFLOW } from "../../journey-definitions";
import { AGENT_ROLE, BACKOFFICE_ROLE } from "../../constants";

test("requestComplete should match expected outcomes", () => {
  const expectedResults = {
    path: "/confirmation",
    template: "request-complete",
    pageContent,
    behaviourForGet,
  };

  expect(requestComplete).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const certificateReference = "E10BE325539";
  const req = {
    session: {
      journeyName: MAINFLOW.name,
      certificateReference,
      applicant: { "add-another-parent": "yes" },
      account: {
        idTokenClaims: {
          roles: [AGENT_ROLE],
        },
      },
    },
  };
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("requestComplete.title"),
    requestComplete: translate("requestComplete.heading"),
    requestPending: translate("requestComplete.pendingHeading"),
    pendingSecondParent: true,
    yourReference: translate("requestComplete.yourReference", {
      certificateReference: formatReference(req.session.certificateReference),
    }),
    whatHappensNextHeading: translate(
      "requestComplete.whatHappensNext.heading"
    ),
    whatHappensNextLineOne: translate(
      "requestComplete.whatHappensNext.firstLine"
    ),
    whatHappensNextLineTwo: translate(
      "requestComplete.whatHappensNext.secondLine",
      { serviceStart: contextPath(START, req.session.journeyName) }
    ),
    pendingWhatHappensNextLineOne: translate(
      "requestComplete.pendingWhatHappensNext.firstLine"
    ),
    pendingWhatHappensNextLineTwo: translate(
      "requestComplete.pendingWhatHappensNext.secondLine"
    ),
    pendingWhatHappensNextLineThree: translate(
      "requestComplete.pendingWhatHappensNext.thirdLine",
      { serviceStart: contextPath(START, req.session.journeyName) }
    ),
    supportOrganisationsHeading: translate("supportOrganisations.heading"),
    supportOrganisationsBody: translate("supportOrganisations.body"),
    supportOrganisationsList: translate("supportOrganisations.listItems"),
    link: translate("requestComplete.link"),
  };
  const result = pageContent({ translate, req });

  expect(result).toEqual(expected);
});

test("pageContent should return expected results with no other parent", () => {
  const certificateReference = "E10BE325539";
  const req = {
    session: {
      journeyName: MAINFLOW.name,
      certificateReference,
      applicant: { "add-another-parent": "no" },
      account: {
        idTokenClaims: {
          roles: [BACKOFFICE_ROLE],
        },
      },
    },
  };
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("requestComplete.title"),
    requestComplete: translate("requestComplete.heading"),
    requestPending: translate("requestComplete.pendingHeading"),
    pendingSecondParent: false,
    yourReference: translate("requestComplete.yourReference", {
      certificateReference: formatReference(req.session.certificateReference),
    }),
    whatHappensNextHeading: translate(
      "requestComplete.whatHappensNext.heading"
    ),
    whatHappensNextLineOne: translate(
      "requestComplete.whatHappensNext.firstLine"
    ),
    whatHappensNextLineTwo: translate(
      "requestComplete.whatHappensNext.secondLine",
      { serviceStart: contextPath(START, req.session.journeyName) }
    ),
    pendingWhatHappensNextLineOne: translate(
      "requestComplete.pendingWhatHappensNext.firstLine"
    ),
    pendingWhatHappensNextLineTwo: translate(
      "requestComplete.pendingWhatHappensNext.secondLine"
    ),
    pendingWhatHappensNextLineThree: translate(
      "requestComplete.pendingWhatHappensNext.thirdLine",
      { serviceStart: contextPath(START, req.session.journeyName) }
    ),
    supportOrganisationsHeading: translate("supportOrganisations.heading"),
    supportOrganisationsBody: translate("supportOrganisations.body"),
    supportOrganisationsList: translate("supportOrganisations.listItems"),
    link: translate("requestComplete.link"),
  };
  const result = pageContent({ translate, req });

  expect(result).toEqual(expected);
});

test("pageContent should return expected results with as other parent", () => {
  const certificateReference = "E10BE325539";
  const req = {
    session: {
      journeyName: MAINFLOW.name,
      certificateReference,
      secondParent: {},
    },
  };
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("requestComplete.title"),
    requestComplete: translate("requestComplete.heading"),
    requestPending: translate("requestComplete.pendingHeading"),
    pendingSecondParent: false,
    yourReference: translate("requestComplete.yourReference", {
      certificateReference: formatReference(req.session.certificateReference),
    }),
    whatHappensNextHeading: translate(
      "requestComplete.whatHappensNext.heading"
    ),
    whatHappensNextLineOne: translate(
      "requestComplete.whatHappensNext.firstLine"
    ),
    whatHappensNextLineTwo: translate(
      "requestComplete.whatHappensNext.secondLine",
      { serviceStart: contextPath(START, req.session.journeyName) }
    ),
    pendingWhatHappensNextLineOne: translate(
      "requestComplete.pendingWhatHappensNext.firstLine"
    ),
    pendingWhatHappensNextLineTwo: translate(
      "requestComplete.pendingWhatHappensNext.secondLine"
    ),
    pendingWhatHappensNextLineThree: translate(
      "requestComplete.pendingWhatHappensNext.thirdLine",
      { serviceStart: contextPath(START, req.session.journeyName) }
    ),
    supportOrganisationsHeading: translate("supportOrganisations.heading"),
    supportOrganisationsBody: translate("supportOrganisations.body"),
    supportOrganisationsList: translate("supportOrganisations.listItems"),
    link: translate("requestComplete.link"),
  };
  const result = pageContent({ translate, req });

  expect(result).toEqual(expected);
});

test("behaviourForGet should set Clear-Site-Data header", () => {
  const req = {};
  const res = { set: jest.fn(), locals: {} };
  const next = jest.fn();

  behaviourForGet()(req, res, next);

  expect(res.set).toBeCalledTimes(1);
  expect(res.set).toBeCalledWith("Clear-Site-Data", '"storage"');
  expect(next).toBeCalledTimes(1);
});
