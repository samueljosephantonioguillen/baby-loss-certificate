import expect from "expect";
import express from "express";

import { configureAuthentication } from "../application/flow-control/middleware/configure-authentication";
import { config } from "../../../config";

const mockSessionDestroy = jest.fn();
jest.mock("../application/steps/common/session-destroy", () => ({
  sessionDestroyButKeepLoggedIn: mockSessionDestroy,
}));

jest.mock("../../../common/azure-utils", () => ({
  isUserAuthenticated: jest.fn(),
}));

import { registerStartRoute, START } from "./start";

const res = {
  redirect: jest.fn(),
  cookie: jest.fn(),
  clearCookie: jest.fn(),
  status: jest.fn().mockReturnThis(),
  render: jest.fn(),
};
const next = jest.fn();
const app = {
  use: jest.fn(),
};

beforeEach(() => {
  jest.clearAllMocks();
});

test("registerStartRoute should destroy session and return page content", () => {
  const router = {
    ...jest.requireActual("express"),
    get: jest.fn(),
  };
  jest.spyOn(express, "Router").mockImplementationOnce(() => router);

  config.environment.USE_AUTHENTICATION = true;

  const req = {
    t: (string) => string,
    session: {
      destroy: jest.fn(),
      isAuthenticated: true,
      account: {
        username: "test@mail.com",
      },
    },
  };

  const expectedPageContent = {
    title: "start.title",
    heading: "start.heading",
    bodyLineOne: "start.body.firstLine",
    bodyLineTwo: "start.body.secondLine",
    bodyLineThree: "start.body.thirdLine",
    requestACertificateHeading: "start.requestACertificate.heading",
    requestACertificateLineOne: "start.requestACertificate.firstLine",
    requestACertificateListItemOne: "start.requestACertificate.firstListItem",
    requestACertificateListItemTwo: "start.requestACertificate.secondListItem",
    requestACertificateListItemThree: "start.requestACertificate.thirdListItem",
    requestACertificateListItemFour: "start.requestACertificate.fourthListItem",
    requestACertificateListItemFive: "start.requestACertificate.fifthListItem",
    requestACertificateListItemSix: "start.requestACertificate.sixthListItem",
    requestACertificateLineTwo: "start.requestACertificate.secondLine",
    requestACertificateLineThree: "start.requestACertificate.thirdLine",
    beforeYouStartHeading: "start.beforeYouStart.heading",
    beforeYouStartLineOne: "start.beforeYouStart.firstLine",
    beforeYouStartLineTwo: "start.beforeYouStart.secondLine",
    beforeYouStartListItemOne: "start.beforeYouStart.firstListItem",
    bodyLineFive: "start.body.fifthLine",
    bodyLineFour: "start.body.fourthLine",
    startNow: "start.startNow",
    startPath: "/test-context/live-in-england",
  };

  router.get.mockImplementation((path, configureAuthentication, callback) => {
    callback(req, res, next);
  });

  registerStartRoute(app);

  expect(app.use).toBeCalledTimes(2);
  expect(router.get).toBeCalledTimes(1);
  expect(router.get).toHaveBeenNthCalledWith(
    1,
    "/test-context" + START,
    configureAuthentication,
    expect.anything()
  );
  expect(mockSessionDestroy).toBeCalled();
  expect(mockSessionDestroy).toBeCalledWith(req, res);
  expect(res.clearCookie).toBeCalledWith("lang");
  expect(res.render).toBeCalledWith("start", expectedPageContent);
});

test("registerStartRoute should redirect user if not authenticated", () => {
  const router = {
    ...jest.requireActual("express"),
    get: jest.fn(),
  };
  jest.spyOn(express, "Router").mockImplementationOnce(() => router);

  const req = {
    t: (string) => string,
    session: {
      destroy: jest.fn(),
      isAuthenticated: false,
    },
  };

  router.get.mockImplementation((path, configureAuthentication, callback) => {
    callback(req, res, next);
  });

  config.environment.USE_AUTHENTICATION = false;

  registerStartRoute(app);

  expect(app.use).toBeCalledTimes(2);
  expect(router.get).toBeCalledTimes(1);
  expect(router.get).toHaveBeenNthCalledWith(
    1,
    "/test-context" + START,
    configureAuthentication,
    expect.anything()
  );
  expect(res.redirect).toBeCalledTimes(1);
  expect(res.redirect).toBeCalledWith(process.env.START_PAGE_URL);
});
