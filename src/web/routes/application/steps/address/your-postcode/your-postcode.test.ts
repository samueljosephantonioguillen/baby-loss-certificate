import expect from "expect";

import {
  yourPostcode,
  pageContent,
  isNavigable,
  sanitise,
  validate,
} from "./your-postcode";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";

test("yourPostcode should match expected outcomes", () => {
  const expectedResults = {
    path: "/your-postcode",
    template: "your-postcode",
    pageContent,
    isNavigable,
    sanitise,
    validate,
  };

  expect(yourPostcode).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("yourPostcode.title"),
    sectionTitle: translate("section.yourDetails"),
    heading: translate("yourPostcode.heading"),
    body: translate("yourPostcode.body"),
    postcode: translate("yourPostcode.postcode"),
    backLinkText: translate("buttons:back"),
    continueButtonText: translate("buttons:continue"),
    firstParentFlow: true,
  };
  const result = pageContent({
    translate,
    req: {
      session: {
        journeyName: MAINFLOW.name,
      },
    },
  });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when know-your-nhs-number is no and date-of-birth fields populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "know-your-nhs-number": "no",
          "date-of-birth": "1/1/1999",
          "date-of-birth-day": "1",
          "date-of-birth-month": "1",
          "date-of-birth-year": "1999",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when know-your-nhs-number is yes and date-of-birth fields populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "know-your-nhs-number": "yes",
          "date-of-birth": "1/1/1999",
          "date-of-birth-day": "1",
          "date-of-birth-month": "1",
          "date-of-birth-year": "1999",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when know-your-nhs-number is yes and date-of-birth fields are not populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "know-your-nhs-number": "yes",
          "date-of-birth": undefined,
          "date-of-birth-day": undefined,
          "date-of-birth-month": undefined,
          "date-of-birth-year": undefined,
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});
