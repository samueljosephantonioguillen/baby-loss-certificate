import expect from "expect";
import { isErrorStatusCode } from "./predicates";
import { StatusCodes } from "http-status-codes";

test("isErrorStatusCode() returns true for error status code", () => {
  expect(isErrorStatusCode(StatusCodes.BAD_REQUEST)).toBe(true);
  expect(isErrorStatusCode(StatusCodes.INTERNAL_SERVER_ERROR)).toBe(true);
  expect(isErrorStatusCode(StatusCodes.OK)).toBe(false);
});
