import * as express from "express";
import { configureAuthentication } from "../application/flow-control/middleware/configure-authentication";
import { sessionDestroyButKeepLoggedIn } from "../application/steps/common/session-destroy";
import { SP_FIRST_STEP } from "../application/register-journey-routes";
import { firstParentContextPath } from "../application/paths/context-path";
import { START } from "../start";

const SP_CONTEXT_PATH = process.env.SP_CONTEXT_PATH ?? "";
const SP_START = "/request-a-baby-loss-certificate";

const pageContent = ({ translate }) => ({
  title: translate("spStart.title"),
  heading: translate("spStart.heading"),
  bodyLineOne: translate("spStart.body.firstLine"),
  bodyLineTwo: translate("spStart.body.secondLine"),
  requestACertificateHeading: translate("spStart.requestACertificate.heading"),
  requestACertificateLineOne: translate(
    "spStart.requestACertificate.firstLine"
  ),
  requestACertificateLineTwo: translate(
    "spStart.requestACertificate.secondLine"
  ),
  requestACertificateLineThree: translate(
    "spStart.requestACertificate.thirdLine"
  ),
  requestACertificateLineFour: translate(
    "spStart.requestACertificate.fourthLine",
    {
      path: firstParentContextPath(START),
    }
  ),
  requestACertificateLineFive: translate(
    "spStart.requestACertificate.fifthLine"
  ),
  aboutTheCertificateHeading: translate("spStart.aboutTheCertificate.heading"),
  aboutTheCertificateInsetText: translate(
    "spStart.aboutTheCertificate.insetText"
  ),
  aboutTheCertificateLineOne: translate(
    "spStart.aboutTheCertificate.firstLine"
  ),
  aboutTheCertificateListItemOne: translate(
    "spStart.aboutTheCertificate.firstListItem"
  ),
  aboutTheCertificateListItemTwo: translate(
    "spStart.aboutTheCertificate.secondListItem"
  ),
  aboutTheCertificateListItemThree: translate(
    "spStart.aboutTheCertificate.thirdListItem"
  ),
  aboutTheCertificateListItemFour: translate(
    "spStart.aboutTheCertificate.fourthListItem"
  ),
  aboutTheCertificateListItemFive: translate(
    "spStart.aboutTheCertificate.fifthListItem"
  ),
  aboutTheCertificateListItemSix: translate(
    "spStart.aboutTheCertificate.sixthListItem"
  ),
  aboutTheCertificateLineTwo: translate(
    "spStart.aboutTheCertificate.secondLine"
  ),
  aboutTheCertificateLineThree: translate(
    "spStart.aboutTheCertificate.thirdLine"
  ),
  beforeYouStartHeading: translate("spStart.beforeYouStart.heading"),
  beforeYouStartLineOne: translate("spStart.beforeYouStart.firstLine"),
  beforeYouStartListItemOne: translate("spStart.beforeYouStart.firstListItem"),
  beforeYouStartListItemTwo: translate("spStart.beforeYouStart.secondListItem"),
  beforeYouStartListItemThree: translate(
    "spStart.beforeYouStart.thirdListItem"
  ),
  startNow: translate("spStart.startNow"),
  startPath: SP_FIRST_STEP,
});

const renderSecondParentStartRoute = (app) => {
  const router = express.Router();
  app.use((req, res, next) => {
    res.set("Clear-Site-Data", '"storage"');
    next();
  });
  app.use(router);

  // This will create a redirect on the route to go to the start url
  router.get(SP_CONTEXT_PATH, configureAuthentication, (req, res) => {
    res.redirect(SP_CONTEXT_PATH + SP_START);
  });

  router.get(
    SP_CONTEXT_PATH + SP_START,
    configureAuthentication,
    (req, res) => {
      sessionDestroyButKeepLoggedIn(req, res);
      res.clearCookie("lang");
      res.render("sp-start", {
        ...pageContent({ translate: req.t }),
      });
    }
  );
};

const registerSecondParentStartRoute = (app) =>
  renderSecondParentStartRoute(app);

export { registerSecondParentStartRoute, SP_START, pageContent };
