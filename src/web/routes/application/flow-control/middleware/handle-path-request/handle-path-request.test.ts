import expect from "expect";
import { REQUEST_COMPLETE_URL } from "../../../paths/paths";
import { IN_PROGRESS, COMPLETED } from "../../states";
import { buildSessionForJourney } from "../../test-utils/test-utils";

const MAINFLOW = "MAINFLOW";

const journey = {
  name: MAINFLOW,
  steps: [{ path: "/first", next: () => "/second" }, { path: "/second" }],
  pathsInSequence: ["/first", "/second"],
};

const mockSessionDestroy = jest.fn();
const mockSessionDestroyButKeepLoggedIn = jest.fn();

jest.mock("../../../steps/common/session-destroy", () => ({
  sessionDestroy: mockSessionDestroy,
  sessionDestroyButKeepLoggedIn: mockSessionDestroyButKeepLoggedIn,
}));

import { handleRequestForPath } from "./handle-path-request";
import { AGENT_ROLE, BACKOFFICE_ROLE } from "../../../constants";
import { config } from "../../../../../../config";

config.environment.USE_AUTHENTICATION = false;

test("handleRequestForPath should redirect to next allowed step if requested path is not allowed", () => {
  const req = {
    path: "/second",
    session: {
      ...buildSessionForJourney({
        journeyName: MAINFLOW,
        state: IN_PROGRESS,
        nextAllowedPath: "/first",
      }),
    },
  };

  const redirect = jest.fn();
  const res = { redirect };
  const next = jest.fn();

  handleRequestForPath(journey)(req, res, next);

  expect(redirect).toBeCalledWith("/first");
  expect(next).toBeCalledTimes(0);
});

test("handleRequestForPath should call next() if requested path is allowed", () => {
  const req = {
    path: "/second",
    session: {
      ...buildSessionForJourney({
        journeyName: MAINFLOW,
        state: IN_PROGRESS,
        nextAllowedPath: "/second",
      }),
    },
  };

  const redirect = jest.fn();
  const res = { redirect };
  const next = jest.fn();

  handleRequestForPath(journey)(req, res, next);

  expect(redirect).toBeCalledTimes(0);
  expect(next).toBeCalled();
});

test(`handleRequestForPath should call mockSessionDestroyButKeepLoggedIn and redirect to external service start when navigating away from ${REQUEST_COMPLETE_URL} to a path in sequence`, () => {
  const clearCookie = jest.fn();
  const redirect = jest.fn();
  const next = jest.fn();

  const req = {
    path: "/second",
    session: {
      ...buildSessionForJourney({
        journeyName: MAINFLOW,
        state: COMPLETED,
        nextAllowedPath: undefined,
      }),
    },
  };

  const res = {
    clearCookie,
    redirect,
  };

  handleRequestForPath(journey)(req, res, next);

  expect(mockSessionDestroyButKeepLoggedIn).toBeCalled();
  expect(mockSessionDestroyButKeepLoggedIn).toBeCalledWith(req, res);
  expect(clearCookie).toBeCalledWith("lang");
  expect(redirect).toBeCalledWith(process.env.START_PAGE_URL);
});

test(`handleRequestForPath should call mockSessionDestroy and redirects to external service start when a ${COMPLETED} journey exists in session`, () => {
  const clearCookie = jest.fn();
  const redirect = jest.fn();
  const next = jest.fn();

  const req = {
    path: "/second",
    session: {
      ...buildSessionForJourney({
        journeyName: MAINFLOW,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
      ...buildSessionForJourney({
        journeyName: "another-journey",
        state: COMPLETED,
        nextAllowedPath: undefined,
      }),
    },
  };

  const res = {
    clearCookie,
    redirect,
  };

  handleRequestForPath(journey)(req, res, next);

  expect(mockSessionDestroyButKeepLoggedIn).toBeCalled();
  expect(mockSessionDestroyButKeepLoggedIn).toBeCalledWith(req, res);
  expect(clearCookie).toBeCalledWith("lang");
  expect(redirect).toBeCalledWith(process.env.START_PAGE_URL);
});

test("handleRequestForPath should allow redirect to CRM search-details for agent in COMPLETED state", () => {
  config.environment.USE_AUTHENTICATION = true;

  const req = {
    path: "/search-details",
    session: {
      account: {
        idTokenClaims: {
          roles: [AGENT_ROLE],
        },
      },
      ...buildSessionForJourney({
        journeyName: MAINFLOW,
        state: COMPLETED,
        nextAllowedPath: undefined,
      }),
    },
  };

  const redirect = jest.fn();
  const clearCookie = jest.fn();
  const res = { redirect, clearCookie };
  const next = jest.fn();

  handleRequestForPath(journey)(req, res, next);

  expect(redirect).toBeCalledTimes(1);
  expect(redirect).toBeCalledWith("/crm-test-context/search-details");
});

test("handleRequestForPath should allow redirect to CRM search-details for agent in COMPLETED state", () => {
  config.environment.USE_AUTHENTICATION = true;

  const req = {
    path: "/search-details",
    session: {
      account: {
        idTokenClaims: {
          roles: [BACKOFFICE_ROLE],
        },
      },
      ...buildSessionForJourney({
        journeyName: MAINFLOW,
        state: COMPLETED,
        nextAllowedPath: undefined,
      }),
    },
  };

  const redirect = jest.fn();
  const clearCookie = jest.fn();
  const res = { redirect, clearCookie };
  const next = jest.fn();

  handleRequestForPath(journey)(req, res, next);

  expect(redirect).toBeCalledTimes(1);
  expect(redirect).toBeCalledWith("/crm-test-context/search-details");
});
