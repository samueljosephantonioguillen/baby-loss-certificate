import { validate } from "./validate";
import { sanitise } from "../sanitise";
import { getFlowDataObjectName } from "../../../tools/data-object";
import { MAINFLOW } from "../../../journey-definitions";

const pageContent = ({ translate, req }) => ({
  title: translate("yourPostcode.title"),
  sectionTitle: translate("section.yourDetails"),
  heading: translate("yourPostcode.heading"),
  body: translate("yourPostcode.body"),
  postcode: translate("yourPostcode.postcode"),
  backLinkText: translate("buttons:back"),
  continueButtonText: translate("buttons:continue"),
  firstParentFlow: req.session.journeyName === MAINFLOW.name,
});

const isNavigable = (req) =>
  req.session[getFlowDataObjectName(req.session.journeyName)][
    "know-your-nhs-number"
  ] === "no" &&
  !!req.session[getFlowDataObjectName(req.session.journeyName)][
    "date-of-birth"
  ] &&
  !!req.session[getFlowDataObjectName(req.session.journeyName)][
    "date-of-birth-day"
  ] &&
  !!req.session[getFlowDataObjectName(req.session.journeyName)][
    "date-of-birth-month"
  ] &&
  !!req.session[getFlowDataObjectName(req.session.journeyName)][
    "date-of-birth-year"
  ];

const yourPostcode = {
  path: "/your-postcode",
  template: "your-postcode",
  pageContent,
  isNavigable,
  sanitise,
  validate,
};

export { yourPostcode, pageContent, isNavigable, sanitise, validate };
