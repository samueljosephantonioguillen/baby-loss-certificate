import expect from "expect";
import * as sinon from "sinon";
import { callMiddlewareQueue } from "./apply-express-validation";

const REQ = "request";
const RES = "response";

const assertCorrectMiddlewareCall = (middleware) => {
  expect(middleware.calledOnce).toBe(true);
  expect(middleware.getCall(0).args[0]).toBe(REQ);
  expect(middleware.getCall(0).args[1]).toBe(RES);
};

test("callMiddlewareQueue() calls all middleware in queue", async () => {
  const middlewareOne = sinon.spy();
  const middlewareTwo = sinon.spy();
  const middlewareThree = sinon.spy();
  const middleware = [middlewareOne, middlewareTwo, middlewareThree];

  await callMiddlewareQueue(REQ, RES, middleware);
  assertCorrectMiddlewareCall(middlewareOne);
  assertCorrectMiddlewareCall(middlewareTwo);
  assertCorrectMiddlewareCall(middlewareThree);
});
