import expect from "expect";
import { sanitise } from "./sanitise";

test("sanitise() should trim the leading zeros", () => {
  const req = {
    body: {
      "year-of-loss": "030",
    },
  };

  const expectedReq = {
    body: {
      "year-of-loss": "30",
    },
  };

  const next = jest.fn();

  sanitise()(req, {}, next);
  expect(req).toEqual(expectedReq);
});
