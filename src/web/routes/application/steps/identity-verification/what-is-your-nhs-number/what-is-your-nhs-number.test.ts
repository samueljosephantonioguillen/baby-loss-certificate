import expect from "expect";

import {
  whatIsYourNhsNumber,
  pageContent,
  isNavigable,
  sanitise,
  validate,
} from "./what-is-your-nhs-number";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";
import { BACKOFFICE_ROLE } from "../../../constants";
import { config } from "../../../../../../config";

test("whatIsYourNhsNumber should match expected outcomes", () => {
  const expectedResults = {
    path: "/what-is-your-nhs-number",
    template: "what-is-your-nhs-number",
    pageContent,
    isNavigable,
    sanitise,
    validate,
  };

  expect(whatIsYourNhsNumber).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("whatIsYourNhsNumber.title"),
    sectionTitle: translate("section.yourDetails"),
    heading: translate("whatIsYourNhsNumber.heading"),
    hintLineOne: translate("whatIsYourNhsNumber.hint.firstLine"),
    hintLineTwo: translate("whatIsYourNhsNumber.hint.secondLine"),
    nhsNumber: translate("whatIsYourNhsNumber.nhsNumber"),
    backLinkText: translate("buttons:back"),
    insetText: translate("whatIsYourNhsNumber.insetText"),
    continueButtonText: translate("buttons:continue"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when know-your-nhs-number is yes", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "know-your-nhs-number": "yes",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when know-your-nhs-number is no", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "know-your-nhs-number": "no",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when know-your-nhs-number is not populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "know-your-nhs-number": undefined,
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when know-your-nhs-number is yes and user is back office", () => {
    config.environment.USE_AUTHENTICATION = true;
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "know-your-nhs-number": "yes",
        },
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when know-your-nhs-number is no and user is back office", () => {
    config.environment.USE_AUTHENTICATION = true;
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "know-your-nhs-number": "no",
        },
      },
      account: {
        idTokenClaims: {
          roles: [BACKOFFICE_ROLE],
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when know-your-nhs-number is not populated and user is back office", () => {
    config.environment.USE_AUTHENTICATION = true;
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "know-your-nhs-number": undefined,
        },
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});
