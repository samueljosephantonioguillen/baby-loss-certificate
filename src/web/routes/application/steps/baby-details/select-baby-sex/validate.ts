import { check } from "express-validator";
import { translateValidationMessage } from "../../common/translate-validation-message";

const validate = () => [
  check("baby-sex")
    .not()
    .isEmpty()
    .bail()
    .withMessage(translateValidationMessage("validation:selectSexOfBaby")),
];

export { validate };
