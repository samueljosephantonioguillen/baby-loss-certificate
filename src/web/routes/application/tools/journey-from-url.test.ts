import {
  CONTEXT_PATH,
  CRM_CONTEXT_PATH,
  SP_CONTEXT_PATH,
} from "../../static-pages/paths";
import { CRM_FLOW, MAINFLOW, SECOND_PARENT_FLOW } from "../journey-definitions";
import { getJourneyFromUrl } from "./journey-from-url";

test("return first parent journey name from url", () => {
  const returnedJourney = getJourneyFromUrl(CONTEXT_PATH);
  expect(returnedJourney).toEqual(MAINFLOW.name);
});

test("return first parent journey name from url with path", () => {
  const returnedJourney = getJourneyFromUrl(CONTEXT_PATH + "/path");
  expect(returnedJourney).toEqual(MAINFLOW.name);
});

test("return second parent journey name from url", () => {
  const returnedJourney = getJourneyFromUrl(SP_CONTEXT_PATH);
  expect(returnedJourney).toEqual(SECOND_PARENT_FLOW.name);
});

test("return second parent journey name from url with path", () => {
  const returnedJourney = getJourneyFromUrl(SP_CONTEXT_PATH + "/path");
  expect(returnedJourney).toEqual(SECOND_PARENT_FLOW.name);
});

test("return crm journey name from url", () => {
  const returnedJourney = getJourneyFromUrl(CRM_CONTEXT_PATH);
  expect(returnedJourney).toEqual(CRM_FLOW.name);
});

test("return crm journey name from url with path", () => {
  const returnedJourney = getJourneyFromUrl(CRM_CONTEXT_PATH + "/path");
  expect(returnedJourney).toEqual(CRM_FLOW.name);
});

test("return expected error if no url is passed in", () => {
  const testEmptyJourneyURL = null;
  const emptyUrlError1 = new TypeError(
    "Cannot read properties of null (reading 'split')"
  );
  const emptyUrlError2 = new TypeError("Cannot read property 'split' of null");
  try {
    getJourneyFromUrl(testEmptyJourneyURL);
  } catch (error) {
    expect([emptyUrlError1.message, emptyUrlError2.message]).toContain(
      error.message
    );
  }
});
