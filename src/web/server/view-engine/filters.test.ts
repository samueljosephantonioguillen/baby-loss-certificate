import expect from "expect";

import { toErrorList, getErrorForField } from "./filters";

const selectPregnancyLossInEngland =
  "Select yes if your baby loss happened in England";
const selectYearOfLoss =
  "Select yes if you had a baby loss in the last 5 years.";

const errors = [
  {
    path: "enter-location",
    msg: selectPregnancyLossInEngland,
  },
  {
    path: "loss-in-five-years",
    msg: selectYearOfLoss,
  },
];

test("toErrorList should return expected list of errors", () => {
  const expected = [
    {
      text: selectPregnancyLossInEngland,
      href: "#enter-location",
      attributes: {
        id: "error-link-enter-location",
      },
    },
    {
      text: selectYearOfLoss,
      href: "#loss-in-five-years",
      attributes: {
        id: "error-link-loss-in-five-years",
      },
    },
  ];

  const errorList = toErrorList(errors);

  expect(errorList).toEqual(expected);
});

describe("getErrorForField", () => {
  test("return nothing when no errors match given field", () => {
    const result = getErrorForField(errors, "relation-to-baby");
    expect(result).toBe(null);
  });

  test("return expected message when field matches list of errors", () => {
    const result = getErrorForField(errors, "enter-location");

    const expected = {
      text: selectPregnancyLossInEngland,
    };

    expect(result).toEqual(expected);
  });

  test("return nothing when no error list is provided", () => {
    const result = getErrorForField(undefined, "loss-in-five-years");
    expect(result).toEqual(null);
  });
});
