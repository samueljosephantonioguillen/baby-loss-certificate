import expect from "expect";

import {
  dateOfBirth,
  pageContent,
  isNavigable,
  sanitise,
  validate,
  behaviourForGet,
  behaviourForPost,
} from "./date-of-birth";

import { buildSessionForJourney } from "../../flow-control/test-utils/test-utils";
import {
  IN_PROGRESS,
  IN_DETAILS_REVIEW,
  IN_DETAILS_REVIEW_PROGRESS,
} from "../../flow-control/states";
import { CHECK_DETAILS_URL } from "../../paths/paths";
import { contextPath } from "../../paths/context-path";
import { BACKOFFICE_ROLE } from "../../constants";
import { config } from "../../../../../config";
import { MAINFLOW } from "../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../tools/data-object";

test("dateOfBirth should match expected outcomes", () => {
  const expectedResults = {
    path: "/date-of-birth",
    template: "date-of-birth",
    pageContent,
    isNavigable,
    sanitise,
    validate,
    behaviourForGet,
    behaviourForPost,
  };
  expect(dateOfBirth).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("dateOfBirth.title"),
    sectionTitle: translate("section.yourDetails"),
    heading: translate("dateOfBirth.heading"),
    hint: translate("dateOfBirth.hint"),
    day: translate("day"),
    month: translate("month"),
    year: translate("year"),
    backLinkText: translate("buttons:back"),
    continueButtonText: translate("buttons:continue"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  config.environment.USE_AUTHENTICATION = true;
  test("should return true when know-your-nhs-number is no and first-name and last-name are populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "know-your-nhs-number": "no",
          "first-name": "first",
          "last-name": "last",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when know-your-nhs-number is yes and first-name and last-name are populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "know-your-nhs-number": "yes",
          "first-name": "first",
          "last-name": "last",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when know-your-nhs-number is no and first-name and last-name are not populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "know-your-nhs-number": "no",
          "first-name": undefined,
          "last-name": undefined,
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return true when user is BackOffice and first-name and last-name are populated", () => {
    config.environment.USE_AUTHENTICATION = true;
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "know-your-nhs-number": undefined,
          "first-name": "first",
          "last-name": "last",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when user is BackOffice and first-name is not populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "know-your-nhs-number": undefined,
          "first-name": undefined,
          "last-name": "last",
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when user is BackOffice and last-name is not populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "know-your-nhs-number": undefined,
          "first-name": "first",
          "last-name": undefined,
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});

describe("behaviourForGet", () => {
  const config = {};
  const journey = { name: MAINFLOW.name };

  test("should call set to IN_DETAILS_REVIEW when in IN_DETAILS_REVIEW_PROGRESS state", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "date-of-birth": "",
          "date-of-birth-day": "",
          "date-of-birth-month": "",
          "date-of-birth-year": "",
        },
        submittedDob: "2000-01-01",
        submittedDobDay: "01",
        submittedDobMonth: "01",
        submittedDobYear: "01",
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_DETAILS_REVIEW_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = { locals: { previous: undefined } };
    const next = jest.fn();

    behaviourForGet(config, journey)(req, res, next);

    expect(req.session[MAINFLOW_DATA_OBJECT_NAME]["date-of-birth"]).toEqual(
      req.session.submittedDob
    );
    expect(req.session[MAINFLOW_DATA_OBJECT_NAME]["date-of-birth-day"]).toEqual(
      req.session.submittedDobDay
    );
    expect(
      req.session[MAINFLOW_DATA_OBJECT_NAME]["date-of-birth-month"]
    ).toEqual(req.session.submittedDobMonth);
    expect(
      req.session[MAINFLOW_DATA_OBJECT_NAME]["date-of-birth-year"]
    ).toEqual(req.session.submittedDobYear);
    expect(req.session.journeys[journey.name].state).toEqual(IN_DETAILS_REVIEW);
    expect(res.locals.previous).toBe(
      contextPath(CHECK_DETAILS_URL, MAINFLOW.name)
    );
    expect(next).toBeCalledTimes(1);
  });

  test("should call next when not in IN_REVIEW_PROGRESS state", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "date-of-birth": "",
          "date-of-birth-day": "",
          "date-of-birth-month": "",
          "date-of-birth-year": "",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = { locals: { previous: undefined } };
    const next = jest.fn();

    behaviourForGet(config, journey)(req, res, next);

    expect(res.locals.previous).toBe(undefined);
    expect(next).toBeCalledTimes(1);
  });
});

describe("behaviourForPost", () => {
  const config = {};
  const journey = { name: MAINFLOW.name };

  test("should set state to IN_REVIEW_PROGRESS when IN_REVIEW and age requirement is not met", () => {
    const dob = "2000-01-01";

    const req = {
      body: {
        "date-of-birth": "2023-01-01",
        "date-of-birth-day": "01",
        "date-of-birth-month": "01",
        "date-of-birth-year": "01",
      },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          olderThanAgeRequirement: false,
          "date-of-birth": "2000-01-01",
          "date-of-birth-day": "01",
          "date-of-birth-month": "01",
          "date-of-birth-year": "01",
        },
        submittedDob: "",
        submittedDobDay: "",
        submittedDobMonth: "",
        submittedDobYear: "",
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_DETAILS_REVIEW,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = {};
    const next = jest.fn();

    behaviourForPost(config, journey)(req, res, next);

    expect(req.session.submittedDob).toEqual(dob);
    expect(req.session.journeys[journey.name].state).toEqual(
      IN_DETAILS_REVIEW_PROGRESS
    );
  });

  test("should set state to IN_DETAILS_REVIEW when IN_DETAILS_REVIEW_PROGRESS and age requirement is met", () => {
    const req = {
      body: {
        "date-of-birth": "2000-01-01",
        "date-of-birth-day": "01",
        "date-of-birth-month": "01",
        "date-of-birth-year": "2000",
      },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: { olderThanAgeRequirement: true },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_DETAILS_REVIEW_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = {};
    const next = jest.fn();

    behaviourForPost(config, journey)(req, res, next);

    const expectedSession = {
      [MAINFLOW_DATA_OBJECT_NAME]: { olderThanAgeRequirement: true },
      journeyName: MAINFLOW.name,
      ...buildSessionForJourney({
        journeyName: journey.name,
        state: IN_DETAILS_REVIEW,
        nextAllowedPath: undefined,
      }),
    };

    expect(req.session).toEqual(expectedSession);
  });

  test("when state is IN_PROGRESS, remain IN_PROGRESS", () => {
    const req = {
      body: {
        "date-of-birth": "2000-01-01",
        "date-of-birth-day": "01",
        "date-of-birth-month": "01",
        "date-of-birth-year": "2000",
      },
      session: {
        journeyName: MAINFLOW.name,
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = {};
    const next = jest.fn();

    behaviourForPost(config, journey)(req, res, next);

    const expectedSession = {
      journeyName: MAINFLOW.name,
      ...buildSessionForJourney({
        journeyName: journey.name,
        state: IN_PROGRESS,
        nextAllowedPath: undefined,
      }),
    };

    expect(req.session).toEqual(expectedSession);
  });
});
