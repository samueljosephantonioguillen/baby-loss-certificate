import { registerProblemWithServiceError } from "./problem-with-service";

const PROBLEM_WITH_SERVICE_URL = "/test-context/problem-with-the-service";
const TIMED_OUT_URL = "/test-context/timed-out";
const SP_PROBLEM_WITH_SERVICE_URL = "/sp-test-context/problem-with-the-service";
const SP_TIMED_OUT_URL = "/sp-test-context/timed-out";

test(`registerProblemWithServiceError() should redirect to ${PROBLEM_WITH_SERVICE_URL} or ${TIMED_OUT_URL}`, () => {
  const req = { t: (string) => string };
  const app = {
    ...jest.requireActual("express"),
    use: jest.fn(),
    get: jest.fn(),
    all: jest.fn(),
  };
  const res = { render: jest.fn() };

  app.all.mockImplementation((path, configureAuthentication, callback) => {
    callback(req, res);
  });

  registerProblemWithServiceError(app);

  expect(app.get).toBeCalledTimes(1);

  expect(app.get).toHaveBeenNthCalledWith(
    1,
    [
      PROBLEM_WITH_SERVICE_URL,
      SP_PROBLEM_WITH_SERVICE_URL,
      TIMED_OUT_URL,
      SP_TIMED_OUT_URL,
    ],
    expect.any(Function)
  );
});
