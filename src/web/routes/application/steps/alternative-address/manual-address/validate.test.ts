import expect from "expect";

import { validate } from "./validate";
import { applyExpressValidation } from "../../common/test/apply-express-validation";

test("should pass validation when valid address fields (no optional fields)", async () => {
  const req = {
    body: {
      "address-line-1-ccs": "Alan Jeffery Engineering",
      "address-line-2-ccs": "",
      "address-line-3-ccs": "Plymouth",
      "address-line-4-ccs": "",
      "postcode-ccs": "PL7 1RF",
      sanitisedPostcodeCcs: "PL7 1RF",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("should pass validation when valid address fields (optional fields)", async () => {
  const req = {
    body: {
      "address-line-1-ccs": "Alan Jeffery Engineering",
      "address-line-2-ccs": "1 Valley Road",
      "address-line-3-ccs": "Plymouth",
      "address-line-4-ccs": "Devon",
      "postcode-ccs": "PL7 1RF",
      sanitisedPostcodeCcs: "PL7 1RF",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

describe("address line one", () => {
  test("should return expected error when empty", async () => {
    const req = {
      body: {
        "address-line-1-ccs": "",
        "address-line-2-ccs": "",
        "address-line-3-ccs": "Plymouth",
        "address-line-4-ccs": "",
        "postcode-ccs": "PL7 1RF",
        sanitisedPostcodeCcs: "PL7 1RF",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    const expectedError = {
      type: "field",
      value: "",
      msg: "validation:missingAddressLine1",
      path: "address-line-1-ccs",
      location: "body",
    };

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });

  test("should return expected error when invalid characters", async () => {
    const req = {
      body: {
        "address-line-1-ccs": "@£$%!",
        "address-line-2-ccs": "",
        "address-line-3-ccs": "Plymouth",
        "address-line-4-ccs": "",
        "postcode-ccs": "PL7 1RF",
        sanitisedPostcodeCcs: "PL7 1RF",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    const expectedError = {
      type: "field",
      value: "@£$%!",
      msg: "validation:patternAddressLine1",
      path: "address-line-1-ccs",
      location: "body",
    };

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });

  test("should return expected error when over 50 characters", async () => {
    const req = {
      body: {
        "address-line-1-ccs": "A".repeat(51),
        "address-line-2-ccs": "",
        "address-line-3-ccs": "Plymouth",
        "address-line-4-ccs": "",
        "postcode-ccs": "PL7 1RF",
        sanitisedPostcodeCcs: "PL7 1RF",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    const expectedError = {
      type: "field",
      value: "A".repeat(51),
      msg: "validation:addressLine1TooLong",
      path: "address-line-1-ccs",
      location: "body",
    };

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });
});

describe("address line two", () => {
  test("should return expected error when invalid characters", async () => {
    const req = {
      body: {
        "address-line-1-ccs": "Alan Jeffery Engineering",
        "address-line-2-ccs": "@£$%!",
        "address-line-3-ccs": "Plymouth",
        "address-line-4-ccs": "",
        "postcode-ccs": "PL7 1RF",
        sanitisedPostcodeCcs: "PL7 1RF",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    const expectedError = {
      type: "field",
      value: "@£$%!",
      msg: "validation:patternAddressLine2",
      path: "address-line-2-ccs",
      location: "body",
    };

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });

  test("should return expected error when over 50 characters", async () => {
    const req = {
      body: {
        "address-line-1-ccs": "Alan Jeffery Engineering",
        "address-line-2-ccs": "A".repeat(51),
        "address-line-3-ccs": "Plymouth",
        "address-line-4-ccs": "",
        "postcode-ccs": "PL7 1RF",
        sanitisedPostcodeCcs: "PL7 1RF",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    const expectedError = {
      type: "field",
      value: "A".repeat(51),
      msg: "validation:addressLine2TooLong",
      path: "address-line-2-ccs",
      location: "body",
    };

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });
});

describe("town or city", () => {
  test("should return expected error when empty", async () => {
    const req = {
      body: {
        "address-line-1-ccs": "Alan Jeffery Engineering",
        "address-line-2-ccs": "",
        "address-line-3-ccs": "",
        "address-line-4-ccs": "",
        "postcode-ccs": "PL7 1RF",
        sanitisedPostcodeCcs: "PL7 1RF",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    const expectedError = {
      type: "field",
      value: "",
      msg: "validation:missingAddressTownOrCity",
      path: "address-line-3-ccs",
      location: "body",
    };

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });

  test("should return expected error when invalid characters", async () => {
    const req = {
      body: {
        "address-line-1-ccs": "Alan Jeffery Engineering",
        "address-line-2-ccs": "",
        "address-line-3-ccs": "@£$%!",
        "address-line-4-ccs": "",
        "postcode-ccs": "PL7 1RF",
        sanitisedPostcodeCcs: "PL7 1RF",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    const expectedError = {
      type: "field",
      value: "@£$%!",
      msg: "validation:patternTownOrCity",
      path: "address-line-3-ccs",
      location: "body",
    };

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });

  test("should return expected error when over 50 characters", async () => {
    const req = {
      body: {
        "address-line-1-ccs": "Alan Jeffery Engineering",
        "address-line-2-ccs": "",
        "address-line-3-ccs": "P".repeat(51),
        "address-line-4-ccs": "",
        "postcode-ccs": "PL7 1RF",
        sanitisedPostcodeCcs: "PL7 1RF",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    const expectedError = {
      type: "field",
      value: "P".repeat(51),
      msg: "validation:townOrCityTooLong",
      path: "address-line-3-ccs",
      location: "body",
    };

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });
});

describe("county", () => {
  test("should return expected error when invalid characters", async () => {
    const req = {
      body: {
        "address-line-1-ccs": "Alan Jeffery Engineering",
        "address-line-2-ccs": "",
        "address-line-3-ccs": "Plymouth",
        "address-line-4-ccs": "@£$%!",
        "postcode-ccs": "PL7 1RF",
        sanitisedPostcodeCcs: "PL7 1RF",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    const expectedError = {
      type: "field",
      value: "@£$%!",
      msg: "validation:patternCounty",
      path: "address-line-4-ccs",
      location: "body",
    };

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });

  test("should return expected error when over 50 characters", async () => {
    const req = {
      body: {
        "address-line-1-ccs": "Alan Jeffery Engineering",
        "address-line-2-ccs": "",
        "address-line-3-ccs": "Plymouth",
        "address-line-4-ccs": "D".repeat(51),
        "postcode-ccs": "PL7 1RF",
        sanitisedPostcodeCcs: "PL7 1RF",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    const expectedError = {
      type: "field",
      value: "D".repeat(51),
      msg: "validation:countyTooLong",
      path: "address-line-4-ccs",
      location: "body",
    };

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });
});

describe("postcode", () => {
  test("should return expected error when empty", async () => {
    const req = {
      body: {
        "address-line-1-ccs": "Alan Jeffery Engineering",
        "address-line-2-ccs": "",
        "address-line-3-ccs": "Plymouth",
        "address-line-4-ccs": "",
        "postcode-ccs": "",
        sanitisedPostcodeCcs: "",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    const expectedError = {
      type: "field",
      value: "",
      msg: "validation:missingPostcode",
      path: "postcode-ccs",
      location: "body",
    };

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });

  test("should return expected error when invalid", async () => {
    const req = {
      body: {
        "address-line-1-ccs": "Alan Jeffery Engineering",
        "address-line-2-ccs": "",
        "address-line-3-ccs": "Plymouth",
        "address-line-4-ccs": "",
        "postcode-ccs": "invalid postcode",
        sanitisedPostcodeCcs: "invalid postcode",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    const expectedError = {
      type: "field",
      value: "invalid postcode",
      msg: "validation:invalidPostcode",
      path: "postcode-ccs",
      location: "body",
    };

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });
});
