import { isNil, isEmpty } from "ramda";

const nilToString = (value) => (isNil(value) ? "" : value.toString());

const toDateString = (day, month, year) => {
  const parseDay = isEmpty(day) ? "" : nilToString(day).padStart(2, "0");
  const parseMonth = isEmpty(month) ? "" : nilToString(month).padStart(2, "0");

  return isEmpty(parseDay) && isEmpty(parseMonth) && isEmpty(year)
    ? ""
    : [year, parseMonth, parseDay].join("-");
};

const toLongLocaleDateString = (day, month, year) => {
  if (isEmpty(month)) {
    return year;
  }

  const date = new Date(toDateString(day, month, year));

  if (isEmpty(day)) {
    return date.toLocaleDateString("en-gb", {
      year: "numeric",
      month: "long",
    });
  }

  return date.toLocaleDateString("en-gb", {
    year: "numeric",
    month: "long",
    day: "numeric",
  });
};

const dateAsString = ({
  date = new Date(),
  monthAdjustment = 0,
  yearAdjustment = 0,
} = {}) => {
  const dateToChange = new Date(date);
  dateToChange.setMonth(dateToChange.getMonth() + monthAdjustment);
  dateToChange.setFullYear(dateToChange.getFullYear() + yearAdjustment);

  const dd = dateToChange.getDate();
  const mm = dateToChange.getMonth() + 1;
  const yyyy = dateToChange.getFullYear();
  return toDateString(dd, mm, yyyy);
};

const formatDate = (dateString) => {
  const date = new Date(dateString);
  const year = date.getFullYear();
  const month = ("0" + (date.getMonth() + 1)).slice(-2);
  const day = ("0" + date.getDate()).slice(-2);
  return `${year}-${month}-${day}`;
};

export const dateToShortMonthFormat = (date: Date) => {
  return date.toLocaleDateString("en-GB", {
    day: "numeric",
    month: "short",
    year: "numeric",
  });
};

export const dateToFullLenghtFormatWithTwelveHourTime = (date: Date) => {
  const timestring = date.toLocaleString("en-GB", {
    day: "numeric",
    month: "long",
    year: "numeric",
    hour: "numeric",
    minute: "numeric",
    hour12: true,
  });

  // regex replace 00: to 12:
  return timestring.replace(" 00:", " 12:").replace(" 0:", " 12:");
};

export { toDateString, toLongLocaleDateString, dateAsString, formatDate };
