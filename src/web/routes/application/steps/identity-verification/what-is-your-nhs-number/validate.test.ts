import expect from "expect";
import { validate } from "./validate";
import { applyExpressValidation } from "../../common/test/apply-express-validation";

test("validation middleware errors for nhs-number field is empty", async () => {
  const req = {
    body: {
      "nhs-number": "",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const errorOne = result.array()[0];

  expect(result.array().length).toBe(1);
  expect(errorOne.msg).toBe("validation:missingNhsNumber");
});

test("validation middleware errors for nhs-number field is too short", async () => {
  const req = {
    body: {
      "nhs-number": "123",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const errorOne = result.array()[0];

  expect(result.array().length).toBe(1);
  expect(errorOne.msg).toBe("validation:invalidNhsNumberLength");
});

test("validation middleware errors for nhs-number field is too long", async () => {
  const req = {
    body: {
      "nhs-number": "12345678910",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const errorOne = result.array()[0];

  expect(result.array().length).toBe(1);
  expect(errorOne.msg).toBe("validation:invalidNhsNumberLength");
});

test("validation middleware errors for nhs-number field is in an invalid format (contains non-digits)", async () => {
  const req = {
    body: {
      "nhs-number": "123test4",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const errorOne = result.array()[0];

  expect(result.array().length).toBe(1);
  expect(errorOne.msg).toBe("validation:invalidNhsNumberFormat");
});

test("validation middleware errors for nhs-number field is too short and only has invalid characters", async () => {
  const req = {
    body: {
      "nhs-number": "abc",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const errorOne = result.array()[0];

  expect(result.array().length).toBe(1);
  expect(errorOne.msg).toBe("validation:invalidNhsNumberFormat");
});

test("no validation error when valid nhs-number", async () => {
  const req = {
    body: {
      "nhs-number": "3141592653",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});
