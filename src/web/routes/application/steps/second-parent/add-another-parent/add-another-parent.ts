import { stateMachine } from "../../../flow-control/state-machine";
import { INCREMENT_NEXT_ALLOWED_PATH } from "../../../flow-control/state-machine/actions";
import { IN_REVIEW, IN_REVIEW_PROGRESS } from "../../../flow-control/states";
import { contextPath } from "../../../paths/context-path";
import { CHECK_ANSWERS_URL } from "../../../paths/paths";
import { isNilOrEmpty } from "../../../../../../common/predicates";

import { validationResult } from "express-validator";
import { validate } from "./validate";
import { getFlowDataObjectName } from "../../../tools/data-object";

const pageContent = ({ translate }) => ({
  title: translate("addAnotherParent.title"),
  sectionTitle: translate("section.certificateDetails"),
  heading: translate("addAnotherParent.heading"),
  yes: translate("yes"),
  no: translate("no"),
  continueButtonText: translate("buttons:continue"),
  backLinkText: translate("buttons:back"),
});

const missingSecondParentFields = (data) =>
  isNilOrEmpty(data["their-relation-to-baby"]) ||
  isNilOrEmpty(data["their-email"]) ||
  isNilOrEmpty(data["their-email-confirm"]) ||
  isNilOrEmpty(data["their-date-of-birth"]) ||
  isNilOrEmpty(data["their-date-of-birth-day"]) ||
  isNilOrEmpty(data["their-date-of-birth-month"]) ||
  isNilOrEmpty(data["their-date-of-birth-year"]);

const deleteSecondParentFields = (data) => {
  delete data["their-relation-to-baby"];
  delete data["their-email"];
  delete data["their-email-confirm"];
  delete data["confirm-told-other-parent"];
  delete data["their-date-of-birth"];
  delete data["their-date-of-birth-day"];
  delete data["their-date-of-birth-month"];
  delete data["their-date-of-birth-year"];
  delete data["their-first-name"];
  delete data["their-last-name"];
  delete data["their-reference-number-type"];
  delete data["their-reference-number"];
  delete data["their-reference-number-country"];
};

const behaviourForGet = (config, journey) => (req, res, next) => {
  if (stateMachine.getState(req, journey) === IN_REVIEW_PROGRESS) {
    const dataObjectName = getFlowDataObjectName(journey.name);
    if (
      req.session[dataObjectName]["add-another-parent"] === "yes" &&
      missingSecondParentFields(req.session[dataObjectName])
    ) {
      req.session[dataObjectName]["add-another-parent"] = "no";
      deleteSecondParentFields(req.session[dataObjectName]);
    }
    stateMachine.setState(IN_REVIEW, req, journey);
    stateMachine.dispatch(INCREMENT_NEXT_ALLOWED_PATH, req, journey);

    res.locals.previous = contextPath(CHECK_ANSWERS_URL, journey.name);
  }

  next();
};

const behaviourForPost = (config, journey) => (req, res, next) => {
  if (!validationResult(req).isEmpty()) {
    return next();
  }

  if (
    stateMachine.getState(req, journey) === IN_REVIEW &&
    req.body["add-another-parent"] === "yes"
  ) {
    stateMachine.setState(IN_REVIEW_PROGRESS, req, journey);
  }

  if (req.body["add-another-parent"] === "no") {
    deleteSecondParentFields(req.session[getFlowDataObjectName(journey.name)]);
  }

  next();
};

const addAnotherParent = {
  path: "/would-you-like-to-add-another-parent",
  template: "add-another-parent",
  pageContent,
  validate,
  behaviourForGet,
  behaviourForPost,
};

export {
  addAnotherParent,
  pageContent,
  validate,
  behaviourForGet,
  behaviourForPost,
};
