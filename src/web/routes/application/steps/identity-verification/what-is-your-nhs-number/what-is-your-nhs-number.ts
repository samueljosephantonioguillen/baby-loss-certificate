import { userIsBackOffice } from "../../../../azure-routes/auth";
import { getFlowDataObjectName } from "../../../tools/data-object";
import { sanitise } from "./sanitise";
import { validate } from "./validate";

const pageContent = ({ translate }) => ({
  title: translate("whatIsYourNhsNumber.title"),
  sectionTitle: translate("section.yourDetails"),
  heading: translate("whatIsYourNhsNumber.heading"),
  hintLineOne: translate("whatIsYourNhsNumber.hint.firstLine"),
  hintLineTwo: translate("whatIsYourNhsNumber.hint.secondLine"),
  nhsNumber: translate("whatIsYourNhsNumber.nhsNumber"),
  insetText: translate("whatIsYourNhsNumber.insetText"),
  backLinkText: translate("buttons:back"),
  continueButtonText: translate("buttons:continue"),
});

const isNavigable = (req) =>
  req.session[getFlowDataObjectName(req.session.journeyName)][
    "know-your-nhs-number"
  ] === "yes" && !userIsBackOffice(req);

const whatIsYourNhsNumber = {
  path: "/what-is-your-nhs-number",
  template: "what-is-your-nhs-number",
  pageContent,
  isNavigable,
  sanitise,
  validate,
};

export { whatIsYourNhsNumber, pageContent, isNavigable, sanitise, validate };
