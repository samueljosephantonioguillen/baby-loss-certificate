const removeAllSpaces = (value) => {
  return value.replace(/\s/g, "");
};

const sanitise = () => (req, res, next) => {
  req.body["security-code"] = removeAllSpaces(req.body["security-code"]);

  next();
};

export { sanitise };
