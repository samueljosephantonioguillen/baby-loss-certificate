import expect from "expect";

import {
  spCheckYourAnswers,
  pageContent,
  behaviourForGet,
  validate,
  behaviourForPost,
  getBabyDateOfLoss,
} from "./sp-check-your-answers";

import { START } from "../../../start";
import { contextPath, secondParentContextPath } from "../../paths/context-path";
import { toLongLocaleDateString } from "../common/format-date";
import { buildSessionForJourney } from "../../flow-control/test-utils/test-utils";
import { IN_PROGRESS, IN_REVIEW } from "../../flow-control/states";
import { SECOND_PARENT_DATA_OBJECT_NAME } from "../../tools/data-object";
import { isNilOrEmpty } from "../../../../../common/predicates";
import { SECOND_PARENT_FLOW } from "../../journey-definitions";
import { stateMachine } from "../../flow-control/state-machine";
import { DECLARATION_URL } from "../../paths/paths";

test("checkYourAnswers should match expected outcomes", () => {
  const expectedResults = {
    path: "/confirm-details",
    template: "sp-check-your-answers",
    pageContent,
    behaviourForGet,
    validate,
    behaviourForPost,
  };

  expect(spCheckYourAnswers).toEqual(expectedResults);
});

describe("pageContent", () => {
  test("should return expected results with all fields", () => {
    const translate = (string, object?) => `${string}${object}`;

    const secondParent = {
      "date-of-birth": "2000-02-02",
      "address-line-1": "Alan Jeffery Engineering",
      "address-line-2": "1 Valley Road",
      "town-or-city": "Plymouth",
      county: "Devon",
      postcode: "PL7 1RF",
      "first-name": "Jonathan",
      "last-name": "Doe",
      "relation-to-baby": "Father",
      "fp-relation-to-baby": "Mother",
      "fp-first-name": "Jane",
      "fp-last-name": "Doe",
      "baby-name": "Baby Name",
      "baby-sex": "Sex",
      "baby-date-of-loss": "2023-06-08",
      "baby-loss-location": "Birmingham",
    };
    const req = {
      session: { secondParent, journeyName: SECOND_PARENT_FLOW.name },
    };

    const expected = {
      title: translate("spCheckYourAnswers.title"),
      heading: translate("spCheckYourAnswers.heading"),
      firstParent: translate("spCheckYourAnswers.firstParent"),
      checkFirstParent: translate(
        "spCheckYourAnswers.checkCertificateDetails.checkFirstParent",
        {
          firstParentName: `${req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-first-name"]} ${req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-last-name"]}`,
          firstParentRelation:
            req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-relation-to-baby"],
        }
      ),
      secondParent: translate("spCheckYourAnswers.secondParent"),
      checkSecondParent: translate(
        "spCheckYourAnswers.checkCertificateDetails.checkSecondParent",
        {
          secondParentName: `${
            req.session[SECOND_PARENT_DATA_OBJECT_NAME]["first-name"] || ""
          } ${req.session[SECOND_PARENT_DATA_OBJECT_NAME]["last-name"] || ""}`,
          secondParentRelation:
            req.session[SECOND_PARENT_DATA_OBJECT_NAME]["relation-to-baby"],
        }
      ),
      babyDetails: translate("spCheckYourAnswers.babyDetails"),
      checkBabyDetails: translate(
        "spCheckYourAnswers.checkCertificateDetails.checkBabyDetails",
        {
          babyName: req.session[SECOND_PARENT_DATA_OBJECT_NAME]["baby-name"],
          babySex: req.session[SECOND_PARENT_DATA_OBJECT_NAME]["baby-sex"],
          babyLossDate: isNilOrEmpty(
            req.session[SECOND_PARENT_DATA_OBJECT_NAME][
              "baby-date-of-loss-year"
            ]
          )
            ? ""
            : toLongLocaleDateString(
                req.session[SECOND_PARENT_DATA_OBJECT_NAME][
                  "baby-date-of-loss-day"
                ],
                req.session[SECOND_PARENT_DATA_OBJECT_NAME][
                  "baby-date-of-loss-month"
                ],
                req.session[SECOND_PARENT_DATA_OBJECT_NAME][
                  "baby-date-of-loss-year"
                ]
              ),
          babyLossLocation:
            req.session[SECOND_PARENT_DATA_OBJECT_NAME]["baby-loss-location"],
        }
      ),
      change: translate("change"),
      button: translate("spCheckYourAnswers.button"),
      areDetailsCorrect: translate("spCheckYourAnswers.areDetailsCorrect"),
      yes: translate("yes"),
      no: translate("no"),
    };
    const result = pageContent({ translate, req });

    expect(result).toEqual(expected);
  });
});

describe("getBabyDateOfLoss", () => {
  test("should return empty string for baby date of loss", () => {
    const req = {
      session: {
        journeyName: SECOND_PARENT_FLOW.name,
        [SECOND_PARENT_DATA_OBJECT_NAME]: {
          "print-date-of-loss": false,
        },
      },
    };
    const expectedBabyDateOfLoss = "";
    const result = getBabyDateOfLoss(req);

    expect(result).toEqual(expectedBabyDateOfLoss);
  });

  test("should return only year for baby date of loss", () => {
    const req = {
      session: {
        journeyName: SECOND_PARENT_FLOW.name,
        [SECOND_PARENT_DATA_OBJECT_NAME]: {
          "baby-date-of-loss-year": "2023",
        },
      },
    };
    const expectedBabyDateOfLoss = "2023";
    const result = getBabyDateOfLoss(req);

    expect(result).toEqual(expectedBabyDateOfLoss);
  });

  test("should return year and month for baby date of loss", () => {
    const req = {
      session: {
        journeyName: SECOND_PARENT_FLOW.name,
        [SECOND_PARENT_DATA_OBJECT_NAME]: {
          "baby-date-of-loss-year": "2023",
          "baby-date-of-loss-month": "09",
        },
      },
    };
    const expectedBabyDateOfLoss = "September 2023";
    const result = getBabyDateOfLoss(req);

    expect(result).toEqual(expectedBabyDateOfLoss);
  });

  test("should return full date for baby date of loss", () => {
    const req = {
      session: {
        journeyName: SECOND_PARENT_FLOW.name,
        [SECOND_PARENT_DATA_OBJECT_NAME]: {
          "baby-date-of-loss-year": "2023",
          "baby-date-of-loss-month": "09",
          "baby-date-of-loss-day": "15",
        },
      },
    };
    const expectedBabyDateOfLoss = "15 September 2023";
    const result = getBabyDateOfLoss(req);

    expect(result).toEqual(expectedBabyDateOfLoss);
  });

  test("should return invalid date for date of baby loss", () => {
    const req = {
      session: {
        journeyName: SECOND_PARENT_FLOW.name,
        [SECOND_PARENT_DATA_OBJECT_NAME]: {
          "baby-date-of-loss-year": "2023",
          "baby-date-of-loss-month": "a",
          "baby-date-of-loss-day": "15",
        },
      },
    };
    const expectedBabyLossDate = "Invalid Date";
    const result = getBabyDateOfLoss(req);

    expect(result).toEqual(expectedBabyLossDate);
  });
});

describe("behaviourForPost", () => {
  const config = {};
  test("should call next and set state to IN_PROGRESS", () => {
    const req = {
      session: {
        journeyName: SECOND_PARENT_FLOW.name,
        [SECOND_PARENT_DATA_OBJECT_NAME]: {
          "date-of-birth": "2000-02-02",
          "address-line-1": "Alan Jeffery Engineering",
          "town-or-city": "Plymouth",
          postcode: "PL7 1RF",
          "first-name": "Jane",
          "last-name": "Doe",
          "relation-to-baby": "surrogate",
          "fp-first-name": "Jane",
          "fp-last-name": "Doe",
          "fp-relation-to-baby": "surrogate",
        },
        ...buildSessionForJourney({
          journeyName: SECOND_PARENT_FLOW.name,
          state: IN_REVIEW,
          nextAllowedPath: secondParentContextPath(DECLARATION_URL),
        }),
      },
    };

    const res = {};
    const next = jest.fn();

    behaviourForPost(config, SECOND_PARENT_FLOW)(req, res, next);

    const expectedSession = {
      ...req.session,
      ...buildSessionForJourney({
        journeyName: SECOND_PARENT_FLOW.name,
        state: IN_PROGRESS,
        nextAllowedPath: secondParentContextPath(DECLARATION_URL),
      }),
    };

    expect(req.session).toEqual(expectedSession);

    expect(next).toBeCalledTimes(1);
  });
});

describe("behaviourForGet", () => {
  const config = {};

  test("should call next when required fields are populated and set state to IN_REVIEW", () => {
    const req = {
      session: {
        journeyName: SECOND_PARENT_FLOW.name,
        [SECOND_PARENT_DATA_OBJECT_NAME]: {
          "date-of-birth": "2000-02-02",
          "address-line-1": "Alan Jeffery Engineering",
          "town-or-city": "Plymouth",
          postcode: "PL7 1RF",
          "first-name": "Jane",
          "last-name": "Doe",
          "relation-to-baby": "surrogate",
          "fp-first-name": "Jane",
          "fp-last-name": "Doe",
          "fp-relation-to-baby": "surrogate",
        },
        ...buildSessionForJourney({
          journeyName: SECOND_PARENT_FLOW.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };

    const res = { redirect: jest.fn() };
    const next = jest.fn();

    behaviourForGet(config, SECOND_PARENT_FLOW)(req, res, next);

    const expectedSession = {
      ...req.session,
      ...buildSessionForJourney({
        journeyName: SECOND_PARENT_FLOW.name,
        state: IN_REVIEW,
        nextAllowedPath: "/sp-test-context/confirm-details",
      }),
    };

    expect(req.session).toEqual(expectedSession);
    expect(res.redirect).toBeCalledTimes(0);
    expect(next).toBeCalledTimes(1);
  });

  test("should redirect to service start page when missing required fields", () => {
    const secondParent = {
      "date-of-birth": "2000-02-02",
      "address-line-1": "Alan Jeffery Engineering",
      "town-or-city": "Plymouth",
      postcode: "",
      "first-name": "Jane",
      "last-name": "Doe",
      "fp-first-name": "Jane",
      "fp-last-name": "Doe",
      "fp-relation-to-baby": "surrogate",
    };
    const req = {
      session: { secondParent, journeyName: SECOND_PARENT_FLOW.name },
    };

    const res = { redirect: jest.fn() };
    const next = jest.fn();

    stateMachine.setState = jest.fn();
    stateMachine.dispatch = jest.fn();

    behaviourForGet(config, SECOND_PARENT_FLOW)(req, res, next);

    expect(res.redirect).toBeCalledTimes(1);
    expect(res.redirect).toBeCalledWith(
      contextPath(START, SECOND_PARENT_FLOW.name)
    );
    expect(next).toBeCalledTimes(0);
  });
});
