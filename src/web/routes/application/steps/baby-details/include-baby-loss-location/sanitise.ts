const sanitise = () => (req, res, next) => {
  req.body["baby-loss-location"] = req.body["baby-loss-location"].trim();

  req.body["baby-loss-location"] = req.body["baby-loss-location"].replace(
    /`/g,
    "'"
  );

  req.body["baby-loss-location"] = req.body["baby-loss-location"].replace(
    /’/g,
    "'"
  );

  // capitalise first letter of each word
  req.body["baby-loss-location"] = req.body["baby-loss-location"]
    .split(" ")
    .map((word) => {
      return word.charAt(0).toUpperCase() + word.slice(1);
    })
    .join(" ");

  next();
};

export { sanitise };
