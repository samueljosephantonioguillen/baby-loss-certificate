import expect from "expect";

import { sanitise } from "./sanitise";

const next = jest.fn();

test("trims starting and ending spaces", () => {
  const req = {
    body: {
      "their-first-name": "   John   ",
      "their-last-name": "   Smith   ",
    },
  };

  sanitise()(req, {}, next);

  expect(req.body).toEqual({
    "their-first-name": "John",
    "their-last-name": "Smith",
  });

  expect(next).toBeCalledTimes(1);
});

test("should capitalise first letter of their-first-name and their-last-name", () => {
  const req = {
    body: {
      "their-first-name": "firstname",
      "their-last-name": "lastname",
    },
  };
  const res = { redirect: jest.fn() };
  const next = jest.fn();

  const expected = {
    "their-first-name": "Firstname",
    "their-last-name": "Lastname",
  };

  sanitise()(req, res, next);

  expect(req.body).toEqual(expected);
});
