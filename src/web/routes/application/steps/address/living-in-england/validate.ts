import { check } from "express-validator";

import { translateValidationMessage } from "../../common/translate-validation-message";

const validate = () => [
  check("living-in-england")
    .not()
    .isEmpty()
    .bail()
    .withMessage(
      translateValidationMessage("validation:selectLivingInEngland")
    ),
];

export { validate };
