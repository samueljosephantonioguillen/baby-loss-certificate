import expect from "expect";

import { validate } from "./validate";
import { applyExpressValidation } from "../../common/test/apply-express-validation";

test("returns no validation errors when living-in-england field is valid", async () => {
  const req = {
    body: {
      "living-in-england": "yes",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("returns expected validation error when living-in-england field is empty", async () => {
  const req = {
    body: {
      "living-in-england": "",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "",
    msg: "validation:selectLivingInEngland",
    path: "living-in-england",
    location: "body",
  };

  expect(result.array().length).toBe(1);
  expect(error).toEqual(expectedError);
});
