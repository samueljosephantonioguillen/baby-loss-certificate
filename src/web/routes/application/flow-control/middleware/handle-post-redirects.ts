import { stateMachine } from "../state-machine";
import { GET_NEXT_PATH } from "../state-machine/actions";

const handlePostRedirects = (journey) => (req, res, next) => {
  if (res.locals.errors) {
    return next();
  }

  const nextPage = stateMachine.dispatch(GET_NEXT_PATH, req, journey);
  return res.redirect(nextPage);
};

export { handlePostRedirects };
