import expect from "expect";
import * as sinon from "sinon";

const getAdditionalDataForStep = sinon.stub().returns({ foo: "bar" });

jest.mock("../../session-accessors", () => ({
  getAdditionalDataForStep,
}));

import { renderView } from "./render-view";

const step = {
  template: "template",
  pageContent: () => ({ title: "What’s your name?" }),
  next: "redirect",
};

test("renderView() should call res.render() on GET request", async () => {
  const render = sinon.spy();
  const csrfToken = () => "myCsrfToken";

  const req = {
    method: "GET",
    csrfToken,
    t: () => {
      /* explicit empty function */
    },
    session: {},
  };

  const res = {
    render,
    locals: {},
  };

  renderView(step)(req, res);

  expect(render.called).toBe(true);

  const expectedArg = {
    title: "What’s your name?",
    foo: "bar",
    csrfToken: "myCsrfToken",
  };
  expect(render.getCall(0).args[1]).toEqual(expectedArg);
});
