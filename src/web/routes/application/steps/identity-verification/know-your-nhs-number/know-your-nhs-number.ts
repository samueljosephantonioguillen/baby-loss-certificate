import { validationResult } from "express-validator";
import { validate } from "./validate";

import { isNilOrEmpty } from "../../../../../../common/predicates";
import { contextPath } from "../../../paths/context-path";
import { CHECK_DETAILS_URL } from "../../../paths/paths";

import { stateMachine } from "../../../flow-control/state-machine";
import {
  IN_DETAILS_REVIEW,
  IN_DETAILS_REVIEW_PROGRESS,
  IN_PROGRESS,
} from "../../../flow-control/states";
import { INCREMENT_NEXT_ALLOWED_PATH } from "../../../flow-control/state-machine/actions";
import { userIsBackOffice } from "../../../../azure-routes/auth";
import { getFlowDataObjectName } from "../../../tools/data-object";

const pageContent = ({ translate }) => ({
  title: translate("knowYourNhsNumber.title"),
  sectionTitle: translate("section.yourDetails"),
  heading: translate("knowYourNhsNumber.heading"),
  bodyLineOne: translate("knowYourNhsNumber.body.firstLine"),
  bodyLineTwo: translate("knowYourNhsNumber.body.secondLine"),
  yes: translate("knowYourNhsNumber.yes"),
  no: translate("knowYourNhsNumber.no"),
  whereToFindNhsNumberSummary: translate(
    "knowYourNhsNumber.whereToFindNhsNumberSummary"
  ),
  whereToFindNhsNumberText1: translate(
    "knowYourNhsNumber.whereToFindNhsNumber1"
  ),
  whereToFindNhsNumberText2: translate(
    "knowYourNhsNumber.whereToFindNhsNumber2"
  ),
  whereToFindNhsNumberText3: translate(
    "knowYourNhsNumber.whereToFindNhsNumber3"
  ),
  whereToFindNhsNumberText4: translate(
    "knowYourNhsNumber.whereToFindNhsNumber4"
  ),
  backLinkText: translate("buttons:back"),
  continueButtonText: translate("buttons:continue"),
});

const isNavigable = (req) => !userIsBackOffice(req);

const behaviourForGet = (config, journey) => (req, res, next) => {
  if (stateMachine.getState(req, journey) === IN_DETAILS_REVIEW_PROGRESS) {
    const dataObjectName = getFlowDataObjectName(journey.name);
    if (
      req.session[dataObjectName]["know-your-nhs-number"] === "yes" &&
      isNilOrEmpty(req.session[dataObjectName]["nhs-number"])
    ) {
      req.session[dataObjectName]["know-your-nhs-number"] = "no";
    }
    stateMachine.setState(IN_DETAILS_REVIEW, req, journey);
    stateMachine.dispatch(INCREMENT_NEXT_ALLOWED_PATH, req, journey);
    res.locals.previous = contextPath(CHECK_DETAILS_URL, journey.name);
  }

  next();
};

const behaviourForPost = (config, journey) => (req, res, next) => {
  const dataObjectName = getFlowDataObjectName(journey.name);

  if (!validationResult(req).isEmpty()) {
    return next();
  }

  if (stateMachine.getState(req, journey) === IN_DETAILS_REVIEW) {
    stateMachine.setState(IN_DETAILS_REVIEW_PROGRESS, req, journey);
  }

  if (req.body["know-your-nhs-number"] === "no") {
    delete req.session[dataObjectName]["nhs-number"];
    delete req.session[dataObjectName].formattedNhsNumber;
    stateMachine.setState(IN_PROGRESS, req, journey);
  }

  if (
    req.body["know-your-nhs-number"] === "yes" ||
    req.session[dataObjectName]["nhsNumberPdsRequestMade"]
  ) {
    delete req.session[dataObjectName]["first-name"];
    delete req.session[dataObjectName]["last-name"];
    delete req.session[dataObjectName]["date-of-birth"];
    delete req.session[dataObjectName]["date-of-birth-day"];
    delete req.session[dataObjectName]["date-of-birth-month"];
    delete req.session[dataObjectName]["date-of-birth-year"];
    delete req.session[dataObjectName]["postcode"];
    delete req.session[dataObjectName]["sanitisedPostcode"];
    delete req.session[dataObjectName]["pds-nhs-number"];
    delete req.session[dataObjectName].olderThanAgeRequirement;
    delete req.session[dataObjectName].spDobNotMatched;
  }
  next();
};

const knowYourNhsNumber = {
  path: "/do-you-know-your-nhs-number",
  template: "know-your-nhs-number",
  pageContent,
  isNavigable,
  validate,
  behaviourForGet,
  behaviourForPost,
};

export {
  knowYourNhsNumber,
  pageContent,
  isNavigable,
  validate,
  behaviourForGet,
  behaviourForPost,
};
