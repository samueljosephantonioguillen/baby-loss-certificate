import { sanitise } from "../sanitise";
import { validate } from "./validate";

import { userIsAgent, userIsBackOffice } from "../../../../azure-routes/auth";
import { getFlowDataObjectName } from "../../../tools/data-object";
import { IN_REVIEW } from "../../../flow-control/states";

const pageContent = ({ translate }) => ({
  title: translate("manualAddress.title"),
  sectionTitle: translate("section.yourDetails"),
  heading: translate("manualAddress.heading"),
  addressLineOne: translate("manualAddress.addressLine.firstLine"),
  addressLineTwo: translate("manualAddress.addressLine.secondLine"),
  townOrCity: translate("manualAddress.townOrCity"),
  county: translate("manualAddress.county"),
  postcode: translate("manualAddress.postcode"),
  backLinkText: translate("buttons:back"),
  continueButtonText: translate("buttons:continue"),
});

const isNavigable = (req) =>
  (((userIsAgent(req) &&
    req.session[getFlowDataObjectName(req.session.journeyName)][
      "confirm-your-address"
    ] === "alt") ||
    userIsBackOffice(req)) &&
    req.session[getFlowDataObjectName(req.session.journeyName)][
      "selected-address"
    ] === undefined) ||
  (userIsBackOffice(req) &&
    req.session.journeys[req.session.journeyName].state === IN_REVIEW);

const manualAddress = {
  path: "/enter-your-address",
  template: "manual-address",
  pageContent,
  isNavigable,
  sanitise,
  validate,
};

export { manualAddress, pageContent, isNavigable, sanitise, validate };
