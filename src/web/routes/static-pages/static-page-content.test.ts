import { renderStaticPage } from "./static-page-content";

test("renderStaticPage should call res.render with given page url and correct page content", () => {
  const req = { t: (string) => string };
  const res = { render: jest.fn() };

  const pageUrl = "cookies";
  const pageContent = ({ translate }) => ({
    title: translate("privacyNotice.title"),
    heading: translate("privacyNotice.heading"),
  });

  const translate = (string) => string;

  renderStaticPage(pageUrl, pageContent)(req, res);

  expect(res.render).toBeCalledTimes(1);
  expect(res.render).toBeCalledWith(
    "static-pages/" + pageUrl,
    pageContent({ translate })
  );
});
