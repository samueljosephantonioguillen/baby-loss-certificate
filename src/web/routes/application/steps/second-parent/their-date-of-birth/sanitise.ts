const trimLeadingZero = (input) => input.replace(/\b0+/g, "");

const sanitise = () => (req, res, next) => {
  req.body["their-date-of-birth-day"] = trimLeadingZero(
    req.body["their-date-of-birth-day"]
  );
  req.body["their-date-of-birth-month"] = trimLeadingZero(
    req.body["their-date-of-birth-month"]
  );
  req.body["their-date-of-birth-year"] = trimLeadingZero(
    req.body["their-date-of-birth-year"]
  );

  next();
};

export { sanitise, trimLeadingZero };
