import { check } from "express-validator";

import { translateValidationMessage } from "../../common/translate-validation-message";

const validate = () => [
  check("confirm-your-address")
    .not()
    .isEmpty()
    .bail()
    .withMessage(translateValidationMessage("validation:confirmAddress")),
];

export { validate };
