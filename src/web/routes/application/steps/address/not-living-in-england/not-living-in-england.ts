import { getFlowDataObjectName } from "../../../tools/data-object";

const pageContent = ({ translate }) => ({
  title: translate("notLivingInEngland.title"),
  heading: translate("notLivingInEngland.heading"),
  bodyLineOne: translate("notLivingInEngland.body.firstLine"),
  bodyLineTwo: translate("notLivingInEngland.body.secondLine"),
  supportOrganisationsHeading: translate("supportOrganisations.heading"),
  supportOrganisationsBody: translate("supportOrganisations.body"),
  supportOrganisationsList: translate("supportOrganisations.listItems"),
  backLinkText: translate("buttons:back"),
});

const isNavigable = (req) =>
  req.session[getFlowDataObjectName(req.session.journeyName)][
    "living-in-england"
  ] !== "yes";

const notLivingInEngland = {
  path: "/cannot-request-not-living-in-england",
  template: "not-eligible",
  pageContent,
  isNavigable,
};

export { notLivingInEngland, pageContent, isNavigable };
