import {
  firstParentContextPath,
  secondParentContextPath,
} from "../application/paths/context-path";
import { renderErrorPageContent } from "./error-page-content";

const registerProblemWithServiceError = (app) => {
  app.use((req, res, next) => {
    res.set("Clear-Site-Data", '"storage"');
    next();
  });

  app.get(
    [
      firstParentContextPath("/problem-with-the-service"),
      secondParentContextPath("/problem-with-the-service"),
      firstParentContextPath("/timed-out"),
      secondParentContextPath("/timed-out"),
    ],
    renderErrorPageContent("problem-with-service")
  );
};

export { registerProblemWithServiceError };
