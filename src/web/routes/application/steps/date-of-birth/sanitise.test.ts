import expect from "expect";
import { sanitise } from "./sanitise";

test("sanitise() should trim the leading zeros", () => {
  const req = {
    body: {
      "date-of-birth-year": "002002",
      "date-of-birth-month": "004",
      "date-of-birth-day": "0027",
    },
  };

  const expectedReq = {
    body: {
      "date-of-birth-year": "2002",
      "date-of-birth-month": "4",
      "date-of-birth-day": "27",
    },
  };

  const next = jest.fn();

  sanitise()(req, {}, next);
  expect(req).toEqual(expectedReq);
});
