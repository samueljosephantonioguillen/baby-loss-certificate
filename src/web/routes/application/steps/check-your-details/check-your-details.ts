import { START } from "../../../start";
import { contextPath, secondParentContextPath } from "../../paths/context-path";

import { toTitleCase } from "../common/formats";
import { toLongLocaleDateString } from "../common/format-date";
import { isNilOrEmpty } from "../../../../../common/predicates";

import { logger } from "../../../../logger/logger";

import { stateMachine } from "../../flow-control/state-machine";
import { IN_PROGRESS, IN_DETAILS_REVIEW } from "../../flow-control/states";
import {
  INCREMENT_NEXT_ALLOWED_PATH,
  SET_NEXT_ALLOWED_PATH,
} from "../../flow-control/state-machine/actions";

import { getSecurityCode } from "../identity-verification/get-security-code";
import {
  isDateAHundredTwentyOrMoreYearsInThePast,
  isDateInPast,
  isDateRealDate,
  isDateSixteenOrMoreYearsInThePast,
} from "../common/validate-date";

import { requestPdsLookup } from "./request-pds-lookup";
import { notMatched } from "../identity-verification/not-matched";
import { notOldEnoughPds } from "../not-old-enough-pds";
import { noContactDetails } from "../identity-verification/no-contact-details";
import { noFoundAddress } from "../identity-verification/no-found-address";
import { spDobNotMatched } from "../sp-dob-not-matched";
import { requestPdsSearch } from "./request-pds-search";
import {
  FIRST_NAME_MAX_LENGTH,
  LAST_NAME_MAX_LENGTH,
  NAME_PATTERN,
  UK_POSTCODE_PATTERN,
} from "../../constants";
import { userIsBackOffice } from "../../../azure-routes/auth";
import {
  SECOND_PARENT_DATA_OBJECT_NAME,
  getFlowDataObjectName,
} from "../../tools/data-object";
import { SECOND_PARENT_FLOW } from "../../journey-definitions";
import { isValidPhoneNumber } from "libphonenumber-js";

const tryAgainStatusCodes = [408, 429];

const refreshPdsData = (req) => {
  const dataObjectName = getFlowDataObjectName(req.session.journeyName);
  delete req.session[dataObjectName]["first-name"];
  delete req.session[dataObjectName]["last-name"];
  delete req.session[dataObjectName]["olderThanAgeRequirementPds"];
  delete req.session[dataObjectName]["date-of-birth"];
  delete req.session[dataObjectName]["date-of-birth-year"];
  delete req.session[dataObjectName]["date-of-birth-month"];
  delete req.session[dataObjectName]["date-of-birth-year"];
  delete req.session[dataObjectName].email;
  delete req.session[dataObjectName]["phone-number"];
  delete req.session[dataObjectName]["address-line-1"];
  delete req.session[dataObjectName]["address-line-2"];
  delete req.session[dataObjectName]["address-line-3"];
  delete req.session[dataObjectName]["address-line-4"];
  delete req.session[dataObjectName]["address-line-5"];
  delete req.session[dataObjectName].postcode;
  delete req.session[dataObjectName]["sanitisedPostcode"];
  delete req.session[dataObjectName]["pds-nhs-number"];
  delete req.session[dataObjectName]["spDobNotMatched"];
};

const refreshKickOutFlags = (req) => {
  delete req.session[getFlowDataObjectName(req.session.journeyName)][
    "addressNotFound"
  ];
  delete req.session[getFlowDataObjectName(req.session.journeyName)][
    "notMatched"
  ];
  delete req.session[getFlowDataObjectName(req.session.journeyName)][
    "noContactDetails"
  ];
  delete req.session[getFlowDataObjectName(req.session.journeyName)][
    "noContactDetailsnhsNumberPdsRequestMade"
  ];
  delete req.session[getFlowDataObjectName(req.session.journeyName)][
    "spDobNotMatched"
  ];
};

const createDetails = (req, details) => {
  if (
    req.session[getFlowDataObjectName(req.session.journeyName)][
      "know-your-nhs-number"
    ] === "yes"
  ) {
    return [details.find((item) => item.key === "NHS number")];
  }
  details.shift();

  return details;
};

const isNavigable = (req) => !userIsBackOffice(req);

const pageContent = ({ translate, req }) => ({
  title: translate("checkYourDetails.title"),
  sectionTitle: translate("section.yourDetails"),
  heading: translate("checkYourDetails.heading"),
  details: createDetails(
    req,
    translate("checkYourDetails.details", {
      nhsNumber:
        req.session[getFlowDataObjectName(req.session.journeyName)][
          "formattedNhsNumber"
        ],
      name: toTitleCase(
        `${
          req.session[getFlowDataObjectName(req.session.journeyName)][
            "first-name"
          ]
        } ${
          req.session[getFlowDataObjectName(req.session.journeyName)][
            "last-name"
          ]
        }`
      ),
      dateOfBirth: toLongLocaleDateString(
        req.session[getFlowDataObjectName(req.session.journeyName)][
          "date-of-birth-day"
        ],
        req.session[getFlowDataObjectName(req.session.journeyName)][
          "date-of-birth-month"
        ],
        req.session[getFlowDataObjectName(req.session.journeyName)][
          "date-of-birth-year"
        ]
      ),
      postcode:
        req.session[getFlowDataObjectName(req.session.journeyName)][
          "sanitisedPostcode"
        ],
    })
  ),
  body: translate("checkYourDetails.body"),
  list: translate("checkYourDetails.list"),
  change: translate("change"),
  button: translate("checkYourDetails.button"),
  backLinkText: translate("buttons:back"),
});

const requiredFields = ["date-of-birth", "postcode", "first-name", "last-name"];

const missingNonNhsNumberRequiredFields = (data) =>
  requiredFields.some((field) => isNilOrEmpty(data[field]));

const isNhsNumberFlow = (req) =>
  !!req.session[getFlowDataObjectName(req.session.journeyName)]["nhs-number"] &&
  req.session[getFlowDataObjectName(req.session.journeyName)][
    "know-your-nhs-number"
  ] === "yes";

const isNonNhsNumberFlow = (req) =>
  !missingNonNhsNumberRequiredFields(
    req.session[getFlowDataObjectName(req.session.journeyName)]
  ) &&
  req.session[getFlowDataObjectName(req.session.journeyName)][
    "know-your-nhs-number"
  ] === "no";

const behaviourForGet = (config, journey) => (req, res, next) => {
  if (!isNhsNumberFlow(req) && !isNonNhsNumberFlow(req)) {
    return res.redirect(contextPath(START, journey.name));
  }

  stateMachine.setState(IN_DETAILS_REVIEW, req, journey);
  stateMachine.dispatch(INCREMENT_NEXT_ALLOWED_PATH, req, journey);

  next();
};

const behaviourForPost = (config, journey) => async (req, res) => {
  stateMachine.setState(IN_PROGRESS, req, journey);
  stateMachine.dispatch(
    SET_NEXT_ALLOWED_PATH,
    req,
    journey,
    contextPath(getSecurityCode.path, journey.name)
  );

  try {
    refreshKickOutFlags(req);

    if (
      req.session[getFlowDataObjectName(req.session.journeyName)]["nhs-number"]
    ) {
      if (
        journey.name === SECOND_PARENT_FLOW.name &&
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["nhs-number"] ===
          req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-reference-number"] &&
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-reference-type"] === 1
      ) {
        logger.error("PDS lookup (NHS number) - Duplicate NHS number", req);
        req.session[getFlowDataObjectName(req.session.journeyName)][
          "notMatched"
        ] = true;
        return res.redirect(
          contextPath(notMatched.path, req.session.journeyName)
        );
      }
      return await nhsNumberPdsRequest(req, res, config);
    }

    if (
      journey.name === SECOND_PARENT_FLOW.name &&
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["sp-date-of-birth"] !==
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["date-of-birth"]
    ) {
      logger.info(
        "Other parent entered DoB not matching DoB provided in main application",
        req
      );
      req.session[getFlowDataObjectName(req.session.journeyName)][
        "spDobNotMatched"
      ] = true;
      return res.redirect(
        contextPath(spDobNotMatched.path, req.session.journeyName)
      );
    }

    return await nonNhsNumberPdsRequest(req, res, config);
  } catch (error) {
    if (tryAgainStatusCodes.includes(error.response?.status)) {
      logger.error(
        "PDS request failed via request issue - redirecting to try again error screen",
        req
      );
      return res.redirect(
        contextPath("/cannot-request-try-again", req.session.journeyName)
      );
    }
    if (error.response?.status > 400) {
      logger.error(
        "PDS request failed via server issue - redirecting to service error screen",
        req
      );
      return res.redirect(
        contextPath("/problem-with-the-service", req.session.journeyName)
      );
    }
    req.session[getFlowDataObjectName(req.session.journeyName)]["notMatched"] =
      true;
    logger.error("PDS request failed - redirecting to kickout screen", req);
    return res.redirect(contextPath(notMatched.path, req.session.journeyName));
  }
};

const isValidDateOfBirth = (dob) => {
  if (
    !dob ||
    isNilOrEmpty(dob) ||
    !isDateRealDate(dob) ||
    !isDateInPast(dob) ||
    isDateAHundredTwentyOrMoreYearsInThePast(dob)
  ) {
    return false;
  }

  return true;
};

const nhsNumberPdsRequest = async (req, res, config) => {
  const journey = req.session.journeyName;
  const dataObjectName = getFlowDataObjectName(journey);
  const dataObject = req.session[dataObjectName];

  dataObject["nhsNumberPdsRequestMade"] = true;

  refreshPdsData(req);
  const response = await requestPdsLookup(
    req,
    config,
    dataObject["nhs-number"]
  );

  if (secondParentFlowDuplicateNHSNumber(journey, req, response)) {
    logger.error("PDS lookup (NHS number) - Duplicate NHS number", req);
    dataObject["notMatched"] = true;
    return res.redirect(contextPath(notMatched.path, journey));
  }

  dataObject["noContactDetailsnhsNumberPdsRequestMade"] = true;

  if (noPostcodeFound(response, journey)) {
    logger.error("PDS lookup (NHS number) - No postcode found", req);
    dataObject["addressNotFound"] = true;
    return res.redirect(contextPath(noFoundAddress.path, journey));
  }
  const postcode: string = response.postcode ?? "";
  if (isPostcodeInvalid(postcode, journey)) {
    logger.error("PDS lookup (NHS number) - Postcode invalid", req);
    dataObject["notMatched"] = true;
    return res.redirect(contextPath(notMatched.path, journey));
  }

  if (isAddressFound(response, journey)) {
    logger.error("PDS lookup (NHS number) - No address found", req);
    dataObject["addressNotFound"] = true;
    return res.redirect(contextPath(noFoundAddress.path, journey));
  }

  if (isValidContactMethodFound(response)) {
    logger.error(
      "PDS lookup (NHS-number) - No active contact method retrieved",
      req
    );
    dataObject["noContactDetails"] = true;
    return res.redirect(contextPath(noContactDetails.path, journey));
  }

  if (response.hasDeceasedDateTime === true) {
    logger.error("PDS lookup (NHS number) - Deceased date time populated", req);
    dataObject["notMatched"] = true;
    return res.redirect(contextPath(notMatched.path, journey));
  }

  if (isActiveUsualNameFound(response)) {
    logger.error(
      "PDS lookup (NHS number) - No active usual name retrieved",
      req
    );
    dataObject["notMatched"] = true;
    return res.redirect(contextPath(notMatched.path, journey));
  }

  validateName(response, res, req, dataObject, journey);

  if (!isValidDateOfBirth(response.dateOfBirth)) {
    logger.error("PDS lookup (NHS number) - Date of birth is not valid", req);
    dataObject["notMatched"] = true;

    return res.redirect(contextPath(notMatched.path, journey));
  }

  if (isDateOfBirthIneligible(response, journey)) {
    logger.error(
      "PDS lookup (NHS number) - Date of birth is not eligible",
      req
    );
    dataObject["olderThanAgeRequirementPds"] = false;
    return res.redirect(contextPath(notOldEnoughPds.path, journey));
  }

  if (isPDSDateOfBirthMatchApplication(journey, response, dataObject)) {
    logger.info(
      "PDS lookup (NHS number) - Date of birth from PDS does not match main applicant provided value",
      req
    );
    dataObject["spDobNotMatched"] = true;
    return res.redirect(contextPath(spDobNotMatched.path, journey));
  }

  if (!response.nhsNumber) {
    logger.error("PDS lookup (NHS number) - No NHS number returned");
    dataObject["notMatched"] = true;
    return res.redirect(contextPath(notMatched.path, journey));
  }

  const dob = new Date(response.dateOfBirth);
  dataObject["olderThanAgeRequirementPds"] = true;

  dataObject["first-name"] = response.firstName;
  dataObject["last-name"] = response.lastName;
  dataObject["date-of-birth"] = response.dateOfBirth;
  dataObject["pds-nhs-number"] = response.nhsNumber;
  dataObject["date-of-birth-year"] = `${dob.getFullYear()}`;
  dataObject["date-of-birth-month"] = `${dob.getMonth() + 1}`;
  dataObject["date-of-birth-day"] = `${dob.getDate()}`;
  dataObject.email = response.email;

  const phoneNumber = response.phoneNumber || "";
  if (isValidPhoneNumber(phoneNumber, "GB")) {
    dataObject["phone-number"] = phoneNumber;
  }

  populateAddressLines(journey, dataObject, response);

  dataObject["notMatched"] = false;

  if (journey === SECOND_PARENT_FLOW.name) {
    dataObject["spDobNotMatched"] = false;
  }

  logger.info("PDS lookup (NHS number) - Completed successfully", req);

  return res.redirect(contextPath(getSecurityCode.path, journey));
};

function validateName(response, res, req, dataObject, journey) {
  const fName = response.firstName ?? "";
  const lName = response.lastName ?? "";

  const nameTooLong = isNameTooLong(fName, lName);
  const nameInvalid = isNameInvalid(fName, lName);

  if (nameTooLong || nameInvalid) {
    const errorMessage = nameTooLong ? "Name too long" : "Name invalid";
    logger.error(`PDS lookup (NHS number) - ${errorMessage}`, req);
    dataObject["notMatched"] = true;
    return res.redirect(contextPath(notMatched.path, journey));
  }
}

function populateAddressLines(
  journey,
  dataObject,
  response: {
    firstName: null;
    lastName: null;
    dateOfBirth;
    email: null;
    phoneNumber: null;
    addressLines: never[];
    postcode: null;
    hasDeceasedDateTime: boolean;
    nhsNumber: null;
  }
) {
  if (journey !== SECOND_PARENT_FLOW.name) {
    dataObject["address-line-1"] = response.addressLines[0];
    dataObject["address-line-2"] = response.addressLines[1];
    dataObject["address-line-3"] = response.addressLines[2];
    dataObject["address-line-4"] = response.addressLines[3];
    dataObject["address-line-5"] = response.addressLines[4];
    dataObject.postcode = response.postcode;
  }
}

function secondParentFlowDuplicateNHSNumber(journey, req, response) {
  return (
    journey === SECOND_PARENT_FLOW.name &&
    duplicateNhsNumber(req, response.nhsNumber)
  );
}

function noPostcodeFound(response, journey) {
  return !response.postcode && journey !== SECOND_PARENT_FLOW.name;
}

function isPDSDateOfBirthMatchApplication(journey, response, dataObject) {
  return (
    journey === SECOND_PARENT_FLOW.name &&
    response.dateOfBirth !== dataObject["sp-date-of-birth"]
  );
}

function isDateOfBirthIneligible(response, journey) {
  return (
    !isDateSixteenOrMoreYearsInThePast(response.dateOfBirth) &&
    journey !== SECOND_PARENT_FLOW.name
  );
}

function isNameInvalid(fName, lName) {
  return !NAME_PATTERN.exec(fName) || !NAME_PATTERN.exec(lName);
}

function isNameTooLong(fName, lName) {
  return (
    fName.length > FIRST_NAME_MAX_LENGTH || lName.length > LAST_NAME_MAX_LENGTH
  );
}

function isActiveUsualNameFound(response) {
  return !response.firstName || !response.lastName;
}

function isValidContactMethodFound(response) {
  const phoneNumber = response.phoneNumber || "";
  return !isValidPhoneNumber(phoneNumber, "GB") && !response.email;
}

function isAddressFound(response, journey) {
  return (
    (!response.addressLines || response.addressLines.length < 1) &&
    journey !== SECOND_PARENT_FLOW.name
  );
}

function isPostcodeInvalid(postcode, journey) {
  return (
    !UK_POSTCODE_PATTERN.exec(postcode) && journey !== SECOND_PARENT_FLOW.name
  );
}

function duplicateNhsNumber(req, spNhsNumber) {
  return (
    req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-reference-number"] ===
    spNhsNumber
  );
}

const nonNhsNumberPdsRequest = async (req, res, config) => {
  const response = await requestPdsSearch(req, config);
  const dataObjectName = getFlowDataObjectName(req.session.journeyName);

  if (
    req.session.journeyName === SECOND_PARENT_FLOW.name &&
    response.nhsNumber ===
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-reference-number"] &&
    req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-reference-type"] === 1
  ) {
    logger.error("PDS search (non-NHS number) - Duplicate NHS number");
    req.session[SECOND_PARENT_DATA_OBJECT_NAME]["notMatched"] = true;
    return res.redirect(secondParentContextPath(notMatched.path));
  }

  const userInputPostcode =
    `${req.session[dataObjectName]["sanitisedPostcode"]}`
      .toLowerCase()
      .replace(/\s/g, "");
  const pdsPostcode = `${response.postcode}`.toLowerCase().replace(/\s/g, "");
  const phoneNumber = response.phoneNumber || "";
  const validPhoneNumber = isValidPhoneNumber(phoneNumber, "GB");

  if (
    userInputPostcode !== pdsPostcode &&
    req.session.journeyName !== SECOND_PARENT_FLOW.name
  ) {
    logger.error(
      "PDS search (non-NHS number) - Postcode from PDS does not match user input postcode"
    );
    req.session[getFlowDataObjectName(req.session.journeyName)][
      "addressNotFound"
    ] = true;
    return res.redirect(
      contextPath(noFoundAddress.path, req.session.journeyName)
    );
  }

  if (
    (!response.addressLines || response.addressLines.length < 1) &&
    req.session.journeyName !== SECOND_PARENT_FLOW.name
  ) {
    logger.error("PDS search (non-NHS number) - No address found", req);
    req.session[getFlowDataObjectName(req.session.journeyName)][
      "addressNotFound"
    ] = true;
    return res.redirect(
      contextPath(noFoundAddress.path, req.session.journeyName)
    );
  }

  if (!validPhoneNumber && !response.email) {
    logger.error(
      "PDS search (non-NHS number) - No active contact method retrieved",
      req
    );
    req.session[getFlowDataObjectName(req.session.journeyName)][
      "noContactDetails"
    ] = true;
    return res.redirect(
      contextPath(noContactDetails.path, req.session.journeyName)
    );
  }

  if (response.hasDeceasedDateTime === true) {
    logger.error(
      "PDS search (non-NHS number) - Deceased date time populated",
      req
    );
    req.session[getFlowDataObjectName(req.session.journeyName)]["notMatched"] =
      true;
    return res.redirect(contextPath(notMatched.path, req.session.journeyName));
  }

  if (!response.firstName || !response.lastName) {
    logger.error(
      "PDS search (non-NHS number) - Name from PDS does not match user input names"
    );
    req.session[getFlowDataObjectName(req.session.journeyName)]["notMatched"] =
      true;
    return res.redirect(contextPath(notMatched.path, req.session.journeyName));
  }

  if (!response.nhsNumber) {
    logger.error("PDS search (non-NHS number) - No NHS number returned");
    req.session[getFlowDataObjectName(req.session.journeyName)]["notMatched"] =
      true;
    return res.redirect(contextPath(notMatched.path, req.session.journeyName));
  }

  const dob = new Date(response.dateOfBirth);

  req.session[dataObjectName]["first-name"] = response.firstName;
  req.session[dataObjectName]["last-name"] = response.lastName;
  req.session[dataObjectName]["pds-nhs-number"] = response.nhsNumber;
  req.session[dataObjectName]["date-of-birth"] = response.dateOfBirth;
  req.session[dataObjectName]["date-of-birth-year"] = `${dob.getFullYear()}`;
  req.session[dataObjectName]["date-of-birth-month"] = `${dob.getMonth() + 1}`;
  req.session[dataObjectName]["date-of-birth-day"] = `${dob.getDate()}`;
  req.session[dataObjectName].olderThanAgeRequirement = true;
  req.session[dataObjectName].email = response.email;

  if (validPhoneNumber) {
    req.session[dataObjectName]["phone-number"] = response.phoneNumber;
  }

  if (req.session.journeyName !== SECOND_PARENT_FLOW.name) {
    req.session[dataObjectName]["address-line-1"] = response.addressLines[0];
    req.session[dataObjectName]["address-line-2"] = response.addressLines[1];
    req.session[dataObjectName]["address-line-3"] = response.addressLines[2];
    req.session[dataObjectName]["address-line-4"] = response.addressLines[3];
    req.session[dataObjectName]["address-line-5"] = response.addressLines[4];
    req.session[dataObjectName]["postcode"] = response.postcode;
  }

  req.session[getFlowDataObjectName(req.session.journeyName)]["notMatched"] =
    false;

  if (req.session.journeyName === SECOND_PARENT_FLOW.name) {
    req.session[getFlowDataObjectName(req.session.journeyName)][
      "spDobNotMatched"
    ] = false;
  }

  logger.info("PDS search (non-NHS number) - Completed successfully");

  return res.redirect(
    contextPath(getSecurityCode.path, req.session.journeyName)
  );
};

const checkYourDetails = {
  path: "/check-your-details",
  template: "check-your-details",
  pageContent,
  isNavigable,
  behaviourForGet,
  behaviourForPost,
};

export {
  checkYourDetails,
  pageContent,
  behaviourForGet,
  behaviourForPost,
  createDetails,
  isNavigable,
};
