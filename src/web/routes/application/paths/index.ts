export default {
  ...require("./paths"),
  ...require("./context-path"),
  ...require("./prefix-path"),
};
