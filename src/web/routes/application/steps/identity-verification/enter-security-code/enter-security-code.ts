import { validationResult } from "express-validator";

import { sanitise } from "./sanitise";
import { validate } from "./validate";

import {
  obfuscateEmail,
  obfuscatePhoneNumber,
} from "../../common/obfuscate-contact";
import { sessionDestroyButKeepLoggedIn } from "../../common/session-destroy";
import { TOO_MANY_ATTEMPTS_PATH } from "../../../../error-pages/too-many-attempts";
import { contextPath } from "../../../paths/context-path";
import { userIsBackOffice } from "../../../../azure-routes/auth";
import { getFlowDataObjectName } from "../../../tools/data-object";

const MAXIMUM_ALLOWED_OTP_ATTEMPTS = 9;

const reachedMaximumAllowedAttempts = (req) =>
  req.session[getFlowDataObjectName(req.session.journeyName)].otpAttempts >=
  MAXIMUM_ALLOWED_OTP_ATTEMPTS;

const pageContent = ({ translate, req }) => ({
  title: translate("enterSecurityCode.title"),
  sectionTitle: translate("section.yourDetails"),
  heading: translate("enterSecurityCode.heading"),
  renderEmailBody:
    req.session[getFlowDataObjectName(req.session.journeyName)][
      "security-code-method"
    ] === "email",
  renderTextBody:
    req.session[getFlowDataObjectName(req.session.journeyName)][
      "security-code-method"
    ] === "text",
  emailBodyFirstLine: translate("enterSecurityCode.emailBody.firstLine", {
    email: obfuscateEmail(
      req.session[getFlowDataObjectName(req.session.journeyName)].email
    ),
  }),
  emailBodySecondLine: translate("enterSecurityCode.emailBody.secondLine"),
  textBody: translate("enterSecurityCode.textBody", {
    phoneNumber: obfuscatePhoneNumber(
      req.session[getFlowDataObjectName(req.session.journeyName)][
        "phone-number"
      ]
    ),
  }),
  body: translate("enterSecurityCode.body", {
    path: contextPath("/security-code", req.session.journeyName),
  }),
  securityCode: translate("enterSecurityCode.securityCode"),
  backLinkText: translate("buttons:back"),
  continueButtonText: translate("buttons:continue"),
});

const isNavigable = (req) =>
  ((req.session[getFlowDataObjectName(req.session.journeyName)][
    "security-code-method"
  ] === "email" &&
    !!req.session[getFlowDataObjectName(req.session.journeyName)].email) ||
    (req.session[getFlowDataObjectName(req.session.journeyName)][
      "security-code-method"
    ] === "text" &&
      !!req.session[getFlowDataObjectName(req.session.journeyName)][
        "phone-number"
      ])) &&
  !userIsBackOffice(req);

const behaviourForPost = () => (req, res, next) => {
  if (!validationResult(req).isEmpty()) {
    if (reachedMaximumAllowedAttempts(req)) {
      const journeyName = req.session.journeyName;
      sessionDestroyButKeepLoggedIn(req, res);
      return res.redirect(contextPath(TOO_MANY_ATTEMPTS_PATH, journeyName));
    }
  }
  next();
};

const behaviourForGet = () => (req, res, next) => {
  delete req.session[getFlowDataObjectName(req.session.journeyName)][
    "security-code"
  ];
  next();
};

const enterSecurityCode = {
  path: "/enter-security-code",
  template: "enter-security-code",
  pageContent,
  isNavigable,
  sanitise,
  validate,
  behaviourForPost,
  behaviourForGet,
};

export {
  MAXIMUM_ALLOWED_OTP_ATTEMPTS,
  enterSecurityCode,
  pageContent,
  isNavigable,
  sanitise,
  validate,
  behaviourForPost,
  behaviourForGet,
};
