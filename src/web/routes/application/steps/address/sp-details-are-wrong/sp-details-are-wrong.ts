import { SECOND_PARENT_DATA_OBJECT_NAME } from "../../../tools/data-object";
import { sessionDestroyButKeepLoggedIn } from "../../common/session-destroy";

const pageContent = ({ translate }) => ({
  title: translate("detailsAreWrong.title"),
  heading: translate("detailsAreWrong.heading"),
  supportOrganisationsHeading: translate("supportOrganisations.heading"),
  supportOrganisationsBody: translate("supportOrganisations.body"),
  supportOrganisationsList: translate("supportOrganisations.listItems"),
});

const isNavigable = (req) =>
  req.session[SECOND_PARENT_DATA_OBJECT_NAME]["cancel-application"] === "yes";

const behaviourForGet = () => (req, res, next) => {
  sessionDestroyButKeepLoggedIn(req, res);
  next();
};

const spDetailsAreWrong = {
  path: "/cannot-request-certificate-details-unconfirmed",
  template: "application-cancelled",
  pageContent,
  isNavigable,
  behaviourForGet,
};

export { spDetailsAreWrong, pageContent, isNavigable, behaviourForGet };
