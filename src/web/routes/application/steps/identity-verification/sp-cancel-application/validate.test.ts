import expect from "expect";
import { validate } from "./validate";
import { applyExpressValidation } from "../../common/test/apply-express-validation";

test("validation middleware errors for nhs-number field is empty", async () => {
  const req = {
    body: {
      "cancel-application": "",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const errorOne = result.array()[0];

  expect(result.array().length).toBe(1);
  expect(errorOne.msg).toBe("validation:selectCancelApplication");
});

test("no validation error when valid nhs-number", async () => {
  const req = {
    body: {
      "cancel-application": "no",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});
