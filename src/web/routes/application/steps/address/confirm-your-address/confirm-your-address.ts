import { validate } from "./validate";
import { ADDRESS_KEYS } from "../../../constants";
import { toMultiLineString } from "../../common/formats";
import { userIsAgent, userIsBackOffice } from "../../../../azure-routes/auth";
import { validationResult } from "express-validator";
import { getFlowDataObjectName } from "../../../tools/data-object";

const pageContent = ({ translate, req }) => ({
  title: translate("confirmYourAddress.title"),
  heading: translate("confirmYourAddress.heading"),
  sectionTitle: translate("section.yourDetails"),
  yes: translate("yes"),
  no: translate("no"),
  or: translate("or"),
  alt: userIsAgent(req) ? translate("alt") : false,
  bodyLineOne:
    userIsAgent(req) || userIsBackOffice(req)
      ? translate("confirmYourAddress.body.firstLineCcs")
      : translate("confirmYourAddress.body.firstLine"),
  bodyLineTwo: translate("confirmYourAddress.body.secondLine"),
  questionHeading: translate("confirmYourAddress.questionHeading"),
  details: translate("confirmYourAddress.details", {
    name: `${
      req.session[getFlowDataObjectName(req.session.journeyName)]["first-name"]
    } ${
      req.session[getFlowDataObjectName(req.session.journeyName)]["last-name"]
    }`,
    address: toMultiLineString(
      ADDRESS_KEYS.map(
        getKeyFromSession(
          req.session[getFlowDataObjectName(req.session.journeyName)]
        )
      )
    ),
  }),
  continueButtonText: translate("buttons:continue"),
  backLinkText: translate("buttons:back"),
});

const getKeyFromSession = (session) => (key) => session[key];

const isNavigable = (req) => !userIsBackOffice(req);

const behaviourForPost = () => (req, res, next) => {
  if (!validationResult(req).isEmpty()) {
    return next();
  }

  const dataObjectName = getFlowDataObjectName(req.session.journeyName);

  if (req.body["confirm-your-address"] === "yes") {
    delete req.session[dataObjectName]["address-line-1-ccs"];
    delete req.session[dataObjectName]["address-line-2-ccs"];
    delete req.session[dataObjectName]["address-line-3-ccs"];
    delete req.session[dataObjectName]["address-line-4-ccs"];
    delete req.session[dataObjectName]["address-line-5-ccs"];
    delete req.session[dataObjectName]["postcode-ccs"];
    delete req.session[dataObjectName].sanitisedPostcodeCcs;
    delete req.session.postcodeLookupResults;
  }
  next();
};

const confirmYourAddress = {
  path: "/confirm-your-address",
  template: "confirm-your-address",
  pageContent,
  isNavigable,
  validate,
  behaviourForPost,
};

export {
  confirmYourAddress,
  pageContent,
  validate,
  isNavigable,
  behaviourForPost,
};
