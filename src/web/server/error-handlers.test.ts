import expect from "expect";
import * as sinon from "sinon";
import * as handler from "./error-handlers";
import {
  MAINFLOW,
  SECOND_PARENT_FLOW,
} from "../routes/application/journey-definitions";
import { CONTEXT_PATH, SP_CONTEXT_PATH } from "../routes/static-pages/paths";
import { START } from "../routes/start";
import { SP_START } from "../routes/sp-start";

test("Error handler should set the status to 500", () => {
  const status = sinon.spy();
  const redirect = sinon.spy();
  const req = {
    session: {
      journeyName: MAINFLOW.name,
    },
    t: () => {
      /* explicit empty function */
    },
  };

  const res = {
    status,
    redirect,
  };

  const next = jest.fn();

  handler.errorHandler({}, req, res, next);
  expect(status.calledWith(500)).toBe(true);
  expect(redirect.calledWith("/test-context/problem-with-the-service")).toBe(
    true
  );
  expect(next).toBeCalledTimes(1);
});

test("Error handler should set the status to 408", () => {
  const status = sinon.spy();
  const redirect = sinon.spy();

  const req = {
    session: {
      journeyName: MAINFLOW.name,
    },
    t: () => {
      /* explicit empty function */
    },
  };

  const res = {
    status,
    redirect,
  };

  const err = {
    statusCode: 408,
  };

  const next = jest.fn();

  handler.errorHandler(err, req, res, next);
  expect(status.calledWith(408)).toBe(true);
  expect(redirect.calledWith("/test-context/timed-out")).toBe(true);
  expect(next).toBeCalledTimes(1);
});

test("Error handler should set the status to 403 but go to session ended page", () => {
  const status = sinon.spy();
  const redirect = sinon.spy();

  const req = {
    session: {
      journeyName: MAINFLOW.name,
    },
    t: () => {
      /* explicit empty function */
    },
  };

  const res = {
    status,
    redirect,
    route: {
      path: "/test-context/default-route",
    },
  };

  const err = {
    statusCode: 403,
  };

  const next = jest.fn();

  handler.errorHandler(err, req, res, next);
  expect(status.calledWith(403)).toBe(true);
  expect(redirect.calledWith("/test-context/session-ended")).toBe(true);
  expect(next).toBeCalledTimes(1);
});

test("Error handler should set the status to 403 but go to first parent start page", () => {
  const status = sinon.spy();
  const redirect = sinon.spy();

  const req = {
    session: {
      journeyName: MAINFLOW.name,
    },
    t: () => {
      /* explicit empty function */
    },
  };

  const res = {
    status,
    redirect,
    route: {
      path: CONTEXT_PATH + START,
    },
  };

  const err = {
    statusCode: 403,
  };

  const next = jest.fn();

  handler.errorHandler(err, req, res, next);
  expect(status.calledWith(403)).toBe(true);
  expect(redirect.calledWith(CONTEXT_PATH + START)).toBe(true);
  expect(next).toBeCalledTimes(1);
});

test("Error handler should set the status to 403 but go to second parent start page", () => {
  const status = sinon.spy();
  const redirect = sinon.spy();

  const req = {
    session: {
      journeyName: SECOND_PARENT_FLOW.name,
    },
    t: () => {
      /* explicit empty function */
    },
  };

  const res = {
    status,
    redirect,
    route: {
      path: SP_CONTEXT_PATH + SP_START,
    },
  };

  const err = {
    statusCode: 403,
  };

  const next = jest.fn();

  handler.errorHandler(err, req, res, next);
  expect(status.calledWith(403)).toBe(true);
  expect(redirect.calledWith(SP_CONTEXT_PATH + SP_START)).toBe(true);
  expect(next).toBeCalledTimes(1);
});

test("registerErrorHandlers() should configure app to use error handler and logging", () => {
  const app = {
    use: jest.fn(),
    listen: jest.fn(),
  };

  handler.registerErrorHandlers(app);
  expect(app.use).toBeCalledTimes(2);
});
