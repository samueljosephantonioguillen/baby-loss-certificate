import expect from "expect";

import { sanitise } from "./sanitise";

const next = jest.fn();

test("sanitise should trim all spaces", () => {
  const req = {
    body: {
      "security-code": "  12 3 45 6",
    },
  };

  sanitise()(req, {}, next);

  expect(req.body).toEqual({
    "security-code": "123456",
  });
});
