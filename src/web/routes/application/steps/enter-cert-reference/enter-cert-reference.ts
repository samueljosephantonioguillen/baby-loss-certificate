import { sanitise } from "./sanitise";
import { validate } from "./validate";

import { logger } from "../../../../logger/logger";
import { lookupCertificateReference } from "../common/pg-client";
import { SECOND_PARENT_DATA_OBJECT_NAME } from "../../tools/data-object";
import { formatDate } from "../common/format-date";

const pageContent = ({ translate }) => ({
  title: translate("enterCertReference.title"),
  heading: translate("enterCertReference.heading"),
  hintTextOne: translate("enterCertReference.hintText.lineOne"),
  hintTextTwo: translate("enterCertReference.hintText.lineTwo"),
  hintTextThree: translate("enterCertReference.hintText.lineThree"),
  continueButtonText: translate("buttons:continue"),
});

const behaviourForGet = () => (req, res, next) => {
  req.session.hideServiceLink = true;
  next();
};

const behaviourForPost = () => async (req, res, next) => {
  if (!req.session[SECOND_PARENT_DATA_OBJECT_NAME]) {
    req.session[SECOND_PARENT_DATA_OBJECT_NAME] = {};
  }

  try {
    const certificateReference = req.body["sp-sanitised-reference-number"];
    logger.info(
      `about to query application table for: ${certificateReference}`
    );
    const result = await lookupCertificateReference(certificateReference);

    if (Array.isArray(result.rows) && result.rows.length === 0) {
      logger.info(
        `no valid row found for certificate reference: ${certificateReference}`
      );
      res.locals.errorTitleText = req.t("validation:errorTitleText");
      res.locals.errors = [
        {
          path: "sp-reference-number",
          msg: req.t("enterCertReference.noCertificateReferenceFoundError"),
        },
      ];
    } else {
      logger.info(
        `retrieved row for certificate reference: ${certificateReference}`
      );

      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-parent-id"] =
        result.rows[0].parent1_id;
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-date-of-birth"] =
        result.rows[0].parent1_date_of_birth;
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-reference-country"] =
        result.rows[0].parent1_reference_number_issuing_country;
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-first-name"] =
        result.rows[0].parent1_given_name;
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-last-name"] =
        result.rows[0].parent1_family_name;
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-relation-to-baby"] =
        result.rows[0].parent1_relationship;
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-reference-number"] =
        result.rows[0].parent1_reference_number;
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-reference-type"] =
        result.rows[0].parent1_reference_type;
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-email"] =
        result.rows[0].parent1_email;
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-created-by"] =
        result.rows[0].parent1_created_by;
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["baby-sex"] =
        result.rows[0].baby_sex;
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["baby-loss-location"] =
        result.rows[0].loss_placename;
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["print-date-of-loss"] =
        result.rows[0].print_date_of_loss;
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["baby-date-of-loss-day"] =
        result.rows[0].loss_day;
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["baby-date-of-loss-month"] =
        result.rows[0].loss_month;
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["baby-date-of-loss-year"] =
        result.rows[0].loss_year;
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["baby-name"] =
        result.rows[0].baby_name;
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["relation-to-baby"] =
        result.rows[0].parent2_relationship;
      const applicationId = result.rows[0].application_id;
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["application-id"] =
        applicationId;
      req.session.applicationId = applicationId;
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["sp-date-of-birth"] =
        formatDate(result.rows[0].parent2_date_of_birth);
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["sp-parent-id"] =
        result.rows[0].parent2_id;
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["sp-email"] =
        result.rows[0].parent2_email;

      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-address-line-1"] =
        result.rows[0].application_address_line_1;
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-address-line-2"] =
        result.rows[0].application_address_line_2;
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-address-line-3"] =
        result.rows[0].application_address_line_3;
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-address-line-4"] =
        result.rows[0].application_address_line_4;
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-address-line-5"] =
        result.rows[0].application_address_line_5;
      req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-postcode"] =
        result.rows[0].application_address_postcode;
    }
  } catch (error) {
    logger.error(`query to application table has failed: ${error}`);
    res.locals.errorTitleText = req.t("validation:errorTitleText");
    res.locals.errors = [
      {
        path: "continue-button",
        msg: req.t("validation:tryAgainLater"),
      },
    ];
  }

  return next();
};

const enterCertReference = {
  path: "/enter-reference-number",
  template: "enter-cert-reference",
  pageContent,
  sanitise,
  validate,
  behaviourForPost,
  behaviourForGet,
};

export {
  enterCertReference,
  pageContent,
  sanitise,
  validate,
  behaviourForPost,
  behaviourForGet,
};
