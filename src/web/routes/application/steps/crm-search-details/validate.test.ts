import expect from "expect";
import { validate } from "./validate";
import { applyExpressValidation } from "../common/test/apply-express-validation";

describe("Validation of all fields", () => {
  test("when all the fields are empty return expected error", async () => {
    const req = {
      body: {
        "first-name": "",
        "last-name": "",
        "certificate-reference-number": "",
        "date-of-birth-day": "",
        "date-of-birth-month": "",
        "date-of-birth-year": "",
        postcode: "",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    const error = result.array();
    expect(result.array().length).toBe(1);
    expect(error[0].msg).toBe("validation:searchCriteriaMissing");
  });
});
describe("First Name and Last Name Validation", () => {
  describe("invalid names", () => {
    test("expected validation middleware errors if first-name and last-name is invalid (numbers)", async () => {
      const req = {
        body: {
          "first-name": "J0hn",
          "last-name": "Sm1th",
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("Express validation failed");
      }

      const errorOne = result.array()[0];
      const errorTwo = result.array()[1];

      expect(result.array().length).toBe(2);
      expect(errorOne.msg).toBe("validation:patternFirstName");
      expect(errorTwo.msg).toBe("validation:patternLastName");
    });

    test("expected validation middleware errors if first-name and last-name is invalid (special characters)", async () => {
      const req = {
        body: {
          "first-name": "J£ssica",
          "last-name": "Sm!th",
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("Express validation failed");
      }

      const errorOne = result.array()[0];
      const errorTwo = result.array()[1];

      expect(result.array().length).toBe(2);
      expect(errorOne.msg).toBe("validation:patternFirstName");
      expect(errorTwo.msg).toBe("validation:patternLastName");
    });

    test("expected validation middleware errors if first-name and last-name is invalid (begins with [-])", async () => {
      const req = {
        body: {
          "first-name": "-Luke",
          "last-name": "-Skywalker",
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("Express validation failed");
      }

      const errorOne = result.array()[0];
      const errorTwo = result.array()[1];

      expect(result.array().length).toBe(2);
      expect(errorOne.msg).toBe("validation:patternFirstName");
      expect(errorTwo.msg).toBe("validation:patternLastName");
    });

    test("expected validation middleware errors if first-name and last-name is invalid (begins with ['])", async () => {
      const req = {
        body: {
          "first-name": "'John",
          "last-name": "'O'Hara",
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("Express validation failed");
      }

      const errorOne = result.array()[0];
      const errorTwo = result.array()[1];

      expect(result.array().length).toBe(2);
      expect(errorOne.msg).toBe("validation:patternFirstName");
      expect(errorTwo.msg).toBe("validation:patternLastName");
    });

    test("No validation error when first-name and last-name fields contains [a-z] [A-Z] ['- space] ", async () => {
      const req = {
        body: {
          "first-name": "John'a -RT",
          "last-name": "O'She-A",
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("Express validation failed");
      }

      expect(result.array().length).toBe(0);
    });
  });
  describe("invalid names length", () => {
    test("validation middleware errors for first-name field exceeds max length", async () => {
      const req = {
        body: {
          "first-name":
            "RalphRalphRalphRalphRalphRalphRalphRalphRalphRalphRalphRalphRalphRalphRalphRalph",
          "last-name": "Smith",
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("Express validation failed");
      }

      const errorOne = result.array()[0];

      expect(result.array().length).toBe(1);
      expect(errorOne.msg).toBe("validation:firstNameTooLong");
    });

    test("validation middleware errors for last-name field exceeds max length", async () => {
      const req = {
        body: {
          "first-name": "John",
          "last-name":
            "SmithSmithSmithSmithSmithSmithSmithSmithSmithSmithSmithSmithSmithSmithSmithSmith",
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("Express validation failed");
      }

      const errorOne = result.array()[0];

      expect(result.array().length).toBe(1);
      expect(errorOne.msg).toBe("validation:lastNameTooLong");
    });
  });
  describe("Valid Names", () => {
    test("No validation error when first-name and last-name fields are valid", async () => {
      const req = {
        body: {
          "first-name": "Ralph",
          "last-name": "Smith",
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("Express validation failed");
      }

      expect(result.array().length).toBe(0);
    });
  });
});
describe("Application Reference number Validation", () => {
  test("when invalid field, return expected error", async () => {
    const req = {
      body: {
        "certificate-reference-number": "*$%!-E586D",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    const expectedError = {
      type: "field",
      value: req.body["certificate-reference-number"],
      msg: "validation:patternReferenceNumber",
      path: "certificate-reference-number",
      location: "body",
    };

    const error = result.array()[0];

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });
  test("when field is too long, return expected error", async () => {
    const req = {
      body: {
        "certificate-reference-number":
          "9AD-E586D-9AD-E586D-9AD-E586D-9AD-E586D",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    const expectedError = {
      type: "field",
      value: req.body["certificate-reference-number"],
      msg: "validation:referenceNumberTooLong",
      path: "certificate-reference-number",
      location: "body",
    };

    const error = result.array()[0];

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });
  test("return no errors when reference number is valid", async () => {
    const req = {
      body: {
        "certificate-reference-number": "9AD-A86-E586D",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    const error = result.array()[0];

    expect(result.array().length).toBe(0);
    expect(error).toBeFalsy();
  });
});
describe("PostCode Validation", () => {
  test("return no errors when postcode is valid", async () => {
    const req = {
      body: {
        postcode: "BS1 4TB",
        sanitisedPostcode: "BS1 4TB",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    const error = result.array()[0];

    expect(result.array().length).toBe(0);
    expect(error).toBeFalsy();
  });
  test("when invalid field, return expected error", async () => {
    const req = {
      body: {
        postcode: "invalid postcode",
        sanitisedPostcode: "invalid postcode",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("async result is undefined after waiting");
    }

    const expectedError = {
      type: "field",
      value: req.body["postcode"],
      msg: "validation:invalidPostcode",
      path: "postcode",
      location: "body",
    };

    const error = result.array()[0];

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });
});
describe("DateOfBirth validation", () => {
  test("should not throw any errors when valid dob", async () => {
    const req = {
      body: {
        "date-of-birth-day": 1,
        "date-of-birth-month": 1,
        "date-of-birth-year": 2000,
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }
    const error = result.array()[0];
    expect(result.array().length).toBe(0);
    expect(error).toBeFalsy();
  });

  test("should throw expected error when dob is not a real date", async () => {
    const req = {
      body: {
        "date-of-birth-day": 45,
        "date-of-birth-month": 1,
        "date-of-birth-year": 2000,
      },
    };
    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    const expectedError = {
      type: "field",
      value: "2000-01-45",
      msg: "validation:dateOfBirthNotReal",
      path: "date-of-birth",
      location: "body",
    };
    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });

  test("should throw expected errors when dob is not a valid date string - invalid dob day", async () => {
    const req = {
      body: {
        "date-of-birth-day": "PPP",
        "date-of-birth-month": 12,
        "date-of-birth-year": 2000,
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    const expectedErrors = [
      {
        type: "field",
        value: "2000-12-PPP",
        msg: "validation:dateOfBirthDayInvalid",
        path: "date-of-birth",
        location: "body",
      },
    ];

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedErrors[0]);
  });

  test("should throw expected errors when dob is not a valid date string - missing dob month", async () => {
    const req = {
      body: {
        "date-of-birth-day": 12,
        "date-of-birth-month": null,
        "date-of-birth-year": 2000,
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    const expectedErrors = [
      {
        type: "field",
        value: "2000-00-12",
        msg: "validation:dateOfBirthMonthInvalid",
        path: "date-of-birth",
        location: "body",
      },
    ];

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedErrors[0]);
  });

  test("should throw expected errors when dob is not a valid date string - missing dob year", async () => {
    const req = {
      body: {
        "date-of-birth-day": 9,
        "date-of-birth-month": 2,
        "date-of-birth-year": "",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    const expectedErrors = [
      {
        type: "field",
        value: "-02-09",
        msg: "validation:missingDateOfBirthYear",
        path: "date-of-birth",
        location: "body",
      },
    ];

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedErrors[0]);
  });

  test("should throw expected errors when dob is not the past", async () => {
    const req = {
      body: {
        "date-of-birth-day": 12,
        "date-of-birth-month": 12,
        "date-of-birth-year": 9999,
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    const expectedError = {
      type: "field",
      value: "9999-12-12",
      msg: "validation:dateOfBirthInFuture",
      path: "date-of-birth",
      location: "body",
    };

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });
});
