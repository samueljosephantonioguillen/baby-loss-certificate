import { check } from "express-validator";

import { translateValidationMessage } from "../../common/translate-validation-message";
import {
  NHS_NUMBER_LENGTH,
  NHS_NUMBER_PATTERN,
  NHS_NUMBER_PATTERN_NO_LENGTH,
} from "../../../constants";

const validate = () => [
  check("nhs-number")
    .not()
    .isEmpty()
    .bail()
    .withMessage(translateValidationMessage("validation:missingNhsNumber"))
    .matches(NHS_NUMBER_PATTERN_NO_LENGTH)
    .bail()
    .withMessage(
      translateValidationMessage("validation:invalidNhsNumberFormat")
    )
    .isLength({ min: NHS_NUMBER_LENGTH, max: NHS_NUMBER_LENGTH })
    .bail()
    .withMessage(
      translateValidationMessage("validation:invalidNhsNumberLength")
    )
    .matches(NHS_NUMBER_PATTERN)
    .bail()
    .withMessage(
      translateValidationMessage("validation:invalidNhsNumberFormat")
    ),
];

export { validate };
