import { check } from "express-validator";
import { translateValidationMessage } from "../../common/translate-validation-message";

const validate = () => [
  check("add-another-parent")
    .not()
    .isEmpty()
    .bail()
    .withMessage(
      translateValidationMessage("validation:selectAddAnotherParentOption")
    ),
];

export { validate };
