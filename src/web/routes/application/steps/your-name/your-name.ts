import { userIsBackOffice } from "../../../azure-routes/auth";
import { getFlowDataObjectName } from "../../tools/data-object";
import { sanitise } from "./sanitise";
import { validate } from "./validate";

const pageContent = ({ translate }) => ({
  title: translate("yourName.title"),
  sectionTitle: translate("section.yourDetails"),
  heading: translate("yourName.heading"),
  body: translate("yourName.body"),
  hint: translate("yourName.hint"),
  firstName: translate("yourName.firstName"),
  lastName: translate("yourName.lastName"),
  insetTextLineOne: translate("yourName.insetTextFirstLine"),
  backLinkText: translate("buttons:back"),
  continueButtonText: translate("buttons:continue"),
});

const isNavigable = (req) =>
  req.session[getFlowDataObjectName(req.session.journeyName)][
    "know-your-nhs-number"
  ] === "no" || userIsBackOffice(req);

const yourName = {
  path: "/your-name",
  template: "your-name",
  pageContent,
  isNavigable,
  sanitise,
  validate,
};

export { yourName, pageContent, isNavigable, sanitise, validate };
