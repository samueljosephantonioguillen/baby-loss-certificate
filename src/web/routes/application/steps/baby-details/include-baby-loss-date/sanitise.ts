const trimLeadingZero = (input) => input.replace(/\b0+/g, "");

const sanitise = () => (req, res, next) => {
  req.body["baby-date-of-loss-day"] = trimLeadingZero(
    req.body["baby-date-of-loss-day"]
  );

  req.body["baby-date-of-loss-month"] = trimLeadingZero(
    req.body["baby-date-of-loss-month"]
  );

  req.body["baby-date-of-loss-year"] = trimLeadingZero(
    req.body["baby-date-of-loss-year"]
  );

  next();
};

export { sanitise, trimLeadingZero };
