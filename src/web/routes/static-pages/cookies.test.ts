import expect from "expect";

import { renderCookies, pageContent, get, post } from "./cookies";
import { COOKIES } from "./paths";

const app = {
  get: jest.fn(),
  post: jest.fn(),
};

const req = {
  t: (string) => string,
  cookies: {
    cookieConsent: "true",
  },
  body: {
    "enable-analytics-cookies": "yes",
  },
  session: {
    savedCookies: true,
    hideServiceLink: true,
  },
};

const res = {
  render: jest.fn(),
  redirect: jest.fn(),
  cookie: jest.fn(),
};

describe("pageContent", () => {
  test("should return expected results when cookieConsent is true", () => {
    const translate = (string, object?) => `${string}${object}`;

    const expected = {
      title: translate("cookies.title"),
      bannerTitle: translate("cookies.bannerTitle"),
      bannerText: translate("cookies.bannerText"),
      whatAreCookiesHeading: translate("cookies.whatAreCookies.heading"),
      whatAreCookiesBody: translate("cookies.whatAreCookies.body"),
      essentialCookiesHeading: translate("cookies.essentialCookies.heading"),
      essentialCookiesBody: translate("cookies.essentialCookies.body"),
      essentialCookiesTableHeadings: translate(
        "cookies.essentialCookies.table.headers"
      ),
      essentialCookiesTableRows: translate(
        "cookies.essentialCookies.table.cookies"
      ),
      analyticsCookiesHeading: translate("cookies.analyticsCookies.heading"),
      analyticsCookiesBody: translate("cookies.analyticsCookies.body"),
      analyticsCookiesTableHeadings: translate(
        "cookies.analyticsCookies.table.headers"
      ),
      analyticsCookiesTableRows: translate(
        "cookies.analyticsCookies.table.cookies"
      ),
      enableAnalyticsCookies: translate("cookies.enableAnalyticsCookies"),
      cookies: "yes",
      savedCookies: req.session.savedCookies,
      yes: translate("yes"),
      no: translate("no"),
      button: translate("cookies.button"),
      hideServiceLink: req.session.hideServiceLink,
    };
    const result = pageContent({ translate, req });

    expect(result).toEqual(expected);
  });

  test("should return expected results when cookieConsent is not true", () => {
    const translate = (string, object?) => `${string}${object}`;

    const request = {
      ...req,
      cookies: {
        cookieConsent: "false",
      },
      body: {
        "enable-analytics-cookies": "no",
      },
      session: {
        cookieConsent: false,
      },
    };

    const expected = {
      title: translate("cookies.title"),
      bannerTitle: translate("cookies.bannerTitle"),
      bannerText: translate("cookies.bannerText"),
      whatAreCookiesHeading: translate("cookies.whatAreCookies.heading"),
      whatAreCookiesBody: translate("cookies.whatAreCookies.body"),
      essentialCookiesHeading: translate("cookies.essentialCookies.heading"),
      essentialCookiesBody: translate("cookies.essentialCookies.body"),
      essentialCookiesTableHeadings: translate(
        "cookies.essentialCookies.table.headers"
      ),
      essentialCookiesTableRows: translate(
        "cookies.essentialCookies.table.cookies"
      ),
      analyticsCookiesHeading: translate("cookies.analyticsCookies.heading"),
      analyticsCookiesBody: translate("cookies.analyticsCookies.body"),
      analyticsCookiesTableHeadings: translate(
        "cookies.analyticsCookies.table.headers"
      ),
      analyticsCookiesTableRows: translate(
        "cookies.analyticsCookies.table.cookies"
      ),
      enableAnalyticsCookies: translate("cookies.enableAnalyticsCookies"),
      cookies: "no",
      yes: translate("yes"),
      no: translate("no"),
      button: translate("cookies.button"),
    };
    const result = pageContent({ translate, req: request });

    expect(result).toEqual(expected);
  });
});

test("renderCookies should register get and post routes with expected callbacks", () => {
  app.get.mockImplementation((path, callback) => {
    callback(req, res);
  });

  app.post.mockImplementation((path, callback) => {
    callback(req, res);
  });

  renderCookies(app);

  expect(app.get).toBeCalledTimes(1);
  expect(app.get).toBeCalledWith("/test-context" + COOKIES, get);

  expect(app.post).toBeCalledTimes(1);
  expect(app.post).toBeCalledWith("/test-context" + COOKIES, post);
});

test("get should set savedCookies to false and render expected content", () => {
  get(req, res);

  expect(req.session.savedCookies).toEqual(false);
  expect(res.render).toBeCalled();
  expect(res.render).toBeCalledWith("static-pages/cookies", {
    ...pageContent({ translate: req.t, req }),
    savedCookies: true,
  });
});

test("post should set savedCookies to true and render expected content", () => {
  post(req, res);

  expect(req.session.savedCookies).toEqual(true);
  expect(res.redirect).toBeCalled();
  expect(res.redirect).toBeCalledWith(process.env.CONTEXT_PATH + "/cookies");
});
