import { REQUEST_COMPLETE_URL } from "../../paths/paths";
import { prefixPath } from "../../paths/prefix-path";
import { logger } from "../../../../logger/logger";
import { isPathAllowed } from "./predicates";
import {
  getNextNavigablePath,
  getNextInDetailReviewPath,
  getNextInReviewPath,
} from "./selectors";

import {
  getStateFromSession,
  setStateInSession,
  getNextAllowedPathFromSession,
  setNextAllowedPathInSession,
  getCurrentPathFromSession,
  setCurrentPathInSession,
} from "../session-accessors";

import {
  IN_PROGRESS,
  IN_DETAILS_REVIEW,
  IN_REVIEW,
  IN_REVIEW_PROGRESS,
  COMPLETED,
  IN_DETAILS_REVIEW_PROGRESS,
} from "../states";

const stateMachine = {
  [IN_PROGRESS]: {
    getNextPath: (req, journey) => getNextNavigablePath(req.path, req, journey),
    isPathAllowed: (req, journey) =>
      isPathAllowed(
        journey.pathsInSequence,
        getNextAllowedPathFromSession(req, journey),
        req.path
      ),
    getNextAllowedPath: getNextAllowedPathFromSession,
    setNextAllowedPath: setNextAllowedPathInSession,
    incrementNextAllowedPath: (req, journey) =>
      setNextAllowedPathInSession(
        req,
        journey,
        getNextNavigablePath(req.path, req, journey)
      ),
  },
  [IN_DETAILS_REVIEW]: {
    getNextPath: (req, journey) =>
      getNextInDetailReviewPath(req, journey.pathPrefix),
    isPathAllowed: (req, journey) =>
      isPathAllowed(
        journey.pathsInSequence,
        getNextAllowedPathFromSession(req, journey),
        req.path
      ),
    getNextAllowedPath: getNextAllowedPathFromSession,
    setNextAllowedPath: setNextAllowedPathInSession,
    incrementNextAllowedPath: (req, journey) =>
      setNextAllowedPathInSession(
        req,
        journey,
        getNextInDetailReviewPath(req, journey.pathPrefix)
      ),
  },
  [IN_DETAILS_REVIEW_PROGRESS]: {
    getNextPath: (req, journey) => getNextNavigablePath(req.path, req, journey),
    isPathAllowed: (req, journey) =>
      isPathAllowed(
        journey.pathsInSequence,
        getNextAllowedPathFromSession(req, journey),
        req.path
      ),
    getNextAllowedPath: getNextAllowedPathFromSession,
    setNextAllowedPath: setNextAllowedPathInSession,
    incrementNextAllowedPath: (req, journey) =>
      setNextAllowedPathInSession(
        req,
        journey,
        getNextNavigablePath(req.path, req, journey)
      ),
  },
  [IN_REVIEW]: {
    getNextPath: (req, journey) => getNextInReviewPath(req, journey.pathPrefix),
    isPathAllowed: (req, journey) =>
      isPathAllowed(
        journey.pathsInSequence,
        getNextAllowedPathFromSession(req, journey),
        req.path
      ),
    getNextAllowedPath: getNextAllowedPathFromSession,
    setNextAllowedPath: setNextAllowedPathInSession,
    incrementNextAllowedPath: (req, journey) =>
      setNextAllowedPathInSession(
        req,
        journey,
        getNextInReviewPath(req, journey.pathPrefix)
      ),
    invalidateReview: (req, journey) =>
      setStateInSession(req, journey, IN_PROGRESS),
  },
  [IN_REVIEW_PROGRESS]: {
    getNextPath: (req, journey) => getNextNavigablePath(req.path, req, journey),
    isPathAllowed: (req, journey) =>
      isPathAllowed(
        journey.pathsInSequence,
        getNextAllowedPathFromSession(req, journey),
        req.path
      ),
    getNextAllowedPath: getNextAllowedPathFromSession,
    setNextAllowedPath: setNextAllowedPathInSession,
    incrementNextAllowedPath: (req, journey) =>
      setNextAllowedPathInSession(
        req,
        journey,
        getNextNavigablePath(req.path, req, journey)
      ),
  },
  [COMPLETED]: {
    getNextPath: (req, journey) =>
      prefixPath(journey.pathPrefix, REQUEST_COMPLETE_URL),
    isPathAllowed: (req, journey) =>
      req.path === prefixPath(journey.pathPrefix, REQUEST_COMPLETE_URL),
    getNextAllowedPath: (req, journey) =>
      prefixPath(journey.pathPrefix, REQUEST_COMPLETE_URL),
    setNextAllowedPath: setNextAllowedPathInSession,
    incrementNextAllowedPath: (req, journey) =>
      setNextAllowedPathInSession(
        req,
        journey,
        prefixPath(journey.pathPrefix, REQUEST_COMPLETE_URL)
      ),
  },

  getState: getStateFromSession,

  getCurrentPath: getCurrentPathFromSession,

  setState: (state, req, journey) => {
    if (getStateFromSession(req, journey) !== state) {
      logger.info(`State set to ${state}`, req);
      setStateInSession(req, journey, state);
    }
  },

  dispatch: (actionType, req, journey, ...args) => {
    const state = getStateFromSession(req, journey);
    const action = stateMachine[state][actionType];
    if (typeof action !== "undefined") {
      setCurrentPathInSession(req, journey, req.path);
      return action(req, journey, ...args);
    }

    return null;
  },
};

export { stateMachine };
