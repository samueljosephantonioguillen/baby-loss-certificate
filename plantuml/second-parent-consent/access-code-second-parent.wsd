@startuml 
' Second Parent Access Code Seond Parent Sequence Diagram

skinparam BoxPadding 10
skinparam sequenceArrowThickness 2
skinparam roundcorner 20

participant "Second Parent" as EU order 1

box "AWS" #SeaShell
participant "BLC Web App" as BLC order 2
participant "Secrets Manager" as Secrets order 3
participant "Postgres" as Postgres order 4
participant "Redis" as Redis order 5
participant "Complete Applications\nSQS" as CompletedApplications order 6
end box

box "External Dependencies" #HoneyDew
participant "PDS FHIR API" as FHIR order 7
participant "Gov.UK Notify" as Notify order 8
end box

EU -> BLC: Launch application
BLC -> EU: Second Parent Start Page


EU --> BLC: Access Code

BLC -> BLC: Check application\nidentifier from url

alt Application found
    BLC -> Postgres: Retrieve incomplete application
    Postgres --> BLC: Incomplete application
    BLC -> EU: Ask for name
    EU --> BLC: Provide name
    BLC -> BLC: Check name matches
    BLC -> EU: Show Application Summary

    EU --> BLC: Accept or decline\nto continue
        
    alt End user declined print certificate
        BLC -> BLC: Check print on decline

        alt Print on decline
            BLC -> Postgres: Remove Incomplete Application
            BLC -> CompletedApplications: Send Complete Application
        else
            BLC -> Postgres: Remove Incomplete Application and store Failed Application
        end
        BLC -> BLC: Get applicant contact details
        BLC -> Notify: Notify applicant of result
        BLC -> EU: Confirmation
    
    else

        alt NHS number known
            EU --> BLC: Provide NHS number
            EU <- BLC: Review NHS number
            EU --> BLC: Confirm NHS number
            BLC -> Secrets: Request access token
            BLC <- Secrets: Access token
            BLC -> FHIR: Request PDS record
            BLC <-- FHIR: PDS record
        
        else NHS number not known
            EU -> BLC: Provide second parent details
            EU <- BLC: Review second parent details
            EU -> BLC: Confirm second parent details
            BLC -> Secrets: Request access token
            BLC <- Secrets: Access token
            BLC -> FHIR: Search for PDS record
            BLC <-- FHIR: PDS record
        end


    group One-Time Passcode
        BLC -> EU: Choose OTP method
        EU --> BLC: Method selection
        BLC -> Redis: Generate and store OTP

    alt SMS
        BLC -> Notify: POST /v2/notifications/sms
        BLC <-- Notify: API response code
        BLC -> EU: Request OTP
        EU --> BLC: Provides OTP

    else Email
        BLC -> Notify: POST /v2/notifications/email
        BLC <-- Notify: API response code
        BLC -> EU: Request OTP
        EU --> BLC: provides OTP
    end

    end

    BLC -> EU: Application Details
    EU --> BLC: Confirmation
    BLC -> EU: Declaration
    EU --> BLC: Confirmation
    BLC -> Postgres: Store complete application
    BLC -> BLC: Check applicant prefrences
    BLC -> Notify: Notify applicant

end

else Applicant invalid/not found
    EU <-- BLC: No result found kickout
end
@enduml