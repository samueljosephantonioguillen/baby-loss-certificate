import express from "express";
import nocache from "nocache";
import { configureGet } from "./flow-control/middleware/configure-get";
import { configurePost } from "./flow-control/middleware/configure-post";
import { configureSessionDetails } from "./flow-control/middleware/session-details";
import { handlePost } from "./flow-control/middleware/handle-post";
import { handleRequestForPath } from "./flow-control/middleware/handle-path-request";
import { handlePostRedirects } from "./flow-control/middleware//handle-post-redirects";
import { injectAdditionalHeaders } from "./flow-control/middleware/inject-headers";
import { renderView } from "./flow-control/middleware/render-view";
import { sanitise } from "./flow-control/middleware/sanitise";
import { escape } from "./flow-control/middleware/unescape-specific";
import { contextPath } from "./paths/context-path";
import { logger } from "../../logger/logger";
import { configureAuthentication } from "./flow-control/middleware/configure-authentication";
import {
  CRM_DATA_OBJECT_NAME,
  MAINFLOW_DATA_OBJECT_NAME,
  SECOND_PARENT_DATA_OBJECT_NAME,
} from "./tools/data-object";
import {
  CONTEXT_PATH,
  CRM_CONTEXT_PATH,
  SP_CONTEXT_PATH,
} from "../static-pages/paths";

const middlewareNoop = () => (req, res, next) => next();

export const FIRST_STEP = CONTEXT_PATH + "/live-in-england";
export const SP_FIRST_STEP = SP_CONTEXT_PATH + "/enter-reference-number";
export const CRM_FIRST_STEP = CRM_CONTEXT_PATH + "/search-details";

export const checkExpiredSession = (journey) => (req, res, next) => {
  // if session cookie is out of date, redirect to session ended page
  if (
    !req.session[MAINFLOW_DATA_OBJECT_NAME] &&
    !req.session[SECOND_PARENT_DATA_OBJECT_NAME] &&
    !req.session[CRM_DATA_OBJECT_NAME] &&
    req.route.path !== FIRST_STEP &&
    req.route.path !== SP_FIRST_STEP &&
    req.route.path !== CRM_FIRST_STEP
  ) {
    logger.info("Session is cleared, redirecting to session ended page", req);
    return res.redirect(contextPath("/session-ended", journey.name));
  }
  next();
};

export const handleOptionalMiddleware =
  (args) =>
  (operation, fallback = middlewareNoop) =>
    typeof operation === "undefined" ? fallback() : operation(...args);

const createRoute = (config, csrfProtection, journey, router) => (step) => {
  const { steps } = journey;
  // Make [config, journey, step] available as arguments to all optional middleware
  const optionalMiddleware = handleOptionalMiddleware([config, journey, step]);

  return router
    .route(step.path)
    .get(
      checkExpiredSession(journey),
      injectAdditionalHeaders,
      csrfProtection,
      configureSessionDetails(journey),
      configureAuthentication,
      configureGet(steps, step, journey),
      handleRequestForPath(journey, step),
      optionalMiddleware(step.behaviourForGet),
      renderView(step)
    )
    .post(
      checkExpiredSession(journey),
      injectAdditionalHeaders,
      csrfProtection,
      sanitise,
      escape(),
      configureSessionDetails(journey),
      configureAuthentication,
      configurePost(steps, step, journey),
      optionalMiddleware(step.sanitise),
      optionalMiddleware(step.validate),
      optionalMiddleware(step.behaviourForPost),
      handlePost(journey),
      handleRequestForPath(journey, step),
      handlePostRedirects(journey),
      renderView(step)
    );
};

export const registerJourneyRoutes =
  (config, csrfProtection, app) => (journey) => {
    const wizard = express.Router();
    journey.steps.forEach(createRoute(config, csrfProtection, journey, wizard));
    app.use(nocache());
    app.use(wizard);
  };
