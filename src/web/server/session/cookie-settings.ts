import * as dotEnv from "dotenv";

dotEnv.config();

const SESSION_TIMEOUT_MINUTES = process.env.SESSION_TIMEOUT_MINUTES ?? "30";
const COOKIE_EXPIRES_MILLISECONDS =
  parseInt(SESSION_TIMEOUT_MINUTES) * 60 * 1000;

export { COOKIE_EXPIRES_MILLISECONDS };
