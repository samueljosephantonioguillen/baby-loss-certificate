import { getFlowDataObjectName } from "../../../tools/data-object";
import { isDateRealDate } from "../../common/validate-date";

const partialDateMonthYearFormat = /^([12]\d{3}-(0[1-9]|1[0-2])-)$/;
const partialYearOnlyFormat = /^([12]\d{3}--)$/;
const eligibleDate = new Date("2018-09-01 00:00:00");

const isGivenYearDoesNotMatch = (req) =>
  req.body["baby-date-of-loss-year"] &&
  req.body["baby-date-of-loss-year"] !==
    req.session[getFlowDataObjectName(req.session.journeyName)]["year-of-loss"];

const isFullDate = (req) =>
  req.body["baby-date-of-loss-day"] &&
  req.body["baby-date-of-loss-month"] &&
  req.body["baby-date-of-loss-year"];

const isMonthYearDate = (req) =>
  !req.body["baby-date-of-loss-day"] &&
  req.body["baby-date-of-loss-month"] &&
  req.body["baby-date-of-loss-year"];

const isYearOnlyDate = (req) =>
  !req.body["baby-date-of-loss-day"] &&
  !req.body["baby-date-of-loss-month"] &&
  req.body["baby-date-of-loss-year"];

const isDateOfLossAfterParentDob = (dateOfLoss, parentDob) => {
  const dateLoss = new Date(dateOfLoss);
  const dateDob = new Date(parentDob);

  return dateLoss > dateDob;
};

const isDateBeforeToday = (dateOfLoss) => {
  const lossDate = new Date(dateOfLoss);
  const today = new Date();

  return lossDate <= today;
};

const isOnOrAfterEligibleDate = (dateOfLoss) => {
  const lossDate = new Date(dateOfLoss);
  return lossDate.getTime() >= eligibleDate.getTime();
};

const isValidMonthYearDate = (partialDate) => {
  return partialDateMonthYearFormat.test(partialDate);
};

const isPartialDateInPast = (partialDate: Date): boolean => {
  const currentDate = new Date();
  const currentYear = currentDate.getFullYear();
  const partialYear = partialDate.getFullYear();
  const currentMonth = currentDate.getMonth();
  const partialMonth = partialDate.getMonth();

  return (
    partialYear < currentYear ||
    (partialYear === currentYear && currentMonth >= partialMonth)
  );
};

const isPartialDateLossAfterParentDob = (partialDate, req) => {
  const parentDob = new Date(
    req.session[getFlowDataObjectName(req.session.journeyName)]["date-of-birth"]
  );
  return partialDate.getTime() > parentDob.getTime();
};

const isValidYearOnlyDate = (partialDate) => {
  return partialYearOnlyFormat.test(partialDate);
};

const isYearOnlyEqualOrAfterEligibleYear = (partialDate) => {
  return partialDate.getFullYear() >= eligibleDate.getFullYear();
};

const validateFullDateOfLoss = (lossDate, req) => {
  if (!isDateRealDate(lossDate)) {
    throw new Error(req.t("validation:dateOfLossNotReal"));
  }

  if (!isDateBeforeToday(lossDate)) {
    throw new Error(req.t("validation:dateOfLossInFuture"));
  }

  if (
    !isDateOfLossAfterParentDob(
      lossDate,
      req.session[getFlowDataObjectName(req.session.journeyName)][
        "date-of-birth"
      ]
    )
  ) {
    throw new Error(req.t("validation:dateOfLossBeforeParentsDob"));
  }

  if (!isOnOrAfterEligibleDate(lossDate)) {
    throw new Error(req.t("validation:dateOfLossNotInRange"));
  }

  return true;
};

const validatePartialMonthYearDateOfLoss = (lossDate, req) => {
  // Month reduced by 1 due to zero-indexing occurring
  const partialDate = new Date(
    req.body["baby-date-of-loss-year"],
    req.body["baby-date-of-loss-month"] - 1
  );

  if (!isValidMonthYearDate(lossDate)) {
    throw new Error(req.t("validation:dateOfLossNotReal"));
  }

  if (!isPartialDateInPast(partialDate)) {
    throw new Error(req.t("validation:dateOfLossInFuture"));
  }

  if (!isPartialDateLossAfterParentDob(partialDate, req)) {
    throw new Error(req.t("validation:dateOfLossBeforeParentsDob"));
  }

  if (!isOnOrAfterEligibleDate(partialDate)) {
    throw new Error(req.t("validation:dateOfLossNotInRange"));
  }

  return true;
};

const validatePartialYearOnlyDateOfLoss = (lossDate, req) => {
  const partialDate = new Date(req.body["baby-date-of-loss-year"]);

  if (!isValidYearOnlyDate(lossDate)) {
    throw new Error(req.t("validation:dateOfLossNotReal"));
  }

  if (!isPartialDateLossAfterParentDob(partialDate, req)) {
    throw new Error(req.t("validation:dateOfLossBeforeParentsDob"));
  }

  if (!isYearOnlyEqualOrAfterEligibleYear(partialDate)) {
    throw new Error(req.t("validation:dateOfLossNotInRange"));
  }

  return true;
};

export {
  isGivenYearDoesNotMatch,
  isFullDate,
  isMonthYearDate,
  isYearOnlyDate,
  isDateOfLossAfterParentDob,
  isOnOrAfterEligibleDate,
  isDateBeforeToday,
  isValidMonthYearDate,
  isPartialDateInPast,
  isPartialDateLossAfterParentDob,
  isValidYearOnlyDate,
  isYearOnlyEqualOrAfterEligibleYear,
  validateFullDateOfLoss,
  validatePartialMonthYearDateOfLoss,
  validatePartialYearOnlyDateOfLoss,
};
