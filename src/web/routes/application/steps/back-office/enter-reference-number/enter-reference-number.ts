import { userIsBackOffice } from "../../../../azure-routes/auth";
import { getFlowDataObjectName } from "../../../tools/data-object";
import { countryCodeMapper } from "../../../tools/country-code-parser";
import { acceptableReferenceNumberTypes } from "../proof-of-name/validate";
import { validate } from "./validate";

const pageContent = ({ translate, req }) => ({
  title: translate("enterReferenceNumber.title"),
  sectionTitle: translate("section.yourDetails"),
  heading: translate("enterReferenceNumber.heading"),
  referenceNumber: translate("enterReferenceNumber.referenceNumber"),
  countryOrigin: translate("enterReferenceNumber.countryOrigin"),
  countryCodes: {
    data: countryCodeMapper(
      req.session[getFlowDataObjectName(req.session.journeyName)][
        "reference-number-country"
      ]
    ),
  },
  continueButtonText: translate("buttons:continue"),
});

const isNavigable = (req) =>
  userIsBackOffice(req) &&
  acceptableReferenceNumberTypes.includes(
    req.session[getFlowDataObjectName(req.session.journeyName)][
      "reference-number-type"
    ]
  );

const enterReferenceNumber = {
  path: "/reference-number",
  template: "enter-reference-number",
  isNavigable,
  pageContent,
  validate,
};

export { enterReferenceNumber, isNavigable, pageContent, validate };
