import expect from "expect";

import { acceptableReferenceNumberTypes, isValid, validate } from "./validate";
import { applyExpressValidation } from "../../common/test/apply-express-validation";

test.each(acceptableReferenceNumberTypes)(
  "should return true when their-reference-number-type is %s",
  (referenceNumberType) => {
    const req = {
      req: {
        body: {
          "their-reference-number-type": referenceNumberType,
        },
      },
    };

    const result = isValid(null, req);

    expect(result).toEqual(true);
  }
);

test("returns expected validation error when their-reference-number-type field is empty", async () => {
  const req = {
    body: {
      "their-reference-number-type": "",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "",
    msg: "validation:selectProofOfName",
    path: "their-reference-number-type",
    location: "body",
  };

  expect(result.array().length).toBe(1);
  expect(error).toEqual(expectedError);
});
