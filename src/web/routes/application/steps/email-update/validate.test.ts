import expect from "expect";
import { validate } from "./validate";
import { applyExpressValidation } from "../common/test/apply-express-validation";

test("return no validation errors when applicant-add-email is yes and other-parent-email is valid email and applicant-email-confirm is same", async () => {
  const req = {
    body: {
      "applicant-add-email": "yes",
      "applicant-email": "te-st@email.com",
      "applicant-email-confirm": "te-st@email.com",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("return expected validation error when applicant-email is invalid email", async () => {
  const req = {
    body: {
      "applicant-add-email": "yes",
      "applicant-email": "test@emailcom",
      "applicant-email-confirm": "test@emailcom",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "test@emailcom",
    msg: "validation:yourEmailInvalidFormat",
    path: "applicant-email",
    location: "body",
  };

  expect(error).toEqual(expectedError);
});

test("return expected validation error when applicant-email is empty", async () => {
  const req = {
    body: {
      "applicant-add-email": "yes",
      "applicant-email": "",
      "applicant-email-confirm": "",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "",
    msg: "validation:missingYourEmail",
    path: "applicant-email",
    location: "body",
  };

  expect(error).toEqual(expectedError);
});

test("return expected validation error when applicant-email is too long", async () => {
  const req = {
    body: {
      "applicant-add-email": "yes",
      "applicant-email": "a".repeat(256),
      "applicant-email-confirm": "a".repeat(256),
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "a".repeat(256),
    msg: "validation:yourEmailTooLong",
    path: "applicant-email",
    location: "body",
  };

  expect(error).toEqual(expectedError);
});

test("return expected validation error when applicant-email-confirm is not same as applicant-email", async () => {
  const req = {
    body: {
      "applicant-add-email": "yes",
      "applicant-email": "test@email.com",
      "applicant-email-confirm": "waesrv",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "waesrv",
    msg: "validation:yourEmailsNotEqual",
    path: "applicant-email-confirm",
    location: "body",
  };

  expect(error).toEqual(expectedError);
});

test("return no validation errors when applicant-add-email is no and applicant-email is empty and applicant-email-confirm is empty and confirm-told-other-parent is true", async () => {
  const req = {
    body: {
      "applicant-add-email": "no",
      "applicant-email": "",
      "applicant-email-confirm": "",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("return expected validation error when applicant-email has invalid characters", async () => {
  const req = {
    body: {
      "applicant-add-email": "yes",
      "applicant-email": "test@£$%^&*()_+{}:@~<>?|",
      "applicant-email-confirm": "test@£$%^&*()_+{}:@~<>?|",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }
  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "test@£$%^&*()_+{}:@~<>?|",
    msg: "validation:yourEmailInvalidCharacters",
    path: "applicant-email",
    location: "body",
  };

  expect(error).toEqual(expectedError);
});

test("return expected validation error when applicant-add-email is empty", async () => {
  const req = {
    body: {},
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }
  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: undefined,
    msg: "validation:yourEmailSelectMissing",
    path: "applicant-add-email",
    location: "body",
  };

  expect(error).toEqual(expectedError);
});
