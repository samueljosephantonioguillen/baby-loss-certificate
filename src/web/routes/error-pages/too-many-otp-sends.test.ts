import { MAINFLOW } from "../application/journey-definitions";
import {
  firstParentContextPath,
  secondParentContextPath,
} from "../application/paths/context-path";
import { CONTEXT_PATH } from "../static-pages/paths";
import {
  registerTooManyOTPSends,
  pageContent,
  get,
  TOO_MANY_OTP_SENDS_PATH,
} from "./too-many-otp-sends";

const app = {
  ...jest.requireActual("express"),
  get: jest.fn(),
};

const req = {
  t: jest.fn(),
  originalUrl: "/" + CONTEXT_PATH,
  session: {
    journeyName: MAINFLOW.name,
  },
};

const res = {
  render: jest.fn(),
};

test("registerTooManyOTPSends should register get routes with expected callbacks", () => {
  app.get.mockImplementation((path, callback) => {
    callback(req, res);
  });

  registerTooManyOTPSends(app);

  expect(app.get).toBeCalledTimes(1);
  expect(app.get).toBeCalledWith(
    [
      firstParentContextPath(TOO_MANY_OTP_SENDS_PATH),
      secondParentContextPath(TOO_MANY_OTP_SENDS_PATH),
    ],
    get
  );
});

test("get should render expected content", () => {
  get(req, res);

  expect(res.render).toBeCalled();
  expect(res.render).toBeCalledWith("cannot-request-another-code", {
    ...pageContent({ translate: req.t, serviceStartPage: "" }),
  });
});
