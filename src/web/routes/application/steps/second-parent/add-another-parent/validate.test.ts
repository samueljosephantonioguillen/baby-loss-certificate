import expect from "expect";

import { validate } from "./validate";
import { applyExpressValidation } from "../../common/test/apply-express-validation";

test("return no validation errors when add-another-parent field is yes", async () => {
  const req = {
    body: {
      "add-another-parent": "yes",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("return no validation errors when add-another-parent field is no", async () => {
  const req = {
    body: {
      "add-another-parent": "no",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("return expected validation error when add-another-parent field is empty", async () => {
  const req = {
    body: {
      "add-another-parent": "",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "",
    msg: "validation:selectAddAnotherParentOption",
    path: "add-another-parent",
    location: "body",
  };

  expect(error).toEqual(expectedError);
});
