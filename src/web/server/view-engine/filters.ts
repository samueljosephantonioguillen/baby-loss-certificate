import { compose, map, values, isNil } from "ramda";

const toError = (error) => ({
  text: error.msg,
  href: `#${error.path}`,
  attributes: {
    id: `error-link-${error.path}`,
  },
});

const toErrorList = compose(values, map(toError));

const getErrorByParam = (errors, field) =>
  isNil(errors) ? null : errors.find((error) => error.path === field);

const getErrorForField = (errors, field) => {
  const error = getErrorByParam(errors, field);

  return error
    ? {
        text: error.msg,
      }
    : null;
};

export { toErrorList, getErrorForField };
