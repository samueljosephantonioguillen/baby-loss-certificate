import { validationResult } from "express-validator";
import { sanitise } from "./sanitise";
import { validate } from "./validate";
import { getFlowDataObjectName } from "../../../tools/data-object";

const pageContent = ({ translate }) => ({
  title: translate("includeBabyLossDate.title"),
  sectionTitle: translate("section.certificateDetails"),
  heading: translate("includeBabyLossDate.heading"),
  insetTextLineOne: translate("includeBabyLossDate.insetTextFirstLine"),
  insetTextLineTwo: translate("includeBabyLossDate.insetTextSecondLine"),
  hintLineOne: translate("includeBabyLossDate.hint.firstLine"),
  hintLineTwo: translate("includeBabyLossDate.hint.secondLine"),
  yes: translate("yes"),
  no: translate("no"),
  or: translate("or"),
  unknown: translate("includeBabyLossDate.unknown"),
  day: translate("day"),
  month: translate("month"),
  year: translate("year"),
  continueButtonText: translate("buttons:continue"),
  backLinkText: translate("buttons:back"),
  legend: translate("includeBabyLossDate.legend"),
});

const behaviourForPost = () => (req, res, next) => {
  if (!validationResult(req).isEmpty()) {
    return next();
  }

  const dataObjectName = getFlowDataObjectName(req.session.journeyName);

  if (
    req.body["include-baby-date-of-loss"] === "no" ||
    req.body["include-baby-date-of-loss"] === "unknown"
  ) {
    delete req.body["baby-date-of-loss"];
    delete req.body["baby-date-of-loss-day"];
    delete req.body["baby-date-of-loss-month"];
    delete req.body["baby-date-of-loss-year"];

    delete req.session[dataObjectName]["baby-date-of-loss"];
    delete req.session[dataObjectName]["baby-date-of-loss-day"];
    delete req.session[dataObjectName]["baby-date-of-loss-month"];
    delete req.session[dataObjectName]["baby-date-of-loss-year"];
  }

  next();
};

const includeBabyLossDate = {
  path: "/date-on-certificate",
  template: "include-baby-loss-date",
  pageContent,
  sanitise,
  validate,
  behaviourForPost,
};

export {
  includeBabyLossDate,
  pageContent,
  sanitise,
  validate,
  behaviourForPost,
};
