import expect from "expect";

const isEmpty = jest.fn();

jest.mock("express-validator", () => ({
  validationResult: () => ({
    isEmpty: isEmpty,
  }),
}));

import {
  confirmYourAddress,
  pageContent,
  validate,
  isNavigable,
  behaviourForPost,
} from "./confirm-your-address";
import { AGENT_ROLE, BACKOFFICE_ROLE } from "../../../constants";
import { config } from "../../../../../../config";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";

test("confirmYourAddress should match expected outcomes", () => {
  const expectedResults = {
    path: "/confirm-your-address",
    template: "confirm-your-address",
    pageContent,
    validate,
    isNavigable,
    behaviourForPost,
  };
  expect(confirmYourAddress).toEqual(expectedResults);
});

describe("pageContent", () => {
  test("should return expected results for only one address line", () => {
    const translate = (string, object?) => `${string}${object}`;

    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "first-name": "Example",
          "last-name": "name",
          "address-line-1": "72 Guild Street",
          postcode: "SE23 6FH",
        },
      },
    };

    const expectedAddressLines = `${req.session[MAINFLOW_DATA_OBJECT_NAME]["address-line-1"]}
        ${req.session[MAINFLOW_DATA_OBJECT_NAME].postcode}`;

    const expected = {
      title: translate("confirmYourAddress.title"),
      heading: translate("confirmYourAddress.heading"),
      sectionTitle: translate("section.yourDetails"),
      yes: translate("yes"),
      no: translate("no"),
      or: translate("or"),
      alt: false,
      bodyLineOne: translate("confirmYourAddress.body.firstLine"),
      bodyLineTwo: translate("confirmYourAddress.body.secondLine"),
      questionHeading: translate("confirmYourAddress.questionHeading"),
      details: translate("confirmYourAddress.details", {
        name: `${req.session[MAINFLOW_DATA_OBJECT_NAME]["first-name"]} ${req.session[MAINFLOW_DATA_OBJECT_NAME]["last-name"]}`,
        address: expectedAddressLines,
      }),
      continueButtonText: translate("buttons:continue"),
      backLinkText: translate("buttons:back"),
    };

    const result = pageContent({ translate, req });

    expect(result).toEqual(expected);
  });

  test("should return expected results for only two address lines", () => {
    const translate = (string, object?) => `${string}${object}`;

    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "first-name": "Example",
          "last-name": "name",
          "address-line-1": "72 Guild Street",
          "address-line-2": "London",
          postcode: "SE23 6FH",
        },
      },
    };

    const expectedAddressLines = `${req.session[MAINFLOW_DATA_OBJECT_NAME]["address-line-1"]}
        ${req.session[MAINFLOW_DATA_OBJECT_NAME]["address-line-2"]}
        ${req.session[MAINFLOW_DATA_OBJECT_NAME].postcode}`;

    const expected = {
      title: translate("confirmYourAddress.title"),
      heading: translate("confirmYourAddress.heading"),
      sectionTitle: translate("section.yourDetails"),
      yes: translate("yes"),
      no: translate("no"),
      or: translate("or"),
      alt: false,
      bodyLineOne: translate("confirmYourAddress.body.firstLine"),
      bodyLineTwo: translate("confirmYourAddress.body.secondLine"),
      questionHeading: translate("confirmYourAddress.questionHeading"),
      details: translate("confirmYourAddress.details", {
        name: `${req.session[MAINFLOW_DATA_OBJECT_NAME]["first-name"]} ${req.session[MAINFLOW_DATA_OBJECT_NAME]["last-name"]}`,
        address: expectedAddressLines,
      }),
      continueButtonText: translate("buttons:continue"),
      backLinkText: translate("buttons:back"),
    };

    const result = pageContent({ translate, req });

    expect(result).toEqual(expected);
  });

  test("should return expected results for all the address lines", () => {
    const translate = (string, object?) => `${string}${object}`;

    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "first-name": "Example",
          "last-name": "name",
          "address-line-1": "72 Guild Street",
          "address-line-2": "Line 2",
          "address-line-3": "Line 3",
          "address-line-4": "Line 4",
          "address-line-5": "London",
          postcode: "SE23 6FH",
        },
      },
    };

    const expectedAddressLines = `${req.session[MAINFLOW_DATA_OBJECT_NAME]["address-line-1"]}
        ${req.session[MAINFLOW_DATA_OBJECT_NAME]["address-line-2"]}
        ${req.session[MAINFLOW_DATA_OBJECT_NAME]["address-line-3"]}
        ${req.session[MAINFLOW_DATA_OBJECT_NAME]["address-line-4"]}
        ${req.session[MAINFLOW_DATA_OBJECT_NAME]["address-line-5"]}
        ${req.session[MAINFLOW_DATA_OBJECT_NAME].postcode}`;

    const expected = {
      title: translate("confirmYourAddress.title"),
      heading: translate("confirmYourAddress.heading"),
      sectionTitle: translate("section.yourDetails"),
      yes: translate("yes"),
      no: translate("no"),
      or: translate("or"),
      alt: false,
      bodyLineOne: translate("confirmYourAddress.body.firstLine"),
      bodyLineTwo: translate("confirmYourAddress.body.secondLine"),
      questionHeading: translate("confirmYourAddress.questionHeading"),
      details: translate("confirmYourAddress.details", {
        name: `${req.session[MAINFLOW_DATA_OBJECT_NAME]["first-name"]} ${req.session[MAINFLOW_DATA_OBJECT_NAME]["last-name"]}`,
        address: expectedAddressLines,
      }),
      continueButtonText: translate("buttons:continue"),
      backLinkText: translate("buttons:back"),
    };

    const result = pageContent({ translate, req });

    expect(result).toEqual(expected);
  });

  test("should return expected results for when agent_role", () => {
    const translate = (string, object?) => `${string}${object}`;

    config.environment.USE_AUTHENTICATION = true;

    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "first-name": "Example",
          "last-name": "name",
          "address-line-1": "72 Guild Street",
          postcode: "SE23 6FH",
        },
        account: {
          idTokenClaims: {
            roles: [AGENT_ROLE],
          },
        },
      },
    };

    const expectedAddressLines = `${req.session[MAINFLOW_DATA_OBJECT_NAME]["address-line-1"]}
        ${req.session[MAINFLOW_DATA_OBJECT_NAME].postcode}`;

    const expected = {
      title: translate("confirmYourAddress.title"),
      heading: translate("confirmYourAddress.heading"),
      sectionTitle: translate("section.yourDetails"),
      yes: translate("yes"),
      no: translate("no"),
      or: translate("or"),
      alt: translate("alt"),
      bodyLineOne: translate("confirmYourAddress.body.firstLineCcs"),
      bodyLineTwo: translate("confirmYourAddress.body.secondLine"),
      questionHeading: translate("confirmYourAddress.questionHeading"),
      details: translate("confirmYourAddress.details", {
        name: `${req.session[MAINFLOW_DATA_OBJECT_NAME]["first-name"]} ${req.session[MAINFLOW_DATA_OBJECT_NAME]["last-name"]}`,
        address: expectedAddressLines,
      }),
      continueButtonText: translate("buttons:continue"),
      backLinkText: translate("buttons:back"),
    };

    const result = pageContent({ translate, req });

    expect(result).toEqual(expected);
  });
});

describe("isNavigable", () => {
  test("should return true when user is not back office", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {},
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when user is back office", () => {
    config.environment.USE_AUTHENTICATION = true;

    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {},
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});

describe("behaviourForPost", () => {
  test("should call next when validation error", () => {
    isEmpty.mockReturnValue(false);

    const req = {};
    const res = {};
    const next = jest.fn();

    behaviourForPost()(req, res, next);
    expect(next).toBeCalledTimes(1);
  });

  test("should delete ccs data when yes option is selected", () => {
    isEmpty.mockReturnValue(true);

    const req = {
      body: { "confirm-your-address": "yes" },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "address-line-1-ccs": "address line 1 ccs",
          "address-line-2-ccs": "address line 2 ccs",
          "address-line-3-ccs": "address line 3 ccs",
          "address-line-4-ccs": "address line 4 ccs",
          "address-line-5-ccs": "address line 5 ccs",
          "postcode-ccs": "b3   2dx",
          sanitisedPostcodeCcs: "B3 2DX",
        },
      },
    };
    const res = {};
    const next = jest.fn();

    behaviourForPost()(req, res, next);
    expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual({});
    expect(next).toBeCalledTimes(1);
  });

  test("should not delete ccs data when alt option is selected", () => {
    isEmpty.mockReturnValue(true);

    const req = {
      body: { "confirm-your-address": "alt" },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "address-line-1-ccs": "address line 1 ccs",
          "address-line-2-ccs": "address line 2 ccs",
          "address-line-3-ccs": "address line 3 ccs",
          "address-line-4-ccs": "address line 4 ccs",
          "address-line-5-ccs": "address line 5 ccs",
          "postcode-ccs": "b3   2dx",
          sanitisedPostcodeCcs: "B3 2DX",
        },
      },
    };

    const expectedBlcSession = {
      "address-line-1-ccs": "address line 1 ccs",
      "address-line-2-ccs": "address line 2 ccs",
      "address-line-3-ccs": "address line 3 ccs",
      "address-line-4-ccs": "address line 4 ccs",
      "address-line-5-ccs": "address line 5 ccs",
      "postcode-ccs": "b3   2dx",
      sanitisedPostcodeCcs: "B3 2DX",
    };
    const res = {};
    const next = jest.fn();

    behaviourForPost()(req, res, next);
    expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(expectedBlcSession);
    expect(next).toBeCalledTimes(1);
  });
});
