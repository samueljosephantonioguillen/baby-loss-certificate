import expect from "expect";

import {
  theirRelationToBaby,
  pageContent,
  isNavigable,
  validate,
  behaviourForPost,
} from "./their-relation-to-baby";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";
import { buildSessionForJourney } from "../../../flow-control/test-utils/test-utils";
import {
  IN_PROGRESS,
  IN_REVIEW,
  IN_REVIEW_PROGRESS,
} from "../../../flow-control/states";
import { BACKOFFICE_ROLE } from "../../../constants";
import { config } from "../../../../../../config";

test("theirRelationToBaby should match expected outcomes", () => {
  const expectedResults = {
    path: "/other-parents-relationship-to-baby",
    template: "their-relation-to-baby",
    pageContent,
    isNavigable,
    validate,
    behaviourForPost,
  };

  expect(theirRelationToBaby).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("theirRelationToBaby.title"),
    sectionTitle: translate("section.otherParentsDetails"),
    heading: translate("theirRelationToBaby.heading"),
    hint: translate("theirRelationToBaby.hint"),
    mother: translate("relationToBaby.radios.mother"),
    father: translate("relationToBaby.radios.father"),
    parent: translate("relationToBaby.radios.parent"),
    surrogate: translate("relationToBaby.radios.surrogate"),
    backLinkText: translate("buttons:back"),
    continueButtonText: translate("buttons:continue"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when add-another-parent is yes", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "add-another-parent": "yes",
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when add-another-parent is no", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "add-another-parent": "no",
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("isNavigable should return false when add-another-parent is undefined", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "add-another-parent": undefined,
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("isNavigable should return false when user is back office", () => {
    config.environment.USE_AUTHENTICATION = true;
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "add-another-parent": "yes",
        },
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});

describe("behaviourForPost", () => {
  test("should set state to IN_REVIEW when current journey state is IN_REVIEW_PROGRESS", () => {
    const config = {};
    const journey = { name: MAINFLOW.name };

    const req = {
      session: {
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "their-relation-to-baby": "Mother",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_REVIEW_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = {};
    const next = jest.fn();

    behaviourForPost(config, journey)(req, res, next);

    const expectedSession = {
      ...req.session,
      ...buildSessionForJourney({
        journeyName: journey.name,
        state: IN_REVIEW,
        nextAllowedPath: undefined,
      }),
    };

    expect(req.session).toEqual(expectedSession);
    expect(next).toBeCalled();
  });

  test("should call next and not change state when state isn't IN_REVIEW_PROGRESS", () => {
    const config = {};
    const journey = { name: MAINFLOW.name };

    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "their-relation-to-baby": "Mother",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = {};
    const next = jest.fn();

    behaviourForPost(config, journey)(req, res, next);

    const expectedReq = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "their-relation-to-baby": "Mother",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };

    expect(req).toEqual(expectedReq);
    expect(next).toBeCalled();
  });
});
