import { stateMachine } from "../../flow-control/state-machine";
import { IN_REVIEW, IN_REVIEW_PROGRESS } from "../../flow-control/states";
import { INCREMENT_NEXT_ALLOWED_PATH } from "../../flow-control/state-machine/actions";

import { contextPath } from "../../paths/context-path";
import { CHECK_ANSWERS_URL } from "../../paths/paths";

import { notIsUndefinedOrNullOrEmpty } from "../../../../../common/predicates";

import { acceptedRelationsToBaby, validate } from "./validate";
import { getFlowDataObjectName } from "../../tools/data-object";

const pageContent = ({ translate }) => ({
  title: translate("relationToBaby.title"),
  sectionTitle: translate("section.confirmEligibility"),
  hintLineOne: translate("relationToBaby.hint.firstLine"),
  heading: translate("relationToBaby.heading"),
  mother: translate("relationToBaby.radios.mother"),
  father: translate("relationToBaby.radios.father"),
  parent: translate("relationToBaby.radios.parent"),
  surrogate: translate("relationToBaby.radios.surrogate"),
  noneOfTheAbove: translate("relationToBaby.radios.noneOfTheAbove"),
  or: translate("or"),
  backLinkText: translate("buttons:back"),
  continueButtonText: translate("buttons:continue"),
});

const isNavigable = (req) =>
  req.session[getFlowDataObjectName(req.session.journeyName)][
    "loss-in-five-years"
  ] === "yes" &&
  notIsUndefinedOrNullOrEmpty(
    req.session[getFlowDataObjectName(req.session.journeyName)]["year-of-loss"]
  );

const behaviourForGet = (config, journey) => (req, res, next) => {
  if (stateMachine.getState(req, journey) === IN_REVIEW_PROGRESS) {
    req.session[getFlowDataObjectName(req.session.journeyName)][
      "relation-to-baby"
    ] = req.session.submittedRelation;
    stateMachine.setState(IN_REVIEW, req, journey);
    stateMachine.dispatch(INCREMENT_NEXT_ALLOWED_PATH, req, journey);
    res.locals.previous = contextPath(CHECK_ANSWERS_URL, journey.name);
  }

  next();
};

const behaviourForPost = (config, journey) => (req, res, next) => {
  if (
    stateMachine.getState(req, journey) === IN_REVIEW &&
    req.body["relation-to-baby"] === "None"
  ) {
    req.session.submittedRelation =
      req.session[getFlowDataObjectName(req.session.journeyName)][
        "relation-to-baby"
      ];
    stateMachine.setState(IN_REVIEW_PROGRESS, req, journey);
  }

  if (
    stateMachine.getState(req, journey) === IN_REVIEW_PROGRESS &&
    acceptedRelationsToBaby.includes(req.body["relation-to-baby"])
  ) {
    stateMachine.setState(IN_REVIEW, req, journey);
  }

  next();
};

const relationToBaby = {
  path: "/relationship-to-baby",
  template: "relation-to-baby",
  pageContent,
  isNavigable,
  validate,
  behaviourForGet,
  behaviourForPost,
};

export {
  relationToBaby,
  pageContent,
  isNavigable,
  validate,
  behaviourForGet,
  behaviourForPost,
};
