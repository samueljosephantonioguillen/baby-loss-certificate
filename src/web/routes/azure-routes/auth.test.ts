const mockGeneratePkceCodes = {
  verifier: jest.fn(),
  challenge: jest.fn(),
};
const mockGetAuthCode = jest.fn();

const mockConfidentialClientApplication = jest.fn().mockImplementation(() => {
  return {
    generatePkceCodes: () => mockGeneratePkceCodes,
    getAuthCodeUrl: mockGetAuthCode,
  };
});
const mockCryptoProvider = jest.fn().mockImplementation(() => {
  return { generatePkceCodes: () => mockGeneratePkceCodes };
});

import { redirectToAuthCodeUrl, userIsAgent, userIsBackOffice } from "./auth";
import { environment } from "../../../config/environment";
import { AGENT_ROLE, BACKOFFICE_ROLE } from "../application/constants";
import { config } from "../../../config";

jest.mock("@azure/msal-node", () => ({
  ConfidentialClientApplication: mockConfidentialClientApplication,
  CryptoProvider: mockCryptoProvider,
}));

const mockReq = {
  session: {},
};

const mockRes = {
  redirect: jest.fn(),
};

const mockNext = jest.fn();

beforeEach(() => {
  jest.resetAllMocks();
  jest.clearAllMocks();
  jest.resetModules();
});

describe("redirectToAuthCodeUrl", () => {
  test("should redirect to auth code url with pkce codes and request parameters in session", async () => {
    mockGetAuthCode.mockReturnValue("url.com");
    const authCodeUrlRequestParams = { scope: "openid profile" };
    const authCodeRequestParams = {
      authority: "https://login.microsoftonline.com",
    };

    await redirectToAuthCodeUrl(
      mockReq,
      mockRes,
      mockNext,
      authCodeUrlRequestParams,
      authCodeRequestParams
    );

    expect(mockReq.session["pkceCodes"]).toEqual({
      challengeMethod: "S256",
      verifier: mockGeneratePkceCodes.verifier,
      challenge: mockGeneratePkceCodes.challenge,
    });
    expect(mockReq.session["authCodeUrlRequest"]).toEqual({
      redirectUri: environment.REDIRECT_URI,
      responseMode: "form_post",
      codeChallenge: mockGeneratePkceCodes.challenge,
      codeChallengeMethod: "S256",
      ...authCodeUrlRequestParams,
    });
    expect(mockReq.session["authCodeRequest"]).toEqual({
      redirectUri: environment.REDIRECT_URI,
      code: "",
      ...authCodeRequestParams,
    });
    expect(mockRes.redirect).toHaveBeenCalled();
    expect(mockRes.redirect).toBeCalledWith("url.com");
  });

  test("should call next with error if there`s an error while getting auth code url", async () => {
    const authCodeUrlRequestParams = { scope: "openid profile" };
    const authCodeRequestParams = {
      authority: "https://login.microsoftonline.com",
    };
    const mockError = new Error("Auth code url could not be obtained");

    mockGetAuthCode.mockRejectedValue(mockError);

    await redirectToAuthCodeUrl(
      mockReq,
      mockRes,
      mockNext,
      authCodeUrlRequestParams,
      authCodeRequestParams
    );

    expect(mockNext).toHaveBeenCalledWith(mockError);
  });
});

describe("userIsAgent", () => {
  test("userIsAgent should return true if user is agent", () => {
    config.environment.USE_AUTHENTICATION = true;

    const req = {
      session: {
        account: {
          idTokenClaims: {
            roles: [AGENT_ROLE],
          },
        },
      },
    };

    const result = userIsAgent(req);

    expect(result).toBeTruthy();
  });

  test("userIsAgent should return false if user is not agent", () => {
    const req = {
      session: {
        account: {
          idTokenClaims: {
            roles: ["not-agent"],
          },
        },
      },
    };

    const result = userIsAgent(req);

    expect(result).toBeFalsy();
  });

  test("userIsAgent should return false account is undefined", () => {
    const req = {
      session: {},
    };

    const result = userIsAgent(req);
    expect(result).toBeFalsy();
  });
});

describe("userIsBackOffice", () => {
  test("userIsBackOffice should return true if user is backoffice", () => {
    config.environment.USE_AUTHENTICATION = true;

    const req = {
      session: {
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };

    const result = userIsBackOffice(req);

    expect(result).toBeTruthy();
  });

  test("userIsBackOffice should return false if user is not backoffice", () => {
    const req = {
      session: {
        account: {
          idTokenClaims: {
            roles: ["not-backoffice"],
          },
        },
      },
    };

    const result = userIsBackOffice(req);

    expect(result).toBeFalsy();
  });

  test("userIsBackOffice should return false account is undefined", () => {
    const req = {
      session: {},
    };

    const result = userIsBackOffice(req);
    expect(result).toBeFalsy();
  });
});
