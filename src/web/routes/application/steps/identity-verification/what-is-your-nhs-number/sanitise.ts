import { compose } from "ramda";

const removeAllSpaces = (value) => {
  return value.replace(/\s/g, "");
};

const addSingleSpaces = (value) => {
  const first = value.substring(0, 3);
  const second = value.substring(3, 6);
  const third = value.substring(6, 10);

  return `${first} ${second} ${third}`;
};

const formatNhsNumber = compose(addSingleSpaces, removeAllSpaces);

const sanitise = () => (req, res, next) => {
  req.body["nhs-number"] = removeAllSpaces(req.body["nhs-number"]);
  req.body["formattedNhsNumber"] = formatNhsNumber(req.body["nhs-number"]);

  next();
};

export { sanitise };
