import axios from "axios";
import { DateTime } from "luxon";
import { v4 as uuidv4 } from "uuid";
import { logger } from "../../../../logger/logger";
import {
  filterNames,
  filterAddresses,
  filterContactMethods,
  deceasedDateTime,
} from "./pds-filters";
import { getPdsOauthToken } from "./get-pds-oauth-token";
import axiosRetry from "axios-retry";
import { toTitleCase } from "../common/formats";

const REQUEST_TIMEOUT = 30 * 1000;

const requestPdsLookup = async (req, config, nhsNumber) => {
  const { PDS_URI } = config.environment;
  const uri = `${PDS_URI}/Patient/${nhsNumber}`;
  try {
    const oauthToken = await getPdsOauthToken(req);
    const requestStartTime = DateTime.now();
    const uuid = uuidv4();

    axiosRetry(axios, {
      retryDelay: () => 200,
      retries: 3,
      retryCondition() {
        logger.warn(`Error with Axios Call`);
        return true;
      },
    });
    const response = await axios.get(uri, {
      headers: {
        Authorization: `Bearer ${oauthToken}`,
        "X-Request-ID": uuid,
      },
      timeout: REQUEST_TIMEOUT,
    });

    const requestEndTime = DateTime.now();
    const requestTimeInSeconds =
      requestEndTime.diff(requestStartTime).milliseconds;
    logger.debug(`Sent PDS request with uuid: ${uuid}`, req);
    logger.debug(
      `PDS lookup (NHS number) API request time in milliseconds: ${requestTimeInSeconds}`,
      req
    );

    const phoneNumbers = filterContactMethods(response.data, "phone");
    let phoneNumber = null;
    if (phoneNumbers?.length > 0) {
      phoneNumber = phoneNumbers[0].value;
    }

    const emails = filterContactMethods(response.data, "email");
    let email = null;
    if (emails?.length > 0) {
      email = emails[0].value;
    }

    const names = filterNames(response.data);
    let firstName = null;
    if (names?.length > 0) {
      firstName = toTitleCase(names[0].given[0]);
    }
    let lastName = null;
    if (names?.length > 0) {
      lastName = toTitleCase(names[0].family);
    }

    const homeAddresses = filterAddresses(response.data);
    let homeAddressLines = [];
    let postcode = null;
    if (homeAddresses?.length > 0) {
      homeAddressLines = homeAddresses[0].line;
      postcode = homeAddresses[0].postalCode.toUpperCase();
    }

    const hasDeceasedDateTime = deceasedDateTime(response.data);

    let nhsNumberFromResponse = null;
    if (response.data.id) {
      nhsNumberFromResponse = response.data.id;
    }

    return {
      firstName: firstName,
      lastName: lastName,
      dateOfBirth: response.data.birthDate || null,
      email: email,
      phoneNumber: phoneNumber,
      addressLines: homeAddressLines,
      postcode: postcode,
      hasDeceasedDateTime: hasDeceasedDateTime,
      nhsNumber: nhsNumberFromResponse,
    };
  } catch (error) {
    let errorMsg = "Error caught in NHS number PDS lookup (NHS number)";
    if (error.response) {
      errorMsg += ` - Response Code: ${error.response.status}`;
    }
    if (error.message) {
      errorMsg += ` - ${error.message}`;
    }
    logger.error(errorMsg, req);
    throw error;
  }
};

export { requestPdsLookup };
