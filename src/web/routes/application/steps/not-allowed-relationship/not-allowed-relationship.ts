import { getFlowDataObjectName } from "../../tools/data-object";

const pageContent = ({ translate }) => ({
  title: translate("notAllowedRelationship.title"),
  heading: translate("notAllowedRelationship.heading"),
  supportOrganisationsHeading: translate("supportOrganisations.heading"),
  supportOrganisationsBody: translate("supportOrganisations.body"),
  supportOrganisationsList: translate("supportOrganisations.listItems"),
  backLinkText: translate("buttons:back"),
});

const isNavigable = (req) =>
  req.session[getFlowDataObjectName(req.session.journeyName)][
    "relation-to-baby"
  ] === "None";

const notAllowedRelationship = {
  path: "/cannot-request-relationship",
  template: "not-eligible",
  pageContent,
  isNavigable,
};

export { notAllowedRelationship, pageContent, isNavigable };
