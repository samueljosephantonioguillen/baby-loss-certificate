import expect from "expect";

import { sanitise } from "./sanitise";

const next = jest.fn();

test("trims starting and ending spaces", () => {
  const req = {
    body: {
      "first-name": "   John   ",
      "last-name": "   Smith   ",
    },
  };

  sanitise()(req, {}, next);

  expect(req.body).toEqual({
    "first-name": "John",
    "last-name": "Smith",
  });

  expect(next).toBeCalledTimes(1);
});

test("replaces backticks with apostrophe", () => {
  const req = {
    body: {
      "first-name": "O`Conor",
      "last-name": "O`Brian",
    },
  };

  sanitise()(req, {}, next);

  expect(req.body).toEqual({
    "first-name": "O'Conor",
    "last-name": "O'Brian",
  });

  expect(next).toBeCalledTimes(1);
});

test("replaces autofilled apostrophe with standard apostrophe", () => {
  const req = {
    body: {
      "first-name": "O’Conor",
      "last-name": "O’Brian",
    },
  };

  sanitise()(req, {}, next);

  expect(req.body).toEqual({
    "first-name": "O'Conor",
    "last-name": "O'Brian",
  });

  expect(next).toBeCalledTimes(1);
});

test("should capitalise first letter of first-name and last-name", () => {
  const req = {
    body: {
      "first-name": "firstname",
      "last-name": "lastname",
    },
  };
  const res = { redirect: jest.fn() };
  const next = jest.fn();

  const expected = {
    "first-name": "Firstname",
    "last-name": "Lastname",
  };

  sanitise()(req, res, next);

  expect(req.body).toEqual(expected);
});
