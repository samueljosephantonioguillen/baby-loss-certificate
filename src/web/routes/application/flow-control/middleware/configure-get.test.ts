import expect from "expect";

import * as previousPath from "../get-previous-path";
import * as prefixPath from "../../paths/prefix-path";

import { buildSessionForJourney } from "../test-utils/test-utils";
import { IN_DETAILS_REVIEW, IN_PROGRESS, IN_REVIEW } from "../states";
import { CHECK_ANSWERS_URL, CHECK_DETAILS_URL } from "../../paths/paths";

import { configureGet } from "./configure-get";
import { contextPath } from "../../paths/context-path";
import { MAINFLOW } from "../../journey-definitions";

const stepOne = { path: "/first" };
const stepTwo = { path: "/second" };
const stepThree = { path: "/third" };
const stepFour = { path: contextPath(CHECK_ANSWERS_URL, MAINFLOW.name) };
const steps = [stepOne, stepTwo, stepThree, stepFour];

const journey = {
  name: MAINFLOW.name,
  pathPrefix: "/blc",
};

const res = {
  locals: {
    previous: null,
  },
  redirect: jest.fn(),
};

jest.spyOn(previousPath, "getPreviousPath");
jest.spyOn(prefixPath, "prefixPath");

describe("configureGet", () => {
  test("should return previous path in journey when state is IN_PROGRESS", () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const next = jest.fn();

    configureGet(steps, stepTwo, journey)(req, res, next);
    expect(previousPath.getPreviousPath).toBeCalledTimes(1);
    expect(previousPath.getPreviousPath).toBeCalledWith(steps, stepTwo, req);
    expect(res.locals.previous).toBe(stepOne.path);
  });

  test("should return check-your-details path in journey when state is IN_DETAILS_REVIEW", () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW.name,
          state: IN_DETAILS_REVIEW,
          nextAllowedPath: undefined,
        }),
      },
    };
    const next = jest.fn();

    configureGet(steps, stepTwo, journey)(req, res, next);
    expect(previousPath.getPreviousPath).toBeCalledTimes(0);
    expect(prefixPath.prefixPath).toBeCalledTimes(1);
    expect(prefixPath.prefixPath).toBeCalledWith(
      journey.pathPrefix,
      CHECK_DETAILS_URL
    );
    expect(res.locals.previous).toBe(journey.pathPrefix + CHECK_DETAILS_URL);
  });

  test("should return check-your-answers path in journey when state is IN_REVIEW", () => {
    const req = {
      session: {
        ...buildSessionForJourney({
          journeyName: MAINFLOW.name,
          state: IN_REVIEW,
          nextAllowedPath: undefined,
        }),
      },
    };
    const next = jest.fn();

    configureGet(steps, stepTwo, journey)(req, res, next);
    expect(previousPath.getPreviousPath).toBeCalledTimes(0);
    expect(prefixPath.prefixPath).toBeCalledTimes(1);
    expect(prefixPath.prefixPath).toBeCalledWith(
      journey.pathPrefix,
      CHECK_ANSWERS_URL
    );
    expect(res.locals.previous).toBe(journey.pathPrefix + CHECK_ANSWERS_URL);
  });
});
