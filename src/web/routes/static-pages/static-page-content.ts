export const renderStaticPage = (url, content) => (req, res) => {
  res.render("static-pages/" + url, content({ translate: req.t, req }));
};
