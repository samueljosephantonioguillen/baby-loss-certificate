import {
  filterContactMethods,
  filterAddresses,
  filterNames,
  matchSearchName,
  deceasedDateTime,
} from "./pds-filters";

describe("filterContactMethods", () => {
  test("should return empty array as use is not email", () => {
    const data = {
      telecom: [
        {
          system: "phone",
          period: {
            start: "2020-01-01",
          },
          value: "123456789",
        },
      ],
    };
    const result = filterContactMethods(data, "email");
    expect(result.length).toEqual(0);
  });

  test("should return empty array as period start is in the future", () => {
    const data = {
      telecom: [
        {
          system: "phone",
          period: {
            start: "2099-01-01",
          },
          value: "123456789",
        },
      ],
    };
    const result = filterContactMethods(data, "phone");
    expect(result.length).toEqual(0);
  });

  test("should return empty array as period end is before today", () => {
    const data = {
      telecom: [
        {
          system: "phone",
          period: {
            start: "1999-01-01",
            end: "2000-01-01",
          },
          value: "123456789",
        },
      ],
    };
    const result = filterContactMethods(data, "phone");
    expect(result.length).toEqual(0);
  });

  test("should return valid mobile that is active as no period end", () => {
    const data = {
      telecom: [
        {
          system: "phone",
          use: "mobile",
          period: {
            start: "1999-01-01",
          },
          value: "123456789",
        },
      ],
    };
    const result = filterContactMethods(data, "phone");
    expect(result.length).toEqual(1);
  });

  test("should return valid mobile that is active as no period field", () => {
    const data = {
      telecom: [
        {
          system: "phone",
          use: "mobile",
          value: "123456789",
        },
      ],
    };
    const result = filterContactMethods(data, "phone");
    expect(result.length).toEqual(1);
  });

  test("should sort contact methods by most recent first", () => {
    const data = {
      telecom: [
        {
          system: "phone",
          use: "mobile",
          period: {
            start: "1999-01-01",
          },
          value: "123456789",
        },
        {
          system: "phone",
          use: "mobile",
          period: {
            start: "2000-01-01",
          },
          value: "987654321",
        },
      ],
    };
    const result = filterContactMethods(data, "phone");
    expect(result.length).toEqual(2);
    expect(result[0].value).toEqual("987654321");
  });

  test("should sort contact methods when second item has no period field", () => {
    const data = {
      telecom: [
        {
          system: "phone",
          use: "mobile",
          value: "123456789",
          period: {
            start: "2000-01-01",
          },
        },
        {
          system: "phone",
          use: "mobile",
          value: "987654321",
        },
      ],
    };
    const result = filterContactMethods(data, "phone");
    expect(result.length).toEqual(2);
    expect(result[0].value).toEqual("123456789");
  });

  test("should sort contact methods when first item has no period field", () => {
    const data = {
      telecom: [
        {
          system: "phone",
          use: "mobile",
          value: "123456789",
        },
        {
          system: "phone",
          use: "mobile",
          period: {
            start: "2000-01-01",
          },
          value: "987654321",
        },
      ],
    };
    const result = filterContactMethods(data, "phone");
    expect(result.length).toEqual(2);
    expect(result[0].value).toEqual("987654321");
  });

  test("should retain order when no period on all items", () => {
    const data = {
      telecom: [
        {
          system: "phone",
          use: "mobile",
          value: "123456789",
        },
        {
          system: "phone",
          use: "mobile",
          value: "987654321",
        },
      ],
    };
    const result = filterContactMethods(data, "phone");
    expect(result.length).toEqual(2);
    expect(result[0].value).toEqual("123456789");
  });

  test("should return mobile phone number only and ignore home phone number", () => {
    const data = {
      telecom: [
        {
          system: "phone",
          use: "home",
          value: "123456789",
        },
        {
          system: "phone",
          use: "mobile",
          period: {
            start: "2000-01-01",
          },
          value: "987654321",
        },
      ],
    };
    const result = filterContactMethods(data, "phone");
    expect(result.length).toEqual(1);
    expect(result[0].value).toEqual("987654321");
  });

  test("should return contact method with no period when the other is expired", () => {
    const data = {
      telecom: [
        {
          system: "email",
          period: {
            start: "2000-01-01",
            end: "2001-01-01",
          },
          value: "expired@example.com",
        },
        {
          system: "email",
          value: "test@example.com",
        },
      ],
    };
    const result = filterContactMethods(data, "email");
    expect(result.length).toEqual(1);
    expect(result[0].value).toEqual("test@example.com");
  });

  test("should return valid email when given a more recent but expired email that should be ignored", () => {
    const data = {
      telecom: [
        {
          system: "email",
          period: {
            start: "1999-01-01",
          },
          value: "olderbutactive@example.com",
        },
        {
          system: "email",
          period: {
            start: "2022-01-01",
            end: "2023-01-01",
          },
          value: "expired@example.com",
        },
      ],
    };
    const result = filterContactMethods(data, "email");
    expect(result.length).toEqual(1);
    expect(result[0].value).toEqual("olderbutactive@example.com");
  });

  test("should return empty array as email is in the future", () => {
    const data = {
      telecom: [
        {
          system: "email",
          period: {
            start: "2099-01-01",
          },
          value: "future@example.com",
        },
      ],
    };
    const result = filterContactMethods(data, "email");
    expect(result).toEqual([]);
  });

  test("should return null as has no telecom field", () => {
    const data = {};
    const result = filterContactMethods(data, "phone");
    expect(result).toEqual(null);
  });
});

describe("filterAddresses", () => {
  test("should return empty array as use is not home", () => {
    const data = {
      address: [
        {
          use: "temp",
          period: {
            start: "2020-01-01",
          },
          line: ["1 Trevelyan Square", "Leeds"],
          postalCode: "LS1 6AE",
        },
      ],
    };
    const result = filterAddresses(data);
    expect(result.length).toEqual(0);
  });

  test("should return empty array as period start is in the future", () => {
    const data = {
      address: [
        {
          use: "home",
          period: {
            start: "2099-01-01",
          },
          line: ["1 Trevelyan Square", "Leeds"],
          postalCode: "LS1 6AE",
        },
      ],
    };
    const result = filterAddresses(data);
    expect(result.length).toEqual(0);
  });

  test("should return empty array as period end is before today", () => {
    const data = {
      address: [
        {
          use: "home",
          period: {
            start: "1999-01-01",
            end: "2000-01-01",
          },
          line: ["1 Trevelyan Square", "Leeds"],
          postalCode: "LS1 6AE",
        },
      ],
    };
    const result = filterAddresses(data);
    expect(result.length).toEqual(0);
  });

  test("should return valid address that is active as no period end", () => {
    const data = {
      address: [
        {
          use: "home",
          period: {
            start: "1999-01-01",
          },
          line: ["1 Trevelyan Square", "Leeds"],
          postalCode: "LS1 6AE",
        },
      ],
    };
    const result = filterAddresses(data);
    expect(result.length).toEqual(1);
  });

  test("should return valid address that is active as no period field", () => {
    const data = {
      address: [
        {
          use: "home",
          line: ["1 Trevelyan Square", "Leeds"],
          postalCode: "LS1 6AE",
        },
      ],
    };
    const result = filterAddresses(data);
    expect(result.length).toEqual(1);
  });

  test("should sort addresses by most recent first", () => {
    const data = {
      address: [
        {
          use: "home",
          period: {
            start: "1999-01-01",
          },
          line: ["1 Trevelyan Square", "Leeds"],
          postalCode: "LS1 6AE",
        },
        {
          use: "home",
          period: {
            start: "2000-01-01",
          },
          line: ["1 More Recent Square", "Birmingham"],
          postalCode: "B3 2DX",
        },
      ],
    };
    const result = filterAddresses(data);
    expect(result.length).toEqual(2);
    expect(result[0].line[0]).toEqual("1 More Recent Square");
    expect(result[0].postalCode).toEqual("B3 2DX");
  });

  test("should sort addresses by most recent first when second item has no period field then treated as older", () => {
    const data = {
      address: [
        {
          use: "home",
          line: ["1 Trevelyan Square", "Leeds"],
          period: {
            start: "2000-01-01",
          },
          postalCode: "LS1 6AE",
        },
        {
          use: "home",
          line: ["1 More Recent Square", "Birmingham"],
          postalCode: "B3 2DX",
        },
      ],
    };
    const result = filterAddresses(data);
    expect(result.length).toEqual(2);
    expect(result[0].line[0]).toEqual("1 Trevelyan Square");
    expect(result[0].postalCode).toEqual("LS1 6AE");
  });

  test("should sort addresses by most recent first when first item has no period field then treated as older", () => {
    const data = {
      address: [
        {
          use: "home",
          line: ["1 Trevelyan Square", "Leeds"],
          postalCode: "LS1 6AE",
        },
        {
          use: "home",
          period: {
            start: "2000-01-01",
          },
          line: ["1 More Recent Square", "Birmingham"],
          postalCode: "B3 2DX",
        },
      ],
    };
    const result = filterAddresses(data);
    expect(result.length).toEqual(2);
    expect(result[0].line[0]).toEqual("1 More Recent Square");
    expect(result[0].postalCode).toEqual("B3 2DX");
  });

  test("should retain order when no period on all items", () => {
    const data = {
      address: [
        {
          use: "home",
          line: ["1 Trevelyan Square", "Leeds"],
          postalCode: "LS1 6AE",
        },
        {
          use: "home",
          line: ["1 More Recent Square", "Birmingham"],
          postalCode: "B3 2DX",
        },
      ],
    };
    const result = filterAddresses(data);
    expect(result.length).toEqual(2);
    expect(result[0].line[0]).toEqual("1 Trevelyan Square");
    expect(result[0].postalCode).toEqual("LS1 6AE");
  });

  test("should return address with no period when the other is expired", () => {
    const data = {
      address: [
        {
          use: "home",
          line: ["1 Expired Lane", "Leeds"],
          period: {
            start: "1999-01-01",
            end: "2000-01-01",
          },
          postalCode: "LS1 6AE",
        },
        {
          use: "home",
          line: ["1 Active Square", "Birmingham"],
          postalCode: "B3 2DX",
        },
      ],
    };
    const result = filterAddresses(data);
    expect(result.length).toEqual(1);
    expect(result[0].line[0]).toEqual("1 Active Square");
    expect(result[0].postalCode).toEqual("B3 2DX");
  });

  test("should return valid address when given a more recent but expired address that should be ignored", () => {
    const data = {
      address: [
        {
          use: "home",
          period: {
            start: "1999-01-01",
          },
          line: ["1 Valid Square", "Leeds"],
          postalCode: "LS1 6AE",
        },
        {
          use: "home",
          period: {
            start: "2000-01-01",
            end: "2001-01-01",
          },
          line: ["1 More Recent Square", "Birmingham"],
          postalCode: "B3 2DX",
        },
      ],
    };
    const result = filterAddresses(data);
    expect(result.length).toEqual(1);
    expect(result[0].line[0]).toEqual("1 Valid Square");
    expect(result[0].postalCode).toEqual("LS1 6AE");
  });

  test("should return as data has no address field", () => {
    const data = {};
    const result = filterAddresses(data);
    expect(result).toEqual(null);
  });
});

describe("filterNames", () => {
  test("should return empty array as use is not usual", () => {
    const data = {
      name: [
        {
          use: "temp",
          period: {
            start: "2020-01-01",
          },
          given: ["Temp"],
          family: "Temp",
        },
      ],
    };
    const result = filterNames(data);
    expect(result.length).toEqual(0);
  });

  test("should return empty array as period start is in the future", () => {
    const data = {
      name: [
        {
          use: "usual",
          period: {
            start: "2099-01-01",
          },
          given: ["Future"],
          family: "Future",
        },
      ],
    };
    const result = filterNames(data);
    expect(result.length).toEqual(0);
  });

  test("should return empty array as period end is before today", () => {
    const data = {
      name: [
        {
          use: "usual",
          period: {
            start: "2000-01-01",
            end: "2001-01-01",
          },
          given: ["Expired"],
          family: "Expired",
        },
      ],
    };
    const result = filterNames(data);
    expect(result.length).toEqual(0);
  });

  test("should return valid name that is active as no period end", () => {
    const data = {
      name: [
        {
          use: "usual",
          period: {
            start: "2020-01-01",
          },
          given: ["Valid"],
          family: "Valid",
        },
      ],
    };
    const result = filterNames(data);
    expect(result.length).toEqual(1);
  });

  test("should sort names by most recent first", () => {
    const data = {
      name: [
        {
          use: "usual",
          period: {
            start: "2012-01-01",
            end: "2099-01-01",
          },
          given: ["First"],
          family: "First",
        },
        {
          use: "usual",
          period: {
            start: "2023-01-01",
          },
          given: ["More recent"],
          family: "More recent",
        },
      ],
    };
    const result = filterNames(data);
    expect(result.length).toEqual(2);
    expect(result[0].given[0]).toEqual("More recent");
    expect(result[0].family).toEqual("More recent");
  });

  test("should sort names by most recent first when second item has no period field then treated as older", () => {
    const data = {
      name: [
        {
          use: "usual",
          given: ["First"],
          family: "With period",
          period: {
            start: "2023-01-01",
          },
        },
        {
          use: "usual",

          given: ["Second"],
          family: "Without period",
        },
      ],
    };
    const result = filterNames(data);
    expect(result.length).toEqual(2);
    expect(result[0].given[0]).toEqual("First");
    expect(result[0].family).toEqual("With period");
  });

  test("should sort names by most recent first when first item has no period field then treated as older", () => {
    const data = {
      name: [
        {
          use: "usual",
          given: ["First"],
          family: "Without period",
        },
        {
          use: "usual",
          period: {
            start: "2023-01-01",
          },
          given: ["Second"],
          family: "With period",
        },
      ],
    };
    const result = filterNames(data);
    expect(result.length).toEqual(2);
    expect(result[0].given[0]).toEqual("Second");
    expect(result[0].family).toEqual("With period");
  });

  test("should retain order when no period on all items", () => {
    const data = {
      name: [
        {
          use: "usual",
          given: ["First"],
          family: "First",
        },
        {
          use: "usual",
          given: ["Second"],
          family: "Second",
        },
      ],
    };
    const result = filterNames(data);
    expect(result.length).toEqual(2);
    expect(result[0].given[0]).toEqual("First");
    expect(result[0].family).toEqual("First");
  });

  test("should return name with no period when the other is expired", () => {
    const data = {
      name: [
        {
          use: "usual",
          period: {
            start: "2020-01-01",
            end: "2021-01-01",
          },
          given: ["Expired"],
          family: "Expired",
        },
        {
          use: "usual",
          given: ["Active"],
          family: "Second",
        },
      ],
    };
    const result = filterNames(data);
    expect(result.length).toEqual(1);
    expect(result[0].given[0]).toEqual("Active");
    expect(result[0].family).toEqual("Second");
  });

  test("should return valid name when given a more recent but expired name that should be ignored", () => {
    const data = {
      name: [
        {
          use: "usual",
          period: {
            start: "2020-01-01",
            end: "2099-01-01",
          },
          given: ["Valid"],
          family: "Valid",
        },
        {
          use: "usual",
          period: {
            start: "2022-01-01",
            end: "2022-02-01",
          },
          given: ["More recent"],
          family: "Expired",
        },
      ],
    };
    const result = filterNames(data);
    expect(result.length).toEqual(1);
    expect(result[0].given[0]).toEqual("Valid");
    expect(result[0].family).toEqual("Valid");
  });

  test("should return as data has no name field", () => {
    const data = {};
    const result = filterNames(data);
    expect(result).toEqual(null);
  });
});

describe("matchSearchName", () => {
  test("should return recent name when input names match ignoring case (first given name)", () => {
    const names = [
      {
        id: "123",
        use: "usual",
        period: {
          start: "2020-01-01",
        },
        given: ["Ricky", "Bobby Jack"],
        family: "Daniels",
      },
    ];
    const result = matchSearchName(names, "ricky", "daniels");
    expect(result.firstName).toEqual("Ricky");
    expect(result.lastName).toEqual("Daniels");
  });

  test("should return recent name when input names match ignoring case (second given name)", () => {
    const names = [
      {
        id: "123",
        use: "usual",
        period: {
          start: "2020-01-01",
        },
        given: ["Ricky", "Bobby Jack"],
        family: "Daniels",
      },
    ];
    const result = matchSearchName(names, "bobby Jack", "Daniels");
    expect(result.firstName).toEqual("Bobby Jack");
    expect(result.lastName).toEqual("Daniels");
  });

  test("should return nulls for names object when input last name does not match family name", () => {
    const names = [
      {
        id: "123",
        use: "usual",
        period: {
          start: "2020-01-01",
        },
        given: ["Ricky", "Bobby Jack"],
        family: "Daniels",
      },
    ];
    const result = matchSearchName(names, "Ricky", "Invalid");
    expect(result.firstName).toEqual(null);
    expect(result.lastName).toEqual(null);
  });

  test("should return nulls for names object when input first name is not a given name", () => {
    const names = [
      {
        id: "123",
        use: "usual",
        period: {
          start: "2020-01-01",
        },
        given: ["Ricky", "Bobby Jack"],
        family: "Daniels",
      },
    ];
    const result = matchSearchName(names, "Invalid", "Daniels");
    expect(result.firstName).toEqual(null);
    expect(result.lastName).toEqual(null);
  });

  test("should return nulls for names object when names array is empty", () => {
    const names = [];
    const result = matchSearchName(names, "Invalid", "Daniels");
    expect(result.firstName).toEqual(null);
    expect(result.lastName).toEqual(null);
  });

  test("should return nulls for names object when names array is null", () => {
    const names = null;
    const result = matchSearchName(names, "Invalid", "Daniels");
    expect(result.firstName).toEqual(null);
    expect(result.lastName).toEqual(null);
  });
});

describe("deceasedDateTime", () => {
  test("should return true when patient has a deceasedDateTime populated", () => {
    const data = {
      resourceType: "Patient",
      deceasedDateTime: "2010-10-22T00:00:00+00:00",
    };
    const result = deceasedDateTime(data);
    expect(result).toEqual(true);
  });

  test("should return false when patient does not have a deceasedDateTime field", () => {
    const data = {
      resourceType: "Patient",
    };
    const result = deceasedDateTime(data);
    expect(result).toEqual(false);
  });

  test("should return false when patient does not have a deceasedDateTime populated", () => {
    const data = {
      resourceType: "Patient",
      deceasedDateTime: null,
    };
    const result = deceasedDateTime(data);
    expect(result).toEqual(false);
  });
});
