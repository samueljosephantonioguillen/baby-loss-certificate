import expect from "expect";
import { getNextPathFromSteps, getNextForStep } from "./get-next-for-step";

const step1 = {
  path: "/first",
};

const step2 = {
  path: "/second",
};

const step3 = {
  path: "/third",
};

const steps = [step1, step2, step3];

test("getNextPathFromSteps gets the path for the next step in sequence of steps", () => {
  const journey = { steps };
  const result = getNextPathFromSteps(journey, step1);
  expect(result).toBe("/second");
});

test("getNextForStep returns path for next step in sequence if next property is undefined on step", () => {
  const req = {};
  const journey = { steps };
  const result = getNextForStep(req, journey, step2);

  expect(result).toBe("/third");
});

test("getNextForStep() throws error if next is not a function", () => {
  const step = { next: "This is not a function" };
  const req = {};
  const journey = { steps: [...steps, step] };
  const result = () => getNextForStep(req, journey, step);

  expect(result).toThrowError(/Next property for step must be a function/);
});

test("getNextForStep throws error if result of calling next is not a string", () => {
  const step = { next: () => null };
  const req = {};
  const journey = { steps: [...steps, step] };
  const result = () => getNextForStep(req, journey, step);

  expect(result).toThrowError(
    /Next function must return a string starting with a forward slash/
  );
});

test("getNextForStep throws error if result of calling next does not start with forward slash", () => {
  const step = { next: () => "path-without-forward-slash" };
  const req = {};
  const journey = { steps: [...steps, step] };
  const result = () => getNextForStep(req, journey, step);

  expect(result).toThrowError(
    /Next function must return a string starting with a forward slash/
  );
});

test("getNextForStep should return the result of calling next", () => {
  const step = { next: () => "/the-next-path" };
  const req = {};
  const journey = { steps: [...steps, step] };
  const result = getNextForStep(req, journey, step);

  expect(result).toBe("/the-next-path");
});

test("getNextForStep passes request as an argument when calling next function", () => {
  const req = { session: { some: "/path" } };
  const next = (req) => req.session.some;
  const step = { next };
  const journey = { steps: [...steps, step] };
  const result = getNextForStep(req, journey, step);

  expect(result).toBe("/path");
});
