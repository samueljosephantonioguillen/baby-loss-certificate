import { validate } from "./validate";
import { validationResult } from "express-validator";
import {
  getFlowDataObjectName,
  MAINFLOW_DATA_OBJECT_NAME,
} from "../../tools/data-object";

const pageContent = ({ translate, req }) => ({
  title: translate("emailUpdate.title"),
  sectionTitle: translate("section.yourDetails"),
  heading:
    req.session[MAINFLOW_DATA_OBJECT_NAME]["add-another-parent"] === "yes"
      ? translate("emailUpdate.headingDouble")
      : translate("emailUpdate.headingSingle"),
  body: translate("emailUpdate.body"),
  continueButtonText: translate("buttons:continue"),
  yes: translate("yes"),
  no: translate("no"),
});

const behaviourForPost = () => (req, res, next) => {
  if (!validationResult(req).isEmpty()) {
    return next();
  }

  if (req.body["applicant-add-email"] === "no") {
    delete req.body["applicant-email"];
    delete req.session[getFlowDataObjectName(req.session.journeyName)][
      "applicant-email"
    ];
    delete req.body["applicant-email-confirm"];
    delete req.session[getFlowDataObjectName(req.session.journeyName)][
      "applicant-email-confirm"
    ];
  }

  next();
};

const emailUpdate = {
  path: "/email-update",
  template: "email-update",
  pageContent,
  validate,
  behaviourForPost,
};

export { emailUpdate, pageContent, validate, behaviourForPost };
