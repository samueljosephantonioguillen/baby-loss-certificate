import expect from "expect";

import { notOldEnough, pageContent, isNavigable } from "./not-old-enough";
import { MAINFLOW } from "../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../tools/data-object";

test("notOldEnough should match expected outcomes", () => {
  const expectedResults = {
    path: "/cannot-request-date-of-birth",
    template: "not-eligible",
    pageContent,
    isNavigable,
  };

  expect(notOldEnough).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("notOldEnough.title"),
    heading: translate("notOldEnough.heading"),
    supportOrganisationsHeading: translate("supportOrganisations.heading"),
    supportOrganisationsBody: translate("supportOrganisations.body"),
    supportOrganisationsList: translate("supportOrganisations.listItems"),
    backLinkText: translate("buttons:back"),
  };

  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when requirement is not met", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          olderThanAgeRequirement: false,
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when age requirement is met", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          olderThanAgeRequirement: true,
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});
