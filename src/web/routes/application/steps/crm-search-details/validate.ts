import { body, check } from "express-validator";
import { translateValidationMessage } from "../common/translate-validation-message";
import {
  CERT_REFERENCE_PATTERN,
  FIRST_NAME_MAX_LENGTH,
  LAST_NAME_MAX_LENGTH,
  MAX_CERT_REFERENCE_LENGTH,
  NAME_PATTERN,
  UK_POSTCODE_PATTERN,
} from "../../constants";
import { isNilOrEmpty } from "../../../../../common/predicates";
import {
  isDateAHundredTwentyOrMoreYearsInThePast,
  isDateInPast,
  isDateRealDate,
  isDateSixteenOrMoreYearsInThePast,
} from "../common/validate-date";
import { toDateString } from "../common/format-date";
import {
  validateDateOfBirthDay,
  validateDateOfBirthMonth,
  validateDateOfBirthYear,
} from "../date-of-birth/validate";

const checkPostcodeIsValid = (_, { req }) =>
  UK_POSTCODE_PATTERN.test(req.body.sanitisedPostcode);

const isFieldNotEmpty = (field) => {
  return (value, { req }) => {
    return !isNilOrEmpty(req.body[field]);
  };
};

const validateCertMaxLength = (ref, { req }) => {
  if (ref.length > MAX_CERT_REFERENCE_LENGTH) {
    throw new Error(
      req.t("validation:referenceNumberTooLong", {
        maxLength: MAX_CERT_REFERENCE_LENGTH,
      })
    );
  }

  return true;
};

const addDobToBody = (req, res, next) => {
  if (
    !isNilOrEmpty(req.body["date-of-birth-day"]) ||
    !isNilOrEmpty(req.body["date-of-birth-month"]) ||
    !isNilOrEmpty(req.body["date-of-birth-year"])
  ) {
    req.body["date-of-birth"] = toDateString(
      req.body["date-of-birth-day"],
      req.body["date-of-birth-month"],
      req.body["date-of-birth-year"]
    );
  }
  next();
};

const validateDateOfBirth = (dob, { req }) => {
  if (isNilOrEmpty(dob)) {
    throw new Error(req.t("validation:otherParentMissingDateOfBirth"));
  }
  validateDateOfBirthDay(req.body["date-of-birth-day"], { req });
  validateDateOfBirthMonth(req.body["date-of-birth-month"], { req });
  validateDateOfBirthYear(req.body["date-of-birth-year"], { req });
  if (!isDateRealDate(dob)) {
    throw new Error(req.t("validation:dateOfBirthNotReal"));
  }

  if (!isDateInPast(dob)) {
    throw new Error(req.t("validation:dateOfBirthInFuture"));
  }

  if (isDateAHundredTwentyOrMoreYearsInThePast(dob)) {
    const oldestAge = 120;
    const today = new Date();
    const oldestDob = new Date();
    oldestDob.setFullYear(today.getFullYear() - oldestAge);
    const dd = oldestDob.getDate().toString().padStart(2, "0");
    const mm = (oldestDob.getMonth() + 1).toString().padStart(2, "0");
    const yyyy = oldestDob.getFullYear().toString();
    const dateString = `${dd}-${mm}-${yyyy}`;

    throw new Error(
      req.t("validation:dateOfBirthTooOld", {
        oldestDob: dateString,
      })
    );
  }

  if (!isDateSixteenOrMoreYearsInThePast(dob)) {
    throw new Error(req.t("validation:dateOfBirthTooYoung"));
  }

  return true;
};

const validate = () => [
  check("check-empty")
    .custom((value, { req }) => {
      const fields = req.body;
      return !(
        isNilOrEmpty(fields["first-name"]) &&
        isNilOrEmpty(fields["last-name"]) &&
        isNilOrEmpty(fields["certificate-reference-number"]) &&
        isNilOrEmpty(fields["date-of-birth-day"]) &&
        isNilOrEmpty(fields["date-of-birth-month"]) &&
        isNilOrEmpty(fields["date-of-birth-year"]) &&
        isNilOrEmpty(fields["postcode"])
      );
    })
    .bail()
    .withMessage(
      translateValidationMessage("validation:searchCriteriaMissing")
    ),
  check("certificate-reference-number")
    .if(isFieldNotEmpty("certificate-reference-number"))
    .custom(validateCertMaxLength)
    .bail()
    .matches(CERT_REFERENCE_PATTERN)
    .bail()
    .withMessage(
      translateValidationMessage("validation:patternReferenceNumber")
    ),
  check("first-name")
    .if(isFieldNotEmpty("first-name"))
    .isLength({ max: FIRST_NAME_MAX_LENGTH })
    .bail()
    .withMessage(translateValidationMessage("validation:firstNameTooLong"))
    .matches(NAME_PATTERN)
    .bail()
    .withMessage(translateValidationMessage("validation:patternFirstName")),
  check("last-name")
    .if(isFieldNotEmpty("last-name"))
    .isLength({ max: LAST_NAME_MAX_LENGTH })
    .bail()
    .withMessage(translateValidationMessage("validation:lastNameTooLong"))
    .matches(NAME_PATTERN)
    .bail()
    .withMessage(translateValidationMessage("validation:patternLastName")),
  check("postcode")
    .if(isFieldNotEmpty("postcode"))
    .custom(checkPostcodeIsValid)
    .withMessage(translateValidationMessage("validation:invalidPostcode")),

  addDobToBody,
  body("date-of-birth")
    .if(isFieldNotEmpty("date-of-birth"))
    .custom(validateDateOfBirth),
];

export { validate };
