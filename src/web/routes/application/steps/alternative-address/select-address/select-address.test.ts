import { buildSessionForJourney } from "../../../flow-control/test-utils/test-utils";
import { IN_PROGRESS } from "../../../flow-control/states";
import {
  selectAddress,
  pageContent,
  isNavigable,
  validate,
  behaviourForGet,
  behaviourForPost,
} from "./select-address";
import { AGENT_ROLE, BACKOFFICE_ROLE } from "../../../constants";
import { config } from "../../../../../../config";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";

const CONFIG = {};
const JOURNEY = { name: MAINFLOW.name };
const STEP = { path: "/step-path" };
const POSTCODE_LOOKUP_RESULTS = [
  {
    ADDRESS: "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF",
    ORGANISATION_NAME: "ALAN JEFFERY ENGINEERING",
    BUILDING_NUMBER: "1",
    THOROUGHFARE_NAME: "VALLEY ROAD",
    DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
    POST_TOWN: "PLYMOUTH",
    POSTCODE: "PL7 1RF",
    LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
    UDPRN: "50265368",
  },
  {
    ADDRESS: "DULUX DECORATOR CENTRE, 2, VALLEY ROAD, PLYMOUTH, PL7 1RF",
    ORGANISATION_NAME: "DULUX DECORATOR CENTRE",
    BUILDING_NUMBER: "2",
    THOROUGHFARE_NAME: "VALLEY ROAD",
    DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
    POST_TOWN: "PLYMOUTH",
    POSTCODE: "PL7 1RF",
    LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
    UDPRN: "19000955",
  },
  {
    ADDRESS: "MILL AUTOS, 3, VALLEY ROAD, PLYMOUTH, PL7 1RF",
    ORGANISATION_NAME: "MILL AUTOS",
    BUILDING_NUMBER: "3",
    THOROUGHFARE_NAME: "VALLEY ROAD",
    DEPENDENT_THOROUGHFARE_NAME: "UPPER VALLEY ROAD",
    POST_TOWN: "PLYMOUTH",
    POSTCODE: "PL7 1RF",
    LOCAL_CUSTODIAN_CODE_DESCRIPTION: "CITY OF PLYMOUTH",
    UDPRN: "19000927",
  },
];

test("selectAddress should match expected outcomes", () => {
  const expectedResults = {
    path: "/your-address",
    template: "select-address",
    pageContent,
    isNavigable,
    validate,
    behaviourForGet,
    behaviourForPost,
  };

  expect(selectAddress).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("selectAddress.title"),
    sectionTitle: translate("section.yourDetails"),
    heading: translate("selectAddress.heading"),
    postcode: translate("selectAddress.postcode"),
    hint: translate("selectAddress.hint"),
    addressNotListed: translate("selectAddress.addressNotListed"),
    change: translate("change"),
    continueButtonText: translate("buttons:continue"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when agent_role and postcodeLookupResults exists", () => {
    config.environment.USE_AUTHENTICATION = true;

    const req = {
      session: {
        journeyName: MAINFLOW.name,
        postcodeLookupResults: POSTCODE_LOOKUP_RESULTS,
        account: {
          idTokenClaims: {
            roles: [AGENT_ROLE],
          },
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when agent_role and postcodeLookupResults doesn't exist", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        postcodeLookupResults: null,
        account: {
          idTokenClaims: {
            roles: [AGENT_ROLE],
          },
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return true when back_office_role and postcodeLookupResults exists", () => {
    config.environment.USE_AUTHENTICATION = true;

    const req = {
      session: {
        journeyName: MAINFLOW.name,
        postcodeLookupResults: POSTCODE_LOOKUP_RESULTS,
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when back_office_role and postcodeLookupResults does not exist", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        postcodeLookupResults: null,
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when no role and postcodeLookupResults exists", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        postcodeLookupResults: POSTCODE_LOOKUP_RESULTS,
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});

describe("behaviourForGet", () => {
  test("should call add addresses to stepData when no postcode lookup error", async () => {
    const req = {
      t: (key, parameters) => `${parameters.count} addresses found`,
      session: {
        journeyName: MAINFLOW.name,
        postcodeLookupResults: POSTCODE_LOOKUP_RESULTS,
        postcodeLookupError: false,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "selected-address":
            "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF",
          "address-id": "50265368",
        },
        stepData: {},
        ...buildSessionForJourney({
          journeyName: MAINFLOW.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = { locals: { postcodeLookupError: undefined } };
    const next = jest.fn();
    const expected = [
      {
        value: "",
        text: "3 addresses found",
        disabled: true,
        selected: false,
      },
      {
        text: "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF",
        value: "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF",
        selected: true,
      },
      {
        text: "DULUX DECORATOR CENTRE, 2, VALLEY ROAD, PLYMOUTH, PL7 1RF",
        value: "DULUX DECORATOR CENTRE, 2, VALLEY ROAD, PLYMOUTH, PL7 1RF",
        selected: false,
      },
      {
        text: "MILL AUTOS, 3, VALLEY ROAD, PLYMOUTH, PL7 1RF",
        value: "MILL AUTOS, 3, VALLEY ROAD, PLYMOUTH, PL7 1RF",
        selected: false,
      },
    ];

    behaviourForGet(CONFIG, JOURNEY, STEP)(req, res, next);
    const stepData = req.session.stepData["/step-path"];

    expect(stepData.addresses).toEqual(expected);
    expect(res.locals.postcodeLookupError).toBe(false);
    expect(
      req.session[MAINFLOW_DATA_OBJECT_NAME]["selected-address"]
    ).toBeFalsy();
    expect(next).toBeCalledTimes(1);
  });

  test("should add an empty array to stepData if no addresses found", () => {
    const req = {
      t: (key, parameters) => `${parameters.count} addresses found`,
      session: {
        journeyName: MAINFLOW.name,
        postcodeLookupResults: [],
        postcodeLookupError: false,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "selected-address":
            "ALAN JEFFERY ENGINEERING, 1, VALLEY ROAD, PLYMOUTH, PL7 1RF",
          "address-id": "19000955",
        },
        stepData: {},
        ...buildSessionForJourney({
          journeyName: MAINFLOW.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };

    const res = { locals: { postcodeLookupError: undefined } };
    const next = jest.fn();
    const expected = [];

    behaviourForGet(CONFIG, JOURNEY, STEP)(req, res, next);

    const stepData = req.session.stepData["/step-path"];
    expect(stepData.addresses).toEqual(expected);
    expect(res.locals.postcodeLookupError).toBe(false);
    expect(
      req.session[MAINFLOW_DATA_OBJECT_NAME]["selected-address"]
    ).toBeFalsy();
    expect(next).toBeCalledTimes(1);
  });
});
