import {
  firstParentContextPath,
  secondParentContextPath,
} from "../application/paths/context-path";
import { registerTryAgain, pageContent, get, post, PATH } from "./try-again";

const currentStep = "/blc/current-step";
const app = {
  ...jest.requireActual("express"),
  get: jest.fn(),
  post: jest.fn(),
};
const req = {
  t: (string) => string,
  session: {
    journeys: {
      MAINFLOW: {
        currentStep,
      },
    },
  },
};
const res = {
  redirect: jest.fn(),
  render: jest.fn(),
};

test("registerTryAgain should register get and post routes with expected callbacks", () => {
  app.get.mockImplementation((path, callback) => {
    callback(req, res);
  });

  app.post.mockImplementation((path, callback) => {
    callback(req, res);
  });

  registerTryAgain(app);

  expect(app.get).toBeCalledTimes(1);
  expect(app.get).toBeCalledWith(
    [firstParentContextPath(PATH), secondParentContextPath(PATH)],
    get
  );

  expect(app.post).toBeCalledTimes(1);
  expect(app.post).toBeCalledWith(
    [firstParentContextPath(PATH), secondParentContextPath(PATH)],
    post
  );
});

test("get should render expected content", () => {
  get(req, res);

  expect(res.render).toBeCalled();
  expect(res.render).toBeCalledWith("try-again", {
    ...pageContent({ translate: req.t }),
  });
});

test("post should redirect to journey's currentStep", () => {
  post(req, res);

  expect(res.redirect).toBeCalled();
  expect(res.redirect).toBeCalledWith(currentStep);
});
