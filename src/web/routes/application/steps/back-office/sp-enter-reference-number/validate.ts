import { body, check } from "express-validator";
import { translateValidationMessage } from "../../common/translate-validation-message";
import { getCountryCodes } from "../../../tools/country-code-parser";

const isValidCountryCode = (value, { req }) => {
  const countryCode = req.body["their-reference-number-country"];
  const country = getCountryCodes().find((c) => c["alpha-3"] === countryCode);

  if (country === undefined) {
    throw new Error(req.t("validation:invalidCountry"));
  }
  return true;
};

const validate = () => [
  check("their-reference-number")
    .not()
    .isEmpty()
    .bail()
    .withMessage(translateValidationMessage("validation:missingReferenceId"))
    .isLength({ max: 64 })
    .bail()
    .withMessage(translateValidationMessage("validation:referenceIdTooLong")),
  body("their-reference-number-country").custom(isValidCountryCode),
];

export { validate };
