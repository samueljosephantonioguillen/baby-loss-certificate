import { CONTEXT_PATH, CONTACT_US } from "./paths";
import { renderStaticPage } from "./static-page-content";

const pageContent = ({ translate, req }) => ({
  title: translate("contactUs.title"),
  heading: translate("contactUs.heading"),
  telephoneHeading: translate("contactUs.telephone.heading"),
  telephoneBody: translate("contactUs.telephone.body"),
  telephoneContactList: translate("contactUs.telephone.list"),
  telephoneCallChargesText: translate("contactUs.telephone.chargesText"),
  telephoneCallChargesLink: translate("contactUs.telephone.chargesLink"),
  emailHeading: translate("contactUs.email.heading"),
  emailTag: translate("contactUs.email.tag"),
  emailContact: translate("contactUs.email.contact"),
  emailBody: translate("contactUs.email.body"),
  hideServiceLink: req.session.hideServiceLink,
});

const renderContactUs = (app) => {
  app.get(
    CONTEXT_PATH + CONTACT_US,
    renderStaticPage("contact-us", pageContent)
  );
};

export { renderContactUs, pageContent };
