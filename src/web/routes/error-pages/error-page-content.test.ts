import {
  renderErrorPageContent,
  getErrorPageContent,
  getSessionEndedContent,
  renderSessionEndedPageContent,
} from "./error-page-content";

export const expectedProblemWithServiceTranslation = {
  title: "errors:problemWithTheService.title",
  heading: "errors:problemWithTheService.heading",
  body: "errors:problemWithTheService.body",
  startAgain: "errors:problemWithTheService.startAgain",
};

export const expectedSessionEndedTranslation = {
  heading: "errors:sessionEnded.heading",
  paragraphOne: "errors:sessionEnded.paragraphOne",
  paragraphTwo: "errors:sessionEnded.paragraphTwo",
  startAgain: "errors:sessionEnded.startAgain",
  title: "errors:sessionEnded.title",
};

const errorUrls = [
  "problem-with-the-service",
  "page-not-found",
  "timed-out",
  "cookies-enabler",
  "session-ended",
];

const req = { t: (string) => string };
const res = { render: jest.fn(), status: jest.fn().mockReturnThis() };

describe("getErrorPageContent should call res.render with page url and correct page content and 500 status code", () => {
  test.each(errorUrls)(
    "should render %s error page with expected translation content",
    (url) => {
      renderErrorPageContent(url)(req, res);

      expect(res.status).toBeCalledWith(500);
      expect(res.render).toBeCalledTimes(1);
      expect(res.render).toBeCalledWith(
        "error-pages/" + url,
        expectedProblemWithServiceTranslation
      );
    }
  );

  it("getErrorPageContent should return correct array of content", () => {
    const expectedContent = {
      title: req.t("errors:problemWithTheService.title"),
      heading: req.t("errors:problemWithTheService.heading"),
      body: req.t("errors:problemWithTheService.body"),
      startAgain: req.t("errors:problemWithTheService.startAgain"),
    };

    const result = getErrorPageContent(req);

    expect(result).toStrictEqual(expectedContent);
  });
});

describe("getSessionEndedContent() should call res.render with page url and correct page content", () => {
  test.each(errorUrls)(
    "should render %s error page with expected translation content",
    (url) => {
      renderSessionEndedPageContent(url)(req, res);

      expect(res.render).toBeCalledTimes(1);
      expect(res.render).toBeCalledWith(
        "error-pages/" + url,
        expectedSessionEndedTranslation
      );
    }
  );

  it("getSessionEndedContent() should return correct array of content", () => {
    const expectedContent = {
      heading: req.t("errors:sessionEnded.heading"),
      paragraphOne: req.t("errors:sessionEnded.paragraphOne"),
      paragraphTwo: req.t("errors:sessionEnded.paragraphTwo"),
      startAgain: req.t("errors:sessionEnded.startAgain"),
      title: req.t("errors:sessionEnded.title"),
    };

    const result = getSessionEndedContent(req);

    expect(result).toStrictEqual(expectedContent);
  });
});
