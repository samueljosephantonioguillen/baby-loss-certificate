import expect from "expect";

import { sanitise } from "./sanitise";

const next = jest.fn();

test("should trims spaces and format nhs number", () => {
  const req = {
    body: {
      "nhs-number": "  314 159 2653",
    },
  };

  sanitise()(req, {}, next);

  expect(req.body).toEqual({
    "nhs-number": "3141592653",
    formattedNhsNumber: "314 159 2653",
  });

  expect(next).toBeCalledTimes(1);
});
