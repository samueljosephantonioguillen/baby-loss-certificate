import { NotifyClient } from "notifications-node-client";

import { config } from "../../../../../config";
import { logger } from "../../../../logger/logger";

const notifyClient = new NotifyClient(config.environment.GOV_NOTIFY_API_KEY);
const START_PAGE_URL = config.environment.START_PAGE_URL;

const sendOtpEmail = async (req, email, securityCode) => {
  const response = await notifyClient.sendEmail(
    config.environment.EMAIL_OTP_TEMPLATE_ID,
    email,
    {
      personalisation: {
        "one-time-passcode": securityCode,
      },
    }
  );

  logger.info(`OTP email was sent successfully: ${response.status}`, req);
};

const sendOtpSms = async (req, phoneNumber, securityCode) => {
  const response = await notifyClient.sendSms(
    config.environment.SMS_OTP_TEMPLATE_ID,
    phoneNumber,
    {
      personalisation: {
        "one-time-passcode": securityCode,
      },
    }
  );

  logger.info(`OTP text was sent successfully: ${response.status}`, req);
};

const sendUnsuccessfulApplicationEmail = async (
  req,
  email,
  fullName,
  referenceNumber
) => {
  const response = await notifyClient.sendEmail(
    config.environment.BLC_EMAIL_UNSUCCESSFUL_APPLICATION_TEMPLATE_ID,
    email,
    {
      personalisation: {
        first_parent_name: fullName,
        ref_number: referenceNumber,
        link: START_PAGE_URL,
      },
    }
  );

  logger.info(
    `Application Rejection email was sent successfully: ${response.status}`,
    req
  );
};

export { sendOtpEmail, sendOtpSms, sendUnsuccessfulApplicationEmail };
