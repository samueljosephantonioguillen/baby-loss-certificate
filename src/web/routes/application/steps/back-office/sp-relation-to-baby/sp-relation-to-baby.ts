import { userIsBackOffice } from "../../../../azure-routes/auth";
import { getFlowDataObjectName } from "../../../tools/data-object";
import { validate } from "../../second-parent/their-relation-to-baby/validate";

const pageContent = ({ translate }) => ({
  title: translate("theirRelationToBaby.title"),
  sectionTitle: translate("section.otherParentsDetails"),
  heading: translate("theirRelationToBaby.heading"),
  hint: translate("theirRelationToBaby.hint"),
  mother: translate("relationToBaby.radios.mother"),
  father: translate("relationToBaby.radios.father"),
  parent: translate("relationToBaby.radios.parent"),
  surrogate: translate("relationToBaby.radios.surrogate"),
  backLinkText: translate("buttons:back"),
  continueButtonText: translate("buttons:continue"),
});

const isNavigable = (req) =>
  req.session[getFlowDataObjectName(req.session.journeyName)][
    "add-another-parent"
  ] === "yes" && userIsBackOffice(req);

const spRelationToBaby = {
  path: "/their-relationship-to-baby",
  template: "their-relation-to-baby",
  pageContent,
  isNavigable,
  validate,
};

export { spRelationToBaby, pageContent, isNavigable, validate };
