import { isUsingAuthentication } from "../../../azure-routes/auth";

const sessionDestroy = (req, res) => {
  req.session.destroy();
  res.clearCookie("lang");
};

const deleteSessionExcept = (req, parametersToKeep) => {
  Object.keys(req.session).forEach(function (key) {
    if (!parametersToKeep.includes(key)) {
      delete req.session[key];
    }
  });
};

const sessionDestroyButKeepLoggedIn = (req, res) => {
  if (isUsingAuthentication()) {
    deleteSessionExcept(req, [
      "cookie",
      "csrfToken",
      "pkceCodes",
      "authCodeUrlRequest",
      "authCodeRequest",
      "accessToken",
      "idToken",
      "account",
      "isAuthenticated",
      "userCypher",
    ]);
  } else {
    sessionDestroy(req, res);
  }
};

export { sessionDestroy, deleteSessionExcept, sessionDestroyButKeepLoggedIn };
