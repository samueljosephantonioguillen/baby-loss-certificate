export const GET_NEXT_PATH = "getNextPath";
export const IS_PATH_ALLOWED = "isPathAllowed";
export const GET_NEXT_ALLOWED_PATH = "getNextAllowedPath";
export const SET_NEXT_ALLOWED_PATH = "setNextAllowedPath";
export const INCREMENT_NEXT_ALLOWED_PATH = "incrementNextAllowedPath";
export const INVALIDATE_REVIEW = "invalidateReview";
