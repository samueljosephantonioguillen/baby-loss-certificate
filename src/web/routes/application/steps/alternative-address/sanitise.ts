import { toUpper, compose } from "ramda";

const replaceMultipleSpacesWithOne = (value) => {
  return value.replace(/ {2,}/g, " ");
};

const addSpaceBeforeLastThreeCharacters = (value) => {
  return value.replace(/^(.*)(\d)/, "$1 $2");
};

const replaceMultipleSpacesAndAddSingleSpaceThenUppercase = compose(
  replaceMultipleSpacesWithOne,
  addSpaceBeforeLastThreeCharacters,
  toUpper
);

const sanitise = () => (req, res, next) => {
  req.body.sanitisedPostcodeCcs =
    replaceMultipleSpacesAndAddSingleSpaceThenUppercase(
      req.body["postcode-ccs"]
    );

  if (req.body["address-line-1-ccs"]) {
    req.body["address-line-1-ccs"] = req.body["address-line-1-ccs"].trim();
  }

  if (req.body["address-line-2-ccs"]) {
    req.body["address-line-2-ccs"] = req.body["address-line-2-ccs"].trim();
  }

  if (req.body["address-line-3-ccs"]) {
    req.body["address-line-3-ccs"] = req.body["address-line-3-ccs"].trim();
  }

  if (req.body["address-line-4-ccs"]) {
    req.body["address-line-4-ccs"] = req.body["address-line-4-ccs"].trim();
  }

  next();
};

export { sanitise };
