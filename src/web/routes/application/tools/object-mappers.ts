export const convertReferenceTypeToNumber = (referenceType) => {
  switch (referenceType) {
    case "nhsNumber":
      return 1;
    case "passport":
      return 2;
    case "birthCertificate":
      return 3;
    case "eeaMemberCard":
      return 4;
    case "drivingLicence":
      return 5;
    case "oldDrivingLicence":
      return 6;
    case "photoRegistration":
      return 7;
    case "benefitBook":
      return 8;
    case "residencePermit":
      return 9;
    case "nationalIdentity":
      return 10;
    default:
      return 1;
  }
};
