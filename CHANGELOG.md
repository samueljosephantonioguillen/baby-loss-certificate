# # Baby Loss Certificate Changelog

## {version number} - {date}

:new: **New features**

- {feature}

:wrench: **Fixes**

- {issue}
