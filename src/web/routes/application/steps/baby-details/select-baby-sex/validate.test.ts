import expect from "expect";

import { validate } from "./validate";
import { applyExpressValidation } from "../../common/test/apply-express-validation";

test("return no validation errors when baby-sex field is valid", async () => {
  const req = {
    body: {
      "baby-sex": "female",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("return expected validation error when baby-sex field is empty", async () => {
  const req = {
    body: {
      "baby-sex": "",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "",
    msg: "validation:selectSexOfBaby",
    path: "baby-sex",
    location: "body",
  };

  expect(error).toEqual(expectedError);
});
