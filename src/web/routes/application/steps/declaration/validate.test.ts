import expect from "expect";

import { validate } from "./validate";
import { applyExpressValidation } from "../common/test/apply-express-validation";

test("return no validation errors when declaration field isn't empty", async () => {
  const req = {
    body: {
      applicantDeclaration: "agree",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("returns expected validation error when applicantDeclaration is empty", async () => {
  const req = {
    body: {
      applicantDeclaration: "",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  expect(error.msg).toBe("validation:selectDeclarationAgree");
});
