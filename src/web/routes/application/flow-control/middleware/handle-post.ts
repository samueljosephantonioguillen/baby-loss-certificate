import { StatusCodes } from "http-status-codes";
import { validationResult } from "express-validator";
import { wrapError } from "../../errors";
import { stateMachine } from "../state-machine";

import { INCREMENT_NEXT_ALLOWED_PATH } from "../state-machine/actions";
import { logger } from "../../../../logger/logger";
import { getFlowDataObjectName } from "../../tools/data-object";

const handlePost = (journey) => (req, res, next) => {
  try {
    const dataObjectName = getFlowDataObjectName(journey.name);
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      logger.info(`Validation errors count ${errors.array.length}`, req);
      res.locals.errors = errors.array();
      res.locals.errorTitleText = req.t("validation:errorTitleText");
      res.locals[dataObjectName] = {
        ...res.locals[dataObjectName],
        ...req.body,
      };
      return next();
    }

    req.session[dataObjectName] = {
      ...req.session[dataObjectName],
      ...req.body,
    };

    stateMachine.dispatch(INCREMENT_NEXT_ALLOWED_PATH, req, journey);

    return next();
  } catch (error) {
    next(
      wrapError({
        cause: error,
        message: `Error posting ${req.path}`,
        statusCode: StatusCodes.INTERNAL_SERVER_ERROR,
      })
    );
  }
};

export { handlePost };
