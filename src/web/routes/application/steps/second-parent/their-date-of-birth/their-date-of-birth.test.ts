import { BACKOFFICE_ROLE } from "../../../constants";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";
import {
  theirDateOfBirth,
  pageContent,
  isNavigable,
  sanitise,
  validate,
} from "./their-date-of-birth";
import { config } from "../../../../../../config";

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("theirDateOfBirth.title"),
    sectionTitle: translate("section.otherParentsDetails"),
    heading: translate("theirDateOfBirth.heading"),
    hint: translate("theirDateOfBirth.hint"),
    day: translate("day"),
    month: translate("month"),
    year: translate("year"),
    backLinkText: translate("buttons:back"),
    continueButtonText: translate("buttons:continue"),
  };

  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

test("theirDateOfBirth should match expected outcomes", () => {
  const expectedResults = {
    path: "/other-parent-date-of-birth",
    template: "their-date-of-birth",
    pageContent,
    isNavigable,
    sanitise,
    validate,
  };

  expect(theirDateOfBirth).toEqual(expectedResults);
});

describe("isNavigable", () => {
  test("should return true when their-email is populated is yes", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "their-email": "test@test.com",
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when their-email is not populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "their-email": undefined,
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return true when user is back office and their name is populated", () => {
    config.environment.USE_AUTHENTICATION = true;

    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "their-first-name": "test",
          "their-last-name": "test",
        },
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when user is back office and their name is not populated", () => {
    config.environment.USE_AUTHENTICATION = true;

    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {},
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});
