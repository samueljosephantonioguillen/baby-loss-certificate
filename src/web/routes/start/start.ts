import * as express from "express";
import { configureAuthentication } from "../application/flow-control/middleware/configure-authentication";
import { sessionDestroyButKeepLoggedIn } from "../application/steps/common/session-destroy";
import { isUsingAuthentication } from "../azure-routes/auth";
import { FIRST_STEP } from "../application/register-journey-routes";

const CONTEXT_PATH = process.env.CONTEXT_PATH ?? "";
const START_PAGE_URL = process.env.START_PAGE_URL ?? "";
const START = "/request-a-baby-loss-certificate";

const pageContent = ({ translate }) => ({
  title: translate("start.title"),
  heading: translate("start.heading"),
  bodyLineOne: translate("start.body.firstLine"),
  bodyLineTwo: translate("start.body.secondLine"),
  bodyLineThree: translate("start.body.thirdLine"),
  bodyLineFour: translate("start.body.fourthLine"),
  bodyLineFive: translate("start.body.fifthLine", {
    contextPath: CONTEXT_PATH,
  }),
  requestACertificateHeading: translate("start.requestACertificate.heading"),
  requestACertificateLineOne: translate("start.requestACertificate.firstLine"),
  requestACertificateListItemOne: translate(
    "start.requestACertificate.firstListItem"
  ),
  requestACertificateListItemTwo: translate(
    "start.requestACertificate.secondListItem"
  ),
  requestACertificateListItemThree: translate(
    "start.requestACertificate.thirdListItem"
  ),
  requestACertificateListItemFour: translate(
    "start.requestACertificate.fourthListItem"
  ),
  requestACertificateListItemFive: translate(
    "start.requestACertificate.fifthListItem"
  ),
  requestACertificateListItemSix: translate(
    "start.requestACertificate.sixthListItem"
  ),
  requestACertificateLineTwo: translate("start.requestACertificate.secondLine"),
  requestACertificateLineThree: translate(
    "start.requestACertificate.thirdLine"
  ),
  beforeYouStartHeading: translate("start.beforeYouStart.heading"),
  beforeYouStartLineOne: translate("start.beforeYouStart.firstLine"),
  beforeYouStartListItemOne: translate("start.beforeYouStart.firstListItem"),
  beforeYouStartLineTwo: translate("start.beforeYouStart.secondLine"),
  startNow: translate("start.startNow"),
  startPath: FIRST_STEP,
});

const renderStartRoute = (app) => {
  const router = express.Router();
  app.use((req, res, next) => {
    res.set("Clear-Site-Data", '"storage"');
    next();
  });
  app.use(router);
  router.get(CONTEXT_PATH + START, configureAuthentication, (req, res) => {
    if (!isUsingAuthentication()) {
      return res.redirect(START_PAGE_URL);
    } else {
      sessionDestroyButKeepLoggedIn(req, res);
      res.clearCookie("lang");
      res.render("start", {
        ...pageContent({ translate: req.t }),
      });
    }
  });
};

const registerStartRoute = (app) => renderStartRoute(app);

export { registerStartRoute, START, pageContent };
