import { getFlowDataObjectName } from "../../tools/data-object";

const pageContent = ({ translate }) => ({
  title: translate("notEligibleYear.title"),
  heading: translate("notEligibleYear.heading"),
  supportOrganisationsHeading: translate("supportOrganisations.heading"),
  supportOrganisationsBody: translate("supportOrganisations.body"),
  supportOrganisationsList: translate("supportOrganisations.listItems"),
  backLinkText: translate("buttons:back"),
});

const isNavigable = (req) =>
  req.session[getFlowDataObjectName(req.session.journeyName)][
    "loss-in-five-years"
  ] === "no";

const notEligibleYear = {
  path: "/cannot-request-year-of-baby-loss",
  template: "not-eligible",
  pageContent,
  isNavigable,
};

export { notEligibleYear, pageContent, isNavigable };
