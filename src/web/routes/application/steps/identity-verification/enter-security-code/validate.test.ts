import expect from "expect";

import { validate } from "./validate";
import { applyExpressValidation } from "../../common/test/apply-express-validation";
import { config } from "../../../../../../config";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";

config.environment.NODE_ENV = "dev";

test("returns no validation errors when security-code field matches", async () => {
  const req = {
    session: {
      journeyName: MAINFLOW.name,
      [MAINFLOW_DATA_OBJECT_NAME]: {
        otp: "456789",
      },
    },
    body: {
      "security-code": "456789",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("returns expected validation error when security-code field does not match", async () => {
  const req = {
    session: {
      journeyName: MAINFLOW.name,
      [MAINFLOW_DATA_OBJECT_NAME]: {
        otp: "456789",
        otpAttempts: 2,
      },
    },
    body: {
      "security-code": "654321",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "654321",
    msg: "validation:invalidSecurityCode",
    path: "security-code",
    location: "body",
  };

  expect(result.array().length).toBe(1);
  expect(error).toEqual(expectedError);
  expect(req.session[MAINFLOW_DATA_OBJECT_NAME].otpAttempts).toBe(3);
});

test("returns expected validation error when security-code field is empty", async () => {
  const req = {
    session: {
      otp: "456789",
      otpAttempts: 1,
    },
    body: {
      "security-code": "",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "",
    msg: "validation:missingSecurityCode",
    path: "security-code",
    location: "body",
  };

  expect(result.array().length).toBe(1);
  expect(error).toEqual(expectedError);
  expect(req.session.otpAttempts).toBe(1);
});

test("returns no validation errors when security-code field matches and NODE_ENV is dev and enter override code", async () => {
  const req = {
    session: {
      otp: "456789",
    },
    body: {
      "security-code": "123456",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("returns expected validation error when NODE_ENV is prod and enter override code", async () => {
  config.environment.NODE_ENV = "prod";

  const req = {
    session: {
      journeyName: MAINFLOW.name,
      [MAINFLOW_DATA_OBJECT_NAME]: {
        otp: "456789",
        otpAttempts: 2,
      },
    },
    body: {
      "security-code": "123456",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "123456",
    msg: "validation:invalidSecurityCode",
    path: "security-code",
    location: "body",
  };

  expect(result.array().length).toBe(1);
  expect(error).toEqual(expectedError);
  expect(req.session[MAINFLOW_DATA_OBJECT_NAME].otpAttempts).toBe(3);
});
