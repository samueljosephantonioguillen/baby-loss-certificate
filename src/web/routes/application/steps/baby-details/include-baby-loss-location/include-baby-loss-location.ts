import { validationResult } from "express-validator";
import { validate } from "./validate";
import { sanitise } from "./sanitise";
import { getFlowDataObjectName } from "../../../tools/data-object";

const pageContent = ({ translate }) => ({
  title: translate("includeBabyLossLocation.title"),
  sectionTitle: translate("section.certificateDetails"),
  heading: translate("includeBabyLossLocation.heading"),
  bodyLineOne: translate("includeBabyLossLocation.body.firstLine"),
  hintLineOne: translate("includeBabyLossLocation.hint.firstLine"),
  hintLineTwo: translate("includeBabyLossLocation.hint.secondLine"),
  insetTextLineOne: translate("includeBabyLossLocation.insetText"),
  yes: translate("yes"),
  label: translate("includeBabyLossLocation.label"),
  no: translate("no"),
  or: translate("or"),
  unknown: translate("includeBabyLossLocation.unknown"),
  continueButtonText: translate("buttons:continue"),
  backLinkText: translate("buttons:back"),
});

const behaviourForPost = () => (req, res, next) => {
  if (!validationResult(req).isEmpty()) {
    return next();
  }

  if (
    req.body["include-baby-loss-location"] === "no" ||
    req.body["include-baby-loss-location"] === "unknown"
  ) {
    delete req.body["baby-loss-location"];
    delete req.session[getFlowDataObjectName(req.session.journeyName)][
      "baby-loss-location"
    ];
  }
  next();
};

const includeBabyLossLocation = {
  path: "/place-of-baby-loss",
  template: "include-baby-loss-location",
  pageContent,
  sanitise,
  validate,
  behaviourForPost,
};

export {
  includeBabyLossLocation,
  pageContent,
  sanitise,
  validate,
  behaviourForPost,
};
