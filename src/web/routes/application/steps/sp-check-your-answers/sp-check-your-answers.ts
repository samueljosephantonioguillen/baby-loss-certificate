import { START } from "../../../start";
import { contextPath } from "../../paths/context-path";

import { toLongLocaleDateString } from "../common/format-date";
import { isNilOrEmpty } from "../../../../../common/predicates";

import { stateMachine } from "../../flow-control/state-machine";
import { IN_PROGRESS, IN_REVIEW } from "../../flow-control/states";
import { INCREMENT_NEXT_ALLOWED_PATH } from "../../flow-control/state-machine/actions";
import { logger } from "../../../../logger/logger";
import { SECOND_PARENT_DATA_OBJECT_NAME } from "../../tools/data-object";
import { validate } from "./validate";

const getKeyFromSession = (session) => (key) => session[key];

const pageContent = ({ translate, req }) => ({
  title: translate("spCheckYourAnswers.title"),
  heading: translate("spCheckYourAnswers.heading"),
  firstParent: translate("spCheckYourAnswers.firstParent"),
  checkFirstParent: translate(
    "spCheckYourAnswers.checkCertificateDetails.checkFirstParent",
    {
      firstParentName: `${req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-first-name"]} ${req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-last-name"]}`,
      firstParentRelation:
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-relation-to-baby"],
    }
  ),
  secondParent: translate("spCheckYourAnswers.secondParent"),
  checkSecondParent: translate(
    "spCheckYourAnswers.checkCertificateDetails.checkSecondParent",
    {
      secondParentName: `${
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["first-name"] || ""
      } ${req.session[SECOND_PARENT_DATA_OBJECT_NAME]["last-name"] || ""}`,
      secondParentRelation:
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["relation-to-baby"],
    }
  ),
  babyDetails: translate("spCheckYourAnswers.babyDetails"),
  checkBabyDetails: translate(
    "spCheckYourAnswers.checkCertificateDetails.checkBabyDetails",
    {
      babyName: req.session[SECOND_PARENT_DATA_OBJECT_NAME]["baby-name"],
      babySex: req.session[SECOND_PARENT_DATA_OBJECT_NAME]["baby-sex"],
      babyLossDate: getBabyDateOfLoss(req),
      babyLossLocation:
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["baby-loss-location"],
    }
  ),
  change: translate("change"),
  button: translate("spCheckYourAnswers.button"),
  areDetailsCorrect: translate("spCheckYourAnswers.areDetailsCorrect"),
  yes: translate("yes"),
  no: translate("no"),
});

const citizenRequiredFields = [
  "first-name",
  "last-name",
  "fp-first-name",
  "fp-last-name",
  "fp-relation-to-baby",
  "relation-to-baby",
];

const getBabyDateOfLoss = (req) => {
  return req.session[SECOND_PARENT_DATA_OBJECT_NAME]["print-date-of-loss"] ===
    false
    ? ""
    : toLongLocaleDateString(
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["baby-date-of-loss-day"] ??
          "",
        req.session[SECOND_PARENT_DATA_OBJECT_NAME][
          "baby-date-of-loss-month"
        ] ?? "",
        req.session[SECOND_PARENT_DATA_OBJECT_NAME]["baby-date-of-loss-year"]
      );
};

const behaviourForGet = (config, journey) => (req, res, next) => {
  if (req.session[SECOND_PARENT_DATA_OBJECT_NAME]["cancel-application"]) {
    delete req.session[SECOND_PARENT_DATA_OBJECT_NAME]["cancel-application"];
  }
  if (
    citizenRequiredFields.some((field) =>
      isNilOrEmpty(req.session[SECOND_PARENT_DATA_OBJECT_NAME][field])
    )
  ) {
    logger.error("Missing required fields in check-your-answers");
    return res.redirect(contextPath(START, journey.name));
  }

  stateMachine.setState(IN_REVIEW, req, journey);

  stateMachine.dispatch(INCREMENT_NEXT_ALLOWED_PATH, req, journey);

  next();
};

const behaviourForPost = (config, journey) => (req, res, next) => {
  stateMachine.setState(IN_PROGRESS, req, journey);
  next();
};

const spCheckYourAnswers = {
  path: "/confirm-details",
  template: "sp-check-your-answers",
  pageContent,
  behaviourForGet,
  validate,
  behaviourForPost,
};

export {
  spCheckYourAnswers,
  pageContent,
  behaviourForGet,
  getKeyFromSession,
  validate,
  behaviourForPost,
  getBabyDateOfLoss,
};
