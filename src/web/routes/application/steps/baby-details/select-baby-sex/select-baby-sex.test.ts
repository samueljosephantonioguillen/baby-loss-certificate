import expect from "expect";

import {
  selectBabySex,
  pageContent,
  isNavigable,
  validate,
  behaviourForPost,
} from "./select-baby-sex";
import { buildSessionForJourney } from "../../../flow-control/test-utils/test-utils";
import {
  IN_PROGRESS,
  IN_REVIEW,
  IN_REVIEW_PROGRESS,
} from "../../../flow-control/states";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";

test("selectBabySex should match expected outcomes", () => {
  const expectedResults = {
    path: "/enter-baby-sex",
    template: "select-baby-sex",
    pageContent,
    isNavigable,
    validate,
    behaviourForPost,
  };

  expect(selectBabySex).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("selectBabySex.title"),
    sectionTitle: translate("section.certificateDetails"),
    heading: translate("selectBabySex.heading"),
    male: translate("selectBabySex.male"),
    female: translate("selectBabySex.female"),
    continueButtonText: translate("buttons:continue"),
    backLinkText: translate("buttons:back"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when include-baby-sex is yes", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "include-baby-sex": "yes",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when include-baby-sex is no", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "include-baby-sex": "no",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});

describe("behaviourForPost", () => {
  test("should set state to IN_REVIEW when current journey state is IN_REVIEW_PROGRESS", () => {
    const config = {};
    const journey = { name: MAINFLOW.name };

    const req = {
      session: {
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "baby-sex": "Male",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_REVIEW_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = {};
    const next = jest.fn();

    behaviourForPost(config, journey)(req, res, next);

    const expectedSession = {
      ...req.session,
      ...buildSessionForJourney({
        journeyName: journey.name,
        state: IN_REVIEW,
        nextAllowedPath: undefined,
      }),
    };

    expect(req.session).toEqual(expectedSession);
    expect(next).toBeCalled();
  });

  test("should call next and not change state when state isn't IN_REVIEW_PROGRESS", () => {
    const config = {};
    const journey = { name: MAINFLOW.name };

    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "baby-sex": "Male",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = {};
    const next = jest.fn();

    behaviourForPost(config, journey)(req, res, next);

    const expectedReq = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "baby-sex": "Male",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };

    expect(req).toEqual(expectedReq);
    expect(next).toBeCalled();
  });
});
