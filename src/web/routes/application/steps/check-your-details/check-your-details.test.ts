import expect from "expect";

const pdsLookupResult = jest.fn();
jest.mock("./request-pds-lookup", () => ({
  ...jest.requireActual("./request-pds-lookup"),
  requestPdsLookup: pdsLookupResult,
}));

const pdsSearchResult = jest.fn();
jest.mock("./request-pds-search", () => ({
  ...jest.requireActual("./request-pds-search"),
  requestPdsSearch: pdsSearchResult,
}));

import {
  checkYourDetails,
  pageContent,
  behaviourForGet,
  behaviourForPost,
  createDetails,
  isNavigable,
} from "./check-your-details";

import { START } from "../../../start";
import {
  contextPath,
  firstParentContextPath,
  secondParentContextPath,
} from "../../paths/context-path";
import { buildSessionForJourney } from "../../flow-control/test-utils/test-utils";
import { IN_DETAILS_REVIEW, IN_PROGRESS } from "../../flow-control/states";
import { getSecurityCode } from "../identity-verification/get-security-code";
import { notMatched } from "../identity-verification/not-matched";
import { notOldEnoughPds } from "../not-old-enough-pds";
import { noContactDetails } from "../identity-verification/no-contact-details";
import { noFoundAddress } from "../identity-verification/no-found-address";
import { FIRST_NAME_MAX_LENGTH, LAST_NAME_MAX_LENGTH } from "../../constants";
import { MAINFLOW, SECOND_PARENT_FLOW } from "../../journey-definitions";
import {
  MAINFLOW_DATA_OBJECT_NAME,
  SECOND_PARENT_DATA_OBJECT_NAME,
} from "../../tools/data-object";
import { spDobNotMatched } from "../sp-dob-not-matched";

const config = {};
const res = { redirect: jest.fn() };
const next = jest.fn();

test("checkYourAnswers should match expected outcomes", () => {
  const expectedResults = {
    path: "/check-your-details",
    template: "check-your-details",
    pageContent,
    behaviourForGet,
    behaviourForPost,
    isNavigable,
  };

  expect(checkYourDetails).toEqual(expectedResults);
});

describe("createDetails", () => {
  let details;

  beforeEach(() => {
    details = [
      {
        key: "NHS number",
        value: "111 111 1111",
      },
      {
        key: "First Name",
        value: "first",
      },
      {
        key: "Last Name",
        value: "last",
      },
      {
        key: "Date of birth",
        value: "2000-01-01",
      },
      {
        key: "Postcode",
        value: "PL7 1RF",
      },
    ];
  });

  test("should return shifted array when no nhs number", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "know-your-nhs-number": "yes",
          "nhs-number": "1111111111",
        },
      },
    };

    const result = createDetails(req, details);

    details = [
      {
        key: "NHS number",
        value: "111 111 1111",
      },
    ];

    expect(result).toEqual(details);
  });

  test("should return shifted array when no nhs number", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "know-your-nhs-number": "no",
        },
      },
    };

    const result = createDetails(req, details);

    expect(result).toEqual(details);
  });
});

describe("pageContent", () => {
  test("should return expected results with nhs number field", () => {
    const translate = (string, object?) => {
      if (object) {
        return [{ nhsNumber: "111 111 1111", key: "NHS number" }];
      }
      return string;
    };

    const applicant = {
      "know-your-nhs-number": "yes",
      "nhs-number": "1111111111",
      formattedNhsNumber: "111 111 1111",
    };
    const req = { session: { applicant, journeyName: MAINFLOW.name } };

    const expected = {
      title: translate("checkYourDetails.title"),
      sectionTitle: translate("section.yourDetails"),
      heading: translate("checkYourDetails.heading"),
      details: translate("checkYourDetails.details", {
        nhsNumber: req.session[MAINFLOW_DATA_OBJECT_NAME]["formattedNhsNumber"],
      }),
      body: translate("checkYourDetails.body"),
      list: translate("checkYourDetails.list"),
      change: translate("change"),
      button: translate("checkYourDetails.button"),
      backLinkText: translate("buttons:back"),
    };
    const result = pageContent({ translate, req });

    expect(result).toEqual(expected);
  });

  test("should return expected results with non nhs number fields", () => {
    const translate = (string, object?) => {
      if (object) {
        return [
          { nhsNumber: "111 111 1111" },
          { dateOfBirth: "01/01/2000" },
          { postcode: "PL7 1RF" },
          { name: "jane doe" },
        ];
      }
      return string;
    };

    const applicant = {
      "know-your-nhs-number": "no",
      "date-of-birth-day": "01",
      "date-of-birth-month": "01",
      "date-of-birth-year": "2000",
      "date-of-birth": "2000-01-01",
      postcode: "PL71RF",
      sanitisedPostcode: "PL7 1RF",
      "first-name": "jane",
      "last-name": "doe",
    };
    const req = { session: { applicant, journeyName: MAINFLOW.name } };

    const expected = {
      title: translate("checkYourDetails.title"),
      sectionTitle: translate("section.yourDetails"),
      heading: translate("checkYourDetails.heading"),
      details: [
        { dateOfBirth: "01/01/2000" },
        { postcode: "PL7 1RF" },
        { name: "jane doe" },
      ],
      body: translate("checkYourDetails.body"),
      list: translate("checkYourDetails.list"),
      change: translate("change"),
      button: translate("checkYourDetails.button"),
      backLinkText: translate("buttons:back"),
    };
    const result = pageContent({ translate, req });

    expect(result).toEqual(expected);
  });
});

describe("behaviourForGet", () => {
  const journey = { name: MAINFLOW.name };

  test("should set state to IN_DETAILS_REVIEW when all non nhs number required fields exist", () => {
    const req = {
      session: {
        journeyName: journey.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "know-your-nhs-number": "no",
          "date-of-birth-day": "01",
          "date-of-birth-month": "01",
          "date-of-birth-year": "2000",
          "date-of-birth": "2000-01-01",
          postcode: "PL71RF",
          sanitisedPostcode: "PL7 1RF",
          "first-name": "Jane",
          "last-name": "Doe",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };

    behaviourForGet(config, journey)(req, res, next);

    const expectedSession = {
      ...req.session,
      ...buildSessionForJourney({
        journeyName: journey.name,
        state: IN_DETAILS_REVIEW,
        nextAllowedPath: "/check-your-details",
      }),
    };

    expect(req.session).toEqual(expectedSession);
    expect(res.redirect).toBeCalledTimes(0);
    expect(next).toBeCalledTimes(1);
  });

  test("should set state to IN_DETAILS_REVIEW when nhs number exists", () => {
    const req = {
      session: {
        journeyName: journey.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "know-your-nhs-number": "yes",
          "nhs-number": "1111111111",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };

    behaviourForGet(config, journey)(req, res, next);

    const expectedSession = {
      ...req.session,
      ...buildSessionForJourney({
        journeyName: journey.name,
        state: IN_DETAILS_REVIEW,
        nextAllowedPath: "/check-your-details",
      }),
    };

    expect(req.session).toEqual(expectedSession);
    expect(res.redirect).toBeCalledTimes(0);
    expect(next).toBeCalledTimes(1);
  });

  test("should redirect to service start page when missing non nhs number required fields and no nhs number", () => {
    const applicant = {
      postcode: "PL71RF",
      sanitisedPostcode: "PL7 1RF",
      "first-name": "Jane",
      "last-name": "Doe",
    };
    const req = { session: { applicant, journeyName: MAINFLOW.name } };

    behaviourForGet(config, journey)(req, res, next);

    expect(res.redirect).toBeCalledTimes(1);
    expect(res.redirect).toBeCalledWith(contextPath(START, MAINFLOW.name));
    expect(next).toBeCalledTimes(0);
  });
});

describe("behaviourForPost", () => {
  describe("nhs number flow", () => {
    test(`should redirect to ${getSecurityCode.path} on successful PDS request`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: "2000-01-01",
          email: "test@email.com",
          phoneNumber: "1234556789",
          addressLines: [
            "Address Line 1",
            "Address Line 2",
            "Address Line 3",
            "Address Line 4",
            "Address Line 5",
          ],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };

      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        email: "test@email.com",
        "phone-number": "1234556789",
        "address-line-1": "Address Line 1",
        "address-line-2": "Address Line 2",
        "address-line-3": "Address Line 3",
        "address-line-4": "Address Line 4",
        "address-line-5": "Address Line 5",
        postcode: "B3 2DX",
        "nhs-number": "1111111111",
        "pds-nhs-number": "1111111111",
        "year-of-loss": 2020,
        noContactDetailsnhsNumberPdsRequestMade: true,
        olderThanAgeRequirementPds: true,
        notMatched: false,
        nhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${getSecurityCode.path} on successful PDS request (superseded NHS-number)`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: "2000-01-01",
          email: "test@email.com",
          phoneNumber: "1234556789",
          addressLines: [
            "Address Line 1",
            "Address Line 2",
            "Address Line 3",
            "Address Line 4",
            "Address Line 5",
          ],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "9999999999",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        email: "test@email.com",
        "phone-number": "1234556789",
        "address-line-1": "Address Line 1",
        "address-line-2": "Address Line 2",
        "address-line-3": "Address Line 3",
        "address-line-4": "Address Line 4",
        "address-line-5": "Address Line 5",
        postcode: "B3 2DX",
        "nhs-number": "1111111111",
        "pds-nhs-number": "9999999999",
        "year-of-loss": 2020,
        noContactDetailsnhsNumberPdsRequestMade: true,
        olderThanAgeRequirementPds: true,
        notMatched: false,
        nhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${getSecurityCode.path} on successful PDS request with phone number that has a space`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: "2000-01-01",
          email: null,
          phoneNumber: "07491 282828",
          addressLines: [
            "Address Line 1",
            "Address Line 2",
            "Address Line 3",
            "Address Line 4",
            "Address Line 5",
          ],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };

      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        email: null,
        "phone-number": "07491 282828",
        "address-line-1": "Address Line 1",
        "address-line-2": "Address Line 2",
        "address-line-3": "Address Line 3",
        "address-line-4": "Address Line 4",
        "address-line-5": "Address Line 5",
        postcode: "B3 2DX",
        "nhs-number": "1111111111",
        "pds-nhs-number": "1111111111",
        "year-of-loss": 2020,
        noContactDetailsnhsNumberPdsRequestMade: true,
        olderThanAgeRequirementPds: true,
        notMatched: false,
        nhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${getSecurityCode.path} on successful PDS request with phone number that has an international number`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: "2000-01-01",
          email: null,
          phoneNumber: "0041446681800",
          addressLines: [
            "Address Line 1",
            "Address Line 2",
            "Address Line 3",
            "Address Line 4",
            "Address Line 5",
          ],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };

      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        email: null,
        "phone-number": "0041446681800",
        "address-line-1": "Address Line 1",
        "address-line-2": "Address Line 2",
        "address-line-3": "Address Line 3",
        "address-line-4": "Address Line 4",
        "address-line-5": "Address Line 5",
        postcode: "B3 2DX",
        "nhs-number": "1111111111",
        "pds-nhs-number": "1111111111",
        "year-of-loss": 2020,
        noContactDetailsnhsNumberPdsRequestMade: true,
        olderThanAgeRequirementPds: true,
        notMatched: false,
        nhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${getSecurityCode.path} on successful PDS request with phone number that is mobile formatted`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: "2000-01-01",
          email: null,
          phoneNumber: "+442828282828",
          addressLines: [
            "Address Line 1",
            "Address Line 2",
            "Address Line 3",
            "Address Line 4",
            "Address Line 5",
          ],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };

      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        email: null,
        "phone-number": "+442828282828",
        "address-line-1": "Address Line 1",
        "address-line-2": "Address Line 2",
        "address-line-3": "Address Line 3",
        "address-line-4": "Address Line 4",
        "address-line-5": "Address Line 5",
        postcode: "B3 2DX",
        "nhs-number": "1111111111",
        "pds-nhs-number": "1111111111",
        "year-of-loss": 2020,
        noContactDetailsnhsNumberPdsRequestMade: true,
        olderThanAgeRequirementPds: true,
        notMatched: false,
        nhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${getSecurityCode.path} on successful PDS request with phone number that is a landline number`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: "2000-01-01",
          email: null,
          phoneNumber: "02828282828",
          addressLines: [
            "Address Line 1",
            "Address Line 2",
            "Address Line 3",
            "Address Line 4",
            "Address Line 5",
          ],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };

      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        email: null,
        "phone-number": "02828282828",
        "address-line-1": "Address Line 1",
        "address-line-2": "Address Line 2",
        "address-line-3": "Address Line 3",
        "address-line-4": "Address Line 4",
        "address-line-5": "Address Line 5",
        postcode: "B3 2DX",
        "nhs-number": "1111111111",
        "pds-nhs-number": "1111111111",
        "year-of-loss": 2020,
        noContactDetailsnhsNumberPdsRequestMade: true,
        olderThanAgeRequirementPds: true,
        notMatched: false,
        nhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${notOldEnoughPds.path} on valid response from PDS request with DoB < 16 years old`, async () => {
      const journey = { name: MAINFLOW.name };
      // now - 15 years
      const dob = new Date();
      dob.setFullYear(dob.getFullYear() - 15);
      const dobStr = dob.toISOString().slice(0, 10);
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: dobStr,
          email: "test@email.com",
          phoneNumber: "1234556789",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": new Date().getFullYear(),
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "nhs-number": "1111111111",
        "year-of-loss": new Date().getFullYear(),
        noContactDetailsnhsNumberPdsRequestMade: true,
        olderThanAgeRequirementPds: false,
        nhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        firstParentContextPath(notOldEnoughPds.path)
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${notMatched.path} on valid response from PDS request with no DoB returned`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: null,
          email: "test@email.com",
          phoneNumber: "1234556789",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "nhs-number": "1111111111",
        "year-of-loss": 2020,
        noContactDetailsnhsNumberPdsRequestMade: true,
        notMatched: true,
        nhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + notMatched.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${noFoundAddress.path} on valid response from PDS request with no postcode returned`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: "2000-01-01",
          email: "test@email.com",
          phoneNumber: "1234556789",
          addressLines: ["Address Line 1"],
          postcode: null,
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "nhs-number": "1111111111",
        noContactDetailsnhsNumberPdsRequestMade: true,
        addressNotFound: true,
        "year-of-loss": 2020,
        nhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + noFoundAddress.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${noFoundAddress.path} on valid response from PDS request with insufficient number of address lines`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: "2000-01-01",
          email: "test@email.com",
          phoneNumber: "1234556789",
          addressLines: [],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "nhs-number": "1111111111",
        noContactDetailsnhsNumberPdsRequestMade: true,
        addressNotFound: true,
        "year-of-loss": 2020,
        nhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + noFoundAddress.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${noFoundAddress.path} on valid response from PDS request with no address`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: "2000-01-01",
          email: "test@email.com",
          phoneNumber: "1234556789",
          addressLines: null,
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "nhs-number": "1111111111",
        noContactDetailsnhsNumberPdsRequestMade: true,
        addressNotFound: true,
        "year-of-loss": 2020,
        nhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + noFoundAddress.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${notMatched.path} on valid response from PDS request with no active name returned`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: null,
          lastName: null,
          dateOfBirth: "2000-01-01",
          email: "test@email.com",
          phoneNumber: null,
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "nhs-number": "1111111111",
        noContactDetailsnhsNumberPdsRequestMade: true,
        notMatched: true,
        "year-of-loss": 2020,
        nhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + notMatched.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${noContactDetails.path} on valid response from PDS request with no contact method`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: "2000-01-01",
          email: null,
          phoneNumber: null,
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "nhs-number": "1111111111",
        noContactDetails: true,
        noContactDetailsnhsNumberPdsRequestMade: true,
        "year-of-loss": 2020,
        nhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + noContactDetails.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${noContactDetails.path} on valid response from PDS request with a phone number which is too long`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: "2000-01-01",
          email: null,
          phoneNumber: "074912960482",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "nhs-number": "1111111111",
        noContactDetails: true,
        noContactDetailsnhsNumberPdsRequestMade: true,
        "year-of-loss": 2020,
        nhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + noContactDetails.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${noContactDetails.path} on valid response from PDS request with a phone number which is too short`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: "2000-01-01",
          email: null,
          phoneNumber: "0742829420",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "nhs-number": "1111111111",
        noContactDetails: true,
        noContactDetailsnhsNumberPdsRequestMade: true,
        "year-of-loss": 2020,
        nhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + noContactDetails.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${noContactDetails.path} on valid response from PDS request with a phone number which is not a GB format`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: "2000-01-01",
          email: null,
          phoneNumber: "0109758351",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "nhs-number": "1111111111",
        noContactDetails: true,
        noContactDetailsnhsNumberPdsRequestMade: true,
        "year-of-loss": 2020,
        nhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + noContactDetails.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${noContactDetails.path} on valid response from PDS request with a phone number which is an international number that is too long`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: "2000-01-01",
          email: null,
          phoneNumber: "00414466818001",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "nhs-number": "1111111111",
        noContactDetails: true,
        noContactDetailsnhsNumberPdsRequestMade: true,
        "year-of-loss": 2020,
        nhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + noContactDetails.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${noContactDetails.path} on valid response from PDS request with a phone number which is an international number that is too short`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: "2000-01-01",
          email: null,
          phoneNumber: "004144668180",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "nhs-number": "1111111111",
        noContactDetails: true,
        noContactDetailsnhsNumberPdsRequestMade: true,
        "year-of-loss": 2020,
        nhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + noContactDetails.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${noContactDetails.path} on valid response from PDS request with a phone number which returns with a letter`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: "2000-01-01",
          email: null,
          phoneNumber: "0749028282a",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "nhs-number": "1111111111",
        noContactDetails: true,
        noContactDetailsnhsNumberPdsRequestMade: true,
        "year-of-loss": 2020,
        nhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + noContactDetails.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${noContactDetails.path} on valid response from PDS request with a phone number which returns with a symbol`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: "2000-01-01",
          email: null,
          phoneNumber: "+4490282828",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "nhs-number": "1111111111",
        noContactDetails: true,
        noContactDetailsnhsNumberPdsRequestMade: true,
        "year-of-loss": 2020,
        nhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + noContactDetails.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${notMatched.path} on valid response from PDS request with a deceased date time`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: "2000-01-01",
          email: null,
          phoneNumber: "07491282828",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
          hasDeceasedDateTime: true,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "nhs-number": "1111111111",
        "year-of-loss": 2020,
        notMatched: true,
        nhsNumberPdsRequestMade: true,
        noContactDetailsnhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + notMatched.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${notMatched.path} on valid response from PDS request with dob greater than 120 years returned`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: "1900-01-01",
          email: null,
          phoneNumber: "07491282828",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: START,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };

      await behaviourForPost(config, journey)(req, res);

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + notMatched.path
      );
    });

    test(`should redirect to ${notMatched.path} on valid response from PDS request with dob in future returned`, async () => {
      const journey = { name: MAINFLOW.name };
      const futureDate = new Date();
      futureDate.setFullYear(futureDate.getFullYear() + 1);
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: futureDate,
          email: null,
          phoneNumber: "07491282828",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: START,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + notMatched.path
      );
    });

    test(`should redirect to ${notMatched.path} on valid response from PDS request with same nhs number as first parent`, async () => {
      const journey = { name: SECOND_PARENT_FLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: "2000-01-01",
          email: null,
          phoneNumber: "07491282828",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: SECOND_PARENT_FLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: START,
            nextAllowedPath: undefined,
          }),
          [SECOND_PARENT_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
            "fp-reference-number": "1111111111",
            "fp-reference-type": 1,
          },
        },
      };

      await behaviourForPost(config, journey)(req, res);

      expect(res.redirect).toBeCalledWith(
        secondParentContextPath(notMatched.path)
      );
    });

    test(`should redirect to ${notMatched.path} on valid response from PDS request with dob not real returned`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: "2020-13-30",
          email: null,
          phoneNumber: "07491282828",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: START,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };

      await behaviourForPost(config, journey)(req, res);

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + notMatched.path
      );
    });

    test(`should redirect to ${notMatched.path} on valid response from PDS request with first name too long`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "J".repeat(FIRST_NAME_MAX_LENGTH + 1),
          lastName: "Dough",
          dateOfBirth: "2000-10-30",
          email: null,
          phoneNumber: "07491282828",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: START,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + notMatched.path
      );
    });

    test(`should redirect to ${notMatched.path} on valid response from PDS request with last name too long`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "d",
          lastName: "J".repeat(LAST_NAME_MAX_LENGTH + 1),
          dateOfBirth: "2000-10-30",
          email: null,
          phoneNumber: "07491282828",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: START,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };

      await behaviourForPost(config, journey)(req, res);

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + notMatched.path
      );
    });

    test(`should redirect to ${notMatched.path} on valid response from PDS request with fname invalid regex`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "<script>alert('hello')</script>",
          lastName: "Dough",
          dateOfBirth: "2000-10-30",
          email: null,
          phoneNumber: "07491282828",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: START,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };

      await behaviourForPost(config, journey)(req, res);

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + notMatched.path
      );
    });

    test(`should redirect to ${notMatched.path} on valid response from PDS request with lname invalid regex`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "<script>alert('hello')</script>",
          dateOfBirth: "2000-10-30",
          email: null,
          phoneNumber: "07491282828",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: START,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };

      await behaviourForPost(config, journey)(req, res);

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + notMatched.path
      );
    });

    test(`should redirect to ${notMatched.path} on valid response from PDS request with postcode invalid regex`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: "2000-10-30",
          email: null,
          phoneNumber: "123456789",
          addressLines: ["Address Line 1"],
          postcode: "<script>alert('hello')</script>",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: START,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + notMatched.path
      );
    });

    test(`should redirect to ${contextPath(
      "/problem-with-the-service",
      MAINFLOW.name
    )} on error response from PDS request`, async () => {
      const pdsError = {
        response: {
          status: 401,
          message: "401 Unauthorised",
        },
      };
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => Promise.reject(pdsError));

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "nhs-number": "1111111111",
        "year-of-loss": 2020,
        nhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        contextPath("/problem-with-the-service", journey.name)
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${notMatched.path} on 400 response from PDS request`, async () => {
      const pdsError = {
        response: {
          status: 400,
          message: "400 Bad Request",
        },
      };
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => Promise.reject(pdsError));

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "nhs-number": "1111111111",
        "year-of-loss": 2020,
        notMatched: true,
        nhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        contextPath(notMatched.path, MAINFLOW.name)
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to /cannot-request-try-again on 408 response from PDS request`, async () => {
      const pdsError = {
        response: {
          status: 408,
          message: "408 Request timed out",
        },
      };
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => Promise.reject(pdsError));

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "nhs-number": "1111111111",
        "year-of-loss": 2020,
        nhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        contextPath("/cannot-request-try-again", journey.name)
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to /cannot-request-try-again on 429 response from PDS request`, async () => {
      const pdsError = {
        response: {
          status: 429,
          message: "429 Too many requests",
        },
      };
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => Promise.reject(pdsError));

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "nhs-number": "1111111111",
        "year-of-loss": 2020,
        nhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        contextPath("/cannot-request-try-again", journey.name)
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${notMatched.path} when no NHS-number returned from PDS`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: "2000-01-01",
          email: "test@email.com",
          phoneNumber: "1234556789",
          addressLines: [
            "Address Line 1",
            "Address Line 2",
            "Address Line 3",
            "Address Line 4",
            "Address Line 5",
          ],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: null,
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "nhs-number": "1111111111",
        notMatched: true,
        noContactDetailsnhsNumberPdsRequestMade: true,
        "year-of-loss": 2020,
        nhsNumberPdsRequestMade: true,
      };

      expect(res.redirect).toBeCalledWith(
        contextPath(notMatched.path, MAINFLOW.name)
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });
  });

  describe("non-nhs number flow", () => {
    test(`should redirect to ${getSecurityCode.path} on successful PDS search request`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsSearchResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          email: "test@email.com",
          phoneNumber: "1234556789",
          addressLines: [
            "Address Line 1",
            "Address Line 2",
            "Address Line 3",
            "Address Line 4",
            "Address Line 5",
          ],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
          dateOfBirth: "2000-01-01",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "first-name": "Jane",
            "last-name": "Dough",
            "date-of-birth": "2000-01-01",
            "date-of-birth-year": "2000",
            "date-of-birth-month": "1",
            "date-of-birth-day": "1",
            postcode: "B3 2DX",
            sanitisedPostcode: "B3 2DX",
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        email: "test@email.com",
        "phone-number": "1234556789",
        "address-line-1": "Address Line 1",
        "address-line-2": "Address Line 2",
        "address-line-3": "Address Line 3",
        "address-line-4": "Address Line 4",
        "address-line-5": "Address Line 5",
        postcode: "B3 2DX",
        sanitisedPostcode: "B3 2DX",
        "pds-nhs-number": "1111111111",
        notMatched: false,
        olderThanAgeRequirement: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${getSecurityCode.path} on successful PDS search request with a phone number that has a space`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsSearchResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          email: null,
          phoneNumber: "07491 282828",
          addressLines: [
            "Address Line 1",
            "Address Line 2",
            "Address Line 3",
            "Address Line 4",
            "Address Line 5",
          ],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
          dateOfBirth: "2000-01-01",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "first-name": "Jane",
            "last-name": "Dough",
            "date-of-birth": "2000-01-01",
            "date-of-birth-year": "2000",
            "date-of-birth-month": "1",
            "date-of-birth-day": "1",
            postcode: "B3 2DX",
            sanitisedPostcode: "B3 2DX",
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        email: null,
        "phone-number": "07491 282828",
        "address-line-1": "Address Line 1",
        "address-line-2": "Address Line 2",
        "address-line-3": "Address Line 3",
        "address-line-4": "Address Line 4",
        "address-line-5": "Address Line 5",
        postcode: "B3 2DX",
        sanitisedPostcode: "B3 2DX",
        "pds-nhs-number": "1111111111",
        notMatched: false,
        olderThanAgeRequirement: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${getSecurityCode.path} on successful PDS search request with a phone number that has an international number`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsSearchResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          email: null,
          phoneNumber: "0041446681800",
          addressLines: [
            "Address Line 1",
            "Address Line 2",
            "Address Line 3",
            "Address Line 4",
            "Address Line 5",
          ],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
          dateOfBirth: "2000-01-01",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "first-name": "Jane",
            "last-name": "Dough",
            "date-of-birth": "2000-01-01",
            "date-of-birth-year": "2000",
            "date-of-birth-month": "1",
            "date-of-birth-day": "1",
            postcode: "B3 2DX",
            sanitisedPostcode: "B3 2DX",
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        email: null,
        "phone-number": "0041446681800",
        "address-line-1": "Address Line 1",
        "address-line-2": "Address Line 2",
        "address-line-3": "Address Line 3",
        "address-line-4": "Address Line 4",
        "address-line-5": "Address Line 5",
        postcode: "B3 2DX",
        sanitisedPostcode: "B3 2DX",
        "pds-nhs-number": "1111111111",
        notMatched: false,
        olderThanAgeRequirement: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${getSecurityCode.path} on successful PDS search request with a phone number that is mobile formatted`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsSearchResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          email: null,
          phoneNumber: "+442828282828",
          addressLines: [
            "Address Line 1",
            "Address Line 2",
            "Address Line 3",
            "Address Line 4",
            "Address Line 5",
          ],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
          dateOfBirth: "2000-01-01",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "first-name": "Jane",
            "last-name": "Dough",
            "date-of-birth": "2000-01-01",
            "date-of-birth-year": "2000",
            "date-of-birth-month": "1",
            "date-of-birth-day": "1",
            postcode: "B3 2DX",
            sanitisedPostcode: "B3 2DX",
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        email: null,
        "phone-number": "+442828282828",
        "address-line-1": "Address Line 1",
        "address-line-2": "Address Line 2",
        "address-line-3": "Address Line 3",
        "address-line-4": "Address Line 4",
        "address-line-5": "Address Line 5",
        postcode: "B3 2DX",
        sanitisedPostcode: "B3 2DX",
        "pds-nhs-number": "1111111111",
        notMatched: false,
        olderThanAgeRequirement: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${getSecurityCode.path} on successful PDS search request with a phone number that is a landline number`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsSearchResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          email: null,
          phoneNumber: "02828282828",
          addressLines: [
            "Address Line 1",
            "Address Line 2",
            "Address Line 3",
            "Address Line 4",
            "Address Line 5",
          ],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
          dateOfBirth: "2000-01-01",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "first-name": "Jane",
            "last-name": "Dough",
            "date-of-birth": "2000-01-01",
            "date-of-birth-year": "2000",
            "date-of-birth-month": "1",
            "date-of-birth-day": "1",
            postcode: "B3 2DX",
            sanitisedPostcode: "B3 2DX",
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        email: null,
        "phone-number": "02828282828",
        "address-line-1": "Address Line 1",
        "address-line-2": "Address Line 2",
        "address-line-3": "Address Line 3",
        "address-line-4": "Address Line 4",
        "address-line-5": "Address Line 5",
        postcode: "B3 2DX",
        sanitisedPostcode: "B3 2DX",
        "pds-nhs-number": "1111111111",
        notMatched: false,
        olderThanAgeRequirement: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${notMatched.path} on valid response from PDS request with same nhs number as first parent`, async () => {
      const journey = { name: SECOND_PARENT_FLOW.name };
      pdsSearchResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: "2000-01-01",
          email: null,
          phoneNumber: "123456789",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: SECOND_PARENT_FLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: START,
            nextAllowedPath: undefined,
          }),
          [SECOND_PARENT_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
            "fp-reference-number": "1111111111",
            "fp-reference-type": 1,
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);

      expect(res.redirect).toBeCalledWith(
        secondParentContextPath(notMatched.path)
      );
    });

    test(`should redirect to ${getSecurityCode.path} on successful PDS search request (ignore casing and spaces for postcodes)`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsSearchResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          email: "test@email.com",
          phoneNumber: "1234556789",
          addressLines: [
            "Address Line 1",
            "Address Line 2",
            "Address Line 3",
            "Address Line 4",
            "Address Line 5",
          ],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
          dateOfBirth: "2000-01-01",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "first-name": "Jane",
            "last-name": "Dough",
            "date-of-birth": "2000-01-01",
            "date-of-birth-year": "2000",
            "date-of-birth-month": "1",
            "date-of-birth-day": "1",
            postcode: "b3 2dx",
            sanitisedPostcode: "B3 2DX",
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        email: "test@email.com",
        "phone-number": "1234556789",
        "address-line-1": "Address Line 1",
        "address-line-2": "Address Line 2",
        "address-line-3": "Address Line 3",
        "address-line-4": "Address Line 4",
        "address-line-5": "Address Line 5",
        postcode: "B3 2DX",
        sanitisedPostcode: "B3 2DX",
        "pds-nhs-number": "1111111111",
        notMatched: false,
        olderThanAgeRequirement: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${noFoundAddress.path} when postcode from PDS does not match user input`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsSearchResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          email: null,
          phoneNumber: "1234556789",
          addressLines: ["Address Line 1"],
          postcode: "B1 0AA",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "first-name": "Jane",
            "last-name": "Dough",
            "date-of-birth": "2000-01-01",
            "date-of-birth-year": "2000",
            "date-of-birth-month": "1",
            "date-of-birth-day": "1",
            postcode: "B3 2DX",
            sanitisedPostcode: "B3 2DX",
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        postcode: "B3 2DX",
        sanitisedPostcode: "B3 2DX",
        addressNotFound: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + noFoundAddress.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${notMatched.path} when name from PDS does not match user input`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsSearchResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: null,
          lastName: null,
          email: "test@email.com",
          phoneNumber: "1234556789",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "first-name": "Jane",
            "last-name": "Dough",
            "date-of-birth": "2000-01-01",
            "date-of-birth-year": "2000",
            "date-of-birth-month": "1",
            "date-of-birth-day": "1",
            postcode: "B3 2DX",
            sanitisedPostcode: "B3 2DX",
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        postcode: "B3 2DX",
        sanitisedPostcode: "B3 2DX",
        notMatched: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + notMatched.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${noFoundAddress.path} on successful PDS search request with insufficient address lines`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsSearchResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          email: null,
          phoneNumber: "123456789",
          addressLines: [],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "first-name": "Jane",
            "last-name": "Dough",
            "date-of-birth": "2000-01-01",
            "date-of-birth-year": "2000",
            "date-of-birth-month": "1",
            "date-of-birth-day": "1",
            postcode: "B3 2DX",
            sanitisedPostcode: "B3 2DX",
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        postcode: "B3 2DX",
        sanitisedPostcode: "B3 2DX",
        addressNotFound: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + noFoundAddress.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${noFoundAddress.path} on successful PDS search request with no address`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsSearchResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          email: null,
          phoneNumber: "123456789",
          addressLines: null,
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "first-name": "Jane",
            "last-name": "Dough",
            "date-of-birth": "2000-01-01",
            "date-of-birth-year": "2000",
            "date-of-birth-month": "1",
            "date-of-birth-day": "1",
            postcode: "B3 2DX",
            sanitisedPostcode: "B3 2DX",
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        postcode: "B3 2DX",
        sanitisedPostcode: "B3 2DX",
        addressNotFound: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + noFoundAddress.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${noContactDetails.path} on successful PDS search request with no contact method`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsSearchResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          email: null,
          phoneNumber: null,
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "first-name": "Jane",
            "last-name": "Dough",
            "date-of-birth": "2000-01-01",
            "date-of-birth-year": "2000",
            "date-of-birth-month": "1",
            "date-of-birth-day": "1",
            postcode: "B3 2DX",
            sanitisedPostcode: "B3 2DX",
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        postcode: "B3 2DX",
        sanitisedPostcode: "B3 2DX",
        noContactDetails: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + noContactDetails.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${noContactDetails.path} on successful PDS search request with a phone number which is too long`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsSearchResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          email: null,
          phoneNumber: "074902828282",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "first-name": "Jane",
            "last-name": "Dough",
            "date-of-birth": "2000-01-01",
            "date-of-birth-year": "2000",
            "date-of-birth-month": "1",
            "date-of-birth-day": "1",
            postcode: "B3 2DX",
            sanitisedPostcode: "B3 2DX",
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        postcode: "B3 2DX",
        sanitisedPostcode: "B3 2DX",
        noContactDetails: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + noContactDetails.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${noContactDetails.path} on successful PDS search request with a phone number which is too short`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsSearchResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          email: null,
          phoneNumber: "0749028282",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "first-name": "Jane",
            "last-name": "Dough",
            "date-of-birth": "2000-01-01",
            "date-of-birth-year": "2000",
            "date-of-birth-month": "1",
            "date-of-birth-day": "1",
            postcode: "B3 2DX",
            sanitisedPostcode: "B3 2DX",
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        postcode: "B3 2DX",
        sanitisedPostcode: "B3 2DX",
        noContactDetails: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + noContactDetails.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${noContactDetails.path} on successful PDS search request with a phone number which is not a GB format`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsSearchResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          email: null,
          phoneNumber: "0109758351",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "first-name": "Jane",
            "last-name": "Dough",
            "date-of-birth": "2000-01-01",
            "date-of-birth-year": "2000",
            "date-of-birth-month": "1",
            "date-of-birth-day": "1",
            postcode: "B3 2DX",
            sanitisedPostcode: "B3 2DX",
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        postcode: "B3 2DX",
        sanitisedPostcode: "B3 2DX",
        noContactDetails: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + noContactDetails.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${noContactDetails.path} on successful PDS search request with a phone number which is an international number that is too long`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsSearchResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          email: null,
          phoneNumber: "00414466818001",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "first-name": "Jane",
            "last-name": "Dough",
            "date-of-birth": "2000-01-01",
            "date-of-birth-year": "2000",
            "date-of-birth-month": "1",
            "date-of-birth-day": "1",
            postcode: "B3 2DX",
            sanitisedPostcode: "B3 2DX",
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        postcode: "B3 2DX",
        sanitisedPostcode: "B3 2DX",
        noContactDetails: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + noContactDetails.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${noContactDetails.path} on successful PDS search request with a phone number which is an international number that is too short`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsSearchResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          email: null,
          phoneNumber: "004144668180",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "first-name": "Jane",
            "last-name": "Dough",
            "date-of-birth": "2000-01-01",
            "date-of-birth-year": "2000",
            "date-of-birth-month": "1",
            "date-of-birth-day": "1",
            postcode: "B3 2DX",
            sanitisedPostcode: "B3 2DX",
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        postcode: "B3 2DX",
        sanitisedPostcode: "B3 2DX",
        noContactDetails: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + noContactDetails.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${noContactDetails.path} on successful PDS search request with a phone number which returns with a letter`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsSearchResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          email: null,
          phoneNumber: "0749028282a",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "first-name": "Jane",
            "last-name": "Dough",
            "date-of-birth": "2000-01-01",
            "date-of-birth-year": "2000",
            "date-of-birth-month": "1",
            "date-of-birth-day": "1",
            postcode: "B3 2DX",
            sanitisedPostcode: "B3 2DX",
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        postcode: "B3 2DX",
        sanitisedPostcode: "B3 2DX",
        noContactDetails: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + noContactDetails.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${noContactDetails.path} on successful PDS search request with a phone number which returns with a symbol`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsSearchResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          email: null,
          phoneNumber: "+4490282828",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "first-name": "Jane",
            "last-name": "Dough",
            "date-of-birth": "2000-01-01",
            "date-of-birth-year": "2000",
            "date-of-birth-month": "1",
            "date-of-birth-day": "1",
            postcode: "B3 2DX",
            sanitisedPostcode: "B3 2DX",
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        postcode: "B3 2DX",
        sanitisedPostcode: "B3 2DX",
        noContactDetails: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + noContactDetails.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${notMatched.path} on successful PDS search request with deceased date time populated`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsSearchResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          email: "test@email.com",
          phoneNumber: null,
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
          hasDeceasedDateTime: true,
          nhsNumber: "1111111111",
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "first-name": "Jane",
            "last-name": "Dough",
            "date-of-birth": "2000-01-01",
            "date-of-birth-year": "2000",
            "date-of-birth-month": "1",
            "date-of-birth-day": "1",
            postcode: "B3 2DX",
            sanitisedPostcode: "B3 2DX",
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        postcode: "B3 2DX",
        sanitisedPostcode: "B3 2DX",
        notMatched: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + notMatched.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${contextPath(
      "/problem-with-the-service",
      MAINFLOW.name
    )} on error response from PDS search request`, async () => {
      const pdsError = {
        response: {
          status: 401,
          message: "401 Unauthorised",
        },
      };
      const journey = { name: MAINFLOW.name };
      pdsSearchResult.mockImplementation(() => Promise.reject(pdsError));

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "first-name": "Jane",
            "last-name": "Dough",
            "date-of-birth": "2000-01-01",
            "date-of-birth-year": "2000",
            "date-of-birth-month": "1",
            "date-of-birth-day": "1",
            postcode: "B3 2DX",
            sanitisedPostcode: "B3 2DX",
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        postcode: "B3 2DX",
        sanitisedPostcode: "B3 2DX",
      };

      expect(res.redirect).toBeCalledWith(
        contextPath("/problem-with-the-service", MAINFLOW.name)
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${notMatched.path} when no NHS-number returned from PDS`, async () => {
      const journey = { name: MAINFLOW.name };
      pdsSearchResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "James",
          lastName: "Simpson",
          email: "test@email.com",
          phoneNumber: "1234556789",
          addressLines: ["Address Line 1"],
          postcode: "B3 2DX",
          hasDeceasedDateTime: false,
          nhsNumber: null,
        });
      });

      const req = {
        session: {
          journeyName: MAINFLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "first-name": "James",
            "last-name": "Simpson",
            "date-of-birth": "2000-01-01",
            "date-of-birth-year": "2000",
            "date-of-birth-month": "1",
            "date-of-birth-day": "1",
            postcode: "B3 2DX",
            sanitisedPostcode: "B3 2DX",
          },
        },
      };
      await behaviourForPost(config, journey)(req, res);
      const expectedBlcSession = {
        "first-name": "James",
        "last-name": "Simpson",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        postcode: "B3 2DX",
        sanitisedPostcode: "B3 2DX",
        notMatched: true,
      };

      expect(res.redirect).toBeCalledWith(
        process.env.CONTEXT_PATH + notMatched.path
      );
      expect(req.session[MAINFLOW_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.CONTEXT_PATH + getSecurityCode.path
      );
    });
  });

  describe("ignore address validation for second parent", () => {
    const journey = { name: SECOND_PARENT_FLOW.name };
    const expectedBlcSession = {
      "first-name": "Jane",
      "last-name": "Dough",
      "date-of-birth": "2000-01-01",
      "date-of-birth-year": "2000",
      "date-of-birth-month": "1",
      "date-of-birth-day": "1",
      postcode: "B3 2DX",
      sanitisedPostcode: "B3 2DX",
      notMatched: false,
      email: null,
      "phone-number": "07491282828",
      "pds-nhs-number": "1111111111",
      olderThanAgeRequirement: true,
      "sp-date-of-birth": "2000-01-01",
      spDobNotMatched: false,
    };

    describe("non nhs number flow", () => {
      const req = {
        session: {
          journeyName: SECOND_PARENT_FLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [SECOND_PARENT_DATA_OBJECT_NAME]: {
            "first-name": "Jane",
            "last-name": "Dough",
            "date-of-birth": "2000-01-01",
            "date-of-birth-year": "2000",
            "date-of-birth-month": "1",
            "date-of-birth-day": "1",
            postcode: "B3 2DX",
            sanitisedPostcode: "B3 2DX",
            "sp-date-of-birth": "2000-01-01",
          },
        },
      };

      test(`should redirect to ${getSecurityCode.path} on successful PDS search request with no address`, async () => {
        pdsSearchResult.mockImplementation(() => {
          return Promise.resolve({
            firstName: "Jane",
            lastName: "Dough",
            email: null,
            phoneNumber: "07491282828",
            addressLines: null,
            postcode: "B3 2DX",
            hasDeceasedDateTime: false,
            nhsNumber: "1111111111",
            dateOfBirth: "2000-01-01",
          });
        });

        await behaviourForPost(config, journey)(req, res);

        expect(res.redirect).toBeCalledWith(
          process.env.SP_CONTEXT_PATH + getSecurityCode.path
        );
        expect(req.session[SECOND_PARENT_DATA_OBJECT_NAME]).toEqual(
          expectedBlcSession
        );
        expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
        expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
          process.env.SP_CONTEXT_PATH + getSecurityCode.path
        );
      });

      test(`should redirect to ${getSecurityCode.path} on successful PDS search request when postcodes don't match`, async () => {
        pdsSearchResult.mockImplementation(() => {
          return Promise.resolve({
            firstName: "Jane",
            lastName: "Dough",
            email: null,
            phoneNumber: "07491282828",
            addressLines: null,
            postcode: "B1 0AA",
            hasDeceasedDateTime: false,
            nhsNumber: "1111111111",
            dateOfBirth: "2000-01-01",
          });
        });

        await behaviourForPost(config, journey)(req, res);

        expect(res.redirect).toBeCalledWith(
          process.env.SP_CONTEXT_PATH + getSecurityCode.path
        );
        expect(req.session[SECOND_PARENT_DATA_OBJECT_NAME]).toEqual(
          expectedBlcSession
        );
        expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
        expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
          process.env.SP_CONTEXT_PATH + getSecurityCode.path
        );
      });
    });

    describe("nhs number flow", () => {
      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        email: "test@email.com",
        "phone-number": "1234556789",
        "nhs-number": "1111111111",
        "pds-nhs-number": "1111111111",
        "year-of-loss": 2020,
        noContactDetailsnhsNumberPdsRequestMade: true,
        olderThanAgeRequirementPds: true,
        notMatched: false,
        nhsNumberPdsRequestMade: true,
        "sp-date-of-birth": "2000-01-01",
        spDobNotMatched: false,
      };
      const req = {
        session: {
          journeyName: SECOND_PARENT_FLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [SECOND_PARENT_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
            "sp-date-of-birth": "2000-01-01",
          },
        },
      };

      test(`should redirect to ${getSecurityCode.path} on successful PDS lookup request with no address`, async () => {
        pdsLookupResult.mockImplementation(() => {
          return Promise.resolve({
            firstName: "Jane",
            lastName: "Dough",
            dateOfBirth: "2000-01-01",
            email: "test@email.com",
            phoneNumber: "1234556789",
            addressLines: null,
            postcode: "B3 2DX",
            hasDeceasedDateTime: false,
            nhsNumber: "1111111111",
          });
        });

        await behaviourForPost(config, journey)(req, res);

        expect(res.redirect).toBeCalledWith(
          process.env.SP_CONTEXT_PATH + getSecurityCode.path
        );
        expect(req.session[SECOND_PARENT_DATA_OBJECT_NAME]).toEqual(
          expectedBlcSession
        );
        expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
        expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
          process.env.SP_CONTEXT_PATH + getSecurityCode.path
        );
      });

      test(`should redirect to ${getSecurityCode.path} on successful PDS lookup request with no postcode`, async () => {
        pdsLookupResult.mockImplementation(() => {
          return Promise.resolve({
            firstName: "Jane",
            lastName: "Dough",
            dateOfBirth: "2000-01-01",
            email: "test@email.com",
            phoneNumber: "1234556789",
            addressLines: ["Test"],
            postcode: null,
            hasDeceasedDateTime: false,
            nhsNumber: "1111111111",
          });
        });

        await behaviourForPost(config, journey)(req, res);

        expect(res.redirect).toBeCalledWith(
          process.env.SP_CONTEXT_PATH + getSecurityCode.path
        );
        expect(req.session[SECOND_PARENT_DATA_OBJECT_NAME]).toEqual(
          expectedBlcSession
        );
        expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
        expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
          process.env.SP_CONTEXT_PATH + getSecurityCode.path
        );
      });

      test(`should redirect to ${getSecurityCode.path} on successful PDS lookup request with non UK postcode`, async () => {
        pdsLookupResult.mockImplementation(() => {
          return Promise.resolve({
            firstName: "Jane",
            lastName: "Dough",
            dateOfBirth: "2000-01-01",
            email: "test@email.com",
            phoneNumber: "1234556789",
            addressLines: ["Test"],
            postcode: "619",
            hasDeceasedDateTime: false,
            nhsNumber: "1111111111",
          });
        });

        await behaviourForPost(config, journey)(req, res);

        expect(res.redirect).toBeCalledWith(
          process.env.SP_CONTEXT_PATH + getSecurityCode.path
        );
        expect(req.session[SECOND_PARENT_DATA_OBJECT_NAME]).toEqual(
          expectedBlcSession
        );
        expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
        expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
          process.env.SP_CONTEXT_PATH + getSecurityCode.path
        );
      });
    });
  });

  describe("second parent dob validation", () => {
    const journey = { name: SECOND_PARENT_FLOW.name };

    test(`should redirect to ${spDobNotMatched} when input date of birth does not matched main applicant provided value`, async () => {
      const req = {
        session: {
          journeyName: SECOND_PARENT_FLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [SECOND_PARENT_DATA_OBJECT_NAME]: {
            "first-name": "Jane",
            "last-name": "Dough",
            "date-of-birth": "2000-01-01",
            "date-of-birth-year": "2000",
            "date-of-birth-month": "1",
            "date-of-birth-day": "1",
            postcode: "B3 2DX",
            sanitisedPostcode: "B3 2DX",
            email: null,
            "phone-number": "123456789",
            "pds-nhs-number": "1111111111",
            "sp-date-of-birth": "1999-12-12",
          },
        },
      };

      const expectedBlcSession = {
        "first-name": "Jane",
        "last-name": "Dough",
        "date-of-birth": "2000-01-01",
        "date-of-birth-year": "2000",
        "date-of-birth-month": "1",
        "date-of-birth-day": "1",
        postcode: "B3 2DX",
        sanitisedPostcode: "B3 2DX",
        email: null,
        "phone-number": "123456789",
        "pds-nhs-number": "1111111111",
        "sp-date-of-birth": "1999-12-12",
        spDobNotMatched: true,
      };

      await behaviourForPost(config, journey)(req, res);

      expect(res.redirect).toBeCalledWith(
        process.env.SP_CONTEXT_PATH + spDobNotMatched.path
      );
      expect(req.session[SECOND_PARENT_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.SP_CONTEXT_PATH + getSecurityCode.path
      );
    });

    test(`should redirect to ${spDobNotMatched.path} on successful PDS lookup request when dob from PDS does not match main applicant provided value`, async () => {
      const req = {
        session: {
          journeyName: SECOND_PARENT_FLOW.name,
          ...buildSessionForJourney({
            journeyName: journey.name,
            state: IN_DETAILS_REVIEW,
            nextAllowedPath: undefined,
          }),
          [SECOND_PARENT_DATA_OBJECT_NAME]: {
            "nhs-number": "1111111111",
            "year-of-loss": 2020,
            "sp-date-of-birth": "2000-01-01",
          },
        },
      };

      pdsLookupResult.mockImplementation(() => {
        return Promise.resolve({
          firstName: "Jane",
          lastName: "Dough",
          dateOfBirth: "1999-12-12",
          email: "test@email.com",
          phoneNumber: "1234556789",
          addressLines: ["Test"],
          postcode: "619",
          hasDeceasedDateTime: false,
          nhsNumber: "1111111111",
        });
      });

      const expectedBlcSession = {
        "nhs-number": "1111111111",
        nhsNumberPdsRequestMade: true,
        noContactDetailsnhsNumberPdsRequestMade: true,
        "sp-date-of-birth": "2000-01-01",
        "year-of-loss": 2020,
        spDobNotMatched: true,
      };

      await behaviourForPost(config, journey)(req, res);

      expect(res.redirect).toBeCalledWith(
        process.env.SP_CONTEXT_PATH + spDobNotMatched.path
      );
      expect(req.session[SECOND_PARENT_DATA_OBJECT_NAME]).toEqual(
        expectedBlcSession
      );
      expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
      expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
        process.env.SP_CONTEXT_PATH + getSecurityCode.path
      );
    });
  });
});
