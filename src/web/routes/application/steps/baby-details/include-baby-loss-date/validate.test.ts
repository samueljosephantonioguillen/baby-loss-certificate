import expect from "expect";

import { validate } from "./validate";
import { toDateString } from "../../common/format-date";
import { applyExpressValidation } from "../../common/test/apply-express-validation";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";

describe("validate 'include-baby-date-of-loss' field", () => {
  test("should return expected error when non digit day", async () => {
    const req = {
      body: {
        "include-baby-date-of-loss": "yes",
        "baby-date-of-loss-day": "a",
        "baby-date-of-loss-month": "5",
        "baby-date-of-loss-year": "2021",
      },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "year-of-loss": "2021",
          "date-of-birth": "1999-01-01",
        },
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const expectedError = {
      type: "field",
      value: "a",
      msg: "validation:dateOfLossNotRealDay",
      path: "baby-date-of-loss-day",
      location: "body",
    };

    const error = result.array()[0];

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });

  test("should return expected error when non digit month", async () => {
    const req = {
      body: {
        "include-baby-date-of-loss": "yes",
        "baby-date-of-loss-day": "5",
        "baby-date-of-loss-month": "a",
        "baby-date-of-loss-year": "2021",
      },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "year-of-loss": "2021",
          "date-of-birth": "1999-01-01",
        },
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const expectedError = {
      type: "field",
      value: "a",
      msg: "validation:dateOfLossNotRealMonth",
      path: "baby-date-of-loss-month",
      location: "body",
    };

    const error = result.array()[0];

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });

  test("should return expected error when non digit year", async () => {
    const req = {
      body: {
        "include-baby-date-of-loss": "yes",
        "baby-date-of-loss-day": "5",
        "baby-date-of-loss-month": "5",
        "baby-date-of-loss-year": "a",
      },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "year-of-loss": "2021",
          "date-of-birth": "1999-01-01",
        },
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const expectedError = {
      type: "field",
      value: "a",
      msg: "validation:dateOfLossNotRealYear",
      path: "baby-date-of-loss-year",
      location: "body",
    };

    const error = result.array()[0];

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });

  test("when empty error should return expected", async () => {
    const req = {
      body: {
        "baby-date-of-loss-day": "",
        "baby-date-of-loss-month": "",
        "baby-date-of-loss-year": "",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const expectedError = {
      type: "field",
      value: undefined,
      msg: "validation:selectInludeBabyDateOfLoss",
      path: "include-baby-date-of-loss",
      location: "body",
    };

    const error = result.array()[0];

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });

  test("when no selected should return no errors", async () => {
    const req = {
      body: {
        "include-baby-date-of-loss": "no",
        "baby-date-of-loss-day": "",
        "baby-date-of-loss-month": "",
        "baby-date-of-loss-year": "",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    expect(result.array().length).toBe(0);
  });

  test("when unknown selected should return no errors", async () => {
    const req = {
      body: {
        "include-baby-date-of-loss": "unknown",
        "baby-date-of-loss-day": "",
        "baby-date-of-loss-month": "",
        "baby-date-of-loss-year": "",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    expect(result.array().length).toBe(0);
  });
});

describe("validate 'baby-date-of-loss' field", () => {
  test("when empty baby date of loss provided should return error", async () => {
    const req = {
      body: {
        "include-baby-date-of-loss": "yes",
        "baby-date-of-loss-day": "",
        "baby-date-of-loss-month": "",
        "baby-date-of-loss-year": "",
      },
    };
    const expectedError = {
      type: "field",
      value: "",
      msg: "validation:missingDateOfLoss",
      path: "baby-date-of-loss",
      location: "body",
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });

  test("when baby-date-of-loss-year does not match year-of-loss should return error", async () => {
    const req = {
      body: {
        "include-baby-date-of-loss": "yes",
        "baby-date-of-loss-year": "2022",
        "baby-date-of-loss-day": "",
        "baby-date-of-loss-month": "",
        "baby-date-of-loss": "2022--",
      },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "year-of-loss": "2021",
          "date-of-birth": "1999-01-01",
        },
      },
    };
    const expectedError = {
      type: "field",
      value: "2022",
      msg: "validation:yearDoesNotMatch",
      path: "baby-date-of-loss-year",
      location: "body",
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });

  test("when invalid partial date of loss provided should return error", async () => {
    const req = {
      body: {
        "include-baby-date-of-loss": "yes",
        "baby-date-of-loss-day": "01",
        "baby-date-of-loss-month": "01",
        "baby-date-of-loss-year": "",
      },
    };
    const expectedError = {
      type: "field",
      value: "",
      msg: "validation:missingDateOfLossYear",
      path: "baby-date-of-loss-year",
      location: "body",
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });

  describe("full date date of loss", () => {
    test("should return no errors when valid full date", async () => {
      const req = {
        body: {
          "include-baby-date-of-loss": "yes",
          "baby-date-of-loss-day": "01",
          "baby-date-of-loss-month": "01",
          "baby-date-of-loss-year": "2021",
          "baby-date-of-loss": "2021-01-01",
        },
        session: {
          journeyName: MAINFLOW.name,
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "year-of-loss": "2021",
            "date-of-birth": "1999-01-01",
          },
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("Express validation failed");
      }

      expect(result.array()).toEqual([]);
      expect(result.array().length).toBe(0);
    });

    test("should return expected error when not a real date", async () => {
      const req = {
        body: {
          "include-baby-date-of-loss": "yes",
          "baby-date-of-loss-day": "01",
          "baby-date-of-loss-month": "99",
          "baby-date-of-loss-year": "2021",
          "baby-date-of-loss": "2021-99-01",
        },
        session: {
          journeyName: MAINFLOW.name,
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "year-of-loss": "2021",
            "date-of-birth": "1999-01-01",
          },
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("Express validation failed");
      }

      const error = result.array()[0];

      const expectedError = {
        type: "field",
        value: "2021-99-01",
        msg: "validation:dateOfLossNotReal",
        path: "baby-date-of-loss",
        location: "body",
      };

      expect(result.array().length).toBe(1);
      expect(error).toEqual(expectedError);
    });

    test("should return expected error when date in future", async () => {
      const today = new Date();
      const tomorrow = new Date(today);
      tomorrow.setDate(tomorrow.getDate() + 1);
      const tomorrowDate = tomorrow.getDate();
      const tomorrowMonth = tomorrow.getMonth() + 1;
      const tomorrowYear = tomorrow.getFullYear();

      const req = {
        body: {
          "include-baby-date-of-loss": "yes",
          "baby-date-of-loss-day": tomorrowDate,
          "baby-date-of-loss-month": tomorrowMonth,
          "baby-date-of-loss-year": tomorrowYear,
          "baby-date-of-loss": toDateString(
            tomorrowDate,
            tomorrowMonth,
            tomorrowYear
          ),
        },
        session: {
          journeyName: MAINFLOW.name,
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "year-of-loss": tomorrowYear,
            "date-of-birth": "1999-01-01",
          },
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("Express validation failed");
      }

      const error = result.array()[0];

      const expectedError = {
        type: "field",
        value: toDateString(tomorrowDate, tomorrowMonth, tomorrowYear),
        msg: "validation:dateOfLossInFuture",
        path: "baby-date-of-loss",
        location: "body",
      };

      expect(result.array().length).toBe(1);
      expect(error).toEqual(expectedError);
    });

    test("should return expected error when date before parent dob", async () => {
      const req = {
        body: {
          "include-baby-date-of-loss": "yes",
          "baby-date-of-loss-day": "01",
          "baby-date-of-loss-month": "01",
          "baby-date-of-loss-year": "1999",
        },
        session: {
          journeyName: MAINFLOW.name,
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "year-of-loss": "1999",
            "date-of-birth": "1999-01-02",
          },
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("Express validation failed");
      }

      const error = result.array()[0];

      const expectedError = {
        type: "field",
        value: "1999-01-01",
        msg: "validation:dateOfLossBeforeParentsDob",
        path: "baby-date-of-loss",
        location: "body",
      };

      expect(result.array().length).toBe(1);
      expect(error).toEqual(expectedError);
    });

    test("should return expected error when date is before eligible date", async () => {
      const req = {
        body: {
          "include-baby-date-of-loss": "yes",
          "baby-date-of-loss-day": "01",
          "baby-date-of-loss-month": "01",
          "baby-date-of-loss-year": "2018",
          "baby-date-of-loss": "2018-01-01",
        },
        session: {
          journeyName: MAINFLOW.name,
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "year-of-loss": "2018",
            "date-of-birth": "1999-01-01",
          },
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("Express validation failed");
      }

      const error = result.array()[0];

      const expectedError = {
        type: "field",
        value: "2018-01-01",
        msg: "validation:dateOfLossNotInRange",
        path: "baby-date-of-loss",
        location: "body",
      };

      expect(result.array().length).toBe(1);
      expect(error).toEqual(expectedError);
    });
  });

  describe("partial month year date of loss", () => {
    test("should return no errors when valid partial date", async () => {
      const req = {
        body: {
          "include-baby-date-of-loss": "yes",
          "baby-date-of-loss-day": "",
          "baby-date-of-loss-month": "01",
          "baby-date-of-loss-year": "2021",
          "baby-date-of-loss": "2021-01-",
        },
        session: {
          journeyName: MAINFLOW.name,
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "year-of-loss": "2021",
            "date-of-birth": "1999-01-01",
          },
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("Express validation failed");
      }

      expect(result.array().length).toBe(0);
    });

    test("should return expected error when not a real date", async () => {
      const req = {
        body: {
          "include-baby-date-of-loss": "yes",
          "baby-date-of-loss-day": "",
          "baby-date-of-loss-month": "13",
          "baby-date-of-loss-year": "2021",
          "baby-date-of-loss": "2021-13-",
        },
        session: {
          journeyName: MAINFLOW.name,
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "year-of-loss": "2021",
            "date-of-birth": "1999-01-01",
          },
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("Express validation failed");
      }

      const error = result.array()[0];

      const expectedError = {
        type: "field",
        value: "2021-13-",
        msg: "validation:dateOfLossNotReal",
        path: "baby-date-of-loss",
        location: "body",
      };

      expect(result.array().length).toBe(1);
      expect(error).toEqual(expectedError);
    });

    test("should return expected error when date in future", async () => {
      const today = new Date();
      const month = today.getMonth() + 1;
      const year = today.getFullYear() + 1;

      const req = {
        body: {
          "include-baby-date-of-loss": "yes",
          "baby-date-of-loss-day": "",
          "baby-date-of-loss-month": month,
          "baby-date-of-loss-year": year,
          "baby-date-of-loss": toDateString("", month, year),
        },
        session: {
          journeyName: MAINFLOW.name,
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "year-of-loss": year,
            "date-of-birth": "1999-01-01",
          },
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("Express validation failed");
      }

      const error = result.array()[0];

      const expectedError = {
        type: "field",
        value: toDateString("", month, year),
        msg: "validation:dateOfLossInFuture",
        path: "baby-date-of-loss",
        location: "body",
      };

      expect(result.array().length).toBe(1);
      expect(error).toEqual(expectedError);
    });

    test("should return expected error when date before parent dob", async () => {
      const req = {
        body: {
          "include-baby-date-of-loss": "yes",
          "baby-date-of-loss-day": "",
          "baby-date-of-loss-month": "01",
          "baby-date-of-loss-year": "1999",
        },
        session: {
          journeyName: MAINFLOW.name,
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "year-of-loss": "1999",
            "date-of-birth": "1999-02-01",
          },
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("Express validation failed");
      }

      const error = result.array()[0];

      const expectedError = {
        type: "field",
        value: "1999-01-",
        msg: "validation:dateOfLossBeforeParentsDob",
        path: "baby-date-of-loss",
        location: "body",
      };

      expect(result.array().length).toBe(1);
      expect(error).toEqual(expectedError);
    });

    test("should return expected error when partial date is before eligible date", async () => {
      const req = {
        body: {
          "include-baby-date-of-loss": "yes",
          "baby-date-of-loss-day": "",
          "baby-date-of-loss-month": "01",
          "baby-date-of-loss-year": "2018",
        },
        session: {
          journeyName: MAINFLOW.name,
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "year-of-loss": "2018",
            "date-of-birth": "1999-01-01",
          },
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("Express validation failed");
      }

      const error = result.array()[0];

      const expectedError = {
        type: "field",
        value: "2018-01-",
        msg: "validation:dateOfLossNotInRange",
        path: "baby-date-of-loss",
        location: "body",
      };

      expect(result.array().length).toBe(1);
      expect(error).toEqual(expectedError);
    });
  });

  describe("partial year only date of loss", () => {
    test("when valid partial year only baby date of loss provided should return no errors", async () => {
      const req = {
        body: {
          "include-baby-date-of-loss": "yes",
          "baby-date-of-loss-day": "",
          "baby-date-of-loss-month": "",
          "baby-date-of-loss-year": "2021",
        },
        session: {
          journeyName: MAINFLOW.name,
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "year-of-loss": "2021",
            "date-of-birth": "1999-01-01",
          },
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("Express validation failed");
      }

      expect(result.array().length).toBe(0);
    });

    test("should return expected error when not a real date", async () => {
      const req = {
        body: {
          "include-baby-date-of-loss": "yes",
          "baby-date-of-loss-day": "",
          "baby-date-of-loss-month": "",
          "baby-date-of-loss-year": "99",
        },
        session: {
          journeyName: MAINFLOW.name,
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "year-of-loss": "99",
            "date-of-birth": "1999-01-01",
          },
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("Express validation failed");
      }

      const error = result.array()[0];

      const expectedError = {
        type: "field",
        value: "99--",
        msg: "validation:dateOfLossNotReal",
        path: "baby-date-of-loss",
        location: "body",
      };

      expect(result.array().length).toBe(1);
      expect(error).toEqual(expectedError);
    });

    test("should return expected error when date before parent dob", async () => {
      const req = {
        body: {
          "include-baby-date-of-loss": "yes",
          "baby-date-of-loss-day": "",
          "baby-date-of-loss-month": "",
          "baby-date-of-loss-year": "1998",
        },
        session: {
          journeyName: MAINFLOW.name,
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "year-of-loss": "1998",
            "date-of-birth": "1999-02-01",
          },
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("Express validation failed");
      }

      const error = result.array()[0];

      const expectedError = {
        type: "field",
        value: "1998--",
        msg: "validation:dateOfLossBeforeParentsDob",
        path: "baby-date-of-loss",
        location: "body",
      };

      expect(result.array().length).toBe(1);
      expect(error).toEqual(expectedError);
    });

    test("should return expected error when partial date is before eligible date year", async () => {
      const req = {
        body: {
          "include-baby-date-of-loss": "yes",
          "baby-date-of-loss-day": "",
          "baby-date-of-loss-month": "",
          "baby-date-of-loss-year": "2017",
        },
        session: {
          journeyName: MAINFLOW.name,
          [MAINFLOW_DATA_OBJECT_NAME]: {
            "year-of-loss": "2017",
            "date-of-birth": "1999-01-01",
          },
        },
      };

      const result = await applyExpressValidation(req, validate());
      if (result === undefined) {
        throw new Error("Express validation failed");
      }

      const error = result.array()[0];

      const expectedError = {
        type: "field",
        value: "2017--",
        msg: "validation:dateOfLossNotInRange",
        path: "baby-date-of-loss",
        location: "body",
      };

      expect(result.array().length).toBe(1);
      expect(error).toEqual(expectedError);
    });
  });
});
