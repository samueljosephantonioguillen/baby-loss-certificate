import expect from "expect";

import { validate } from "./validate";
import { applyExpressValidation } from "../../common/test/apply-express-validation";

test("return no validation errors when include-baby-name and baby-name are valid", async () => {
  const req = {
    body: {
      "include-baby-name": "yes",
      "baby-name": "Valid Name",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("return no validation errors when include-baby-name is no", async () => {
  const req = {
    body: {
      "include-baby-name": "no",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("return expected validation error when include-baby-name field is empty", async () => {
  const req = {
    body: {
      "include-baby-name": "",
      "baby-name": "",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "",
    msg: "validation:selectIncludeBabyNameOption",
    path: "include-baby-name",
    location: "body",
  };

  expect(error).toEqual(expectedError);
});

test("return expected validation error when include-baby-name field is yes but baby-name is empty", async () => {
  const req = {
    body: {
      "include-baby-name": "yes",
      "baby-name": "",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "",
    msg: "validation:missingBabyName",
    path: "baby-name",
    location: "body",
  };

  expect(error).toEqual(expectedError);
});

test("return expected validation error when baby-name is over 75 characters", async () => {
  const string = "A";
  const req = {
    body: {
      "include-baby-name": "yes",
      "baby-name": string.repeat(76),
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: string.repeat(76),
    msg: "validation:babyNameTooLong",
    path: "baby-name",
    location: "body",
  };

  expect(error).toEqual(expectedError);
});

test("return expected validation error when baby-name has invalid characters", async () => {
  const req = {
    body: {
      "include-baby-name": "yes",
      "baby-name": "£",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "£",
    msg: "validation:patternBabyName",
    path: "baby-name",
    location: "body",
  };

  expect(error).toEqual(expectedError);
});
