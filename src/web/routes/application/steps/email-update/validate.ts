import { check } from "express-validator";
import { translateValidationMessage } from "../common/translate-validation-message";
import { EMAIL_REGEX, EMAIL_VALID_CHARACTERS_REGEX } from "../../constants";

const isEmailsEqual = (email, { req }) => {
  if (email !== req.body["applicant-email"]) {
    return false;
  }
  return true;
};

const validate = () => [
  check("applicant-add-email")
    .notEmpty()
    .bail()
    .withMessage(
      translateValidationMessage("validation:yourEmailSelectMissing")
    ),
  check("applicant-email")
    .if((value, { req }) => req.body["applicant-add-email"] === "yes")
    .notEmpty()
    .bail()
    .withMessage(translateValidationMessage("validation:missingYourEmail"))
    .isLength({ max: 255 })
    .bail()
    .withMessage(translateValidationMessage("validation:yourEmailTooLong"))
    .matches(EMAIL_VALID_CHARACTERS_REGEX)
    .bail()
    .withMessage(
      translateValidationMessage("validation:yourEmailInvalidCharacters")
    )
    .matches(EMAIL_REGEX)
    .bail()
    .withMessage(
      translateValidationMessage("validation:yourEmailInvalidFormat")
    ),
  check("applicant-email-confirm")
    .if((value, { req }) => req.body["applicant-add-email"] === "yes")
    .custom(isEmailsEqual)
    .withMessage(translateValidationMessage("validation:yourEmailsNotEqual")),
];

export { validate };
