import expect from "expect";

import { theirEmail, pageContent, isNavigable, validate } from "./their-email";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";
import { BACKOFFICE_ROLE } from "../../../constants";
import { config } from "../../../../../../config";

test("theirEmail should match expected outcomes", () => {
  const expectedResults = {
    path: "/other-parent-email-address",
    template: "their-email",
    pageContent,
    isNavigable,
    validate,
  };

  expect(theirEmail).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("theirEmail.title"),
    sectionTitle: translate("section.otherParentsDetails"),
    heading: translate("theirEmail.heading"),
    body: translate("theirEmail.body", {
      path: "/test-context/contact-us",
    }),
    backLinkText: translate("buttons:back"),
    continueButtonText: translate("buttons:continue"),
    yes: translate("yes"),
    no: translate("no"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when add-another-parent is yes", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "add-another-parent": "yes",
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when add-another-parent is no", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "add-another-parent": "no",
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("isNavigable should return false when add-another-parent is undefined", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "add-another-parent": undefined,
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("isNavigable should return false when back office is role", () => {
    config.environment.USE_AUTHENTICATION = true;
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "add-another-parent": "yes",
        },
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});
