const isEmpty = jest.fn();

jest.mock("express-validator", () => ({
  validationResult: () => ({
    isEmpty: isEmpty,
  }),
}));

import expect from "expect";

import {
  includeBabyLossDate,
  pageContent,
  sanitise,
  validate,
  behaviourForPost,
} from "./include-baby-loss-date";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";

test("includeBabyLossDate should match expected outcomes", () => {
  const expectedResults = {
    path: "/date-on-certificate",
    template: "include-baby-loss-date",
    pageContent,
    sanitise,
    validate,
    behaviourForPost,
  };

  expect(includeBabyLossDate).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("includeBabyLossDate.title"),
    sectionTitle: translate("section.certificateDetails"),
    heading: translate("includeBabyLossDate.heading"),
    insetTextLineOne: translate("includeBabyLossDate.insetTextFirstLine"),
    insetTextLineTwo: translate("includeBabyLossDate.insetTextSecondLine"),
    hintLineOne: translate("includeBabyLossDate.hint.firstLine"),
    hintLineTwo: translate("includeBabyLossDate.hint.secondLine"),
    yes: translate("yes"),
    no: translate("no"),
    or: translate("or"),
    unknown: translate("includeBabyLossDate.unknown"),
    day: translate("day"),
    month: translate("month"),
    year: translate("year"),
    continueButtonText: translate("buttons:continue"),
    backLinkText: translate("buttons:back"),
    legend: translate("includeBabyLossDate.legend"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("behaviourForPost", () => {
  test("should return expected data when no option is selected", () => {
    isEmpty.mockReturnValue(true);

    const req = {
      body: {
        "include-baby-date-of-loss": "no",
        "baby-date-of-loss": "2023-01-01",
        "baby-date-of-loss-day": "01",
        "baby-date-of-loss-month": "01",
        "baby-date-of-loss-year": "2023",
      },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "baby-date-of-loss": "2023-01-01",
          "baby-date-of-loss-day": "01",
          "baby-date-of-loss-month": "01",
          "baby-date-of-loss-year": "2023",
        },
      },
    };
    const res = {};
    const next = jest.fn();

    const expectedReq = {
      body: { "include-baby-date-of-loss": "no" },
      session: { journeyName: MAINFLOW.name, [MAINFLOW_DATA_OBJECT_NAME]: {} },
    };

    behaviourForPost()(req, res, next);
    expect(req).toEqual(expectedReq);
    expect(next).toBeCalledTimes(1);
  });

  test("should return expected data when unknown option is selected", () => {
    isEmpty.mockReturnValue(true);

    const req = {
      body: {
        "include-baby-date-of-loss": "unknown",
        "baby-date-of-loss": "2023-01-01",
        "baby-date-of-loss-day": "01",
        "baby-date-of-loss-month": "01",
        "baby-date-of-loss-year": "2023",
      },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "baby-date-of-loss": "2023-01-01",
          "baby-date-of-loss-day": "01",
          "baby-date-of-loss-month": "01",
          "baby-date-of-loss-year": "2023",
        },
      },
    };
    const res = {};
    const next = jest.fn();
    const expectedReq = {
      body: { "include-baby-date-of-loss": "unknown" },
      session: { journeyName: MAINFLOW.name, [MAINFLOW_DATA_OBJECT_NAME]: {} },
    };

    behaviourForPost()(req, res, next);
    expect(req).toEqual(expectedReq);
    expect(next).toBeCalledTimes(1);
  });

  test("should call when validation error", () => {
    isEmpty.mockReturnValue(false);

    const req = {};
    const res = {};
    const next = jest.fn();

    behaviourForPost()(req, res, next);

    expect(next).toBeCalledTimes(1);
  });
});
