import { CONTEXT_PATH, ACCESSIBILITY_STATEMENT } from "./paths";
import { renderStaticPage } from "./static-page-content";

const pageContent = ({ translate, req }) => ({
  title: translate("accessibilityStatement.title"),
  pageContent: translate("accessibilityStatement"),
  hideServiceLink: req.session.hideServiceLink,
});

const renderAccessibilityStatement = (app) => {
  app.get(
    CONTEXT_PATH + ACCESSIBILITY_STATEMENT,
    renderStaticPage("accessibility-statement", pageContent)
  );
};

export { renderAccessibilityStatement, pageContent };
