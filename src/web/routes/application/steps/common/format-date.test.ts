import expect from "expect";
import {
  toDateString,
  dateAsString,
  toLongLocaleDateString,
  formatDate,
} from "./format-date";

describe("toDateString", () => {
  test("should concatenate digits with hyphens", () => {
    const result = toDateString("31", "13", "1980");
    const expected = "1980-13-31";

    expect(result).toBe(expected);
  });

  test("should concatenate digits and return an empty string", () => {
    const result = toDateString("", "", "");
    const expected = "";

    expect(result).toBe(expected);
  });

  test("should concatenate digits with hyphens and return empty for no values passed", () => {
    const result = toDateString("", "12", "2019");
    const expected = "2019-12-";

    expect(result).toBe(expected);
  });

  test("should concatenate digits with hyphens and return empty for no values passed", () => {
    const result = toDateString("1", "", "2019");
    const expected = "2019--01";

    expect(result).toBe(expected);
  });

  test("should concatenate digits with hyphens and return empty for no values passed", () => {
    const result = toDateString("1", "5", "");
    const expected = "-05-01";

    expect(result).toBe(expected);
  });

  test("should concatenate digits with hyphens and return empty for no values passed", () => {
    const result = toDateString("", "", "2019");
    const expected = "2019--";

    expect(result).toBe(expected);
  });

  test("should pad day and month", () => {
    const result = toDateString("1", "3", "1980");
    const expected = "1980-03-01";

    expect(result).toBe(expected);
  });

  test("should coerce all arguments to strings", () => {
    const result = toDateString(undefined, null, 22);
    const expected = "22-00-00";

    expect(result).toBe(expected);
  });
});

describe("toLongLocaleDateString", () => {
  test("return full date when all three are provided", () => {
    const result = toLongLocaleDateString("9", "6", "2023");
    const expected = "9 June 2023";

    expect(result).toEqual(expected);
  });

  test("return expected date when all day is empty", () => {
    const result = toLongLocaleDateString("", "6", "2023");
    const expected = "June 2023";

    expect(result).toEqual(expected);
  });

  test("return expected date when month and day is not empty", () => {
    const result = toLongLocaleDateString("", "", "2023");
    const expected = "2023";

    expect(result).toEqual(expected);
  });
});

describe("dateAsString", () => {
  test("should return date with correct adjustments", () => {
    const DECEMBER = 11;
    const date = new Date(1999, DECEMBER, 31);

    expect(dateAsString({ date, monthAdjustment: 8 })).toBe("2000-08-31");
    expect(dateAsString({ date })).toBe("1999-12-31");
    expect(dateAsString({ date, monthAdjustment: -2 })).toBe("1999-10-31");
    expect(dateAsString({ date, yearAdjustment: 4 })).toBe("2003-12-31");
    expect(dateAsString({ date, yearAdjustment: -2 })).toBe("1997-12-31");
  });

  test("should return current date as string", () => {
    const today = new Date();
    const day = today.getDate();
    const month = today.getMonth() + 1;
    const year = today.getFullYear();

    const expectedString = toDateString(day, month, year);

    expect(dateAsString()).toBe(expectedString);
  });
});

describe("formatDate", () => {
  test("should format UTC timestamp to yyyy-mm-dd", () => {
    const result = formatDate("2023-11-10T16:14:49Z");
    expect(result).toEqual("2023-11-10");
  });
});
