import { START } from "../start";
import {
  contextPath,
  firstParentContextPath,
  secondParentContextPath,
} from "../application/paths/context-path";
import { getJourneyFromUrl } from "../application/tools/journey-from-url";
import { userIsAgent, userIsBackOffice } from "../azure-routes/auth";
import { CERTIFICATE_MANAGER_HOME } from "../static-pages/paths";

const TOO_MANY_OTP_SENDS_PATH = "/cannot-request-another-code";

const pageContent = ({ translate, serviceStartPage }) => ({
  title: translate("tooManyOtpSends.title"),
  heading: translate("tooManyOtpSends.heading"),
  startAgain: translate("startAgain"),
  body: translate("tooManyOtpSends.body", {
    serviceStart: serviceStartPage,
  }),
});

const get = (req, res) => {
  const journeyName = getJourneyFromUrl(req.originalUrl);
  const serviceStartPage = getServiceStartPage(req, journeyName);
  res.render(
    "cannot-request-another-code",
    pageContent({ translate: req.t, serviceStartPage })
  );
};

const getServiceStartPage = (req, journeyName) => {
  if (userIsAgent(req) || userIsBackOffice(req)) {
    return contextPath(CERTIFICATE_MANAGER_HOME, journeyName);
  }
  return contextPath(START, journeyName);
};

const registerTooManyOTPSends = (app) => {
  app.get(
    [
      firstParentContextPath(TOO_MANY_OTP_SENDS_PATH),
      secondParentContextPath(TOO_MANY_OTP_SENDS_PATH),
    ],
    get
  );
};

export { registerTooManyOTPSends, pageContent, get, TOO_MANY_OTP_SENDS_PATH };
