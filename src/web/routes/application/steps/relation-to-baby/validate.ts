import { check } from "express-validator";

import { translateValidationMessage } from "../common/translate-validation-message";

const acceptedRelationsToBaby = ["Mother", "Father", "Parent", "Surrogate"];

const validate = () => [
  check("relation-to-baby")
    .notEmpty()
    .withMessage(translateValidationMessage("validation:selectRelationToBaby")),
];

export { acceptedRelationsToBaby, validate };
