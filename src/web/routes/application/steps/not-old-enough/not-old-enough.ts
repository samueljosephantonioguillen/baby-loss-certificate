import { getFlowDataObjectName } from "../../tools/data-object";

const pageContent = ({ translate }) => ({
  title: translate("notOldEnough.title"),
  heading: translate("notOldEnough.heading"),
  supportOrganisationsHeading: translate("supportOrganisations.heading"),
  supportOrganisationsBody: translate("supportOrganisations.body"),
  supportOrganisationsList: translate("supportOrganisations.listItems"),
  backLinkText: translate("buttons:back"),
});

const isNavigable = (req) =>
  req.session[getFlowDataObjectName(req.session.journeyName)]
    .olderThanAgeRequirement === false;

const notOldEnough = {
  path: "/cannot-request-date-of-birth",
  template: "not-eligible",
  pageContent,
  isNavigable,
};

export { notOldEnough, pageContent, isNavigable };
