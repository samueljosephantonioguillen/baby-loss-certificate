import { toUpper, compose } from "ramda";

const removeHyphens = (value) => {
  return value.replace(/-/g, "");
};

const removeHyphensThenUppercase = compose(removeHyphens, toUpper);

const sanitise = () => (req, res, next) => {
  req.body["sp-sanitised-reference-number"] = removeHyphensThenUppercase(
    req.body["sp-reference-number"]
  );

  next();
};

export { sanitise };
