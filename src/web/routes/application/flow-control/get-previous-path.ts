import { isNil } from "ramda";
import { stateMachine } from "../flow-control/state-machine";
import { isUndefined } from "../../../../common/predicates";
import { logger } from "../../../../web/logger/logger";
import { MAINFLOW, SECOND_PARENT_FLOW } from "../journey-definitions";
import { checkAuthenticationSet } from "../../../../common/azure-utils";
import { START } from "../../start";
import { contextPath } from "../paths/context-path";

const CONTEXT_PATH = process.env.CONTEXT_PATH ?? "";
const SP_CONTEXT_PATH = process.env.SP_CONTEXT_PATH ?? "";

const getPreviousNavigablePath = (steps, index, req) => {
  if (index === 0) {
    throw new Error("No allowed back route found");
  }

  const previousStep = steps[index - 1];

  const { isNavigable, path, back } = previousStep;

  if (isNil(isNavigable)) {
    return path;
  }

  if (typeof isNavigable !== "function") {
    throw new Error(
      `isNavigable must be a function for step ${JSON.stringify(previousStep)}`
    );
  }

  if (typeof isNavigable(req) !== "boolean") {
    throw new Error(
      `isNavigable must return a boolean for step ${JSON.stringify(
        previousStep
      )}`
    );
  }

  if (isNavigable(req)) {
    if (typeof back === "function") {
      return back(req);
    }

    return path;
  }

  return getPreviousNavigablePath(steps, index - 1, req);
};

const getPreviousNavigablePathFromMainflowJourney = (req) => {
  const currentPathForMainflowJourney = stateMachine.getCurrentPath(req, {
    name: "MAINFLOW",
  });

  if (isUndefined(currentPathForMainflowJourney)) {
    logger.debug("Getting previous navigable path set to root context");
    return CONTEXT_PATH + "/";
  }
  logger.debug(
    `Getting previous navigable path set to ${currentPathForMainflowJourney}`
  );

  return currentPathForMainflowJourney;
};

const getPreviousNavigablePathFromSecondParentFlowJourney = (req) => {
  const currentPathForSecondParentFlowJourney = stateMachine.getCurrentPath(
    req,
    {
      name: "SECOND_PARENT_FLOW",
    }
  );

  if (isUndefined(currentPathForSecondParentFlowJourney)) {
    logger.debug("Getting previous navigable path set to root context");
    return SP_CONTEXT_PATH + "/";
  }
  logger.debug(
    `Getting previous navigable path set to ${currentPathForSecondParentFlowJourney}`
  );

  return currentPathForSecondParentFlowJourney;
};

const getPreviousPath = (steps, step, req) => {
  const index = steps.indexOf(step);
  const secondparentflow = req?.session?.journeyName == SECOND_PARENT_FLOW.name;
  if (index === -1) {
    throw new Error(
      `Unable to find ${JSON.stringify(step)} in the list of steps`
    );
  }

  // The 'embeddedMainJourney' flag allows another journey i.e. cookies to have a back link to another journey step in mainflow
  if (index === 0 && step.embeddedMainJourney !== true && !secondparentflow) {
    logger.debug("Getting previous path set to start page for mainflow");
    if (checkAuthenticationSet()) return CONTEXT_PATH + START;
    return contextPath(START, MAINFLOW.name);
  }

  // The 'embeddedSecondJourney' flag allows another journey i.e. cookies to have a back link to another journey step in second parent flow
  if (index === 0 && step.embeddedSecondJourney !== true && secondparentflow) {
    logger.debug(
      "Getting previous path set to root context for second parent flow"
    );
    return SP_CONTEXT_PATH + "/";
  }

  // If the 'embeddedMainJourney' is set, get the current step url for the blc mainflow journey
  if (steps[index].embeddedMainJourney === true && !secondparentflow) {
    return getPreviousNavigablePathFromMainflowJourney(req);
  }

  // If the 'embeddedSecondJourney' is set, get the current step url for the blc second parent flow journey
  if (steps[index].embeddedSecondJourney === true && secondparentflow) {
    return getPreviousNavigablePathFromSecondParentFlowJourney(req);
  }

  return getPreviousNavigablePath(steps, index, req);
};

export { getPreviousNavigablePath, getPreviousPath };
