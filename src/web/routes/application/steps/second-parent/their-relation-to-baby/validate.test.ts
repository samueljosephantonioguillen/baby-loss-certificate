import expect from "expect";
import { validate } from "./validate";
import { applyExpressValidation } from "../../common/test/apply-express-validation";

test("return no validation errors when their-relation-to-baby field is valid", async () => {
  const req = {
    body: {
      "their-relation-to-baby": "mother",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("return expected validation error when their-relation-to-baby field is empty", async () => {
  const req = {
    body: {
      "their-relation-to-baby": "",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "",
    msg: "validation:selectSecondParentRelationToBabyOption",
    path: "their-relation-to-baby",
    location: "body",
  };

  expect(error).toEqual(expectedError);
});
