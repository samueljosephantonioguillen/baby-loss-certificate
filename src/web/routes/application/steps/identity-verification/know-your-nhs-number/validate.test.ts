import expect from "expect";

import { validate } from "./validate";
import { applyExpressValidation } from "../../common/test/apply-express-validation";

test("returns no validation errors when know-your-nhs-number field is yes", async () => {
  const req = {
    body: {
      "know-your-nhs-number": "yes",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("returns no validation errors when know-your-nhs-number field is no", async () => {
  const req = {
    body: {
      "know-your-nhs-number": "no",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("returns expected validation error when know-your-nhs-number field is empty", async () => {
  const req = {
    body: {
      "know-your-nhs-number": "",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "",
    msg: "validation:selectKnowYourNhsNumber",
    path: "know-your-nhs-number",
    location: "body",
  };

  expect(result.array().length).toBe(1);
  expect(error).toEqual(expectedError);
});
