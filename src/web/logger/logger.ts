import { winstonLogger } from "./winston-logger";
import { REQUEST_ID_HEADER } from "../server/headers";
import { config } from "../../config";
import { LeveledLogMethod } from "winston";

const additionalProperties = { version: config.environment.APP_VERSION };

const logger = {
  info: function (message, req?, meta?) {
    logWithMessageAndRequestDetails(winstonLogger.info, message, req, meta);
  },
  error: function (message: string, req?, meta?) {
    logWithMessageAndRequestDetails(winstonLogger.error, message, req, meta);
  },
  warn: function (message: string, req?, meta?) {
    logWithMessageAndRequestDetails(winstonLogger.warn, message, req, meta);
  },
  debug: function (message: string, req?, meta?) {
    logWithMessageAndRequestDetails(winstonLogger.debug, message, req, meta);
  },
};

function logWithMessageAndRequestDetails(
  level: LeveledLogMethod,
  message,
  req?,
  meta?
) {
  const requestId = req?.headers ? req.headers[REQUEST_ID_HEADER] : undefined;
  const locatorId = req?.session ? req.session.locator : undefined;
  const journeyName = req?.session ? req.session.journeyName : undefined;

  const applicationId = req?.session?.applicationId
    ? req.session.applicationId
    : undefined;
  let requestMeta;

  if (requestId) {
    requestMeta = {
      headers: { [REQUEST_ID_HEADER]: requestId },
      session: { locator: locatorId, applicationId, journeyName },
    };
  }
  if (req?.session?.issuedCertificateDetails) {
    meta = {
      ...meta,
      certificate: req.session.issuedCertificateDetails,
    };
  }

  level(message, {
    ...additionalProperties,
    ...meta,
    req: requestMeta,
  });
}

export { logger, logWithMessageAndRequestDetails };
