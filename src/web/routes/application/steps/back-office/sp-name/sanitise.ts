const sanitise = () => (req, res, next) => {
  req.body["their-first-name"] = req.body["their-first-name"].trim();
  req.body["their-last-name"] = req.body["their-last-name"].trim();

  // capitalise first letter
  req.body["their-first-name"] =
    req.body["their-first-name"].charAt(0).toUpperCase() +
    req.body["their-first-name"].slice(1);

  req.body["their-last-name"] =
    req.body["their-last-name"].charAt(0).toUpperCase() +
    req.body["their-last-name"].slice(1);

  next();
};

export { sanitise };
