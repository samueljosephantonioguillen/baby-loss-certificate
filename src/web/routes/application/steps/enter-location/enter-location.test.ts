import expect from "expect";
import {
  enterLocation,
  pageContent,
  isNavigable,
  validate,
} from "./enter-location";
import { MAINFLOW } from "../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../tools/data-object";

test("enterLocation should match expected outcomes", () => {
  const expectedResults = {
    path: "/living-in-england-at-time-of-baby-loss",
    template: "enter-location",
    pageContent,
    isNavigable,
    validate,
  };

  expect(enterLocation).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("enterLocation.title"),
    sectionTitle: translate("section.confirmEligibility"),
    heading: translate("enterLocation.heading"),
    yes: translate("yes"),
    no: translate("no"),
    emptySelection: translate("validation:selectYesOrNo"),
    continueButtonText: translate("buttons:continue"),
    backLinkText: translate("buttons:back"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when living-in-england is yes", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "living-in-england": "yes",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when living-in-england is no", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "living-in-england": "no",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when living-in-england is not defined", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "living-in-england": undefined,
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});
