import { check } from "express-validator";
import { translateValidationMessage } from "../../common/translate-validation-message";

const validate = () => [
  check("include-baby-sex")
    .not()
    .isEmpty()
    .bail()
    .withMessage(
      translateValidationMessage("validation:selectIncludeBabySexOption")
    ),
];

export { validate };
