import { userIsBackOffice } from "../../../../azure-routes/auth";
import { contextPath } from "../../../paths/context-path";
import { getFlowDataObjectName } from "../../../tools/data-object";
import { checkYourDetails } from "../../check-your-details";

const pageContent = ({ translate }) => ({
  title: translate("notMatched.title"),
  heading: translate("notMatched.heading"),
  bodyLineOne: translate("notMatched.body.firstLine"),
  bodyLineTwo: translate("notMatched.body.secondLine"),
  bodyLineThree: translate("notMatched.body.thirdLine"),
  tryAgainButton: translate("notMatched.button"),
  backLinkText: translate("buttons:back"),
});

const isNavigable = (req) => {
  return (
    req.session[getFlowDataObjectName(req.session.journeyName)][
      "notMatched"
    ] === true && !userIsBackOffice(req)
  );
};

const behaviourForPost = () => (req, res) => {
  res.redirect(contextPath(checkYourDetails.path, req.session.journeyName));
};

const notMatched = {
  path: "/cannot-request-cannot-find-match",
  template: "not-matched",
  pageContent,
  isNavigable,
  behaviourForPost,
};

export { notMatched, pageContent, isNavigable, behaviourForPost };
