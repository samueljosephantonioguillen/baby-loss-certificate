import {
  isUsingAuthentication,
  userIsAgent,
  userIsBackOffice,
} from "../../../azure-routes/auth";
import { sanitise } from "./sanitise";
import { validate } from "./validate";
import { logger } from "../../../../logger/logger";
import { lookupApplicationStatus } from "../common/pg-client";
import {
  CRM_DATA_OBJECT_NAME,
  getFlowDataObjectName,
} from "../../tools/data-object";
import { isNilOrEmpty } from "../../../../../common/predicates";
import {
  dateToFullLenghtFormatWithTwelveHourTime,
  dateToShortMonthFormat,
  formatDate,
} from "../common/format-date";
import { formatReference } from "../common/certificate-reference-generator";
import { crmContextPath } from "../../paths/context-path";

const isNavigable = (req) =>
  isUsingAuthentication() && (userIsAgent(req) || userIsBackOffice(req));

function pushFieldIfPresent(
  field,
  fieldName,
  fieldValue,
  printableFields,
  searchFieldsUsed
) {
  if (field) {
    logger.debug(`Adding ${fieldName} and ${fieldValue}to search fields`);
    searchFieldsUsed.push(fieldName);
    printableFields.push(fieldValue);
  }
}

const pageContent = ({ translate }) => ({
  title: translate("crmSearchDetails.title"),
  heading: translate("crmSearchDetails.heading"),
  certificateNumber: translate("crmSearchDetails.certificateNumber"),
  firstName: translate("crmSearchDetails.firstName"),
  lastName: translate("crmSearchDetails.lastName"),
  postcode: translate("crmSearchDetails.postcode"),
  dob: translate("crmSearchDetails.dob"),
  day: translate("day"),
  month: translate("month"),
  year: translate("year"),
  searchButton: translate("crmSearchDetails.searchButton"),
  hintDOB: translate("dateOfBirth.hint"),
  resetFormButton: translate("crmSearchDetails.resetFormButton"),
});

const behaviourForPost = () => async (req, res, next) => {
  if (req.body["is-cleared"] === "true") {
    delete req.session[CRM_DATA_OBJECT_NAME];
    return res.redirect(crmContextPath("/search-details"));
  }
  try {
    const certificateReference =
      req.body["sanitised-certificate-reference-number"];
    const postcode = req.body["sanitisedPostcode"];
    const firstName = req.body["first-name"];
    const lastName = req.body["last-name"];
    const dob = req.body["date-of-birth"];
    const dataObjectName = getFlowDataObjectName(req.session.journeyName);
    //reset the search fields used and printable fields
    const searchFieldsUsed = [];
    const printableFields = [];

    pushFieldIfPresent(
      certificateReference,
      "certificateReference",
      req.body["certificate-reference-number"],
      printableFields,
      searchFieldsUsed
    );
    pushFieldIfPresent(
      firstName,
      "firstName",
      req.body["first-name"],
      printableFields,
      searchFieldsUsed
    );
    pushFieldIfPresent(
      lastName,
      "lastName",
      req.body["last-name"],
      printableFields,
      searchFieldsUsed
    );
    pushFieldIfPresent(
      dob,
      "dob",
      req.body["date-of-birth"],
      printableFields,
      searchFieldsUsed
    );
    pushFieldIfPresent(
      postcode,
      "postcode",
      req.body["postcode"],
      printableFields,
      searchFieldsUsed
    );

    logger.info(
      `Track A Request: Tracking application as: ${req.session.account.localAccountId}`
    );
    logger.info(
      `Track A Request: Search criteria used for tracking the application status: ${searchFieldsUsed.join(
        ", "
      )}`
    );

    req.session[dataObjectName] = {
      ...req.session[dataObjectName],
      "crm-search-details": printableFields,
      "crm-search-results": mapCrmSearchResults(
        await lookupApplicationStatus(
          isNilOrEmpty(certificateReference) ? null : certificateReference,
          isNilOrEmpty(firstName) ? null : firstName,
          isNilOrEmpty(lastName) ? null : lastName,
          isNilOrEmpty(dob) ? null : formatDate(dob),
          isNilOrEmpty(postcode) ? null : postcode
        )
      ),
    };
    logger.info(
      `Track A Request: Search results length: ${req.session[dataObjectName]["crm-search-results"].length}`
    );
    const searchResults = req.session[dataObjectName]["crm-search-results"];
    logger.info(
      `Track A Request: Search results certificate numbers: ${searchResults
        .map((result) => result.refNumber)
        .join(", ")} performed by ${req.session.account.localAccountId}`
    );
  } catch (error) {
    logger.error(`Track A Request:Search query failed with error ${error}`);
    res.locals.errorTitleText = req.t("validation:errorTitleText");
    res.locals.errors = [
      {
        path: "continue-button",
        msg: req.t("validation:tryAgainLater"),
      },
    ];
  }

  return next();
};

const mapCrmSearchResults = (crmSearchResults) => {
  return crmSearchResults.map((result) => {
    return {
      refNumber: formatReference(result.certificate_reference),
      fName: result.parent1_given_name,
      lName: result.parent1_family_name,
      dob: dateToShortMonthFormat(new Date(result.parent1_date_of_birth)),
      postcode: result.application_address_postcode,
      status: result.application_status,
      email: result.parent1_email_address,
      lossYear: result.loss_year,
      submitted: dateToFullLenghtFormatWithTwelveHourTime(
        new Date(result.created_dtm)
      ),
      address: combineAddressLinesAndPostcode(result),
    };
  });
};

const combineAddressLinesAndPostcode = (obj) => {
  const addressLines = [
    obj.application_address_line_1,
    obj.application_address_line_2,
    obj.application_address_line_3,
    obj.application_address_line_4,
    obj.application_address_line_5,
  ];

  return (
    addressLines.filter((line) => line).join(", ") +
    ", " +
    obj.application_address_postcode
  );
};

const crmSearchDetails = {
  path: "/search-details",
  template: "crm-search-details",
  pageContent,
  isNavigable,
  sanitise,
  validate,
  behaviourForPost,
};

export {
  crmSearchDetails,
  pageContent,
  sanitise,
  validate,
  isNavigable,
  behaviourForPost,
  pushFieldIfPresent,
};
