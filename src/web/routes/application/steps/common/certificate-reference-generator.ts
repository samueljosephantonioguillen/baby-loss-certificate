import { randomBytes } from "crypto";

const HEX_DIGITS: string[] = [
  "0",
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "A",
  "B",
  "C",
  "D",
  "E",
  "F",
  "G",
  "H",
  "I",
  "J",
  "K",
  "L",
  "M",
  "N",
  "O",
  "P",
  "Q",
  "R",
  "S",
  "T",
  "U",
  "V",
  "W",
  "X",
  "Y",
  "Z",
];

const CERTIFICATE_REFERENCE_LENGTH = 11;

const generateReference = (): string => {
  const bytes = randomBytes(CERTIFICATE_REFERENCE_LENGTH);
  return byteToString(bytes);
};

const byteToString = (bytes: Buffer): string => {
  const buffer: string[] = [];
  bytes.forEach((byte) => {
    buffer.push(HEX_DIGITS[byte & 0x0f]);
  });
  return buffer.join("");
};

const formatReference = (reference: string): string => {
  const first = reference.substring(0, 3);
  const second = reference.substring(3, 6);
  const third = reference.substring(6, 11);

  return `${first}-${second}-${third}`;
};

export { generateReference, formatReference, HEX_DIGITS };
