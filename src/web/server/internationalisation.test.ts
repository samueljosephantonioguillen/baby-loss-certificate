import expect from "expect";

import { detection, refreshCookieExpirationDate } from "./internationalisation";

test("detection should return expected results", () => {
  const config = {
    environment: {
      USE_UNSECURE_COOKIE: true,
    },
  };
  const expectedResults = {
    order: ["querystring", "cookie", "header"],
    lookupQuerystring: "lang",
    lookupCookie: "lang",
    cookieSecure: !config.environment.USE_UNSECURE_COOKIE,
    caches: ["cookie"],
  };

  const result = detection(config);

  expect(result).toEqual(expectedResults);
});

test("refreshCookieExpirationDate should set cookieExpirationDate when it exists", () => {
  const req = {
    i18n: {
      options: {
        detection: {
          cookieExpirationDate: undefined,
        },
      },
    },
  };
  const res = {};
  const next = jest.fn();

  refreshCookieExpirationDate(req, res, next);

  expect(req.i18n.options.detection.cookieExpirationDate).toBeDefined();
  expect(next).toBeCalled();
});

test("refreshCookieExpirationDate should not set cookieExpirationDate", () => {
  const req = {
    i18n: undefined,
  };
  const res = {};
  const next = jest.fn();

  refreshCookieExpirationDate(req, res, next);

  expect(req.i18n).toBeFalsy();
  expect(next).toBeCalled();
});
