import expect from "expect";

const isEmpty = jest.fn();
jest.mock("express-validator", () => ({
  validationResult: () => ({
    isEmpty: isEmpty,
  }),
}));

const sessionDestroyButKeepLoggedIn = jest.fn();
jest.mock("../../common/session-destroy", () => ({
  sessionDestroyButKeepLoggedIn: sessionDestroyButKeepLoggedIn,
}));

import { TOO_MANY_ATTEMPTS_PATH } from "../../../../error-pages/too-many-attempts";
import {
  obfuscateEmail,
  obfuscatePhoneNumber,
} from "../../common/obfuscate-contact";
import {
  enterSecurityCode,
  pageContent,
  isNavigable,
  sanitise,
  validate,
  behaviourForPost,
  MAXIMUM_ALLOWED_OTP_ATTEMPTS,
  behaviourForGet,
} from "./enter-security-code";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";
import { contextPath } from "../../../paths/context-path";
import { BACKOFFICE_ROLE } from "../../../constants";
import { config } from "../../../../../../config";

test("whatIsYourNhsNumber should match expected outcomes", () => {
  const expectedResults = {
    path: "/enter-security-code",
    template: "enter-security-code",
    pageContent,
    isNavigable,
    sanitise,
    validate,
    behaviourForPost,
    behaviourForGet,
  };

  expect(enterSecurityCode).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const req = {
    session: {
      journeyName: MAINFLOW.name,
      [MAINFLOW_DATA_OBJECT_NAME]: {
        "security-code-method": "email",
        email: "test@email.com",
        "phone-number": "123456789",
      },
    },
  };

  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("enterSecurityCode.title"),
    sectionTitle: translate("section.yourDetails"),
    heading: translate("enterSecurityCode.heading"),
    renderEmailBody:
      req.session[MAINFLOW_DATA_OBJECT_NAME]["security-code-method"] ===
      "email",
    renderTextBody:
      req.session[MAINFLOW_DATA_OBJECT_NAME]["security-code-method"] === "text",
    emailBodyFirstLine: translate("enterSecurityCode.emailBody.firstLine", {
      email: obfuscateEmail(req.session[MAINFLOW_DATA_OBJECT_NAME].email),
    }),
    emailBodySecondLine: translate("enterSecurityCode.emailBody.secondLine"),
    textBody: translate("enterSecurityCode.textBody", {
      phoneNumber: obfuscatePhoneNumber(
        req.session[MAINFLOW_DATA_OBJECT_NAME]["phone-number"]
      ),
    }),
    body: translate("enterSecurityCode.body", {
      path: "/test-context/security-code",
    }),
    securityCode: translate("enterSecurityCode.securityCode"),
    backLinkText: translate("buttons:back"),
    continueButtonText: translate("buttons:continue"),
  };

  const result = pageContent({ translate, req });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when security-code-method is email and has email populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "security-code-method": "email",
          email: "test@email.com",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when security-code-method is email and email is not populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "security-code-method": "email",
          email: undefined,
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when security-code-method is email and has email populated and user is back office", () => {
    config.environment.USE_AUTHENTICATION = true;
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "security-code-method": "email",
          email: "test@email.com",
        },
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when security-code-method is not populated and email is populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "security-code-method": undefined,
          email: "test@email.com",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return true when security-code-method is text and phone-number is populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "security-code-method": "text",
          "phone-number": "123456789",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when security-code-method is text and phone-number is not populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "security-code-method": "text",
          "phone-number": undefined,
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when security-code-method is text and phone-number is populated and user is back office", () => {
    config.environment.USE_AUTHENTICATION = true;
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "security-code-method": "text",
          "phone-number": "123456789",
        },
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when security-code-method is not populated and phone-number is populated", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "security-code-method": undefined,
          "phone-number": "123456789",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});

describe("behaviourForPost", () => {
  test("should call next when no validation errors", () => {
    const req = {};
    const res = {};
    const next = jest.fn();

    isEmpty.mockReturnValue(true);

    behaviourForPost()(req, res, next);

    expect(next).toBeCalledTimes(1);
  });

  test("should redirect to too many attempts page when otp attempts exceed maximum tries", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          otpAttempts: MAXIMUM_ALLOWED_OTP_ATTEMPTS,
        },
      },
    };
    const res = { redirect: jest.fn() };
    const next = jest.fn();

    isEmpty.mockReturnValue(false);

    behaviourForPost()(req, res, next);

    expect(sessionDestroyButKeepLoggedIn).toBeCalledWith(req, res);
    expect(res.redirect).toBeCalledWith(
      contextPath(TOO_MANY_ATTEMPTS_PATH, MAINFLOW.name)
    );
    expect(next).toBeCalledTimes(0);
  });

  test("should call next when attempts do not exceed maximum tries", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          otpAttempts: MAXIMUM_ALLOWED_OTP_ATTEMPTS - 1,
        },
      },
    };
    const res = { redirect: jest.fn() };
    const next = jest.fn();

    isEmpty.mockReturnValue(false);

    behaviourForPost()(req, res, next);

    expect(sessionDestroyButKeepLoggedIn).toBeCalledTimes(0);
    expect(res.redirect).toBeCalledTimes(0);
    expect(next).toBeCalledTimes(1);
  });
});

describe("behaviourForGet", () => {
  test("should clear security-code from session on get", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "security-code": "123456",
        },
      },
    };
    const res = { redirect: jest.fn() };
    const next = jest.fn();

    behaviourForGet()(req, res, next);

    expect(
      req.session[MAINFLOW_DATA_OBJECT_NAME]["security-code"]
    ).toBeUndefined();
  });
});
