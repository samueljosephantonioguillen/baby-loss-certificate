import { obfuscateEmail, obfuscatePhoneNumber } from "./obfuscate-contact";

describe("obfuscateEmail", () => {
  test("should censor email displaying first two characters and domain parts", () => {
    const email = "temporaryemail@email.com";
    const expectedObfuscatedEmail = "te********@email.com";
    const result = obfuscateEmail(email);
    expect(result).toEqual(expectedObfuscatedEmail);
  });

  test("should return null when input is not populated", () => {
    const email = null;
    const result = obfuscateEmail(email);
    expect(result).toEqual(null);
  });
});

describe("obfuscatePhoneNumber", () => {
  test("should censor phone number displaying last 3 digits", () => {
    const phoneNumber = "12345678987";
    const expectedObfuscatedPhoneNumber = "********987";
    const result = obfuscatePhoneNumber(phoneNumber);
    expect(result).toEqual(expectedObfuscatedPhoneNumber);
  });

  test("should return null when input is not populated", () => {
    const phoneNumber = null;
    const result = obfuscatePhoneNumber(phoneNumber);
    expect(result).toEqual(null);
  });
});
