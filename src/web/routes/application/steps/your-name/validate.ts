import { check } from "express-validator";

import { translateValidationMessage } from "../common/translate-validation-message";
import {
  FIRST_NAME_MAX_LENGTH,
  LAST_NAME_MAX_LENGTH,
  NAME_PATTERN,
} from "../../constants";

const validate = () => [
  check("first-name")
    .not()
    .isEmpty()
    .bail()
    .withMessage(translateValidationMessage("validation:missingFirstName"))
    .isLength({ max: FIRST_NAME_MAX_LENGTH })
    .bail()
    .withMessage(translateValidationMessage("validation:firstNameTooLong"))
    .matches(NAME_PATTERN)
    .bail()
    .withMessage(translateValidationMessage("validation:patternFirstName")),

  check("last-name")
    .not()
    .isEmpty()
    .bail()
    .withMessage(translateValidationMessage("validation:missingLastName"))
    .isLength({ max: LAST_NAME_MAX_LENGTH })
    .bail()
    .withMessage(translateValidationMessage("validation:lastNameTooLong"))
    .matches(NAME_PATTERN)
    .bail()
    .withMessage(translateValidationMessage("validation:patternLastName")),
];

export { validate };
