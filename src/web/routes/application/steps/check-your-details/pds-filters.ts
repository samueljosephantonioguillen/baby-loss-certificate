const filterContactMethods = (data, contactMethod) => {
  if (data.telecom) {
    const filteredContactMethods = filterTelecom(data.telecom, contactMethod);
    return sortedFilteredContactMethods(filteredContactMethods);
  }
  return null;
};

function filterTelecom(telecom, contactMethod) {
  const currentDate = new Date().toISOString().slice(0, 10);
  return telecom.filter((item) => {
    if (item.system !== contactMethod) {
      return false;
    }

    if (contactMethod === "phone" && item.use !== "mobile") {
      return false;
    }

    if (item.period && item.period.start > currentDate) {
      return false;
    }

    if (item.period && !item.period.end) {
      return true;
    }

    if (item.period) {
      const periodEndDate = item.period.end;
      return periodEndDate >= currentDate;
    }

    return true;
  });
}

function sortedFilteredContactMethods(filteredContactMethods) {
  filteredContactMethods.sort((a, b) => {
    if (a.period && b.period) {
      return b.period.start.localeCompare(a.period.start);
    } else if (a.period) {
      return -1;
    } else if (b.period) {
      return 1;
    }
    return 0;
  });

  return filteredContactMethods;
}

const filterAddresses = (data) => {
  if (data.address) {
    const currentDate = new Date().toISOString().slice(0, 10);

    const filteredAddresses = data.address.filter((item) => {
      if (item.use !== "home") {
        return false;
      }

      if (startDateAfterCurrentDate()) {
        return false;
      }

      if (hasNoEndDate()) {
        return true;
      }

      if (item.period) {
        const periodEndDate = item.period.end;
        return periodEndDate >= currentDate;
      }

      return true;

      function hasNoEndDate() {
        return item.period && !item.period.end;
      }

      function startDateAfterCurrentDate() {
        return item.period && item.period.start > currentDate;
      }
    });

    sortFilteredAddresses(filteredAddresses);
    return filteredAddresses;
  }
  return null;
};

function sortFilteredAddresses(filteredAddresses) {
  filteredAddresses.sort((a, b) => {
    if (a.period && b.period) {
      return b.period.start.localeCompare(a.period.start);
    } else if (a.period) {
      return -1;
    } else if (b.period) {
      return 1;
    }
    return 0;
  });
}

const isUsualName = (item) => item.use === "usual";

const isValidPeriod = (period, currentDate) => {
  if (period && period.start > currentDate) {
    return false;
  }

  if (period && !period.end) {
    return true;
  }

  if (period) {
    const periodEndDate = period.end;
    return periodEndDate >= currentDate;
  }

  return true;
};

const filterNames = (data) => {
  if (!data.name) {
    return null;
  }

  const currentDate = new Date().toISOString().slice(0, 10);

  const filteredNames = data.name.filter((item) => {
    if (!isUsualName(item)) {
      return false;
    }

    return isValidPeriod(item.period, currentDate);
  });

  filteredNames.sort((a, b) => {
    if (a.period && b.period) {
      return b.period.start.localeCompare(a.period.start);
    } else if (a.period) {
      return -1;
    } else if (b.period) {
      return 1;
    }
    return 0;
  });

  return filteredNames;
};

const matchSearchName = (names, firstName, lastName) => {
  const lowercaseFirstName = firstName.toLowerCase();
  const lowercaseLastName = lastName.toLowerCase();
  if (names && names.length > 0) {
    const lowercasePdsGivenNames = names[0].given.map((givenName) =>
      givenName.toLowerCase()
    );
    if (
      lowercasePdsGivenNames.includes(lowercaseFirstName) &&
      names[0].family.toLowerCase() === lowercaseLastName
    ) {
      const givenNameIndex = lowercasePdsGivenNames.indexOf(lowercaseFirstName);
      return {
        firstName: names[0].given[givenNameIndex],
        lastName: names[0].family,
      };
    }
  }
  return {
    firstName: null,
    lastName: null,
  };
};

const deceasedDateTime = (data) => {
  if (data.deceasedDateTime) {
    return true;
  }
  return false;
};

export {
  filterContactMethods,
  filterAddresses,
  filterNames,
  matchSearchName,
  deceasedDateTime,
};
