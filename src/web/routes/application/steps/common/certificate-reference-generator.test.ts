import expect from "expect";

import {
  generateReference,
  formatReference,
} from "./certificate-reference-generator";

test("generateReference should create an 11-digit long alphanumeric reference", () => {
  const alphanumeric = /^[0-9A-Z]+$/;
  const reference = generateReference();

  expect(reference.length).toBe(11);
  expect(alphanumeric.test(reference)).toBe(true);
});

test("formatReference should return reference in expected format", () => {
  const reference = "DAB0C2C780B";
  const result = formatReference(reference);

  expect(result).toEqual("DAB-0C2-C780B");
});
