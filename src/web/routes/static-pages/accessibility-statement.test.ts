import expect from "expect";

import {
  renderAccessibilityStatement,
  pageContent,
} from "./accessibility-statement";
import { ACCESSIBILITY_STATEMENT } from "./paths";

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const req = {
    session: {
      hideServiceLink: true,
    },
  };

  const expected = {
    title: translate("accessibilityStatement.title"),
    pageContent: translate("accessibilityStatement"),
    hideServiceLink: req.session.hideServiceLink,
  };
  const result = pageContent({ translate, req });

  expect(result).toEqual(expected);
});

test("renderAccessibilityStatement should render page with expected content", () => {
  const app = {
    get: jest.fn(),
  };

  const req = {
    t: (string) => string,
    session: {
      hideServiceLink: true,
    },
  };

  const res = {
    render: jest.fn(),
  };

  app.get.mockImplementation((path, callback) => {
    callback(req, res);
  });

  renderAccessibilityStatement(app);

  expect(app.get).toBeCalledTimes(1);
  expect(app.get).toBeCalledWith(
    process.env.CONTEXT_PATH + ACCESSIBILITY_STATEMENT,
    expect.anything()
  );

  expect(res.render).toBeCalled();
  expect(res.render).toBeCalledWith(
    "static-pages/accessibility-statement",
    pageContent({ translate: req.t, req })
  );
});
