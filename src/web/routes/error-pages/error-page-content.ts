export const renderErrorPageContent = (url) => (req, res) => {
  res.status(500).render("error-pages/" + url, getErrorPageContent(req));
};

export const getErrorPageContent = (req) => {
  return {
    title: req.t("errors:problemWithTheService.title"),
    heading: req.t("errors:problemWithTheService.heading"),
    body: req.t("errors:problemWithTheService.body"),
    startAgain: req.t("errors:problemWithTheService.startAgain"),
  };
};

export const getPageNotFoundContent = (req) => {
  return {
    title: req.t("errors:pageNotFound.title"),
    heading: req.t("errors:pageNotFound.heading"),
    paragraphOne: req.t("errors:pageNotFound.paragraphOne"),
    paragraphTwo: req.t("errors:pageNotFound.paragraphTwo"),
  };
};

export const renderSessionEndedPageContent = (url) => (req, res) => {
  res.render("error-pages/" + url, getSessionEndedContent(req));
};

export const getSessionEndedContent = (req) => {
  return {
    title: req.t("errors:sessionEnded.title"),
    heading: req.t("errors:sessionEnded.heading"),
    paragraphOne: req.t("errors:sessionEnded.paragraphOne"),
    paragraphTwo: req.t("errors:sessionEnded.paragraphTwo"),
    startAgain: req.t("errors:sessionEnded.startAgain"),
  };
};
