# Baby Loss Certificate

## About the Baby Loss Certificate

Parents who experience baby loss prior to 24 weeks gestation have no means of officially recording their loss, potentially making an already difficult grieving process harder still.

This service will enable eligible individuals affected by baby loss before 24 weeks to optionally request a government-issued certificate that suitably recognises this.

### Local development

#### Initial setup

Ensure you have created and populated `.env` file, refer to sample.env

Ensure you have redis-cli (v7.x.x) installed
- `redis-server` must be running if `USE_REDIS=true` in .env

Ensure you have localstack and aws-cli installed
- Run [install.sh](https://gitlab.com/nhsbsa/citizen-services/blc/pregnancy-loss-certificate/-/blob/main/scripts/install.sh) which will install the required components, provided you have `python3` and `pip3` installed.

You can check localstack has been successfully installed by running:
- `localstack --version`

To run localstack:
- Execute the `start.sh` script, make sure you have docker running otherwise this step will not work.

#### Xerox SQS

Run [create-sqs-queues.sh](https://gitlab.com/nhsbsa/citizen-services/blc/pregnancy-loss-certificate/-/blob/main/scripts/create-sqs-queues.sh) to create the necessary queues.

#### Lambdas

Follow READMEs of the repositories to setup locally, these lambdas must be running to complete the end-to-end BLC journey:

- [pds-authentication-lambda](https://gitlab.com/nhsbsa/citizen-services/blc/lambda/pds-authentication-lambda) this lambda is used to retrieve OAuth tokens for PDS requests, 
- [xerox-queue-processor](https://gitlab.com/nhsbsa/citizen-services/blc/lambda/xerox-queue-processor) this lambda is send the user request to Xerox for printing and store record in DB

#### PDS

NHSD Personal Demographic Service API is used for identity verification within the BLC application.

#### Gov Notify

Gov Notify is used to send one-time-passcodes to users via SMS or e-mail in order to proceed in the user journey within the BLC application.

#### PostgreSQL

PostgreSQL is used locally for creating a local database.

#### Liquibase

Liquibase is used for database schema change management to maintain revisions of the database.

#### Azure Active Directory

Azure AD is used for the single sign on to access the CCS and backoffice journeys, e-mail must be added to the AD with appropriate role to access these journeys.

#### OS Places

OS Places is used for address lookup for the CCS journey.
### Building the application

Ensure you have Node.JS (v16.x.x) installed
- `npm install` to install dependencies
- `npm run build` to build application

### Running the application

`npm run start`

## Contributions

We operate a [code of conduct](CODE_OF_CONDUCT.md) for all contributors.

See our [contributing guide](CONTRIBUTING.md) for guidance on how to contribute.

## License

Released under the [Apache 2 license](LICENCE.txt).
