import { stateMachine } from "../../../flow-control/state-machine";
import { SET_NEXT_ALLOWED_PATH } from "../../../flow-control/state-machine/actions";
import { setAdditionalDataForStep } from "../../../flow-control/session-accessors/session-accessors";
import { contextPath } from "../../../paths/context-path";
import { notIsUndefinedOrNullOrEmpty } from "../../../../../../common/predicates";

import { validate } from "./validate";
import { behaviourForPost } from "./behaviour-for-post";
import { userIsAgent, userIsBackOffice } from "../../../../azure-routes/auth";
import { getFlowDataObjectName } from "../../../tools/data-object";

const pageContent = ({ translate }) => ({
  title: translate("selectAddress.title"),
  sectionTitle: translate("section.yourDetails"),
  heading: translate("selectAddress.heading"),
  postcode: translate("selectAddress.postcode"),
  hint: translate("selectAddress.hint"),
  addressNotListed: translate("selectAddress.addressNotListed"),
  change: translate("change"),
  continueButtonText: translate("buttons:continue"),
});

const isNavigable = (req) =>
  (userIsAgent(req) || userIsBackOffice(req)) &&
  notIsUndefinedOrNullOrEmpty(req.session.postcodeLookupResults);

const behaviourForGet = (config, journey, step) => (req, res, next) => {
  const dataObjectName = getFlowDataObjectName(req.session.journeyName);
  if (req.session[dataObjectName]["selected-address"]) {
    delete req.session[dataObjectName]["selected-address"];
  }

  res.locals.postcodeLookupError = req.session.postcodeLookupError;
  if (!res.locals.postcodeLookupError) {
    const addressData = getAddressDataFromSession(
      req,
      req.session[dataObjectName]["address-id"]
    );
    setAdditionalDataForStep(req, step, addressData);
  }

  stateMachine.dispatch(
    SET_NEXT_ALLOWED_PATH,
    req,
    journey,
    contextPath("/enter-your-address", journey.name)
  );

  next();
};

const buildAddressOption = (addressId) => (result) => ({
  value: result.ADDRESS,
  text: result.ADDRESS,
  selected: result.UDPRN === addressId,
});

const getAddressDataFromSession = (req, addressId) => {
  const results = req.session.postcodeLookupResults.map(
    buildAddressOption(addressId)
  );

  const addressCount = {
    value: "",
    text: req.t("selectAddress.defaultSelectText", { count: results.length }),
    disabled: true,
    selected: !results.some((addr) => addr.selected),
  };
  const addresses = results.length === 0 ? [] : [addressCount, ...results];
  return {
    addresses,
  };
};

const selectAddress = {
  path: "/your-address",
  template: "select-address",
  pageContent,
  isNavigable,
  validate,
  behaviourForGet,
  behaviourForPost,
};

export {
  selectAddress,
  pageContent,
  isNavigable,
  validate,
  behaviourForGet,
  behaviourForPost,
};
