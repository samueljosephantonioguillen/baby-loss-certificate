import expect from "expect";

import {
  spRequestComplete,
  pageContent,
  behaviourForGet,
} from "./sp-request-complete";
import { formatReference } from "../common/certificate-reference-generator";
import { firstParentContextPath } from "../../paths/context-path";
import { START } from "../../../start";
import { SECOND_PARENT_FLOW } from "../../journey-definitions";
import { SECOND_PARENT_DATA_OBJECT_NAME } from "../../tools/data-object";

test("spRequestComplete should match expected outcomes", () => {
  const expectedResults = {
    path: "/confirmation",
    template: "sp-request-complete",
    pageContent,
    behaviourForGet,
  };

  expect(spRequestComplete).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const certificateReference = "E10BE325539";
  const req = {
    session: {
      journeyName: SECOND_PARENT_FLOW.name,
      certificateReference,
      applicant: { "add-another-parent": "yes" },
      [SECOND_PARENT_DATA_OBJECT_NAME]: {
        "sp-sanitised-reference-number": certificateReference,
      },
    },
  };
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("spRequestComplete.title"),
    spRequestComplete: translate("spRequestComplete.heading"),
    yourReference: translate("spRequestComplete.yourReference", {
      certificateReference: formatReference(certificateReference),
    }),
    whatHappensNext: translate("spRequestComplete.whatHappensNext", {
      serviceStart: firstParentContextPath(START),
    }),
    supportOrganisations: translate("supportOrganisations"),
    link: translate("spRequestComplete.link"),
  };
  const result = pageContent({ translate, req });

  expect(result).toEqual(expected);
});

test("behaviourForGet should set Clear-Site-Data header", () => {
  const req = {};
  const res = { set: jest.fn(), locals: {} };
  const next = jest.fn();

  behaviourForGet()(req, res, next);

  expect(res.set).toBeCalledTimes(1);
  expect(res.set).toBeCalledWith("Clear-Site-Data", '"storage"');
  expect(next).toBeCalledTimes(1);
});
