process.env.CONTEXT_PATH = "/test-context";
process.env.SP_CONTEXT_PATH = "/sp-test-context";
process.env.CRM_CONTEXT_PATH = "/crm-test-context";
process.env.APP_VERSION = "example-version";
process.env.GOV_NOTIFY_API_KEY = "test-api-key";
process.env.START_PAGE_URL = "https://www.test-start-page-url.com";
