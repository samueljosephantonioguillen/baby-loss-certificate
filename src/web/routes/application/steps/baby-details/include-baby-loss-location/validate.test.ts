import expect from "expect";

import { validate } from "./validate";
import { applyExpressValidation } from "../../common/test/apply-express-validation";

test("return no validation errors when include-baby-loss-location and baby-loss-location are valid", async () => {
  const req = {
    body: {
      "include-baby-loss-location": "yes",
      "baby-loss-location": "Canterbury, Kent",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("return no validation errors when include-baby-loss-location is no", async () => {
  const req = {
    body: {
      "include-baby-loss-location": "no",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("return no validation errors when include-baby-loss-location is unknown", async () => {
  const req = {
    body: {
      "include-baby-loss-location": "unknown",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("return expected validation error when include-baby-loss-location field is empty", async () => {
  const req = {
    body: {
      "include-baby-loss-location": "",
      "baby-loss-location": "",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "",
    msg: "validation:selectIncludeBabyLossLocationOption",
    path: "include-baby-loss-location",
    location: "body",
  };

  expect(error).toEqual(expectedError);
});

test("return expected validation error when include-baby-loss-location field is yes but baby-loss-location is empty", async () => {
  const req = {
    body: {
      "include-baby-loss-location": "yes",
      "baby-loss-location": "",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "",
    msg: "validation:missingBabyLossLocation",
    path: "baby-loss-location",
    location: "body",
  };

  expect(error).toEqual(expectedError);
});

test("return expected validation error when baby-loss-location is over 100 characters", async () => {
  const string = "A";
  const req = {
    body: {
      "include-baby-loss-location": "yes",
      "baby-loss-location": string.repeat(101),
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: string.repeat(101),
    msg: "validation:babyLossLocationTooLong",
    path: "baby-loss-location",
    location: "body",
  };

  expect(error).toEqual(expectedError);
});

test("return expected validation error when baby-loss-location has invalid characters", async () => {
  const req = {
    body: {
      "include-baby-loss-location": "yes",
      "baby-loss-location": "£%$!",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "£%$!",
    msg: "validation:patternBabyLossLocation",
    path: "baby-loss-location",
    location: "body",
  };

  expect(error).toEqual(expectedError);
});
