import { userIsBackOffice } from "../../../../azure-routes/auth";
import { firstParentContextPath } from "../../../paths/context-path";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";
import { validate } from "./validate";

const pageContent = ({ translate }) => ({
  title: translate("theirEmail.title"),
  sectionTitle: translate("section.otherParentsDetails"),
  heading: translate("theirEmail.heading"),
  body: translate("theirEmail.body", {
    path: firstParentContextPath("/contact-us"),
  }),
  backLinkText: translate("buttons:back"),
  continueButtonText: translate("buttons:continue"),
  yes: translate("yes"),
  no: translate("no"),
});

const isNavigable = (req) =>
  req.session[MAINFLOW_DATA_OBJECT_NAME]["add-another-parent"] === "yes" &&
  !userIsBackOffice(req);

const theirEmail = {
  path: "/other-parent-email-address",
  template: "their-email",
  pageContent,
  isNavigable,
  validate,
};

export { theirEmail, pageContent, isNavigable, validate };
