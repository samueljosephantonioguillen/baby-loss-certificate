import expect from "expect";

import {
  yearOfLoss,
  pageContent,
  isNavigable,
  sanitise,
  validate,
} from "./year-of-loss";
import { MAINFLOW } from "../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../tools/data-object";

test("yearOfLoss should match expected outcomes", () => {
  const expectedResults = {
    path: "/year-of-baby-loss",
    template: "year-of-loss",
    pageContent,
    isNavigable,
    sanitise,
    validate,
  };

  expect(yearOfLoss).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("yearOfLoss.title"),
    sectionTitle: translate("section.confirmEligibility"),
    heading: translate("yearOfLoss.heading"),
    yes: translate("yes"),
    legend: translate("yearOfLoss.legend"),
    no: translate("no"),
    backLinkText: translate("buttons:back"),
    continueButtonText: translate("buttons:continue"),
    hint: translate("yearOfLoss.hint"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when enter-location is yes", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "enter-location": "yes",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when enter-location is no", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "enter-location": "no",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});
