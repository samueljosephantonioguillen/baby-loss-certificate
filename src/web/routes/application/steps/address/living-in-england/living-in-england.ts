import { validate } from "./validate";

const pageContent = ({ translate }) => ({
  title: translate("livingInEngland.title"),
  sectionTitle: translate("section.confirmEligibility"),
  heading: translate("livingInEngland.heading"),
  yes: translate("yes"),
  no: translate("no"),
  continueButtonText: translate("buttons:continue"),
  backLinkText: translate("buttons:back"),
});

const behaviourForGet = () => (req, res, next) => {
  req.session.hideServiceLink = true;
  next();
};

const livingInEngland = {
  path: "/live-in-england",
  template: "living-in-england",
  pageContent,
  behaviourForGet,
  validate,
};

export { livingInEngland, pageContent, validate, behaviourForGet };
