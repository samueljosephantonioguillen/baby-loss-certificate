import { CONTEXT_PATH, PRIVACY_NOTICE } from "./paths";
import { renderStaticPage } from "./static-page-content";

const pageContent = ({ translate, req }) => ({
  title: translate("privacyNotice.title"),
  firstHeading: translate("privacyNotice.firstHeading"),
  firstBody: translate("privacyNotice.firstBody"),
  secondHeading: translate("privacyNotice.secondHeading"),
  secondBody: translate("privacyNotice.secondBody"),
  thirdHeading: translate("privacyNotice.thirdHeading"),
  thirdBody: translate("privacyNotice.thirdBody"),
  fourthHeading: translate("privacyNotice.fourthHeading"),
  fourthBody: translate("privacyNotice.fourthBody"),
  fifthHeading: translate("privacyNotice.fifthHeading"),
  fifthBody: translate("privacyNotice.fifthBody"),
  sixthHeading: translate("privacyNotice.sixthHeading"),
  sixthBody: translate("privacyNotice.sixthBody"),
  hideServiceLink: req.session.hideServiceLink,
});

const renderPrivacyNotice = (app) => {
  app.get(
    CONTEXT_PATH + PRIVACY_NOTICE,
    renderStaticPage("privacy-notice", pageContent)
  );
};

export { renderPrivacyNotice, pageContent };
