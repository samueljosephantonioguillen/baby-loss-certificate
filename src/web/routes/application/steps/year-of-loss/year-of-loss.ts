import { getFlowDataObjectName } from "../../tools/data-object";
import { sanitise } from "./sanitise";
import { validate } from "./validate";

const pageContent = ({ translate }) => ({
  title: translate("yearOfLoss.title"),
  sectionTitle: translate("section.confirmEligibility"),
  heading: translate("yearOfLoss.heading"),
  yes: translate("yes"),
  legend: translate("yearOfLoss.legend"),
  no: translate("no"),
  backLinkText: translate("buttons:back"),
  continueButtonText: translate("buttons:continue"),
  hint: translate("yearOfLoss.hint"),
});

const isNavigable = (req) =>
  req.session[getFlowDataObjectName(req.session.journeyName)][
    "enter-location"
  ] === "yes";

const yearOfLoss = {
  path: "/year-of-baby-loss",
  template: "year-of-loss",
  pageContent,
  isNavigable,
  sanitise,
  validate,
};

export { yearOfLoss, pageContent, isNavigable, sanitise, validate };
