import { StatusCodes } from "http-status-codes";

import { validate } from "./validate";

import { generateOTP } from "./generate-otp";
import { sendOtpEmail, sendOtpSms } from "../../common/notify-client";
import {
  obfuscateEmail,
  obfuscatePhoneNumber,
} from "../../common/obfuscate-contact";
import { sessionDestroyButKeepLoggedIn } from "../../common/session-destroy";
import { contextPath } from "../../../paths/context-path";
import { logger } from "../../../../../logger/logger";
import { PATH as tryAgainPath } from "../../../../error-pages/try-again";
import { TOO_MANY_OTP_SENDS_PATH } from "../../../../error-pages/too-many-otp-sends";
import { userIsBackOffice } from "../../../../azure-routes/auth";
import { getFlowDataObjectName } from "../../../tools/data-object";

const MAXIMUM_ALLOWED_OTP_SENDS = 4;

const reachedMaximumAllowedSends = (req) =>
  req.session[getFlowDataObjectName(req.session.journeyName)].otpEmailSends >=
    MAXIMUM_ALLOWED_OTP_SENDS ||
  req.session[getFlowDataObjectName(req.session.journeyName)].otpTextSends >=
    MAXIMUM_ALLOWED_OTP_SENDS;

const isEmailPresent = (req) => {
  return !!req.session[getFlowDataObjectName(req.session.journeyName)]["email"];
};

const isPhonenumberPresent = (req) => {
  return !!req.session[getFlowDataObjectName(req.session.journeyName)][
    "phone-number"
  ];
};

const pageContent = ({ translate, req }) => ({
  title: translate("getSecurityCode.title"),
  sectionTitle: translate("section.yourDetails"),
  heading: translate("getSecurityCode.heading"),
  bodyLineOne: translate("getSecurityCode.body.firstLine"),
  bodyLineTwo: translate("getSecurityCode.body.secondLine"),
  renderEmailOption: isEmailPresent(req),
  renderTextOption: isPhonenumberPresent(req),
  email: translate("getSecurityCode.email", {
    emailAddress: obfuscateEmail(
      req.session[getFlowDataObjectName(req.session.journeyName)].email
    ),
  }),
  text: translate("getSecurityCode.text", {
    phoneNumber: obfuscatePhoneNumber(
      req.session[getFlowDataObjectName(req.session.journeyName)][
        "phone-number"
      ]
    ),
  }),
  details: translate("getSecurityCode.details"),
  detailsReveal: translate("getSecurityCode.detailsReveal"),
  backLinkText: translate("buttons:back"),
  continueButtonText: translate("buttons:continue"),
});

const isNavigable = (req) => {
  return (
    (isEmailPresent(req) || isPhonenumberPresent(req)) && !userIsBackOffice(req)
  );
};

const behaviourForPost = () => async (req, res, next) => {
  req.session[getFlowDataObjectName(req.session.journeyName)].otp =
    generateOTP();
  req.session[getFlowDataObjectName(req.session.journeyName)].otpAttempts = 0;
  const dataObjectName = getFlowDataObjectName(req.session.journeyName);

  try {
    if (req.body["security-code-method"] === "email") {
      req.session[
        getFlowDataObjectName(req.session.journeyName)
      ].otpEmailSends += 1;
      if (reachedMaximumAllowedSends(req)) {
        const journeyName = req.session.journeyName;
        sessionDestroyButKeepLoggedIn(req, res);
        res.journeyName = journeyName;

        return res.redirect(contextPath(TOO_MANY_OTP_SENDS_PATH, journeyName));
      }

      await sendOtpEmail(
        req,
        req.session[dataObjectName].email,
        req.session[getFlowDataObjectName(req.session.journeyName)].otp
      );
    }

    if (req.body["security-code-method"] === "text") {
      req.session[
        getFlowDataObjectName(req.session.journeyName)
      ].otpTextSends += 1;
      if (reachedMaximumAllowedSends(req)) {
        const journeyName = req.session.journeyName;
        sessionDestroyButKeepLoggedIn(req, res);
        req.journeyName = journeyName;
        return res.redirect(contextPath(TOO_MANY_OTP_SENDS_PATH, journeyName));
      }
      await sendOtpSms(
        req,
        req.session[dataObjectName]["phone-number"],
        req.session[getFlowDataObjectName(req.session.journeyName)].otp
      );
    }
  } catch (error) {
    logger.error(
      `Failure sending ${req.body["security-code-method"]} - Response code: ${error.response.status} - ${error.message}`,
      req
    );

    if (error.response.status === StatusCodes.TOO_MANY_REQUESTS) {
      req.session[dataObjectName] = {
        ...req.session[dataObjectName],
        ...req.body,
      };

      return res.redirect(tryAgainPath);
    }

    const journeyName = req.session?.journeyName;
    sessionDestroyButKeepLoggedIn(req, res);
    return res.redirect(contextPath("/problem-with-the-service", journeyName));
  }

  next();
};

const getSecurityCode = {
  path: "/security-code",
  template: "get-security-code",
  pageContent,
  isNavigable,
  validate,
  behaviourForPost,
};

export {
  MAXIMUM_ALLOWED_OTP_SENDS,
  getSecurityCode,
  pageContent,
  isNavigable,
  validate,
  behaviourForPost,
};
