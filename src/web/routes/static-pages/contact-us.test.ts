import expect from "expect";

import { renderContactUs, pageContent } from "./contact-us";
import { CONTACT_US } from "./paths";

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;
  const req = {
    session: {
      hideServiceLink: true,
    },
  };

  const expected = {
    title: translate("contactUs.title"),
    heading: translate("contactUs.heading"),
    telephoneHeading: translate("contactUs.telephone.heading"),
    telephoneBody: translate("contactUs.telephone.body"),
    telephoneContactList: translate("contactUs.telephone.list"),
    telephoneCallChargesText: translate("contactUs.telephone.chargesText"),
    telephoneCallChargesLink: translate("contactUs.telephone.chargesLink"),
    emailHeading: translate("contactUs.email.heading"),
    emailTag: translate("contactUs.email.tag"),
    emailContact: translate("contactUs.email.contact"),
    emailBody: translate("contactUs.email.body"),
    hideServiceLink: req.session.hideServiceLink,
  };
  const result = pageContent({ translate, req });

  expect(result).toEqual(expected);
});

test("renderContactUs should render page with expected content", () => {
  const app = {
    get: jest.fn(),
  };

  const req = {
    t: (string) => string,
    session: {
      hideServiceLink: false,
    },
  };

  const res = {
    render: jest.fn(),
  };

  app.get.mockImplementation((path, callback) => {
    callback(req, res);
  });

  renderContactUs(app);

  expect(app.get).toBeCalledTimes(1);
  expect(app.get).toBeCalledWith(
    process.env.CONTEXT_PATH + CONTACT_US,
    expect.anything()
  );

  expect(res.render).toBeCalled();
  expect(res.render).toBeCalledWith(
    "static-pages/contact-us",
    pageContent({ translate: req.t, req })
  );
});
