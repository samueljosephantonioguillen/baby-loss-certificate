import expect from "expect";
import { sanitise } from "./sanitise";

test("sanitise should trim the leading zeros", () => {
  const req = {
    body: {
      "baby-date-of-loss-year": "002002",
      "baby-date-of-loss-month": "004",
      "baby-date-of-loss-day": "0027",
    },
  };

  const expectedReq = {
    body: {
      "baby-date-of-loss-year": "2002",
      "baby-date-of-loss-month": "4",
      "baby-date-of-loss-day": "27",
    },
  };

  const next = jest.fn();

  sanitise()(req, {}, next);
  expect(req).toEqual(expectedReq);
});
