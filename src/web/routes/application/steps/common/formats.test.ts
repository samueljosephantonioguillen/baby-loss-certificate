import safeRegex from "safe-regex";

import { toTitleCase, toMultiLineString, SINGLE_WORD_REGEX } from "./formats";

describe("toTitleCase", () => {
  test("should uppercase the first letter of every word", () => {
    const result = toTitleCase("10A, MY STREET, WESTON-SUPER-MARE");

    const expected = "10a, My Street, Weston-Super-Mare";

    expect(result).toBe(expected);
  });

  test("should return input when not a string", () => {
    const result = toTitleCase(100000000);

    const expected = 100000000;

    expect(result).toBe(expected);
  });
});

test("toMultiLineString should join given keys with new lines", () => {
  const result = toMultiLineString(["a", "test", "string"]);
  const expected = "a\ntest\nstring";

  expect(result).toBe(expected);
});

test("single word regex is safe", () => {
  expect(safeRegex(SINGLE_WORD_REGEX)).toBe(true);
});
