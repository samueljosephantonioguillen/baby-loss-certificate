import expect from "expect";

import { validate } from "./validate";
import { applyExpressValidation } from "../../common/test/apply-express-validation";

test("returns no validation errors when confirm-your-address field is valid", async () => {
  const req = {
    body: {
      "confirm-your-address": "yes",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("returns expected validation error when confirm-your-address field is empty", async () => {
  const req = {
    body: {
      "confirm-your-address": "",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const error = result.array()[0];

  const expectedError = {
    type: "field",
    value: "",
    msg: "validation:confirmAddress",
    path: "confirm-your-address",
    location: "body",
  };

  expect(result.array().length).toBe(1);
  expect(error).toEqual(expectedError);
});
