import expect from "expect";

import {
  notOldEnoughPds,
  pageContent,
  isNavigable,
} from "./not-old-enough-pds";
import { MAINFLOW } from "../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../tools/data-object";

test("notOldEnoughPds should match expected outcomes", () => {
  const expectedResults = {
    path: "/cannot-request-under-age",
    template: "not-eligible",
    pageContent,
    isNavigable,
  };

  expect(notOldEnoughPds).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("notOldEnoughPds.title"),
    heading: translate("notOldEnoughPds.heading"),
    bodyLineOne: translate("notOldEnoughPds.body.firstLine"),
    supportOrganisationsHeading: translate("supportOrganisations.heading"),
    supportOrganisationsBody: translate("supportOrganisations.body"),
    supportOrganisationsList: translate("supportOrganisations.listItems"),
    backLinkText: translate("buttons:back"),
  };

  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when requirement is not met", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          olderThanAgeRequirementPds: false,
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when age requirement is met", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          olderThanAgeRequirementPds: true,
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});
