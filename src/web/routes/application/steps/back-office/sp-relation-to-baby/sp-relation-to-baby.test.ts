import expect from "expect";

import {
  spRelationToBaby,
  isNavigable,
  pageContent,
  validate,
} from "./sp-relation-to-baby";
import { AGENT_ROLE, BACKOFFICE_ROLE } from "../../../constants";
import { config } from "../../../../../../config";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";

test("enterReferenceNumber should match expected outcomes", () => {
  const expectedResults = {
    path: "/their-relationship-to-baby",
    template: "their-relation-to-baby",
    pageContent,
    isNavigable,
    validate,
  };

  expect(spRelationToBaby).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("theirRelationToBaby.title"),
    sectionTitle: translate("section.otherParentsDetails"),
    heading: translate("theirRelationToBaby.heading"),
    hint: translate("theirRelationToBaby.hint"),
    mother: translate("relationToBaby.radios.mother"),
    father: translate("relationToBaby.radios.father"),
    parent: translate("relationToBaby.radios.parent"),
    surrogate: translate("relationToBaby.radios.surrogate"),
    backLinkText: translate("buttons:back"),
    continueButtonText: translate("buttons:continue"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  config.environment.USE_AUTHENTICATION = true;

  test("should return true when  backoffice_role and add-another-parent is yes", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "add-another-parent": "yes",
        },
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when agent_role and add-another-parent is yes", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "add-another-parent": "yes",
        },
        account: {
          idTokenClaims: {
            roles: [AGENT_ROLE],
          },
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when backoffice_role and add-another-parent is no", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "add-another-parent": "no",
        },
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});
