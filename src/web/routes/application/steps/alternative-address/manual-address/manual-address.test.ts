import expect from "expect";

import {
  manualAddress,
  pageContent,
  isNavigable,
  sanitise,
  validate,
} from "./manual-address";
import { AGENT_ROLE, BACKOFFICE_ROLE } from "../../../constants";
import { config } from "../../../../../../config";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";
import { buildSessionForJourney } from "../../../flow-control/test-utils/test-utils";
import { IN_PROGRESS, IN_REVIEW } from "../../../flow-control/states";

test("manualAddress should match expected outcomes", () => {
  const expectedResults = {
    path: "/enter-your-address",
    template: "manual-address",
    pageContent,
    isNavigable,
    sanitise,
    validate,
  };

  expect(manualAddress).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("manualAddress.title"),
    sectionTitle: translate("section.yourDetails"),
    heading: translate("manualAddress.heading"),
    addressLineOne: translate("manualAddress.addressLine.firstLine"),
    addressLineTwo: translate("manualAddress.addressLine.secondLine"),
    townOrCity: translate("manualAddress.townOrCity"),
    county: translate("manualAddress.county"),
    postcode: translate("manualAddress.postcode"),
    backLinkText: translate("buttons:back"),
    continueButtonText: translate("buttons:continue"),
  };

  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when agent_role and alt address and selected-address doesn't exist", () => {
    config.environment.USE_AUTHENTICATION = true;

    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "confirm-your-address": "alt",
        },
        account: {
          idTokenClaims: {
            roles: [AGENT_ROLE],
          },
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when agent_role and confirmed address and selected-address doesn't exist", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "confirm-your-address": "yes",
        },
        account: {
          idTokenClaims: {
            roles: [AGENT_ROLE],
          },
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when agent_role and alt address selected-address exists", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "confirm-your-address": "alt",
          "selected-address": "A Random Address",
        },
        account: {
          idTokenClaims: {
            roles: [AGENT_ROLE],
          },
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return true when back_office_role and selected-address doesn't exist", () => {
    config.environment.USE_AUTHENTICATION = true;

    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {},
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when back_office_role and state is IN_REVIEW", () => {
    config.environment.USE_AUTHENTICATION = true;
    const journey = { name: "MAINFLOW" };
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "selected-address": "A Random Address",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_REVIEW,
          nextAllowedPath: undefined,
        }),
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when back_office_role and selected-address does exist", () => {
    config.environment.USE_AUTHENTICATION = true;
    const journey = { name: "MAINFLOW" };
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "selected-address": "A Random Address",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when no role and selected-address doesn't exist", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {},
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});
