export const CHECK_DETAILS_URL = "/check-your-details";
export const CHECK_ANSWERS_URL = "/check-your-answers";
export const DECLARATION_URL = "/declaration";
export const REQUEST_COMPLETE_URL = "/confirmation";
export const SP_CONFIRM_DETAILS_URL = "/confirm-details";
export const CRM_SEARCH_URL = "/search-details";
