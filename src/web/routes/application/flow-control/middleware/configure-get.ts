import { prefixPath } from "../../paths/prefix-path";
import { CHECK_ANSWERS_URL, CHECK_DETAILS_URL } from "../../paths/paths";
import { stateMachine } from "../state-machine";
import { IN_DETAILS_REVIEW, IN_REVIEW } from "../states";

import { getPreviousPath } from "../get-previous-path";

const configureGet = (steps, step, journey) => (req, res, next) => {
  const { pathPrefix } = journey;

  if (stateMachine.getState(req, journey) === IN_DETAILS_REVIEW) {
    res.locals.previous = prefixPath(pathPrefix, CHECK_DETAILS_URL);
  } else if (stateMachine.getState(req, journey) === IN_REVIEW) {
    res.locals.previous = prefixPath(pathPrefix, CHECK_ANSWERS_URL);
  } else {
    res.locals.previous = getPreviousPath(steps, step, req);
  }

  next();
};

export { configureGet };
