import { check } from "express-validator";
import { translateValidationMessage } from "../../common/translate-validation-message";

const validate = () => [
  check("their-relation-to-baby")
    .not()
    .isEmpty()
    .bail()
    .withMessage(
      translateValidationMessage(
        "validation:selectSecondParentRelationToBabyOption"
      )
    ),
];

export { validate };
