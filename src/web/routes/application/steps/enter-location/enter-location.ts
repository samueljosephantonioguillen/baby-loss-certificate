import { getFlowDataObjectName } from "../../tools/data-object";
import { validate } from "./validate";

const pageContent = ({ translate }) => ({
  title: translate("enterLocation.title"),
  sectionTitle: translate("section.confirmEligibility"),
  heading: translate("enterLocation.heading"),
  yes: translate("yes"),
  no: translate("no"),
  emptySelection: translate("validation:selectYesOrNo"),
  continueButtonText: translate("buttons:continue"),
  backLinkText: translate("buttons:back"),
});

const isNavigable = (req) =>
  req.session[getFlowDataObjectName(req.session.journeyName)][
    "living-in-england"
  ] === "yes";

const enterLocation = {
  path: "/living-in-england-at-time-of-baby-loss",
  template: "enter-location",
  pageContent,
  isNavigable,
  validate,
};

export { enterLocation, pageContent, isNavigable, validate };
