const isEmpty = jest.fn();

jest.mock("express-validator", () => ({
  validationResult: () => ({
    isEmpty: isEmpty,
  }),
}));

import expect from "expect";

import {
  emailUpdate,
  pageContent,
  validate,
  behaviourForPost,
} from "./email-update";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../tools/data-object";
import { MAINFLOW } from "../../journey-definitions";

test("emailUpdate should match expected outcomes", () => {
  const expectedResults = {
    path: "/email-update",
    template: "email-update",
    pageContent,
    validate,
    behaviourForPost,
  };

  expect(emailUpdate).toEqual(expectedResults);
});

test("pageContent should return expected results for single parent journey", () => {
  const req = {
    session: {
      journeyName: MAINFLOW.name,
      [MAINFLOW_DATA_OBJECT_NAME]: {
        "add-another-parent": "no",
      },
    },
  };
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("emailUpdate.title"),
    sectionTitle: translate("section.yourDetails"),
    heading: translate("emailUpdate.headingSingle"),
    body: translate("emailUpdate.body"),
    continueButtonText: translate("buttons:continue"),
    yes: translate("yes"),
    no: translate("no"),
  };
  const result = pageContent({ translate, req });

  expect(result).toEqual(expected);
});

test("pageContent should return expected results for double parent journey", () => {
  const req = {
    session: {
      journeyName: MAINFLOW.name,
      [MAINFLOW_DATA_OBJECT_NAME]: {
        "add-another-parent": "yes",
      },
    },
  };
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("emailUpdate.title"),
    sectionTitle: translate("section.yourDetails"),
    heading: translate("emailUpdate.headingDouble"),
    body: translate("emailUpdate.body"),
    continueButtonText: translate("buttons:continue"),
    yes: translate("yes"),
    no: translate("no"),
  };
  const result = pageContent({ translate, req });

  expect(result).toEqual(expected);
});

describe("behaviourForPost", () => {
  test("should return expected data when no option is selected", () => {
    isEmpty.mockReturnValue(true);

    const req = {
      body: {
        "applicant-add-email": "no",
      },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "applicant-email": "test@test.com",
          "applicant-email-confirm": "test@test.com",
        },
      },
    };
    const res = {};
    const next = jest.fn();

    const expectedReq = {
      body: {
        "applicant-add-email": "no",
      },
      session: { [MAINFLOW_DATA_OBJECT_NAME]: {}, journeyName: MAINFLOW.name },
    };

    behaviourForPost()(req, res, next);
    expect(req).toEqual(expectedReq);
    expect(next).toBeCalledTimes(1);
  });

  test("should call when validation error", () => {
    isEmpty.mockReturnValue(false);

    const req = {};
    const res = {};
    const next = jest.fn();

    behaviourForPost()(req, res, next);
    expect(next).toBeCalledTimes(1);
  });
});
