import { stateMachine } from "../../../flow-control/state-machine";
import { IN_REVIEW, IN_REVIEW_PROGRESS } from "../../../flow-control/states";
import { getFlowDataObjectName } from "../../../tools/data-object";

import { validate } from "./validate";

const pageContent = ({ translate }) => ({
  title: translate("selectBabySex.title"),
  sectionTitle: translate("section.certificateDetails"),
  heading: translate("selectBabySex.heading"),
  male: translate("selectBabySex.male"),
  female: translate("selectBabySex.female"),
  continueButtonText: translate("buttons:continue"),
  backLinkText: translate("buttons:back"),
});

const isNavigable = (req) =>
  req.session[getFlowDataObjectName(req.session.journeyName)][
    "include-baby-sex"
  ] === "yes";

const behaviourForPost = (config, journey) => (req, res, next) => {
  if (stateMachine.getState(req, journey) === IN_REVIEW_PROGRESS) {
    stateMachine.setState(IN_REVIEW, req, journey);
  }

  next();
};

const selectBabySex = {
  path: "/enter-baby-sex",
  template: "select-baby-sex",
  pageContent,
  isNavigable,
  validate,
  behaviourForPost,
};

export { selectBabySex, pageContent, isNavigable, validate, behaviourForPost };
