import { path } from "ramda";
import { toBoolean } from "./to-boolean";
import * as dotEnv from "dotenv";

dotEnv.config();

export const environment = {
  APP_BASE_URL: process.env.APP_BASE_URL,
  NODE_ENV: process.env.NODE_ENV,
  USE_UNSECURE_COOKIE: path(["env", "USE_UNSECURE_COOKIE"], process) === "true",
  LOG_LEVEL: process.env.LOG_LEVEL ?? "debug",
  LOGGING_FILE_NAME: process.env.LOGGING_FILE_NAME ?? "application-%DATE%.log",
  LOGGING_FILE_MAX_SIZE: process.env.LOGGING_FILE_MAX_SIZE ?? "50m",
  LOGGING_FILE_MAX_FILES: process.env.LOGGING_FILE_MAX_FILES ?? "28d",
  APP_VERSION: process.env.APP_VERSION ?? "unknown",
  GA_TRACKING_ID: process.env.GA_TRACKING_ID,
  PDS_URI: process.env.PDS_URI,
  GOV_NOTIFY_API_KEY: process.env.GOV_NOTIFY_API_KEY,
  EMAIL_OTP_TEMPLATE_ID: process.env.EMAIL_OTP_TEMPLATE_ID,
  SMS_OTP_TEMPLATE_ID: process.env.SMS_OTP_TEMPLATE_ID,
  BLC_EMAIL_UNSUCCESSFUL_APPLICATION_TEMPLATE_ID:
    process.env.BLC_EMAIL_UNSUCCESSFUL_APPLICATION_TEMPLATE_ID,
  START_PAGE_URL: process.env.START_PAGE_URL,
  SECOND_PARENT_FLOW_URL: process.env.SECOND_PARENT_FLOW_URL,
  XEROX_QUEUE_URL: process.env.XEROX_QUEUE_URL,
  INCOMPLETE_APPLICATION_QUEUE_URL:
    process.env.INCOMPLETE_APPLICATION_QUEUE_URL,
  PDS_OAUTH_TOKEN_SECRET_NAME: process.env.PDS_OAUTH_TOKEN_SECRET_NAME,
  AWS_CLIENT_ENDPOINT: process.env.AWS_CLIENT_ENDPOINT ?? undefined,
  OS_PLACES_URI: process.env.OS_PLACES_URI,
  OS_PLACES_API_KEY: process.env.OS_PLACES_API_KEY,
  DB_USERNAME: process.env.DB_USERNAME,
  DB_PASSWORD: process.env.DB_PASSWORD,
  DB_SSL_CA: process.env.DB_SSL_CA,
  DB_URL: process.env.DB_URL,
  DB_NAME: process.env.DB_NAME,
  DB_PORT: process.env.DB_PORT,
  CONTEXT_PATH: process.env.CONTEXT_PATH ?? "",
  SP_CONTEXT_PATH: process.env.SP_CONTEXT_PATH ?? "",
  CRM_CONTEXT_PATH: process.env.CRM_CONTEXT_PATH ?? "",

  USE_AUTHENTICATION: toBoolean(process.env.USE_AUTHENTICATION) || false,
  MSALCONFIG: {
    auth: {
      clientId: `${process.env.CLIENT_ID}`,
      authority: `${
        process.env.CLOUD_INSTANCE ?? "https://login.microsoftonline.com/"
      }${process.env.TENANT_ID}`,
      clientSecret: process.env.CLIENT_SECRET,
    },
  },
  REDIRECT_URI: process.env.REDIRECT_URI,
  POST_LOGOUT_REDIRECT_URI: process.env.POST_LOGOUT_REDIRECT_URI,
};
