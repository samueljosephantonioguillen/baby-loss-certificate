import expect from "expect";

import {
  checkYourAnswers,
  pageContent,
  behaviourForGet,
  behaviourForPost,
  isMissingRequiredFields,
  getPostalAddress,
} from "./check-your-answers";

import { START } from "../../../start";
import { DECLARATION_URL } from "../../paths/paths";
import { contextPath } from "../../paths/context-path";
import { toLongLocaleDateString } from "../common/format-date";
import { buildSessionForJourney } from "../../flow-control/test-utils/test-utils";
import { IN_PROGRESS, IN_REVIEW } from "../../flow-control/states";
import { AGENT_ROLE, BACKOFFICE_ROLE } from "../../constants";
import { environment } from "../../../../../config/environment";
import { MAINFLOW } from "../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../tools/data-object";

test("checkYourAnswers should match expected outcomes", () => {
  const expectedResults = {
    path: "/check-your-answers",
    template: "check-your-answers",
    pageContent,
    behaviourForGet,
    behaviourForPost,
  };

  expect(checkYourAnswers).toEqual(expectedResults);
});

describe("pageContent", () => {
  test("should return expected results with all fields", () => {
    const translate = (string, object?) => `${string}${object}`;

    const applicant = {
      "date-of-birth": "2000-02-02",
      "address-line-1": "Alan Jeffery Engineering",
      "address-line-2": "1 Valley Road",
      "town-or-city": "Plymouth",
      county: "Devon",
      postcode: "PL7 1RF",
      "first-name": "Jane",
      "last-name": "Doe",
      "relation-to-baby": "Surrogate",
      "their-first-name": "Jane",
      "their-last-name": "Doe",
      "add-another-parent": "yes",
      "their-date-of-birth-day": "1",
      "their-date-of-birth-month": "1",
      "their-date-of-birth-year": "1999",
      "their-relation-to-baby": "Surrogate",
      "baby-name": "Baby Name",
      "baby-sex": "Sex",
      "baby-date-of-loss": "2023-06-08",
      "baby-loss-location": "Birmingham",
    };
    const req = { session: { applicant, journeyName: MAINFLOW.name } };

    const expected = {
      title: translate("checkYourAnswers.title"),
      heading: translate("checkYourAnswers.heading"),
      yourDetails: translate("checkYourAnswers.yourDetails"),
      checkYourDetails: translate("checkYourAnswers.checkYourDetails", {
        yourName: `${req.session[MAINFLOW_DATA_OBJECT_NAME]["first-name"]} ${req.session[MAINFLOW_DATA_OBJECT_NAME]["last-name"]}`,
        postalAddress: getPostalAddress(req),
        emailAddress: req.session[MAINFLOW_DATA_OBJECT_NAME]["applicant-email"],
      }),
      theirDetails: translate("checkYourAnswers.theirDetails"),
      checkTheirDetails: translate("checkYourAnswers.checkTheirDetails", {
        theirDateOfBirth: "1 January 1999",
        emailAddress: req.session[MAINFLOW_DATA_OBJECT_NAME]["their-email"],
      }),
      certificateDetails: translate("checkYourAnswers.certificateDetails"),
      firstParent: translate("checkYourAnswers.firstParent"),
      checkFirstParent: translate(
        "checkYourAnswers.checkCertificateDetails.checkFirstParent",
        {
          yourName: `${req.session[MAINFLOW_DATA_OBJECT_NAME]["first-name"]} ${req.session[MAINFLOW_DATA_OBJECT_NAME]["last-name"]}`,
          yourRelation:
            req.session[MAINFLOW_DATA_OBJECT_NAME]["relation-to-baby"],
        }
      ),
      secondParent: translate("checkYourAnswers.secondParent"),
      checkSecondParent: translate(
        "checkYourAnswers.checkCertificateDetails.checkSecondParent",
        {
          theirName: translate("checkYourAnswers.otherParentNameTbc"),
          theirRelation:
            req.session[MAINFLOW_DATA_OBJECT_NAME]["their-relation-to-baby"],
          secondParentAdded: "yes",
        }
      ),
      babyDetails: translate("checkYourAnswers.babyDetails"),
      checkBabyDetails: translate(
        "checkYourAnswers.checkCertificateDetails.checkBabyDetails",
        {
          babyName: req.session[MAINFLOW_DATA_OBJECT_NAME]["baby-name"],
          babySex: req.session[MAINFLOW_DATA_OBJECT_NAME]["baby-sex"],
          babyLossDate: toLongLocaleDateString(
            req.session[MAINFLOW_DATA_OBJECT_NAME]["baby-date-of-loss-day"],
            req.session[MAINFLOW_DATA_OBJECT_NAME]["baby-date-of-loss-month"],
            req.session[MAINFLOW_DATA_OBJECT_NAME]["baby-date-of-loss-year"]
          ),
          babyLossLocation:
            req.session[MAINFLOW_DATA_OBJECT_NAME]["baby-loss-location"],
        }
      ),
      change: translate("change"),
      button: translate("checkYourAnswers.button"),
      backLinkText: translate("buttons:back"),
      backOfficeCheckYourDetails: translate(
        "checkYourAnswers.backOfficeCheckYourDetails",
        {
          yourName: `${req.session[MAINFLOW_DATA_OBJECT_NAME]["first-name"]} ${req.session[MAINFLOW_DATA_OBJECT_NAME]["last-name"]}`,
          postalAddress: getPostalAddress(req),
          emailAddress:
            req.session[MAINFLOW_DATA_OBJECT_NAME]["applicant-email"],
        }
      ),
      backOfficeCheckTheirDetails: translate(
        "checkYourAnswers.backOfficeCheckTheirDetails",
        {
          theirDateOfBirth: "1 January 1999",
        }
      ),
      backOfficeCheckFirstParent: translate(
        "checkYourAnswers.checkCertificateDetails.backOfficeCheckFirstParent",
        {
          yourName: `${req.session[MAINFLOW_DATA_OBJECT_NAME]["first-name"]} ${req.session[MAINFLOW_DATA_OBJECT_NAME]["last-name"]}`,
          yourRelation:
            req.session[MAINFLOW_DATA_OBJECT_NAME]["relation-to-baby"],
        }
      ),
      backOfficeCheckSecondParent: translate(
        "checkYourAnswers.checkCertificateDetails.backOfficeCheckSecondParent",
        {
          theirName: "",
          theirRelation:
            req.session[MAINFLOW_DATA_OBJECT_NAME]["their-relation-to-baby"],
        }
      ),
    };
    const result = pageContent({ translate, req });

    expect(result).toEqual(expected);
  });

  test("should return expected results with only required fields", () => {
    const translate = (string, object?) => `${string}${object}`;

    const applicant = {
      "date-of-birth": "2000-02-02",
      postcode: "PL7 1RF",
      "first-name": "Jane",
      "last-name": "Doe",
      "relation-to-baby": "surrogate",
    };
    const req = { session: { applicant, journeyName: MAINFLOW.name } };

    const expected = {
      title: translate("checkYourAnswers.title"),
      heading: translate("checkYourAnswers.heading"),
      yourDetails: translate("checkYourAnswers.yourDetails"),
      checkYourDetails: translate("checkYourAnswers.checkYourDetails", {
        yourName: `${req.session[MAINFLOW_DATA_OBJECT_NAME]["first-name"]} ${req.session[MAINFLOW_DATA_OBJECT_NAME]["last-name"]}`,
        postalAddress: getPostalAddress(req),
        emailAddress: req.session[MAINFLOW_DATA_OBJECT_NAME]["applicant-email"],
      }),
      theirDetails: translate("checkYourAnswers.theirDetails"),
      checkTheirDetails: translate("checkYourAnswers.checkTheirDetails", {}),
      certificateDetails: translate("checkYourAnswers.certificateDetails"),
      firstParent: translate("checkYourAnswers.firstParent"),
      checkFirstParent: translate(
        "checkYourAnswers.checkCertificateDetails.checkFirstParent",
        {
          yourName: `${req.session[MAINFLOW_DATA_OBJECT_NAME]["first-name"]} ${req.session[MAINFLOW_DATA_OBJECT_NAME]["last-name"]}`,
          yourRelation:
            req.session[MAINFLOW_DATA_OBJECT_NAME]["relation-to-baby"],
        }
      ),
      secondParent: translate("checkYourAnswers.secondParent"),
      checkSecondParent: translate(
        "checkYourAnswers.checkCertificateDetails.checkSecondParent",
        {
          theirName: "",
          theirRelation: "",
          secondParentAdded: "no",
        }
      ),
      babyDetails: translate("checkYourAnswers.babyDetails"),
      checkBabyDetails: translate(
        "checkYourAnswers.checkCertificateDetails.checkBabyDetails",
        {
          babyName: "",
          babySex: "",
          babyLossDate: "",
          babyLossLocation: "",
        }
      ),
      change: translate("change"),
      button: translate("checkYourAnswers.button"),
      backLinkText: translate("buttons:back"),
      backOfficeCheckYourDetails: translate(
        "checkYourAnswers.backOfficeCheckYourDetails",
        {
          yourName: `${req.session[MAINFLOW_DATA_OBJECT_NAME]["first-name"]} ${req.session[MAINFLOW_DATA_OBJECT_NAME]["last-name"]}`,
          postalAddress: getPostalAddress(req),
          emailAddress:
            req.session[MAINFLOW_DATA_OBJECT_NAME]["applicant-email"],
        }
      ),
      backOfficeCheckTheirDetails: translate(
        "checkYourAnswers.backOfficeCheckTheirDetails",
        {}
      ),
      backOfficeCheckFirstParent: translate(
        "checkYourAnswers.checkCertificateDetails.backOfficeCheckFirstParent",
        {
          yourName: `${req.session[MAINFLOW_DATA_OBJECT_NAME]["first-name"]} ${req.session[MAINFLOW_DATA_OBJECT_NAME]["last-name"]}`,
          yourRelation:
            req.session[MAINFLOW_DATA_OBJECT_NAME]["relation-to-baby"],
        }
      ),
      backOfficeCheckSecondParent: translate(
        "checkYourAnswers.checkCertificateDetails.backOfficeCheckSecondParent",
        {
          theirName: "",
          theirRelation: "",
        }
      ),
    };
    const result = pageContent({ translate, req });

    expect(result).toEqual(expected);
  });

  test("should return expected results with alternative address", () => {
    const translate = (string, object?) => `${string}${object}`;

    const applicant = {
      "confirm-your-address": "alt",
      "date-of-birth": "2000-02-02",
      "address-line-1-ccs": "Cornerblock",
      "address-line-3-ccs": "Birmingham",
      "postcode-ccs": "B3 2DX",
      "address-line-1": "Upper Crescent",
      "address-line-3": "Belfast",
      postcode: "BT7 1EF",
      "first-name": "Jane",
      "last-name": "Doe",
      "relation-to-baby": "surrogate",
    };
    const req = { session: { applicant, journeyName: MAINFLOW.name } };

    const expected = {
      title: translate("checkYourAnswers.title"),
      heading: translate("checkYourAnswers.heading"),
      yourDetails: translate("checkYourAnswers.yourDetails"),
      checkYourDetails: translate("checkYourAnswers.checkYourDetails", {
        yourName: `${req.session[MAINFLOW_DATA_OBJECT_NAME]["first-name"]} ${req.session[MAINFLOW_DATA_OBJECT_NAME]["last-name"]}`,
        postalAddress: getPostalAddress(req),
        emailAddress: req.session[MAINFLOW_DATA_OBJECT_NAME]["applicant-email"],
      }),
      theirDetails: translate("checkYourAnswers.theirDetails"),
      checkTheirDetails: translate("checkYourAnswers.checkTheirDetails", {
        theirDateOfBirth: "1 January 1999",
      }),
      certificateDetails: translate("checkYourAnswers.certificateDetails"),
      firstParent: translate("checkYourAnswers.firstParent"),
      checkFirstParent: translate(
        "checkYourAnswers.checkCertificateDetails.checkFirstParent",
        {
          yourName: `${req.session[MAINFLOW_DATA_OBJECT_NAME]["first-name"]} ${req.session[MAINFLOW_DATA_OBJECT_NAME]["last-name"]}`,
          yourRelation:
            req.session[MAINFLOW_DATA_OBJECT_NAME]["relation-to-baby"],
        }
      ),
      secondParent: translate("checkYourAnswers.secondParent"),
      checkSecondParent: translate(
        "checkYourAnswers.checkCertificateDetails.checkSecondParent",
        {
          theirName: "",
          theirRelation: "",
          secondParentAdded: "no",
        }
      ),
      babyDetails: translate("checkYourAnswers.babyDetails"),
      checkBabyDetails: translate(
        "checkYourAnswers.checkCertificateDetails.checkBabyDetails",
        {
          babyName: "",
          babySex: "",
          babyLossDate: "",
          babyLossLocation: "",
        }
      ),
      change: translate("change"),
      button: translate("checkYourAnswers.button"),
      backLinkText: translate("buttons:back"),
      backOfficeCheckYourDetails: translate(
        "checkYourAnswers.backOfficeCheckYourDetails",
        {
          yourName: `${req.session[MAINFLOW_DATA_OBJECT_NAME]["first-name"]} ${req.session[MAINFLOW_DATA_OBJECT_NAME]["last-name"]}`,
          postalAddress: getPostalAddress(req),
          emailAddress:
            req.session[MAINFLOW_DATA_OBJECT_NAME]["applicant-email"],
        }
      ),
      backOfficeCheckTheirDetails: translate(
        "checkYourAnswers.backOfficeCheckTheirDetails",
        {
          theirDateOfBirth: "1 January 1999",
        }
      ),
      backOfficeCheckFirstParent: translate(
        "checkYourAnswers.checkCertificateDetails.backOfficeCheckFirstParent",
        {
          yourName: `${req.session[MAINFLOW_DATA_OBJECT_NAME]["first-name"]} ${req.session[MAINFLOW_DATA_OBJECT_NAME]["last-name"]}`,
          yourRelation:
            req.session[MAINFLOW_DATA_OBJECT_NAME]["relation-to-baby"],
        }
      ),
      backOfficeCheckSecondParent: translate(
        "checkYourAnswers.checkCertificateDetails.backOfficeCheckSecondParent",
        {
          theirName: "",
          theirRelation: "",
        }
      ),
    };
    const result = pageContent({ translate, req });

    expect(result).toEqual(expected);
  });
});

test("should return expected results for back office", () => {
  environment.USE_AUTHENTICATION = true;
  const translate = (string, object?) => `${string}${object}`;

  const applicant = {
    "date-of-birth": "2000-02-02",
    postcode: "PL7 1RF",
    "first-name": "Jane",
    "last-name": "Doe",
    "relation-to-baby": "surrogate",
    "their-first-name": "Jane",
    "their-last-name": "Doe",
    "add-another-parent": "yes",
    "their-date-of-birth-day": "1",
    "their-date-of-birth-month": "1",
    "their-date-of-birth-year": "1999",
    "their-relation-to-baby": "Surrogate",
  };

  const req = {
    session: {
      applicant,
      journeyName: MAINFLOW.name,
      account: {
        idTokenClaims: {
          roles: [BACKOFFICE_ROLE],
        },
      },
    },
  };

  const expected = {
    title: translate("checkYourAnswers.title"),
    heading: translate("checkYourAnswers.heading"),
    yourDetails: translate("checkYourAnswers.yourDetails"),
    checkYourDetails: translate("checkYourAnswers.checkYourDetails", {
      yourName: `${req.session[MAINFLOW_DATA_OBJECT_NAME]["first-name"]} ${req.session[MAINFLOW_DATA_OBJECT_NAME]["last-name"]}`,
      postalAddress: getPostalAddress(req),
      emailAddress: req.session[MAINFLOW_DATA_OBJECT_NAME]["applicant-email"],
    }),
    theirDetails: translate("checkYourAnswers.theirDetails"),
    checkTheirDetails: translate("checkYourAnswers.checkTheirDetails", {}),
    certificateDetails: translate("checkYourAnswers.certificateDetails"),
    firstParent: translate("checkYourAnswers.firstParent"),
    checkFirstParent: translate(
      "checkYourAnswers.checkCertificateDetails.checkFirstParent",
      {
        yourName: `${req.session[MAINFLOW_DATA_OBJECT_NAME]["first-name"]} ${req.session[MAINFLOW_DATA_OBJECT_NAME]["last-name"]}`,
        yourRelation:
          req.session[MAINFLOW_DATA_OBJECT_NAME]["relation-to-baby"],
      }
    ),
    secondParent: translate("checkYourAnswers.secondParent"),
    checkSecondParent: translate(
      "checkYourAnswers.checkCertificateDetails.checkSecondParent",
      {
        theirName: `${req.session[MAINFLOW_DATA_OBJECT_NAME]["their-first-name"]} ${req.session[MAINFLOW_DATA_OBJECT_NAME]["their-last-name"]}`,
        theirRelation: "",
        secondParentAdded: "yes",
      }
    ),
    babyDetails: translate("checkYourAnswers.babyDetails"),
    checkBabyDetails: translate(
      "checkYourAnswers.checkCertificateDetails.checkBabyDetails",
      {
        babyName: "",
        babySex: "",
        babyLossDate: "",
        babyLossLocation: "",
      }
    ),
    change: translate("change"),
    button: translate("checkYourAnswers.button"),
    backLinkText: translate("buttons:back"),
    backOfficeCheckYourDetails: translate(
      "checkYourAnswers.backOfficeCheckYourDetails",
      {
        yourName: `${req.session[MAINFLOW_DATA_OBJECT_NAME]["first-name"]} ${req.session[MAINFLOW_DATA_OBJECT_NAME]["last-name"]}`,
        postalAddress: getPostalAddress(req),
        emailAddress: req.session[MAINFLOW_DATA_OBJECT_NAME]["applicant-email"],
      }
    ),
    backOfficeCheckTheirDetails: translate(
      "checkYourAnswers.backOfficeCheckTheirDetails",
      {}
    ),
    backOfficeCheckFirstParent: translate(
      "checkYourAnswers.checkCertificateDetails.backOfficeCheckFirstParent",
      {
        yourName: `${req.session[MAINFLOW_DATA_OBJECT_NAME]["first-name"]} ${req.session[MAINFLOW_DATA_OBJECT_NAME]["last-name"]}`,
        yourRelation:
          req.session[MAINFLOW_DATA_OBJECT_NAME]["relation-to-baby"],
      }
    ),
    backOfficeCheckSecondParent: translate(
      "checkYourAnswers.checkCertificateDetails.backOfficeCheckSecondParent",
      {
        theirName: `${req.session[MAINFLOW_DATA_OBJECT_NAME]["their-first-name"]} ${req.session[MAINFLOW_DATA_OBJECT_NAME]["their-last-name"]}`,
        theirRelation:
          req.session[MAINFLOW_DATA_OBJECT_NAME]["their-relation-to-baby"],
      }
    ),
  };
  const result = pageContent({ translate, req });

  expect(result).toEqual(expected);
});

describe("behaviourForGet", () => {
  const config = {};
  const journey = { name: MAINFLOW.name };

  test("should call next when required fields are populated and set state to IN_REVIEW", () => {
    const req = {
      session: {
        journeyName: journey.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "date-of-birth": "2000-02-02",
          "address-line-1": "Alan Jeffery Engineering",
          "town-or-city": "Plymouth",
          postcode: "PL7 1RF",
          "first-name": "Jane",
          "last-name": "Doe",
          "relation-to-baby": "surrogate",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };

    const res = { redirect: jest.fn() };
    const next = jest.fn();

    behaviourForGet(config, journey)(req, res, next);

    const expectedSession = {
      ...req.session,
      ...buildSessionForJourney({
        journeyName: journey.name,
        state: IN_REVIEW,
        nextAllowedPath: "/check-your-answers",
      }),
    };

    expect(req.session).toEqual(expectedSession);
    expect(res.redirect).toBeCalledTimes(0);
    expect(next).toBeCalledTimes(1);
  });

  test("should redirect to service start page when missing required fields", () => {
    const applicant = {
      "date-of-birth": "2000-02-02",
      "address-line-1": "Alan Jeffery Engineering",
      "town-or-city": "Plymouth",
      postcode: "",
      "first-name": "Jane",
      "last-name": "Doe",
      "relation-to-baby": "surrogate",
    };
    const req = { session: { applicant, journeyName: MAINFLOW.name } };

    const res = { redirect: jest.fn() };
    const next = jest.fn();

    behaviourForGet(config, journey)(req, res, next);

    expect(res.redirect).toBeCalledTimes(1);
    expect(res.redirect).toBeCalledWith(contextPath(START, MAINFLOW.name));
    expect(next).toBeCalledTimes(0);
  });
});

test(`behaviourForPost should redirect to ${DECLARATION_URL}`, () => {
  const config = {};
  const journey = { name: MAINFLOW.name };

  const req = {
    session: {
      ...buildSessionForJourney({
        journeyName: journey.name,
        state: IN_REVIEW,
        nextAllowedPath: undefined,
      }),
    },
  };
  const res = { redirect: jest.fn() };

  behaviourForPost(config, journey)(req, res);

  expect(res.redirect).toBeCalledWith(
    process.env.CONTEXT_PATH + DECLARATION_URL
  );
  expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
  expect(req.session.journeys[journey.name].nextAllowedStep).toEqual(
    process.env.CONTEXT_PATH + DECLARATION_URL
  );
});

describe("isMissingRequiredFields", () => {
  test("should return false when all feilds are set on citizen flow", () => {
    environment.USE_AUTHENTICATION = true;
    const applicant = {
      "date-of-birth": "2000-02-02",
      "address-line-1": "Alan Jeffery Engineering",
      "town-or-city": "Plymouth",
      postcode: "PL7 1RF",
      "first-name": "Jane",
      "last-name": "Doe",
      "relation-to-baby": "surrogate",
    };
    const req = {
      session: {
        applicant,
        account: {
          idTokenClaims: {
            roles: [],
          },
        },
      },
    };

    const result = isMissingRequiredFields(applicant, req);

    expect(result).toEqual(false);
  });

  test("should return false when all feilds are set on backoffice flow", () => {
    environment.USE_AUTHENTICATION = true;
    const applicant = {
      "date-of-birth": "2000-02-02",
      "address-line-1": "Alan Jeffery Engineering",
      "town-or-city": "Plymouth",
      "postcode-ccs": "PL7 1RF",
      "first-name": "Jane",
      "last-name": "Doe",
      "relation-to-baby": "surrogate",
    };
    const req = {
      session: {
        applicant,
        account: {
          idTokenClaims: {
            roles: [BACKOFFICE_ROLE],
          },
        },
      },
    };

    const result = isMissingRequiredFields(applicant, req);

    expect(result).toEqual(false);
  });

  test("should return false when all feilds are set on agent non alt address flow", () => {
    environment.USE_AUTHENTICATION = true;
    const applicant = {
      "date-of-birth": "2000-02-02",
      "address-line-1": "Alan Jeffery Engineering",
      "town-or-city": "Plymouth",
      postcode: "PL7 1RF",
      "first-name": "Jane",
      "last-name": "Doe",
      "relation-to-baby": "surrogate",
      "confirm-your-address": "yes",
    };
    const req = {
      session: {
        applicant,
        account: {
          idTokenClaims: {
            roles: [AGENT_ROLE],
          },
        },
      },
    };

    const result = isMissingRequiredFields(applicant, req);

    expect(result).toEqual(false);
  });

  test("should return false when all feilds are set on agent alt address flow", () => {
    environment.USE_AUTHENTICATION = true;
    const applicant = {
      "date-of-birth": "2000-02-02",
      "address-line-1": "Alan Jeffery Engineering",
      "town-or-city": "Plymouth",
      "postcode-ccs": "PL7 1RF",
      "first-name": "Jane",
      "last-name": "Doe",
      "relation-to-baby": "surrogate",
      "confirm-your-address": "alt",
    };
    const req = {
      session: {
        applicant,
        account: {
          idTokenClaims: {
            roles: [AGENT_ROLE],
          },
        },
      },
    };

    const result = isMissingRequiredFields(applicant, req);

    expect(result).toEqual(false);
  });
});
