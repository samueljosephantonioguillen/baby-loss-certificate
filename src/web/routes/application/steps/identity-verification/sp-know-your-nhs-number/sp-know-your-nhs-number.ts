import { SECOND_PARENT_DATA_OBJECT_NAME } from "../../../tools/data-object";
import {
  pageContent,
  validate,
  behaviourForGet,
  behaviourForPost,
} from "../know-your-nhs-number/know-your-nhs-number";

const isNavigable = (req) =>
  !!req.session[SECOND_PARENT_DATA_OBJECT_NAME]?.["application-id"];

const spKnowYourNhsNumber = {
  path: "/do-you-know-your-nhs-number",
  template: "know-your-nhs-number",
  pageContent,
  isNavigable,
  validate,
  behaviourForGet,
  behaviourForPost,
};

export {
  spKnowYourNhsNumber,
  pageContent,
  isNavigable,
  validate,
  behaviourForGet,
  behaviourForPost,
};
