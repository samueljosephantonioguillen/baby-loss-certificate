import { getUserName, getUserCypher, isUserAuthenticated } from "./azure-utils";

describe("getUserName", () => {
  test("returns lowercase username when session is provided", () => {
    const session = { account: { username: "USER@EXAMPLE.COM" } };
    const result = getUserName(session);
    expect(result).toBe("user@example.com");
  });

  test("returns undefined when session.account is not provided", () => {
    const session = {};
    const result = getUserName(session);
    expect(result).toBeUndefined();
  });

  test("returns undefined when session.account.username is not provided", () => {
    const session = { account: {} };
    const result = getUserName(session);
    expect(result).toBeUndefined();
  });
});

describe("getUserCypher", () => {
  test("returns the correct cypher when given a valid session", () => {
    const session = {
      account: {
        username: "john.doe@example.com",
      },
    };
    expect(getUserCypher(session)).toBe("JOHN.DOE");
  });

  test("throws an error if account or username is missing", () => {
    expect(() => {
      getUserCypher({});
    }).toThrow("Unable to derive cypher from username");

    expect(() => {
      getUserCypher({ account: {} });
    }).toThrow("Unable to derive cypher from username");

    expect(() => {
      getUserCypher({ account: { username: "" } });
    }).toThrow("Unable to derive cypher from username");
  });
});

describe("isUserAuthenticated", () => {
  test("returns true when the user is authenticated", () => {
    const req = {
      session: {
        isAuthenticated: true,
      },
    };
    expect(isUserAuthenticated(req)).toBe(true);
  });

  test("returns false when the user is not authenticated", () => {
    const req = {
      session: {},
    };
    expect(isUserAuthenticated(req)).toBe(false);
  });
});
