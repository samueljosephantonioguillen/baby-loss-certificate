import { MAINFLOW } from "../application/journey-definitions";
import {
  firstParentContextPath,
  secondParentContextPath,
} from "../application/paths/context-path";
import { CONTEXT_PATH } from "../static-pages/paths";
import {
  registerTooManyAttempts,
  pageContent,
  get,
  TOO_MANY_ATTEMPTS_PATH,
} from "./too-many-attempts";

const app = {
  ...jest.requireActual("express"),
  get: jest.fn(),
};

const req = {
  originalUrl: "/" + CONTEXT_PATH,
  session: { journeyName: MAINFLOW.name },
  t: jest.fn(),
};

const res = {
  render: jest.fn(),
};

test("registerTooManyAttempts should register get and post routes with expected callbacks", () => {
  app.get.mockImplementation((path, callback) => {
    callback(req, res);
  });

  registerTooManyAttempts(app);

  expect(app.get).toBeCalledTimes(1);
  expect(app.get).toBeCalledWith(
    [
      firstParentContextPath(TOO_MANY_ATTEMPTS_PATH),
      secondParentContextPath(TOO_MANY_ATTEMPTS_PATH),
    ],
    get
  );
});

test("get should render expected content", () => {
  get(req, res);

  expect(res.render).toBeCalled();
  expect(res.render).toBeCalledWith("too-many-attempts", {
    ...pageContent({ translate: req.t, serviceStartPage: "" }),
  });
});
