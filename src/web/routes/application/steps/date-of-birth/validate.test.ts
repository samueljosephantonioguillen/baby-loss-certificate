import expect from "expect";
import { validate } from "./validate";
import { applyExpressValidation } from "../common/test/apply-express-validation";
import { MAINFLOW } from "../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../tools/data-object";

let req;

beforeEach(() => {
  req = {
    body: {
      "date-of-birth-day": 1,
      "date-of-birth-month": 1,
      "date-of-birth-year": 2000,
    },
    session: {
      journeyName: MAINFLOW.name,
      [MAINFLOW_DATA_OBJECT_NAME]: {
        "year-of-loss": 2020,
      },
    },
  };
});

describe("validateDateOfBirth", () => {
  test("should not throw any errors when valid dob", async () => {
    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    expect(
      req.session[MAINFLOW_DATA_OBJECT_NAME].olderThanAgeRequirement
    ).toEqual(true);
  });

  test("should throw expected error when no dob", async () => {
    req.body["date-of-birth-day"] = "";
    req.body["date-of-birth-month"] = "";
    req.body["date-of-birth-year"] = "";

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    const expectedError = {
      type: "field",
      value: "",
      msg: "validation:missingDateOfBirth",
      path: "date-of-birth",
      location: "body",
    };

    expect(
      req.session[MAINFLOW_DATA_OBJECT_NAME].olderThanAgeRequirement
    ).toBeFalsy();
    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });

  test("should throw expected error when dob is not a real date", async () => {
    req.body["date-of-birth-day"] = "45";

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    const expectedError = {
      type: "field",
      value: "2000-01-45",
      msg: "validation:dateOfBirthNotReal",
      path: "date-of-birth",
      location: "body",
    };

    expect(
      req.session[MAINFLOW_DATA_OBJECT_NAME].olderThanAgeRequirement
    ).toBeFalsy();
    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });

  test("should throw expected errors when dob is not a valid date string for day", async () => {
    req.body["date-of-birth-day"] = "A";

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    const expectedError = {
      type: "field",
      value: "A",
      msg: "validation:dateOfBirthDayInvalid",
      path: "date-of-birth-day",
      location: "body",
    };

    expect(
      req.session[MAINFLOW_DATA_OBJECT_NAME].olderThanAgeRequirement
    ).toBeFalsy();
    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });

  test("should throw expected errors when dob is not a valid date string for month", async () => {
    req.body["date-of-birth-month"] = "A";

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    const expectedError = {
      type: "field",
      value: "A",
      msg: "validation:dateOfBirthMonthInvalid",
      path: "date-of-birth-month",
      location: "body",
    };

    expect(
      req.session[MAINFLOW_DATA_OBJECT_NAME].olderThanAgeRequirement
    ).toBeFalsy();
    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });

  test("should throw expected errors when dob is not a valid date string for year", async () => {
    req.body["date-of-birth-year"] = "A";

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    const expectedError = {
      type: "field",
      value: "A",
      msg: "validation:dateOfBirthYearInvalid",
      path: "date-of-birth-year",
      location: "body",
    };

    expect(
      req.session[MAINFLOW_DATA_OBJECT_NAME].olderThanAgeRequirement
    ).toBeFalsy();
    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });

  test("should throw expected errors when dob is not a valid date string - missing dob day", async () => {
    req.body["date-of-birth-day"] = "";

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array();

    const expectedErrors = [
      {
        type: "field",
        value: "",
        msg: "validation:missingDateOfBirthDay",
        path: "date-of-birth-day",
        location: "body",
      },
    ];

    expect(
      req.session[MAINFLOW_DATA_OBJECT_NAME].olderThanAgeRequirement
    ).toBeFalsy();
    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedErrors);
  });

  test("should throw expected errors when dob is not a valid date string - missing dob month", async () => {
    req.body["date-of-birth-month"] = "";

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array();

    const expectedErrors = [
      {
        type: "field",
        value: "",
        msg: "validation:missingDateOfBirthMonth",
        path: "date-of-birth-month",
        location: "body",
      },
    ];

    expect(
      req.session[MAINFLOW_DATA_OBJECT_NAME].olderThanAgeRequirement
    ).toBeFalsy();
    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedErrors);
  });

  test("should throw expected errors when dob is not a valid date string - missing dob year", async () => {
    req.body["date-of-birth-year"] = "";

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array();

    const expectedErrors = [
      {
        type: "field",
        value: "",
        msg: "validation:missingDateOfBirthYear",
        path: "date-of-birth-year",
        location: "body",
      },
    ];

    expect(
      req.session[MAINFLOW_DATA_OBJECT_NAME].olderThanAgeRequirement
    ).toBeFalsy();
    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedErrors);
  });

  test("should throw expected errors when dob is not the past", async () => {
    req.body["date-of-birth-year"] = "9999";

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    const expectedError = {
      type: "field",
      value: "9999-01-01",
      msg: "validation:dateOfBirthInFuture",
      path: "date-of-birth",
      location: "body",
    };

    expect(
      req.session[MAINFLOW_DATA_OBJECT_NAME].olderThanAgeRequirement
    ).toBeFalsy();
    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedError);
  });

  test("should throw expected errors when dob is not 16 or more years in the past", async () => {
    const today = new Date();
    const fifteenYearsAgoYearsAgo = today.getFullYear() - 15;
    req.body["date-of-birth-year"] = fifteenYearsAgoYearsAgo;
    req.session[MAINFLOW_DATA_OBJECT_NAME]["year-of-loss"] =
      today.getFullYear();

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    expect(
      req.session[MAINFLOW_DATA_OBJECT_NAME].olderThanAgeRequirement
    ).toEqual(false);
  });

  test("should throw expected errors when dob is more than 120 years in the past", async () => {
    const today = new Date();
    const hundredTwentyOneYearsAgo = today.getFullYear() - 121;
    req.body["date-of-birth-year"] = hundredTwentyOneYearsAgo;

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const error = result.array()[0];

    const expectedErrors = {
      type: "field",
      value: `${hundredTwentyOneYearsAgo}-01-01`,
      msg: "validation:dateOfBirthTooOld",
      path: "date-of-birth",
      location: "body",
    };

    expect(
      req.session[MAINFLOW_DATA_OBJECT_NAME].olderThanAgeRequirement
    ).toBeFalsy();
    expect(result.array().length).toBe(1);
    expect(error).toEqual(expectedErrors);
  });
});
