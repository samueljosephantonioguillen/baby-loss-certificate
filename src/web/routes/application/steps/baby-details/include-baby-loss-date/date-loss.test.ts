import expect from "expect";

import {
  isGivenYearDoesNotMatch,
  isFullDate,
  isMonthYearDate,
  isYearOnlyDate,
  isDateOfLossAfterParentDob,
  isOnOrAfterEligibleDate,
  isDateBeforeToday,
  isValidMonthYearDate,
  isPartialDateInPast,
  isPartialDateLossAfterParentDob,
  isValidYearOnlyDate,
  isYearOnlyEqualOrAfterEligibleYear,
  validateFullDateOfLoss,
  validatePartialMonthYearDateOfLoss,
  validatePartialYearOnlyDateOfLoss,
} from "./date-loss";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";

describe("isGivenYearDoesNotMatch", () => {
  test("should return false if baby-date-of-loss-year is not populated", () => {
    const req = {
      body: {},
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "year-of-loss": "2022",
        },
      },
    };
    expect(isGivenYearDoesNotMatch(req)).toBeFalsy();
  });

  test("should return false if baby-date-of-loss-year do not match year-of-loss", () => {
    const req = {
      body: {
        "baby-date-of-loss-year": "2021",
      },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "year-of-loss": "2022",
        },
      },
    };
    expect(isGivenYearDoesNotMatch(req)).toEqual(true);
  });

  test("should return true if baby-date-of-loss-year matches year-of-loss", () => {
    const req = {
      body: {
        "baby-date-of-loss-year": "2021",
      },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "year-of-loss": "2021",
        },
      },
    };
    expect(isGivenYearDoesNotMatch(req)).toBeFalsy();
  });
});

describe("isFullDate", () => {
  test("should return true when all date fields are populated", () => {
    const req = {
      body: {
        "baby-date-of-loss-day": "01",
        "baby-date-of-loss-month": "01",
        "baby-date-of-loss-year": "2021",
      },
    };
    expect(isFullDate(req)).toBeTruthy();
  });

  test("should return false when no date populated", () => {
    const req = {
      body: {},
    };
    expect(isFullDate(req)).toBeFalsy();
  });

  test("should return false when dd-mm only", () => {
    const req = {
      body: {
        "baby-date-of-loss-day": "01",
        "baby-date-of-loss-month": "01",
      },
    };
    expect(isFullDate(req)).toBeFalsy();
  });

  test("should return false when mm-yyyy only", () => {
    const req = {
      body: {
        "baby-date-of-loss-month": "01",
        "baby-date-of-loss-year": "2021",
      },
    };
    expect(isFullDate(req)).toBeFalsy();
  });

  test("should return false when dd-yyyy only", () => {
    const req = {
      body: {
        "baby-date-of-loss-day": "01",
        "baby-date-of-loss-year": "2021",
      },
    };
    expect(isFullDate(req)).toBeFalsy();
  });

  test("should return false when dd only", () => {
    const req = {
      body: {
        "baby-date-of-loss-day": "01",
      },
    };
    expect(isFullDate(req)).toBeFalsy();
  });

  test("should return false when mm only", () => {
    const req = {
      body: {
        "baby-date-of-loss-month": "01",
      },
    };
    expect(isFullDate(req)).toBeFalsy();
  });

  test("should return false when yyyy only", () => {
    const req = {
      body: {
        "baby-date-of-loss-year": "2021",
      },
    };
    expect(isFullDate(req)).toBeFalsy();
  });
});

describe("isMonthYearDate", () => {
  test("should return true when mm-yyyy only", () => {
    const req = {
      body: {
        "baby-date-of-loss-month": "01",
        "baby-date-of-loss-year": "2021",
      },
    };
    expect(isMonthYearDate(req)).toBeTruthy();
  });

  test("should return false when all date fields are populated", () => {
    const req = {
      body: {
        "baby-date-of-loss-day": "01",
        "baby-date-of-loss-month": "01",
        "baby-date-of-loss-year": "2021",
      },
    };
    expect(isMonthYearDate(req)).toBeFalsy();
  });

  test("should return false when no date populated", () => {
    const req = {
      body: {},
    };
    expect(isMonthYearDate(req)).toBeFalsy();
  });

  test("should return false when dd-mm only", () => {
    const req = {
      body: {
        "baby-date-of-loss-day": "01",
        "baby-date-of-loss-month": "01",
      },
    };
    expect(isMonthYearDate(req)).toBeFalsy();
  });

  test("should return false when dd-yyyy only", () => {
    const req = {
      body: {
        "baby-date-of-loss-day": "01",
        "baby-date-of-loss-year": "2021",
      },
    };
    expect(isMonthYearDate(req)).toBeFalsy();
  });

  test("should return false when dd only", () => {
    const req = {
      body: {
        "baby-date-of-loss-day": "01",
      },
    };
    expect(isMonthYearDate(req)).toBeFalsy();
  });

  test("should return false when mm only", () => {
    const req = {
      body: {
        "baby-date-of-loss-month": "01",
      },
    };
    expect(isMonthYearDate(req)).toBeFalsy();
  });

  test("should return false when yyyy only", () => {
    const req = {
      body: {
        "baby-date-of-loss-year": "2021",
      },
    };
    expect(isMonthYearDate(req)).toBeFalsy();
  });
});

describe("isYearOnlyDate", () => {
  test("should return true when yyyy only", () => {
    const req = {
      body: {
        "baby-date-of-loss-year": "2021",
      },
    };
    expect(isYearOnlyDate(req)).toBeTruthy();
  });

  test("should return false when all date fields are populated", () => {
    const req = {
      body: {
        "baby-date-of-loss-day": "01",
        "baby-date-of-loss-month": "01",
        "baby-date-of-loss-year": "2021",
      },
    };
    expect(isYearOnlyDate(req)).toBeFalsy();
  });

  test("should return false when no date populated", () => {
    const req = {
      body: {},
    };
    expect(isYearOnlyDate(req)).toBeFalsy();
  });

  test("should return false when dd-mm only", () => {
    const req = {
      body: {
        "baby-date-of-loss-day": "01",
        "baby-date-of-loss-month": "01",
      },
    };
    expect(isYearOnlyDate(req)).toBeFalsy();
  });

  test("should return false when mm-yyyy only", () => {
    const req = {
      body: {
        "baby-date-of-loss-month": "01",
        "baby-date-of-loss-year": "2021",
      },
    };
    expect(isYearOnlyDate(req)).toBeFalsy();
  });

  test("should return false when dd-yyyy only", () => {
    const req = {
      body: {
        "baby-date-of-loss-day": "01",
        "baby-date-of-loss-year": "2021",
      },
    };
    expect(isYearOnlyDate(req)).toBeFalsy();
  });

  test("should return false when dd only", () => {
    const req = {
      body: {
        "baby-date-of-loss-day": "01",
      },
    };
    expect(isYearOnlyDate(req)).toBeFalsy();
  });

  test("should return false when mm only", () => {
    const req = {
      body: {
        "baby-date-of-loss-month": "01",
      },
    };
    expect(isYearOnlyDate(req)).toBeFalsy();
  });
});

describe("isDateOfLossAfterParentDob", () => {
  test("should return true when date of loss is after parent dob", () => {
    const dateOfLoss = "2022-12-31";
    const parentDob = "2021-01-01";

    expect(isDateOfLossAfterParentDob(dateOfLoss, parentDob)).toEqual(true);
  });

  test("should return false when date of loss is equal to parent dob", () => {
    const dateOfLoss = "2021-01-01";
    const parentDob = "2021-01-01";

    expect(isDateOfLossAfterParentDob(dateOfLoss, parentDob)).toEqual(false);
  });

  test("should return false when date of loss is before parent dob", () => {
    const dateOfLoss = "2021-01-01";
    const parentDob = "2022-12-31";

    expect(isDateOfLossAfterParentDob(dateOfLoss, parentDob)).toEqual(false);
  });
});

describe("isDateBeforeToday", () => {
  const today = new Date();

  test("should return true when date of loss is today", () => {
    expect(isDateBeforeToday(today)).toEqual(true);
  });

  test("should return true when date of loss is in the past", () => {
    const pastDate = new Date("2020-01-01");
    expect(isDateBeforeToday(pastDate)).toEqual(true);
  });

  test("should return true when date of loss is in the future", () => {
    const pastDate = new Date("2099-01-01");
    expect(isDateBeforeToday(pastDate)).toEqual(false);
  });
});

describe("isValidMonthYearDate", () => {
  test("should return true when partial date is in valid format", () => {
    expect(isValidMonthYearDate("2021-01-")).toEqual(true);
  });

  test("should return false when partial date is not in valid format", () => {
    expect(isValidMonthYearDate("2021--")).toEqual(false);
  });
});

describe("isPartialDateInPast", () => {
  test("should return true when partial date is in the past", () => {
    const partialDate = new Date("2021-01");
    expect(isPartialDateInPast(partialDate)).toEqual(true);
  });

  test("should return true when partial date is today", () => {
    const today = new Date();
    const todayMonth = today.getMonth() + 1;
    const todayYear = today.getFullYear();
    const partialDate = new Date(`${todayYear}-${todayMonth}`);
    expect(isPartialDateInPast(partialDate)).toEqual(true);
  });

  test("should return false when partial date is in the future", () => {
    const today = new Date();
    const nextMonthDate = new Date(today);
    nextMonthDate.setMonth(today.getMonth() + 1);
    expect(isPartialDateInPast(nextMonthDate)).toEqual(false);
  });
});

describe("isPartialDateLossBeforeParentDob", () => {
  test("should return true when date of loss is after parent dob", () => {
    const partialDate = new Date("2021-12");
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: { "date-of-birth": "2021-01-01" },
      },
    };
    expect(isPartialDateLossAfterParentDob(partialDate, req)).toEqual(true);
  });

  test("should return false when date of loss is equal to parent dob", () => {
    const partialDate = new Date("2021-12");
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: { "date-of-birth": "2021-12-01" },
      },
    };
    expect(isPartialDateLossAfterParentDob(partialDate, req)).toEqual(false);
  });

  test("should return false when date of loss is before parent dob", () => {
    const partialDate = new Date("2021-01");
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: { "date-of-birth": "2021-12-31" },
      },
    };
    expect(isPartialDateLossAfterParentDob(partialDate, req)).toEqual(false);
  });
});

describe("isValidYearOnlyDate", () => {
  test("should return true when valid year only date", () => {
    expect(isValidYearOnlyDate("2021--")).toEqual(true);
  });

  test("should return false when invalid year only date", () => {
    expect(isValidYearOnlyDate("200--")).toEqual(false);
  });
});

describe("isOnOrAfterEligibleDate", () => {
  test("should return true when on eligible date", () => {
    expect(isOnOrAfterEligibleDate("2018-09-01")).toEqual(true);
  });

  test("should return true when after eligible date", () => {
    expect(isOnOrAfterEligibleDate("2023-09-01")).toEqual(true);
  });

  test("should return false when before eligible date", () => {
    expect(isOnOrAfterEligibleDate("2000-01-01")).toEqual(false);
  });

  test("should return trie when valid partial month year date of loss", () => {
    expect(isOnOrAfterEligibleDate("2018-09-")).toEqual(true);
  });

  test("should return false when invalid partial month year date of loss", () => {
    expect(isOnOrAfterEligibleDate("2018-08-")).toEqual(false);
  });
});

describe("isYearOnlyEqualOrAfterEligibleYear", () => {
  test("should return true when on eligible year", () => {
    expect(isYearOnlyEqualOrAfterEligibleYear(new Date("2018"))).toEqual(true);
  });

  test("should return true when after eligible year", () => {
    expect(isYearOnlyEqualOrAfterEligibleYear(new Date("2019"))).toEqual(true);
  });

  test("should return false when before eligible year", () => {
    expect(isYearOnlyEqualOrAfterEligibleYear(new Date("2000"))).toEqual(false);
  });
});

describe("validateFullDateOfLoss", () => {
  test("should return true when valid full date of loss", () => {
    const req = {
      body: {
        "baby-date-of-loss": "2021-01-01",
      },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "date-of-birth": "1999-01-01",
        },
      },
    };

    const result = validateFullDateOfLoss(req.body["baby-date-of-loss"], req);
    expect(result).toEqual(true);
  });
});

describe("validatePartialMonthYearDateOfLoss", () => {
  test("should return true when valid partial month year date of loss", () => {
    const req = {
      body: {
        "baby-date-of-loss": "2021-01-",
        "baby-date-of-loss-month": "01",
        "baby-date-of-loss-year": "2021",
      },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "date-of-birth": "1999-01-01",
        },
      },
    };

    const result = validatePartialMonthYearDateOfLoss(
      req.body["baby-date-of-loss"],
      req
    );
    expect(result).toEqual(true);
  });
});

describe("validatePartialYearOnlyDateOfLoss", () => {
  test("should return true when valid partial year only date of loss", () => {
    const req = {
      body: {
        "baby-date-of-loss": "2021--",
        "baby-date-of-loss-year": "2021",
      },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "date-of-birth": "1999-01-01",
        },
      },
    };

    const result = validatePartialYearOnlyDateOfLoss(
      req.body["baby-date-of-loss"],
      req
    );
    expect(result).toEqual(true);
  });
});
