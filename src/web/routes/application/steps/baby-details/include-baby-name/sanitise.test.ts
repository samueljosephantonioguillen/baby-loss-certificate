import expect from "expect";

import { sanitise } from "./sanitise";

const next = jest.fn();
const res = { redirect: jest.fn() };

test("should trim whitespace from baby-name", () => {
  const req = {
    body: {
      "baby-name": "  Baby Name  ",
    },
  };

  const expected = {
    "baby-name": "Baby Name",
  };

  sanitise()(req, res, next);

  expect(req.body).toEqual(expected);
});

test("should replace backtick with apostrophe from baby-name", () => {
  const req = {
    body: {
      "baby-name": "Baby`s Name",
    },
  };

  const expected = {
    "baby-name": "Baby's Name",
  };

  sanitise()(req, res, next);

  expect(req.body).toEqual(expected);
});

test("should replace autofilled apostrophe with standard apostrophe from baby-name", () => {
  const req = {
    body: {
      "baby-name": "Baby’s Name",
    },
  };

  const expected = {
    "baby-name": "Baby's Name",
  };

  sanitise()(req, res, next);

  expect(req.body).toEqual(expected);
});

test("should capitalise first letters of baby-name", () => {
  const req = {
    body: {
      "baby-name": "baby name",
    },
  };

  const expected = {
    "baby-name": "Baby Name",
  };

  sanitise()(req, res, next);

  expect(req.body).toEqual(expected);
});
