import { body, check } from "express-validator";
import { translateValidationMessage } from "../../common/translate-validation-message";

import {
  LOSS_LOCATION_MAX_LENGTH,
  LOSS_LOCATION_PATTERN,
} from "../../../constants";

const validate = () => [
  check("include-baby-loss-location")
    .not()
    .isEmpty()
    .bail()
    .withMessage(
      translateValidationMessage(
        "validation:selectIncludeBabyLossLocationOption"
      )
    ),

  check("baby-loss-location")
    .if(body("include-baby-loss-location").equals("yes"))
    .not()
    .isEmpty()
    .bail()
    .withMessage(
      translateValidationMessage("validation:missingBabyLossLocation")
    )
    .isLength({ max: LOSS_LOCATION_MAX_LENGTH })
    .bail()
    .withMessage(
      translateValidationMessage("validation:babyLossLocationTooLong")
    )
    .matches(LOSS_LOCATION_PATTERN)
    .bail()
    .withMessage(
      translateValidationMessage("validation:patternBabyLossLocation")
    ),
];

export { validate };
