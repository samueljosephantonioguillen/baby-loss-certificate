import expect from "expect";

import {
  livingInEngland,
  pageContent,
  validate,
  behaviourForGet,
} from "./living-in-england";

test("livingInEngland should match expected outcomes", () => {
  const expectedResults = {
    path: "/live-in-england",
    template: "living-in-england",
    pageContent,
    validate,
    behaviourForGet,
  };

  expect(livingInEngland).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("livingInEngland.title"),
    sectionTitle: translate("section.confirmEligibility"),
    heading: translate("livingInEngland.heading"),
    yes: translate("yes"),
    no: translate("no"),
    continueButtonText: translate("buttons:continue"),
    backLinkText: translate("buttons:back"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});
