import expect from "expect";
import { sanitise } from "./sanitise";

test("sanitise() should trim the leading zeros", () => {
  const req = {
    body: {
      "their-date-of-birth-year": "002002",
      "their-date-of-birth-month": "004",
      "their-date-of-birth-day": "0027",
    },
  };

  const expectedReq = {
    body: {
      "their-date-of-birth-year": "2002",
      "their-date-of-birth-month": "4",
      "their-date-of-birth-day": "27",
    },
  };

  const next = jest.fn();

  sanitise()(req, {}, next);
  expect(req).toEqual(expectedReq);
});
