import * as validator from "validator";
import { isNil } from "ramda";

import { dateAsString } from "./format-date";

const DATE_PATTERN = /^\d+-\d{2,}-\d{2,}$/;

const isInvalidDate = (dateString) => {
  const inputDate = new Date(dateString);
  return inputDate.toString() === "Invalid Date";
};

const isValidDateString = (dateString) =>
  !isNil(dateString) && validator.matches(dateString, DATE_PATTERN);

const isDateRealDate = (dateString) =>
  isValidDateString(dateString) &&
  validator.isISO8601(dateString, { strict: true });

const isDateInPast = (dateString) =>
  isValidDateString(dateString) &&
  !isInvalidDate(dateString) &&
  validator.isBefore(dateString, dateAsString());

const isDateAfterDateOfLoss = (dateString, yearOfLoss) => {
  if (isValidDateString(dateString) && !isInvalidDate(dateString)) {
    const dateOfBirthYear = new Date(dateString).getUTCFullYear();
    return dateOfBirthYear <= yearOfLoss;
  }
};

const checkIsGivenYearsInThePast = (dateString, yearAdjustment) => {
  return !validator.isAfter(dateString, dateAsString({ yearAdjustment }));
};

const isDateSixteenOrMoreYearsInThePast = (dateString) => {
  const minimumAgeRequirement = 16;
  return (
    isValidDateString(dateString) &&
    !isInvalidDate(dateString) &&
    checkIsGivenYearsInThePast(dateString, -minimumAgeRequirement)
  );
};

const isDateAHundredTwentyOrMoreYearsInThePast = (dateString) => {
  const maximumAgeRequirement = 120;
  return (
    isValidDateString(dateString) &&
    !isInvalidDate(dateString) &&
    checkIsGivenYearsInThePast(dateString, -maximumAgeRequirement)
  );
};

export {
  isInvalidDate,
  isValidDateString,
  isDateRealDate,
  isDateInPast,
  isDateAfterDateOfLoss,
  isDateSixteenOrMoreYearsInThePast,
  isDateAHundredTwentyOrMoreYearsInThePast,
};
