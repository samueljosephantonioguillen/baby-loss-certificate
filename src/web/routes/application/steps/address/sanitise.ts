import { toUpper, compose } from "ramda";

const replaceMultipleSpacesWithOne = (value) => {
  return value.replace(/ {2,}/g, " ");
};

const addSpaceBeforeLastThreeCharacters = (value) => {
  return value.replace(/^(.*)(\d)/, "$1 $2");
};

const replaceMultipleSpacesAndAddSingleSpaceThenUppercase = compose(
  replaceMultipleSpacesWithOne,
  addSpaceBeforeLastThreeCharacters,
  toUpper
);

const sanitise = () => (req, res, next) => {
  req.body.sanitisedPostcode =
    replaceMultipleSpacesAndAddSingleSpaceThenUppercase(req.body.postcode);

  if (req.body["address-line-1"]) {
    req.body["address-line-1"] = req.body["address-line-1"].trim();
  }

  if (req.body["town-or-city"]) {
    req.body["town-or-city"] = req.body["town-or-city"].trim();
  }

  if (req.body["address-line-2"]) {
    req.body["address-line-2"] = req.body["address-line-2"].trim();
  }

  if (req.body["county"]) {
    req.body["county"] = req.body["county"].trim();
  }

  next();
};

export { sanitise, replaceMultipleSpacesAndAddSingleSpaceThenUppercase };
