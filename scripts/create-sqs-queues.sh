#!/bin/bash

REGION=eu-west-2

# set up the dead letter queue
awslocal sqs create-queue \
  --queue-name dead-letter-queue.fifo \
  --region ${REGION} \
  --attributes FifoQueue=true,VisibilityTimeout=30

awslocal sqs create-queue \
  --queue-name xerox-queue.fifo \
  --region ${REGION} \
  --attributes FifoQueue=true,ContentBasedDeduplication=true,VisibilityTimeout=1800

awslocal sqs create-queue \
  --queue-name incomplete-application-queue.fifo \
  --region ${REGION} \
  --attributes FifoQueue=true,ContentBasedDeduplication=true,VisibilityTimeout=1800

awslocal sqs set-queue-attributes \
  --queue-url "http://localstack:4566/000000000000/xerox-queue.fifo" \
  --attributes file://queue-attributes.json
