const isEmpty = jest.fn();

jest.mock("express-validator", () => ({
  validationResult: () => ({
    isEmpty: isEmpty,
  }),
}));

import expect from "expect";

import {
  includeBabyLossLocation,
  pageContent,
  validate,
  behaviourForPost,
  sanitise,
} from "./include-baby-loss-location";
import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";

test("includeBabyLossLocation should match expected outcomes", () => {
  const expectedResults = {
    path: "/place-of-baby-loss",
    template: "include-baby-loss-location",
    pageContent,
    sanitise,
    validate,
    behaviourForPost,
  };

  expect(includeBabyLossLocation).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("includeBabyLossLocation.title"),
    sectionTitle: translate("section.certificateDetails"),
    heading: translate("includeBabyLossLocation.heading"),
    bodyLineOne: translate("includeBabyLossLocation.body.firstLine"),
    hintLineOne: translate("includeBabyLossLocation.hint.firstLine"),
    hintLineTwo: translate("includeBabyLossLocation.hint.secondLine"),
    insetTextLineOne: translate("includeBabyLossLocation.insetText"),
    yes: translate("yes"),
    label: translate("includeBabyLossLocation.label"),
    no: translate("no"),
    or: translate("or"),
    unknown: translate("includeBabyLossLocation.unknown"),
    continueButtonText: translate("buttons:continue"),
    backLinkText: translate("buttons:back"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("behaviourForPost", () => {
  test("should return expected data when no option is selected", () => {
    isEmpty.mockReturnValue(true);

    const req = {
      body: {
        "include-baby-loss-location": "no",
        "baby-loss-location": "Kent",
      },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "baby-loss-location": "Kent",
        },
      },
    };
    const res = {};
    const next = jest.fn();

    const expectedReq = {
      body: {
        "include-baby-loss-location": "no",
      },
      session: { [MAINFLOW_DATA_OBJECT_NAME]: {}, journeyName: MAINFLOW.name },
    };

    behaviourForPost()(req, res, next);
    expect(req).toEqual(expectedReq);
    expect(next).toBeCalledTimes(1);
  });

  test("should return expected data when unknown option is selected", () => {
    isEmpty.mockReturnValue(true);

    const req = {
      body: {
        "include-baby-loss-location": "unknown",
        "baby-loss-location": "Kent",
      },
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          "baby-loss-location": "Kent",
        },
      },
    };
    const res = {};
    const next = jest.fn();

    const expectedReq = {
      body: { "include-baby-loss-location": "unknown" },
      session: { [MAINFLOW_DATA_OBJECT_NAME]: {}, journeyName: MAINFLOW.name },
    };

    behaviourForPost()(req, res, next);
    expect(req).toEqual(expectedReq);
    expect(next).toBeCalledTimes(1);
  });

  test("should call when validation error", () => {
    isEmpty.mockReturnValue(false);

    const req = {};
    const res = {};
    const next = jest.fn();

    behaviourForPost()(req, res, next);
    expect(next).toBeCalledTimes(1);
  });
});
