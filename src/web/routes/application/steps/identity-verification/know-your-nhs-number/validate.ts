import { check } from "express-validator";

import { translateValidationMessage } from "../../common/translate-validation-message";

const validate = () => [
  check("know-your-nhs-number")
    .not()
    .isEmpty()
    .bail()
    .withMessage(
      translateValidationMessage("validation:selectKnowYourNhsNumber")
    ),
];

export { validate };
