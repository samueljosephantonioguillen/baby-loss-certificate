import { toBoolean } from "./to-boolean";
import * as dotEnv from "dotenv";

dotEnv.config();

export const server = {
  PORT: process.env.PORT,
  APP_VERSION: process.env.APP_VERSION,
  CONTEXT_PATH: process.env.CONTEXT_PATH,
  SESSION_SECRET: process.env.SESSION_SECRET,
  SESSION_ID_NAME: "baby-loss-certificate.sid",
  NO_CACHE_VIEW_TEMPLATES: toBoolean(process.env.NO_CACHE_VIEW_TEMPLATES),
  CSRF_SECRET: process.env.CSRF_SECRET,
  SP_CONTEXT_PATH: process.env.SP_CONTEXT_PATH,
  CRM_CONTEXT_PATH: process.env.CRM_CONTEXT_PATH,
  ASSET_PATH: process.env.ASSET_PATH,
};
