import expect from "expect";
import { validate, checkForInput } from "./validate";
import { applyExpressValidation } from "../common/test/apply-express-validation";

test("return no validation errors when enter-location field is valid", async () => {
  const req = {
    body: {
      "enter-location": "yes",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("checkForInput() returns false when enter-location is undefined", () => {
  const req = {
    body: {},
  };

  const retValue = checkForInput(null, { req });
  expect(retValue).toBe(false);
});
