import expect from "expect";

const isEmpty = jest.fn();

jest.mock("express-validator", () => ({
  validationResult: () => ({
    isEmpty: isEmpty,
  }),
}));

import {
  spKnowYourNhsNumber,
  pageContent,
  isNavigable,
  validate,
  behaviourForGet,
  behaviourForPost,
} from "./sp-know-your-nhs-number";
import { buildSessionForJourney } from "../../../flow-control/test-utils/test-utils";
import {
  IN_DETAILS_REVIEW,
  IN_DETAILS_REVIEW_PROGRESS,
  IN_PROGRESS,
} from "../../../flow-control/states";
import { CHECK_DETAILS_URL } from "../../../paths/paths";
import { contextPath } from "../../../paths/context-path";
import { SECOND_PARENT_FLOW } from "../../../journey-definitions";

test("knowYourNhsNumber should match expected outcomes", () => {
  const expectedResults = {
    path: "/do-you-know-your-nhs-number",
    template: "know-your-nhs-number",
    pageContent,
    isNavigable,
    validate,
    behaviourForGet,
    behaviourForPost,
  };

  expect(spKnowYourNhsNumber).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("knowYourNhsNumber.title"),
    sectionTitle: translate("section.yourDetails"),
    heading: translate("knowYourNhsNumber.heading"),
    bodyLineOne: translate("knowYourNhsNumber.body.firstLine"),
    bodyLineTwo: translate("knowYourNhsNumber.body.secondLine"),
    yes: translate("knowYourNhsNumber.yes"),
    no: translate("knowYourNhsNumber.no"),
    whereToFindNhsNumberSummary: translate(
      "knowYourNhsNumber.whereToFindNhsNumberSummary"
    ),
    whereToFindNhsNumberText1: translate(
      "knowYourNhsNumber.whereToFindNhsNumber1"
    ),
    whereToFindNhsNumberText2: translate(
      "knowYourNhsNumber.whereToFindNhsNumber2"
    ),
    whereToFindNhsNumberText3: translate(
      "knowYourNhsNumber.whereToFindNhsNumber3"
    ),
    whereToFindNhsNumberText4: translate(
      "knowYourNhsNumber.whereToFindNhsNumber4"
    ),
    backLinkText: translate("buttons:back"),
    continueButtonText: translate("buttons:continue"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when application-id is populated", () => {
    const req = {
      session: {
        secondParent: {
          "application-id": "123",
        },
      },
    };

    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false continue is not application-id populated", () => {
    const req = {
      session: {},
    };

    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});

describe("behaviourForGet", () => {
  const config = {};
  const journey = { name: SECOND_PARENT_FLOW.name };

  test("should call set to IN_DETAILS_REVIEW when in IN_DETAILS_REVIEW_PROGRESS state", () => {
    const req = {
      session: {
        journeyName: SECOND_PARENT_FLOW.name,
        secondParent: {
          "know-your-nhs-number": "yes",
          "nhs-number": "1111111111",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_DETAILS_REVIEW_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = { locals: { previous: undefined } };
    const next = jest.fn();

    behaviourForGet(config, journey)(req, res, next);

    expect(req.session.journeys[journey.name].state).toEqual(IN_DETAILS_REVIEW);
    expect(res.locals.previous).toBe(
      contextPath(CHECK_DETAILS_URL, SECOND_PARENT_FLOW.name)
    );
    expect(next).toBeCalledTimes(1);
  });

  test("should call set to IN_DETAILS_REVIEW when in IN_DETAILS_REVIEW_PROGRESS state", () => {
    const req = {
      session: {
        journeyName: SECOND_PARENT_FLOW.name,
        secondParent: {
          "know-your-nhs-number": "yes",
          "nhs-number": "",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_DETAILS_REVIEW_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = { locals: { previous: undefined } };
    const next = jest.fn();

    behaviourForGet(config, journey)(req, res, next);

    expect(req.session.secondParent["know-your-nhs-number"]).toEqual("no");
    expect(req.session.journeys[journey.name].state).toEqual(IN_DETAILS_REVIEW);
    expect(res.locals.previous).toBe(
      contextPath(CHECK_DETAILS_URL, SECOND_PARENT_FLOW.name)
    );
    expect(next).toBeCalledTimes(1);
  });
});

describe("behaviourForPost", () => {
  const config = {};
  const journey = { name: SECOND_PARENT_FLOW.name };

  test("should set state to IN_DETAILS_REVIEW_PROGRESS when in IN_DETAILS_REVIEW", () => {
    isEmpty.mockReturnValue(true);

    const req = {
      body: { "know-your-nhs-number": "yes" },
      session: {
        journeyName: SECOND_PARENT_FLOW.name,
        secondParent: {
          "nhs-number": "1111111111",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_DETAILS_REVIEW,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = {};
    const next = jest.fn();

    behaviourForPost(config, journey)(req, res, next);

    expect(req.session.secondParent["nhs-number"]).toEqual("1111111111");
    expect(req.session.journeys[journey.name].state).toEqual(
      IN_DETAILS_REVIEW_PROGRESS
    );
    expect(next).toBeCalledTimes(1);
  });

  test("should return expected data when no option is selected and not change state", () => {
    isEmpty.mockReturnValue(true);

    const req = {
      body: { "know-your-nhs-number": "no" },
      session: {
        journeyName: SECOND_PARENT_FLOW.name,
        secondParent: {
          "nhs-number": "1111111111",
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = {};
    const next = jest.fn();

    behaviourForPost(config, journey)(req, res, next);
    expect(req.session.secondParent).toEqual({});
    expect(req.session.journeys[journey.name].state).toEqual(IN_PROGRESS);
    expect(next).toBeCalledTimes(1);
  });

  test("should call next when validation error", () => {
    isEmpty.mockReturnValue(false);

    const req = {};
    const res = {};
    const next = jest.fn();

    behaviourForPost(config, journey)(req, res, next);
    expect(next).toBeCalledTimes(1);
  });

  test("should return blank name/DOB/Postcode data when no option is selected after nhs number pds call has been made", () => {
    isEmpty.mockReturnValue(true);
    const req = {
      body: { "know-your-nhs-number": "no" },
      session: {
        journeyName: SECOND_PARENT_FLOW.name,
        secondParent: {
          "first-name": "test value",
          "last-name": "test value",
          "date-of-birth": "test value",
          "date-of-birth-day": "test value",
          "date-of-birth-month": "test value",
          "date-of-birth-year": "test value",
          postcode: "test value",
          sanitisedPostcode: "test value",
          nhsNumberPdsRequestMade: true,
        },
        ...buildSessionForJourney({
          journeyName: journey.name,
          state: IN_PROGRESS,
          nextAllowedPath: undefined,
        }),
      },
    };
    const res = {};
    const next = jest.fn();
    behaviourForPost(config, journey)(req, res, next);
    expect(req.session.secondParent).toEqual({ nhsNumberPdsRequestMade: true });
  });
});
