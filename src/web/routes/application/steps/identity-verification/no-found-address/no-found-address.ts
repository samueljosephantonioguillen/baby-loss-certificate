import { userIsBackOffice } from "../../../../azure-routes/auth";
import { getFlowDataObjectName } from "../../../tools/data-object";

const pageContent = ({ translate }) => ({
  title: translate("noFoundAddress.title"),
  heading: translate("noFoundAddress.heading"),
  bodyLineOne: translate("noFoundAddress.body.firstLine"),
  bodyLineTwo: translate("noFoundAddress.body.secondLine"),
  backLinkText: translate("buttons:back"),
});

const isNavigable = (req) =>
  req.session[getFlowDataObjectName(req.session.journeyName)][
    "addressNotFound"
  ] === true && !userIsBackOffice(req);

const noFoundAddress = {
  path: "/cannot-request-cannot-find-address",
  template: "not-eligible",
  pageContent,
  isNavigable,
};

export { noFoundAddress, pageContent, isNavigable };
