import expect from "expect";

import {
  spDetailsAreWrong,
  pageContent,
  isNavigable,
  behaviourForGet,
} from "./sp-details-are-wrong";
import { SECOND_PARENT_FLOW } from "../../../journey-definitions";
import { SECOND_PARENT_DATA_OBJECT_NAME } from "../../../tools/data-object";

test("spDetailsAreWrong should match expected outcomes", () => {
  const expectedResults = {
    path: "/cannot-request-certificate-details-unconfirmed",
    template: "application-cancelled",
    pageContent,
    isNavigable,
    behaviourForGet,
  };

  expect(spDetailsAreWrong).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("detailsAreWrong.title"),
    heading: translate("detailsAreWrong.heading"),
    supportOrganisationsHeading: translate("supportOrganisations.heading"),
    supportOrganisationsBody: translate("supportOrganisations.body"),
    supportOrganisationsList: translate("supportOrganisations.listItems"),
  };

  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when cancel-application is yes", () => {
    const req = {
      session: {
        journeyName: SECOND_PARENT_FLOW.name,
        [SECOND_PARENT_DATA_OBJECT_NAME]: {
          "cancel-application": "yes",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when cancel-application is not defined", () => {
    const req = {
      session: {
        journeyName: SECOND_PARENT_FLOW.name,
        [SECOND_PARENT_DATA_OBJECT_NAME]: {
          "cancel-application": undefined,
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when cancel-application is no", () => {
    const req = {
      session: {
        journeyName: SECOND_PARENT_FLOW.name,
        [SECOND_PARENT_DATA_OBJECT_NAME]: {
          "cancel-application": "no",
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});
