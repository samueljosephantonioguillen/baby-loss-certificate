import { prefixPath } from "../../../paths/prefix-path";
import {
  CHECK_ANSWERS_URL,
  CHECK_DETAILS_URL,
  SP_CONFIRM_DETAILS_URL,
} from "../../../paths/paths";

import { isNextPathNavigable } from "../predicates";
import { getNextForStep } from "./get-next-for-step";
import { SECOND_PARENT_FLOW } from "../../../journey-definitions";

const getStepForPath = (path, steps) =>
  steps.find((step) => {
    if (step.path === path) {
      return true;
    }
  });

const getNextInDetailReviewPath = (req, prefix) => {
  return prefixPath(prefix, CHECK_DETAILS_URL);
};

const getNextInReviewPath = (req, prefix) => {
  if (req.session.journeyName == SECOND_PARENT_FLOW.name) {
    return prefixPath(prefix, SP_CONFIRM_DETAILS_URL);
  }

  return prefixPath(prefix, CHECK_ANSWERS_URL);
};
/**
 * Ask the current step for the next path. Test whether the step matching that path is navigable. If not, ask that step for the next path; repeat.
 * @param path the path of the current step
 * @param req the current request
 * @param steps the steps of the blc journey
 * @returns the path of the next step
 */
const getNextNavigablePath = (path, req, journey) => {
  const { steps } = journey;
  const thisStep = getStepForPath(path, steps);
  const nextPath = getNextForStep(req, journey, thisStep);
  const nextStep = getStepForPath(nextPath, steps);

  if (isNextPathNavigable(nextStep, req)) {
    return nextPath;
  }
  return getNextNavigablePath(nextPath, req, journey);
};

export {
  getNextNavigablePath,
  getNextInDetailReviewPath,
  getNextInReviewPath,
  getStepForPath,
};
