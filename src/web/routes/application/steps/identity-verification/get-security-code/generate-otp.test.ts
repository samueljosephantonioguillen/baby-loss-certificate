import expect from "expect";

import { generateOTP } from "./generate-otp";

test("generateOTP should generate a random 6-digit OTP", () => {
  const otp = generateOTP();
  const regex = /^\d{6}$/;

  expect(regex.test(otp)).toEqual(true);
});
