import { logger } from "../../../../../logger/logger";
import { SECOND_PARENT_FLOW } from "../../../journey-definitions";
import { contextPath } from "../../../paths/context-path";
import { SECOND_PARENT_DATA_OBJECT_NAME } from "../../../tools/data-object";
import { PATH as tryAgainPath } from "../../../../error-pages/try-again";
import { sendUnsuccessfulApplicationEmail } from "../../common/notify-client";
import { rejectCertificate } from "../../common/pg-client";
import { spCheckYourAnswers } from "../../sp-check-your-answers";
import { validate } from "./validate";
import { validationResult } from "express-validator";
import { formatReference } from "../../common/certificate-reference-generator";

const isNavigable = (req) =>
  req.session[SECOND_PARENT_DATA_OBJECT_NAME]["are-details-correct"] === "no";

const pageContent = ({ translate }) => ({
  title: translate("spCancelApplication.title"),
  heading: translate("spCancelApplication.heading"),
  bodyLineOne: translate("spCancelApplication.body.firstLine"),
  yes: translate("yes"),
  no: translate("no"),
  backLinkText: translate("buttons:back"),
  continueButtonText: translate("buttons:continue"),
});

const behaviourForPost = () => async (req, res, next) => {
  if (!validationResult(req).isEmpty()) {
    return next();
  }

  if (req.body["cancel-application"] === "no") {
    return res.redirect(
      contextPath(spCheckYourAnswers.path, SECOND_PARENT_FLOW.name)
    );
  }

  const certificateReference =
    req.session[SECOND_PARENT_DATA_OBJECT_NAME][
      "sp-sanitised-reference-number"
    ];
  logger.info(
    `about to update application table to reject application: ${certificateReference}`
  );
  const result = await rejectCertificate(req, certificateReference);

  if (result === false) {
    logger.warn(`failed to reject certificate: ${certificateReference}`);
    res.locals.errorTitleText = req.t("validation:errorTitleText");
    res.locals.errors = [
      {
        path: "cancel-application",
        msg: req.t("spCancelApplication.failedToReject"),
      },
    ];
    return next();
  }

  const firstParentEmail =
    req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-email"];
  const firstParentGivenName =
    req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-first-name"];
  const firstParentFamilyName =
    req.session[SECOND_PARENT_DATA_OBJECT_NAME]["fp-last-name"];
  const firstParentFullName = `${firstParentGivenName} ${firstParentFamilyName}`;
  const referenceNumber = formatReference(
    req.session[SECOND_PARENT_DATA_OBJECT_NAME]["sp-sanitised-reference-number"]
  );

  if (result === true && firstParentEmail !== null) {
    logger.info("Sending rejection email");
    try {
      await sendUnsuccessfulApplicationEmail(
        req,
        firstParentEmail,
        firstParentFullName,
        referenceNumber
      );
    } catch (error) {
      console.log(error);
      logger.error(
        `Failure sending rejection email for ${certificateReference}`,
        req
      );
      return res.redirect(tryAgainPath);
    }
  }
  next();
};

const spCancelApplication = {
  path: "/confirm-certificate-cancellation",
  template: "sp-cancel-application",
  pageContent,
  isNavigable,
  validate,
  behaviourForPost,
};

export {
  spCancelApplication,
  pageContent,
  isNavigable,
  validate,
  behaviourForPost,
};
