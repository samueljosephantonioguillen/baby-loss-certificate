import { CERTIFICATE_MANAGER_HOME, CONTEXT_PATH } from "./paths";
import * as express from "express";
import { configureAuthentication } from "../application/flow-control/middleware/configure-authentication";
import { sessionDestroyButKeepLoggedIn } from "../application/steps/common/session-destroy";
import { START } from "../start";
import {
  checkAuthenticationSet,
  isUserAuthenticated,
} from "../../../common/azure-utils";

const pageContent = ({ translate }) => ({
  heading: translate("certificateManagerHome.heading"),
  ccsOptions: translate("certificateManagerHome.ccsOptions"),
  backOfficeFirstServiceTitle: translate(
    "certificateManagerHome.backOfficeOptions.firstOption.serviceTitle"
  ),
  backOfficeSecondServiceTitle: translate(
    "certificateManagerHome.backOfficeOptions.secondOption.serviceTitle"
  ),
  ccsFirstServiceTitle: translate(
    "certificateManagerHome.ccsOptions.firstOption.serviceTitle"
  ),
  ccsSecondServiceTitle: translate(
    "certificateManagerHome.ccsOptions.secondOption.serviceTitle"
  ),
  ccsThirdServiceTitle: translate(
    "certificateManagerHome.ccsOptions.thirdOption.serviceTitle"
  ),
});

const renderCertificateMangerHome = (app) => {
  const router = express.Router();
  app.use((req, res, next) => {
    res.set("Clear-Site-Data", '"storage"');
    next();
  });
  app.use(router);

  // This will create a redirect on the route to go to the start url
  router.get(CONTEXT_PATH, configureAuthentication, (req, res) => {
    if (checkAuthenticationSet()) {
      res.redirect(CONTEXT_PATH + CERTIFICATE_MANAGER_HOME);
    } else {
      res.redirect(CONTEXT_PATH + START);
    }
  });

  router.get(
    CONTEXT_PATH + CERTIFICATE_MANAGER_HOME,
    configureAuthentication,
    (req, res) => {
      sessionDestroyButKeepLoggedIn(req, res);
      res.clearCookie("lang");
      if (isUserAuthenticated(req)) {
        res.render("static-pages/certificate-manager-home", {
          ...pageContent({ translate: req.t }),
        });
      } else {
        res.redirect(CONTEXT_PATH + "/page-not-found");
      }
    }
  );
};

const registerCertificateManagerHomeRoute = (app) =>
  renderCertificateMangerHome(app);

export {
  registerCertificateManagerHomeRoute,
  CERTIFICATE_MANAGER_HOME,
  pageContent,
};
