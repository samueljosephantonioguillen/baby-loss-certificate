import { START } from "../start";
import {
  contextPath,
  firstParentContextPath,
  secondParentContextPath,
} from "../application/paths/context-path";
import { getJourneyFromUrl } from "../application/tools/journey-from-url";
import { userIsAgent, userIsBackOffice } from "../azure-routes/auth";
import { CERTIFICATE_MANAGER_HOME } from "../static-pages/paths";

const TOO_MANY_ATTEMPTS_PATH = "/cannot-request-start-again";

const pageContent = ({ translate, serviceStartPage }) => ({
  title: translate("tooManyAttempts.title"),
  heading: translate("tooManyAttempts.heading"),
  startAgain: translate("startAgain"),
  body: translate("tooManyAttempts.body", {
    serviceStart: serviceStartPage,
  }),
});

const get = (req, res) => {
  const journeyName = getJourneyFromUrl(req.originalUrl);
  const serviceStartPage = getServiceStartPage(req, journeyName);
  res.render(
    "too-many-attempts",
    pageContent({ translate: req.t, serviceStartPage })
  );
};

const getServiceStartPage = (req, journeyName) => {
  if (userIsAgent(req) || userIsBackOffice(req)) {
    return contextPath(CERTIFICATE_MANAGER_HOME, journeyName);
  }
  return contextPath(START, journeyName);
};

const registerTooManyAttempts = (app) => {
  app.get(
    [
      firstParentContextPath(TOO_MANY_ATTEMPTS_PATH),
      secondParentContextPath(TOO_MANY_ATTEMPTS_PATH),
    ],
    get
  );
};

export { registerTooManyAttempts, pageContent, get, TOO_MANY_ATTEMPTS_PATH };
