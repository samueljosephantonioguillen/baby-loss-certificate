import { check } from "express-validator";
import { translateValidationMessage } from "../common/translate-validation-message";
import { isNilOrEmpty } from "../../../../../common/predicates";

const checkForInput = (_, { req }) => {
  if (isNilOrEmpty(req.body["enter-location"])) {
    return false;
  }
  return true;
};

const validate = () => [
  check("enter-location")
    .custom(checkForInput)
    .withMessage(
      translateValidationMessage("validation:selectPregnancyLossInEngland")
    ),
];

export { checkForInput, validate };
