import expect from "expect";

import { sanitise } from "./sanitise";

jest.mock("../address/sanitise", () => {
  return {
    replaceMultipleSpacesWithOne: jest.fn(),
    addSpaceBeforeLastThreeCharacters: jest.fn(),
    toUpper: jest.fn(),
    replaceMultipleSpacesAndAddSingleSpaceThenUppercase: jest.fn(),
  };
});

const next = jest.fn();

test("trims starting and ending spaces", () => {
  const req = {
    body: {
      "first-name": "   John   ",
      "last-name": "   Smith   ",
    },
  };

  sanitise()(req, {}, next);

  expect(req.body).toEqual({
    "first-name": "John",
    "last-name": "Smith",
  });

  expect(next).toBeCalledTimes(1);
});

test("should capitalise first letter of first-name and last-name", () => {
  const req = {
    body: {
      "first-name": "firstname",
      "last-name": "lastname",
    },
  };
  const res = { redirect: jest.fn() };
  const next = jest.fn();

  const expected = {
    "first-name": "Firstname",
    "last-name": "Lastname",
  };

  sanitise()(req, res, next);

  expect(req.body).toEqual(expected);
});

test("removes hyphens and uppercases reference number", () => {
  const req = {
    body: {
      "certificate-reference-number": "9ad-a86-e586d",
    },
  };

  sanitise()(req, {}, next);

  expect(req.body).toEqual({
    "certificate-reference-number": "9ad-a86-e586d",
    "sanitised-certificate-reference-number": "9ADA86E586D",
  });

  expect(next).toBeCalledTimes(1);
});

test("uppercases reference number", () => {
  const req = {
    body: {
      "certificate-reference-number": "9ada86e586d",
    },
  };

  sanitise()(req, {}, next);

  expect(req.body).toEqual({
    "certificate-reference-number": "9ada86e586d",
    "sanitised-certificate-reference-number": "9ADA86E586D",
  });

  expect(next).toBeCalledTimes(1);
});

test("removes hyphens from reference number", () => {
  const req = {
    body: {
      "certificate-reference-number": "9AD-A86-E586D",
    },
  };

  sanitise()(req, {}, next);

  expect(req.body).toEqual({
    "certificate-reference-number": "9AD-A86-E586D",
    "sanitised-certificate-reference-number": "9ADA86E586D",
  });

  expect(next).toBeCalledTimes(1);
});

test("no transformation when in sanitised form", () => {
  const req = {
    body: {
      "certificate-reference-number": "9ADA86E586D",
    },
  };

  sanitise()(req, {}, next);

  expect(req.body).toEqual({
    "certificate-reference-number": "9ADA86E586D",
    "sanitised-certificate-reference-number": "9ADA86E586D",
  });

  expect(next).toBeCalledTimes(1);
});
