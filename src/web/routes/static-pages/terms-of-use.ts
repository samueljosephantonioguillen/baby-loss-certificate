import { CONTEXT_PATH, TERMS_OF_USE } from "./paths";
import { renderStaticPage } from "./static-page-content";

const pageContent = ({ translate, req }) => ({
  title: translate("termsOfUse.title"),
  firstHeading: translate("termsOfUse.firstHeading"),
  firstBody: translate("termsOfUse.firstBody"),
  secondHeading: translate("termsOfUse.secondHeading"),
  secondBody: translate("termsOfUse.secondBody"),
  thirdHeading: translate("termsOfUse.thirdHeading"),
  thirdBody: translate("termsOfUse.thirdBody"),
  fourthHeading: translate("termsOfUse.fourthHeading"),
  fourthBody: translate("termsOfUse.fourthBody"),
  fifthHeading: translate("termsOfUse.fifthHeading"),
  fifthBody: translate("termsOfUse.fifthBody"),
  sixthHeading: translate("termsOfUse.sixthHeading"),
  sixthBody: translate("termsOfUse.sixthBody"),
  hideServiceLink: req.session.hideServiceLink,
});

const renderTermsOfUse = (app) => {
  app.get(
    CONTEXT_PATH + TERMS_OF_USE,
    renderStaticPage("terms-of-use", pageContent)
  );
};

export { renderTermsOfUse, pageContent };
