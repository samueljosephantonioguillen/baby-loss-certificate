import { check } from "express-validator";

import { translateValidationMessage } from "../../common/translate-validation-message";
import { config } from "../../../../../../config";
import { getFlowDataObjectName } from "../../../tools/data-object";

const overrideOtpEnvs = ["local", "dev", "test", "stage"];
const overrideOtp = "123456";

const checkSecurityCodeMatches = (_, { req }) => {
  if (
    overrideOtpEnvs.includes(config.environment.NODE_ENV || "") &&
    req.body["security-code"] === overrideOtp
  ) {
    return true;
  }

  if (
    req.body["security-code"] ===
    req.session[getFlowDataObjectName(req.session.journeyName)].otp
  ) {
    return true;
  }

  req.session[getFlowDataObjectName(req.session.journeyName)].otpAttempts += 1;
  return false;
};

const validate = () => [
  check("security-code")
    .not()
    .isEmpty()
    .bail()
    .withMessage(translateValidationMessage("validation:missingSecurityCode"))
    .custom(checkSecurityCodeMatches)
    .bail()
    .withMessage(translateValidationMessage("validation:invalidSecurityCode")),
];

export { validate };
