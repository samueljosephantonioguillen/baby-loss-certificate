const sanitise = () => (req, res, next) => {
  req.body["first-name"] = req.body["first-name"].trim();
  req.body["last-name"] = req.body["last-name"].trim();

  req.body["first-name"] = req.body["first-name"].replace(/`/g, "'");
  req.body["last-name"] = req.body["last-name"].replace(/`/g, "'");

  req.body["first-name"] = req.body["first-name"].replace(/’/g, "'");
  req.body["last-name"] = req.body["last-name"].replace(/’/g, "'");

  // capitalise first letter
  req.body["first-name"] =
    req.body["first-name"].charAt(0).toUpperCase() +
    req.body["first-name"].slice(1);

  req.body["last-name"] =
    req.body["last-name"].charAt(0).toUpperCase() +
    req.body["last-name"].slice(1);

  next();
};

export { sanitise };
