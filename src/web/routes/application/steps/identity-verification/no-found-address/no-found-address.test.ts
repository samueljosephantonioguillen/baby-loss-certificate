import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";
import { noFoundAddress, pageContent, isNavigable } from "./no-found-address";

test("noFoundAddress should match expected outcomes", () => {
  const expectedResults = {
    path: "/cannot-request-cannot-find-address",
    template: "not-eligible",
    pageContent,
    isNavigable,
  };

  expect(noFoundAddress).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("noFoundAddress.title"),
    heading: translate("noFoundAddress.heading"),
    bodyLineOne: translate("noFoundAddress.body.firstLine"),
    bodyLineTwo: translate("noFoundAddress.body.secondLine"),
    backLinkText: translate("buttons:back"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when addressNotFound is true", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          addressNotFound: true,
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when addressNotFound is false", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          addressNotFound: false,
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });

  test("should return false when addressNotFound is not defined", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          addressNotFound: undefined,
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});
