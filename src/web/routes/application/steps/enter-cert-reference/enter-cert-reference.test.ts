import expect from "expect";

import { logger } from "../../../../logger/logger";

jest.mock("../../../../logger/logger", () => ({
  logger: {
    info: jest.fn(),
    error: jest.fn(),
  },
}));

const lookupCertificateReference = jest.fn();
jest.mock("../common/pg-client", () => ({
  lookupCertificateReference: lookupCertificateReference,
}));

import {
  enterCertReference,
  pageContent,
  sanitise,
  validate,
  behaviourForPost,
  behaviourForGet,
} from "./enter-cert-reference";
import { SECOND_PARENT_DATA_OBJECT_NAME } from "../../tools/data-object";
import { formatDate } from "../common/format-date";

test("enterCertReference should match expected outcomes", () => {
  const expectedResults = {
    path: "/enter-reference-number",
    template: "enter-cert-reference",
    pageContent,
    sanitise,
    validate,
    behaviourForPost,
    behaviourForGet,
  };

  expect(enterCertReference).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("enterCertReference.title"),
    heading: translate("enterCertReference.heading"),
    hintTextOne: translate("enterCertReference.hintText.lineOne"),
    hintTextTwo: translate("enterCertReference.hintText.lineTwo"),
    hintTextThree: translate("enterCertReference.hintText.lineThree"),
    continueButtonText: translate("buttons:continue"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("behaviourForPost", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  test("should return next when db call is successful", async () => {
    const certificateReference = "9ADA86E586D";
    const req = {
      body: {
        "sp-reference-number": "9AD-A86-E586D",
        "sp-sanitised-reference-number": certificateReference,
      },
      t: (string) => string,
      session: {
        secondParent: {},
      },
    };
    const data = {
      certificate_reference: certificateReference,
      parent1_given_name: "Given",
      parent1_family_name: "Family",
      parent1_relationship: "Father",
      baby_sex: "Male",
      loss_placename: "Kent",
      loss_day: "01",
      loss_month: "01",
      loss_year: "2020",
      baby_name: "Baby",
      print_date_of_loss: true,
      parent2_id: "mock-uuid2",
      parent2_relationship: "Mother",
      parent2_date_of_birth: "2023-11-10T16:14:49Z",
      parent2_email: "secondParent@test.com",
      application_id: "123",
      application_address_line_1: "addressLine1",
      application_address_line_2: "addressLine2",
      application_address_line_3: "addressLine3",
      application_address_line_4: "addressLine4",
      application_address_line_5: "addressLine5",
      application_address_postcode: "ts11aa",
      parent1_created_by: "citizen",
      parent1_date_of_birth: "01-01-2000",
      parent1_email: "firstParent@test.com",
      parent1_id: "mock-uuid",
      parent1_reference_number_issuing_country: "qqq",
      parent1_reference_number: "1234567890",
      parent1_reference_type: "1",
    };

    lookupCertificateReference.mockResolvedValueOnce({ rows: [data] });

    const expectedLog = `retrieved row for certificate reference: ${certificateReference}`;

    const res = {};
    const next = jest.fn();
    await behaviourForPost()(req, res, next);
    expect(next).toBeCalledTimes(1);
    expect(lookupCertificateReference).toBeCalledWith(certificateReference);
    expect(logger.info).toBeCalledWith(expectedLog);
    expect(req.session[SECOND_PARENT_DATA_OBJECT_NAME]).toEqual({
      "fp-first-name": data.parent1_given_name,
      "fp-last-name": data.parent1_family_name,
      "fp-relation-to-baby": data.parent1_relationship,
      "baby-sex": data.baby_sex,
      "baby-loss-location": data.loss_placename,
      "baby-date-of-loss-day": data.loss_day,
      "baby-date-of-loss-month": data.loss_month,
      "baby-date-of-loss-year": data.loss_year,
      "baby-name": data.baby_name,
      "print-date-of-loss": data.print_date_of_loss,
      "relation-to-baby": data.parent2_relationship,
      "application-id": data.application_id,
      "fp-address-line-1": data.application_address_line_1,
      "fp-address-line-2": data.application_address_line_2,
      "fp-address-line-3": data.application_address_line_3,
      "fp-address-line-4": data.application_address_line_4,
      "fp-address-line-5": data.application_address_line_5,
      "fp-created-by": data.parent1_created_by,
      "fp-date-of-birth": data.parent1_date_of_birth,
      "fp-email": data.parent1_email,
      "fp-parent-id": data.parent1_id,
      "fp-postcode": data.application_address_postcode,
      "fp-reference-country": data.parent1_reference_number_issuing_country,
      "fp-reference-number": data.parent1_reference_number,
      "fp-reference-type": data.parent1_reference_type,
      "sp-date-of-birth": formatDate(data.parent2_date_of_birth),
      "sp-email": data.parent2_email,
      "sp-parent-id": data.parent2_id,
    });
  });

  test("should return error for no row found on successful db call with no data", async () => {
    const certificateReference = "9ADA86E586D";
    const req = {
      body: {
        "sp-reference-number": "9AD-A86-E586D",
        "sp-sanitised-reference-number": certificateReference,
      },
      t: (string) => string,
      session: {},
    };

    lookupCertificateReference.mockResolvedValueOnce({ rows: [] });

    const expectedLog = `no valid row found for certificate reference: ${certificateReference}`;

    const expectedNoRowFoundError = {
      errorTitleText: "validation:errorTitleText",
      errors: [
        {
          msg: "enterCertReference.noCertificateReferenceFoundError",
          path: "sp-reference-number",
        },
      ],
    };

    const res = { locals: {} };
    const next = jest.fn();
    await behaviourForPost()(req, res, next);
    expect(next).toBeCalledTimes(1);
    expect(lookupCertificateReference).toBeCalledWith(certificateReference);
    expect(logger.info).toBeCalledWith(expectedLog);
    expect(res.locals).toEqual(expectedNoRowFoundError);
  });

  test("should return try again error for unsuccessful db call", async () => {
    const certificateReference = "9ADA86E586D";
    const req = {
      body: {
        "sp-reference-number": "9AD-A86-E586D",
        "sp-sanitised-reference-number": certificateReference,
      },
      t: (string) => string,
      session: {},
    };

    const error = "some error";
    lookupCertificateReference.mockRejectedValue(error);

    const expectedLog = `query to application table has failed: ${error}`;

    const expectedNoRowFoundError = {
      errorTitleText: "validation:errorTitleText",
      errors: [
        {
          path: "continue-button",
          msg: "validation:tryAgainLater",
        },
      ],
    };

    const res = { locals: {} };
    const next = jest.fn();
    await behaviourForPost()(req, res, next);
    expect(next).toBeCalledTimes(1);
    expect(lookupCertificateReference).toBeCalledWith(certificateReference);
    expect(logger.error).toBeCalledWith(expectedLog);
    expect(res.locals).toEqual(expectedNoRowFoundError);
  });
});
