import { registerSessionEndedError } from "./session-ended";

const SESSION_ENDED_URL = "/test-context/session-ended";
const SP_SESSION_ENDED_URL = "/sp-test-context/session-ended";

const req = { t: (string) => string };
const app = {
  ...jest.requireActual("express"),
  get: jest.fn(),
  all: jest.fn(),
};
const res = { render: jest.fn() };

test(`registerSessionEndedError() should redirect to ${SESSION_ENDED_URL}`, () => {
  app.all.mockImplementation((path, callback) => {
    callback(req, res);
  });

  registerSessionEndedError(app);

  expect(app.get).toBeCalledTimes(1);
  expect(app.get).toHaveBeenNthCalledWith(
    1,
    [SESSION_ENDED_URL, SP_SESSION_ENDED_URL],
    expect.any(Function)
  );
});
