const NUMBER_OF_ASTERISKS = 8;

const obfuscateEmail = (email) => {
  if (email) {
    const parts = email.split("@");
    const username = parts[0];
    const domain = parts[1];

    const maskedUsername =
      username.substring(0, 2) + "*".repeat(NUMBER_OF_ASTERISKS);
    return `${maskedUsername}@${domain}`;
  }
  return null;
};

const obfuscatePhoneNumber = (phoneNumber) => {
  if (phoneNumber) {
    const length = phoneNumber.length;
    return "*".repeat(NUMBER_OF_ASTERISKS) + phoneNumber.substring(length - 3);
  }
  return null;
};

export { obfuscateEmail, obfuscatePhoneNumber };
