import expect from "expect";
import { validate } from "./validate";
import { applyExpressValidation } from "../common/test/apply-express-validation";

test("return no validation errors when relation-to-baby field is valid", async () => {
  const req = {
    body: {
      "relation-to-baby": "mother",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("return validation errors when relation-to-baby field is empty", async () => {
  const req = {
    body: {
      "relation-to-baby": undefined,
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(1);
  expect(result.array()[0].msg).toBe("validation:selectRelationToBaby");
});
