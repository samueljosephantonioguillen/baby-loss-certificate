import expect from "expect";

import { logger } from "../../../../logger/logger";

jest.mock("../../../../logger/logger", () => ({
  logger: {
    info: jest.fn(),
  },
}));

import {
  sendOtpEmail,
  sendOtpSms,
  sendUnsuccessfulApplicationEmail,
} from "./notify-client";

import { NotifyClient } from "notifications-node-client";
jest.mock("notifications-node-client", () => {
  const mockNotifyClient = {
    sendEmail: jest.fn(),
    sendSms: jest.fn(),
    sendUnsuccessfulApplicationEmail: jest.fn(),
  };
  return { NotifyClient: jest.fn(() => mockNotifyClient) };
});

let notifyClient;

beforeEach(() => {
  notifyClient = new NotifyClient();
});

afterEach(() => {
  jest.clearAllMocks();
});

test("sendOtpEmail", async () => {
  notifyClient.sendEmail.mockResolvedValueOnce({ status: 201 });
  await sendOtpEmail({}, "test@email.com", "123456");

  expect(logger.info).toBeCalled();
});

test("sendOtpSms", async () => {
  notifyClient.sendSms.mockResolvedValueOnce({ status: 201 });
  await sendOtpSms({}, "01632 960669", "123456");

  expect(logger.info).toBeCalled();
});

test("sendUnsuccessfulApplicationEmail", async () => {
  notifyClient.sendEmail.mockResolvedValueOnce({ status: 201 });
  await sendUnsuccessfulApplicationEmail(
    {},
    "test@email.com",
    "testName",
    "1234567890"
  );

  expect(logger.info).toBeCalled();
});
