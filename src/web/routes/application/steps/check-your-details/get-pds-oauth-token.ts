import {
  SecretsManagerClient,
  GetSecretValueCommand,
} from "@aws-sdk/client-secrets-manager";

import { config } from "../../../../../config";
import { logger } from "../../../../logger/logger";

const secretsManagerClient = new SecretsManagerClient({
  region: "eu-west-2",
  endpoint: config.environment.AWS_CLIENT_ENDPOINT,
});
const getOauthTokenCommand = new GetSecretValueCommand({
  SecretId: config.environment.PDS_OAUTH_TOKEN_SECRET_NAME,
});

const getPdsOauthToken = async (req) => {
  try {
    const oauthTokenSecret = await secretsManagerClient.send(
      getOauthTokenCommand
    );

    const oauthTokenSecretString = oauthTokenSecret.SecretString ?? "";
    return JSON.parse(oauthTokenSecretString).oauth_token;
  } catch (error) {
    logger.error("Error getting PDS Oauth Token", req);
    throw error;
  }
};

export { getPdsOauthToken };
