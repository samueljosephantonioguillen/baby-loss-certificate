import { getFlowDataObjectName } from "../../tools/data-object";

const pageContent = ({ translate }) => ({
  title: translate("notInEngland.title"),
  heading: translate("notInEngland.heading"),
  bodyLineOne: translate("notInEngland.body.firstLine"),
  bodyLineTwo: translate("notInEngland.body.secondLine"),
  supportOrganisationsHeading: translate("supportOrganisations.heading"),
  supportOrganisationsBody: translate("supportOrganisations.body"),
  supportOrganisationsList: translate("supportOrganisations.listItems"),
  backLinkText: translate("buttons:back"),
});

const isNavigable = (req) =>
  req.session[getFlowDataObjectName(req.session.journeyName)][
    "enter-location"
  ] === "no";

const notInEngland = {
  path: "/cannot-request-not-in-england",
  template: "not-eligible",
  pageContent,
  isNavigable,
};

export { notInEngland, pageContent, isNavigable };
