import { stateMachine } from "../../../flow-control/state-machine";
import { INCREMENT_NEXT_ALLOWED_PATH } from "../../../flow-control/state-machine/actions";
import { IN_REVIEW, IN_REVIEW_PROGRESS } from "../../../flow-control/states";
import { contextPath } from "../../../paths/context-path";
import { CHECK_ANSWERS_URL } from "../../../paths/paths";
import { isNilOrEmpty } from "../../../../../../common/predicates";

import { validationResult } from "express-validator";
import { validate } from "./validate";
import { getFlowDataObjectName } from "../../../tools/data-object";

const pageContent = ({ translate }) => ({
  title: translate("includeBabySex.title"),
  sectionTitle: translate("section.certificateDetails"),
  heading: translate("includeBabySex.heading"),
  bodyLineOne: translate("includeBabySex.body.firstLine"),
  hintLineOne: translate("includeBabySex.hint.firstLine"),
  yes: translate("yes"),
  no: translate("no"),
  or: translate("or"),
  unknown: translate("includeBabySex.unknown"),
  continueButtonText: translate("buttons:continue"),
  backLinkText: translate("buttons:back"),
});

const behaviourForGet = (config, journey) => (req, res, next) => {
  if (stateMachine.getState(req, journey) === IN_REVIEW_PROGRESS) {
    const dataObjectName = getFlowDataObjectName(journey.name);
    if (
      req.session[dataObjectName]["include-baby-sex"] === "yes" &&
      isNilOrEmpty(req.session[dataObjectName]["baby-sex"])
    ) {
      req.session[dataObjectName]["include-baby-sex"] = "no";
    }
    stateMachine.setState(IN_REVIEW, req, journey);
    stateMachine.dispatch(INCREMENT_NEXT_ALLOWED_PATH, req, journey);
    res.locals.previous = contextPath(CHECK_ANSWERS_URL, journey.name);
  }

  next();
};

const behaviourForPost = (config, journey) => (req, res, next) => {
  if (!validationResult(req).isEmpty()) {
    return next();
  }

  if (
    stateMachine.getState(req, journey) === IN_REVIEW &&
    req.body["include-baby-sex"] === "yes"
  ) {
    stateMachine.setState(IN_REVIEW_PROGRESS, req, journey);
  }

  if (
    req.body["include-baby-sex"] === "no" ||
    req.body["include-baby-sex"] === "unknown"
  ) {
    delete req.session[getFlowDataObjectName(journey.name)]["baby-sex"];
  }

  next();
};

const includeBabySex = {
  path: "/sex-of-baby",
  template: "include-baby-sex",
  pageContent,
  validate,
  behaviourForGet,
  behaviourForPost,
};

export {
  includeBabySex,
  pageContent,
  validate,
  behaviourForGet,
  behaviourForPost,
};
