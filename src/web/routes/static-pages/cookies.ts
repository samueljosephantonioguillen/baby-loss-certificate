import { MAINFLOW } from "../application/journey-definitions";
import { contextPath } from "../application/paths/context-path";
import { CONTEXT_PATH, COOKIES } from "./paths";
import { renderStaticPage } from "./static-page-content";

const pageContent = ({ translate, req }) => ({
  title: translate("cookies.title"),
  bannerTitle: translate("cookies.bannerTitle"),
  bannerText: translate("cookies.bannerText"),
  whatAreCookiesHeading: translate("cookies.whatAreCookies.heading"),
  whatAreCookiesBody: translate("cookies.whatAreCookies.body"),
  essentialCookiesHeading: translate("cookies.essentialCookies.heading"),
  essentialCookiesBody: translate("cookies.essentialCookies.body"),
  essentialCookiesTableHeadings: translate(
    "cookies.essentialCookies.table.headers"
  ),
  essentialCookiesTableRows: translate(
    "cookies.essentialCookies.table.cookies"
  ),
  analyticsCookiesHeading: translate("cookies.analyticsCookies.heading"),
  analyticsCookiesBody: translate("cookies.analyticsCookies.body"),
  analyticsCookiesTableHeadings: translate(
    "cookies.analyticsCookies.table.headers"
  ),
  analyticsCookiesTableRows: translate(
    "cookies.analyticsCookies.table.cookies"
  ),
  enableAnalyticsCookies: translate("cookies.enableAnalyticsCookies"),
  cookies: req.cookies.cookieConsent === "true" ? "yes" : "no",
  savedCookies: req.session.savedCookies,
  yes: translate("yes"),
  no: translate("no"),
  button: translate("cookies.button"),
  hideServiceLink: req.session.hideServiceLink,
});

const get = (req, res) => {
  renderStaticPage("cookies", pageContent)(req, res);
  req.session.savedCookies = false;
};

const post = (req, res) => {
  const enableAnalyticsCookies = req.body["enable-analytics-cookies"] === "yes";
  res.cookie("cookieConsent", enableAnalyticsCookies, { maxAge: 31536000000 });
  req.session.savedCookies = true;

  res.redirect(contextPath("/cookies", MAINFLOW.name));
};

const renderCookies = (app) => {
  app.get(CONTEXT_PATH + COOKIES, get);
  app.post(CONTEXT_PATH + COOKIES, post);
};

export { renderCookies, pageContent, get, post };
