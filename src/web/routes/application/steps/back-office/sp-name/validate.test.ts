import expect from "expect";
import { validate } from "./validate";
import { applyExpressValidation } from "../../common/test/apply-express-validation";

test("validation middleware errors for their-first-name field is empty", async () => {
  const req = {
    body: {
      "their-first-name": "",
      "their-last-name": "Lee",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const errorOne = result.array()[0];

  expect(result.array().length).toBe(1);
  expect(errorOne.msg).toBe("validation:missingFirstName");
});

test("validation middleware errors for their-last-name field is empty", async () => {
  const req = {
    body: {
      "their-first-name": "Übbe",
      "their-last-name": "",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const errorOne = result.array()[0];

  expect(result.array().length).toBe(1);
  expect(errorOne.msg).toBe("validation:missingLastName");
});

describe("invalid names", () => {
  test("expected validation middleware errors if their-first-name and their-last-name is invalid (numbers)", async () => {
    const req = {
      body: {
        "their-first-name": "J0hn",
        "their-last-name": "Sm1th",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const errorOne = result.array()[0];
    const errorTwo = result.array()[1];

    expect(result.array().length).toBe(2);
    expect(errorOne.msg).toBe("validation:patternFirstName");
    expect(errorTwo.msg).toBe("validation:patternLastName");
  });

  test("expected validation middleware errors if their-first-name and their-last-name is invalid (special characters)", async () => {
    const req = {
      body: {
        "their-first-name": "J£ssica",
        "their-last-name": "Sm!th",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const errorOne = result.array()[0];
    const errorTwo = result.array()[1];

    expect(result.array().length).toBe(2);
    expect(errorOne.msg).toBe("validation:patternFirstName");
    expect(errorTwo.msg).toBe("validation:patternLastName");
  });

  test("expected validation middleware errors if their-first-name and their-last-name is invalid (begins with [-])", async () => {
    const req = {
      body: {
        "their-first-name": "-Luke",
        "their-last-name": "-Skywalker",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const errorOne = result.array()[0];
    const errorTwo = result.array()[1];

    expect(result.array().length).toBe(2);
    expect(errorOne.msg).toBe("validation:patternFirstName");
    expect(errorTwo.msg).toBe("validation:patternLastName");
  });

  test("expected validation middleware errors if their-first-name and their-last-name is invalid (begins with ['])", async () => {
    const req = {
      body: {
        "their-first-name": "'John",
        "their-last-name": "'O'Hara",
      },
    };

    const result = await applyExpressValidation(req, validate());
    if (result === undefined) {
      throw new Error("Express validation failed");
    }

    const errorOne = result.array()[0];
    const errorTwo = result.array()[1];

    expect(result.array().length).toBe(2);
    expect(errorOne.msg).toBe("validation:patternFirstName");
    expect(errorTwo.msg).toBe("validation:patternLastName");
  });
});

test("validation middleware errors for their-first-name field exceeds max length", async () => {
  const req = {
    body: {
      "their-first-name":
        "RalphRalphRalphRalphRalphRalphRalphRalphRalphRalphRalphRalphRalphRalphRalphRalph",
      "their-last-name": "Smith",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const errorOne = result.array()[0];

  expect(result.array().length).toBe(1);
  expect(errorOne.msg).toBe("validation:firstNameTooLong");
});

test("validation middleware errors for their-last-name field exceeds max length", async () => {
  const req = {
    body: {
      "their-first-name": "John",
      "their-last-name":
        "SmithSmithSmithSmithSmithSmithSmithSmithSmithSmithSmithSmithSmithSmithSmithSmith",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  const errorOne = result.array()[0];

  expect(result.array().length).toBe(1);
  expect(errorOne.msg).toBe("validation:lastNameTooLong");
});

test("No validation error when their-first-name and their-last-name fields are valid", async () => {
  const req = {
    body: {
      "their-first-name": "Ralph",
      "their-last-name": "Smith",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});

test("No validation error when their-first-name and their-last-name fields contains [a-z] [A-Z] ['- space] ", async () => {
  const req = {
    body: {
      "their-first-name": "John'a -RT",
      "their-last-name": "O'She-A",
    },
  };

  const result = await applyExpressValidation(req, validate());
  if (result === undefined) {
    throw new Error("Express validation failed");
  }

  expect(result.array().length).toBe(0);
});
