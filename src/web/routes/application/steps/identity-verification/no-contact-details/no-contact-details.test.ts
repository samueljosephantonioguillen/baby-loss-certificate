import { MAINFLOW } from "../../../journey-definitions";
import { MAINFLOW_DATA_OBJECT_NAME } from "../../../tools/data-object";
import {
  noContactDetails,
  pageContent,
  isNavigable,
} from "./no-contact-details";

test("noContactDetails should match expected outcomes", () => {
  const expectedResults = {
    path: "/cannot-request-cannot-find-contact-details",
    template: "not-eligible",
    pageContent,
    isNavigable,
  };

  expect(noContactDetails).toEqual(expectedResults);
});

test("pageContent should return expected results", () => {
  const translate = (string, object?) => `${string}${object}`;

  const expected = {
    title: translate("noContactDetails.title"),
    heading: translate("noContactDetails.heading"),
    bodyLineOne: translate("noContactDetails.body.firstLine"),
    bodyLineTwo: translate("noContactDetails.body.secondLine"),
    bodyLineThree: translate("noContactDetails.body.thirdLine"),
    backLinkText: translate("buttons:back"),
  };
  const result = pageContent({ translate });

  expect(result).toEqual(expected);
});

describe("isNavigable", () => {
  test("should return true when noContactDetails is true", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          noContactDetails: true,
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(true);
  });

  test("should return false when noContactDetails is false", () => {
    const req = {
      session: {
        journeyName: MAINFLOW.name,
        [MAINFLOW_DATA_OBJECT_NAME]: {
          noContactDetails: false,
        },
      },
    };
    const result = isNavigable(req);

    expect(result).toEqual(false);
  });
});
