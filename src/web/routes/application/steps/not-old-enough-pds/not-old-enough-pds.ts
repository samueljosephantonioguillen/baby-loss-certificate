import { userIsBackOffice } from "../../../azure-routes/auth";
import { getFlowDataObjectName } from "../../tools/data-object";

const pageContent = ({ translate }) => ({
  title: translate("notOldEnoughPds.title"),
  heading: translate("notOldEnoughPds.heading"),
  bodyLineOne: translate("notOldEnoughPds.body.firstLine"),
  supportOrganisationsHeading: translate("supportOrganisations.heading"),
  supportOrganisationsBody: translate("supportOrganisations.body"),
  supportOrganisationsList: translate("supportOrganisations.listItems"),
  backLinkText: translate("buttons:back"),
});

const isNavigable = (req) =>
  req.session[getFlowDataObjectName(req.session.journeyName)][
    "olderThanAgeRequirementPds"
  ] === false && !userIsBackOffice(req);

const notOldEnoughPds = {
  path: "/cannot-request-under-age",
  template: "not-eligible",
  pageContent,
  isNavigable,
};

export { notOldEnoughPds, pageContent, isNavigable };
