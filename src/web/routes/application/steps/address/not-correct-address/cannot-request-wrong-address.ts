import { userIsBackOffice } from "../../../../azure-routes/auth";
import { getFlowDataObjectName } from "../../../tools/data-object";

const pageContent = ({ translate }) => ({
  title: translate("notCorrectAddress.title"),
  heading: translate("notCorrectAddress.heading"),
  bodyLineOne: translate("notCorrectAddress.body.firstLine"),
  backLinkText: translate("buttons:back"),
});

const isNavigable = (req) =>
  req.session[getFlowDataObjectName(req.session.journeyName)][
    "confirm-your-address"
  ] === "no" && !userIsBackOffice(req);

const notCorrectAddress = {
  path: "/cannot-request-wrong-address",
  template: "not-eligible",
  pageContent,
  isNavigable,
};

export { notCorrectAddress, pageContent, isNavigable };
