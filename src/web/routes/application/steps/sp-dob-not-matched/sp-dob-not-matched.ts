import { SECOND_PARENT_DATA_OBJECT_NAME } from "../../tools/data-object";

const pageContent = ({ translate }) => ({
  title: translate("spDobNotMatched.title"),
  heading: translate("spDobNotMatched.heading"),
  bodyLineOne: translate("spDobNotMatched.body.firstLine"),
  supportOrganisationsHeading: translate("supportOrganisations.heading"),
  supportOrganisationsBody: translate("supportOrganisations.body"),
  supportOrganisationsList: translate("supportOrganisations.listItems"),
  backLinkText: translate("buttons:back"),
});

const isNavigable = (req) =>
  req.session[SECOND_PARENT_DATA_OBJECT_NAME]?.spDobNotMatched === true;

const spDobNotMatched = {
  path: "/cannot-request-certificate-date-of-birth-not-matched",
  template: "not-eligible",
  pageContent,
  isNavigable,
};

export { spDobNotMatched, pageContent, isNavigable };
