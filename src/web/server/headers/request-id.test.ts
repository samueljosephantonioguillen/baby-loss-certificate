import expect from "expect";
import * as sinon from "sinon";
import { requestID } from "./request-id";

test("requestID() should not set request ID header if header already exists", () => {
  const uuidFn = sinon.spy();
  const req = {
    headers: {
      "x-request-id": "1234",
    },
  };
  const res = {};
  const next = () => {
    /* explicit empty function */
  };

  requestID(uuidFn)(req, res, next);

  const result = req.headers["x-request-id"];
  const expected = "1234";

  expect(uuidFn.called).toBe(false);
  expect(result).toBe(expected);
});

test("requestID() should set request ID header if header does not exist", () => {
  const uuidFn = "5678";
  const req = {
    headers: {},
  };
  const res = {};
  const next = () => {
    /* explicit empty function */
  };

  requestID(uuidFn)(req, res, next);

  const result = req.headers["x-request-id"];

  expect(result).toBe(uuidFn);
});
