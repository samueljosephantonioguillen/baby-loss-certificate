import expect from "expect";

import { Pool } from "pg";
jest.mock("pg", () => {
  const mockPool = {
    query: jest.fn(),
  };
  return { Pool: jest.fn(() => mockPool) };
});

import {
  lookupApplicationStatus,
  lookupCertificateReference,
  rejectCertificate,
} from "./pg-client";
import { XeroxStatus } from "./types/xerox-statuses";
import { AGENT_ROLE } from "../../constants";
import { config } from "../../../../../config";

let pool;

jest.useFakeTimers().setSystemTime(new Date("2023-01-01"));

beforeEach(() => {
  pool = new Pool();
});

afterEach(() => {
  jest.clearAllMocks();
});

test("lookupCertificateReference should call with expected values and return log when successful", async () => {
  const certificateReference = "9ADA86E586D";
  const expectedQuery = {
    text: `SELECT 
    application.application_id,
    application.loss_year, 
    application.loss_month, 
    application.loss_day, 
    application.loss_placename, 
    application.baby_sex, 
    application.baby_name,
    application.print_date_of_loss,
    parent1.parent_given_name AS parent1_given_name, 
    parent1.parent_family_name AS parent1_family_name, 
    parent1.parent_relationship AS parent1_relationship, 
    parent1.parent_reference_number AS parent1_reference_number, 
    parent1.parent_reference_type AS parent1_reference_type,
    parent1.parent_email_address AS parent1_email,
    parent1.created_by AS parent1_created_by,

    parent2.parent_id AS parent2_id,
    parent2.parent_relationship AS parent2_relationship,
    parent2.parent_date_of_birth AS parent2_date_of_birth
  FROM 
    application
  JOIN 
    parent AS parent1 ON application.applicant_parent_id = parent1.parent_id
  JOIN 
    parent AS parent2 ON application.second_parent_id = parent2.parent_id
  WHERE application.application_status = '${XeroxStatus.INCOMPLETE}' AND application.certificate_reference = $1`,
    values: [certificateReference],
  };

  pool.query.mockResolvedValueOnce({ rows: [], rowCount: 0 });
  const result = await lookupCertificateReference(certificateReference);

  expect(pool.query).toBeCalledTimes(1);
  expect(pool.query).toBeCalledWith(expectedQuery);
  expect(result).toEqual({ rows: [], rowCount: 0 });
});

test("lookupApplicationStatus should call with application reference number and return empty array", async () => {
  const certificateReference = "9ADA86E586D";
  pool.query.mockResolvedValueOnce({ rows: [], rowCount: 0 });
  const result = await lookupApplicationStatus(
    certificateReference,
    null,
    null,
    null,
    null
  );

  expect(pool.query).toBeCalledTimes(1);
  expect(result).toEqual([]);
});

test("lookupApplicationStatus should call with first parent name  return result array", async () => {
  pool.query.mockResolvedValueOnce({
    rows: [
      {
        application_id: "5615b8c0-8c35-489d-8957-67140792660e",
        application_address_line_1: "Line 1",
        application_address_postcode: "N16 6UE ",
        loss_year: "2000",
        application_status: "incomplete",
      },
    ],
    rowCount: 1,
  });
  const result = await lookupApplicationStatus(
    null,
    "DBGiven",
    null,
    null,
    null
  );

  expect(pool.query).toBeCalledTimes(1);
  expect(result).toEqual([
    {
      application_id: "5615b8c0-8c35-489d-8957-67140792660e",
      application_address_line_1: "Line 1",
      application_address_postcode: "N16 6UE ",
      loss_year: "2000",
      application_status: "incomplete",
    },
  ]);
});

test("rejectCertificate should call with expected values and return true when update is successful", async () => {
  const req = {};
  const certificateReference = "9ADA86E586D";
  const expectedQuery = {
    text: `UPDATE application
    SET application_status = '${XeroxStatus.REJECTED}',
    updated_dtm = $2,
    updated_by = $3,
    version = version + 1
    WHERE certificate_reference = $1
    AND application_status = '${XeroxStatus.INCOMPLETE}'`,
    values: [certificateReference, new Date(), "citizen"],
  };

  pool.query.mockResolvedValueOnce({ rows: [], rowCount: 1 });
  const result = await rejectCertificate(req, certificateReference);

  expect(pool.query).toBeCalledTimes(1);
  expect(pool.query).toBeCalledWith(expectedQuery);
  expect(result).toEqual(true);
});

test("rejectCertificate should call with expected values and return true when update is successful (user is agent)", async () => {
  config.environment.USE_AUTHENTICATION = true;
  const req = {
    session: {
      account: {
        idTokenClaims: {
          roles: [AGENT_ROLE],
        },
      },
    },
  };
  const certificateReference = "9ADA86E586D";
  const expectedQuery = {
    text: `UPDATE application
    SET application_status = '${XeroxStatus.REJECTED}',
    updated_dtm = $2,
    updated_by = $3,
    version = version + 1
    WHERE certificate_reference = $1
    AND application_status = '${XeroxStatus.INCOMPLETE}'`,
    values: [certificateReference, new Date(), "agent"],
  };

  pool.query.mockResolvedValueOnce({ rows: [], rowCount: 1 });
  const result = await rejectCertificate(req, certificateReference);

  expect(pool.query).toBeCalledTimes(1);
  expect(pool.query).toBeCalledWith(expectedQuery);
  expect(result).toEqual(true);
});

test("rejectCertificate should call with expected values and return false when update is not successful", async () => {
  const req = {};
  const certificateReference = "9ADA86E586D";
  const expectedQuery = {
    text: `UPDATE application
    SET application_status = '${XeroxStatus.REJECTED}',
    updated_dtm = $2,
    updated_by = $3,
    version = version + 1
    WHERE certificate_reference = $1
    AND application_status = '${XeroxStatus.INCOMPLETE}'`,
    values: [certificateReference, new Date(), "citizen"],
  };

  pool.query.mockResolvedValueOnce({ rows: [], rowCount: 0 });
  const result = await rejectCertificate(req, certificateReference);

  expect(pool.query).toBeCalledTimes(1);
  expect(pool.query).toBeCalledWith(expectedQuery);
  expect(result).toEqual(false);
});
