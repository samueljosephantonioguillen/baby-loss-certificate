export type ApplicationRequest = {
  applicationId: string;
  certificateReference: string;
  applicantName: string;
  applicantParentId: string;
  secondParentId: string;
  addressLine1: string;
  addressLine2: string;
  addressLine3: string;
  addressLine4: string;
  addressLine5: string;
  postcode: string;
  babyName: string;
  babySex: string;
  lossDateDay: string;
  lossDateMonth: string;
  lossDateYear: string;
  lossDateFull: string;
  printDateOfLoss: boolean;
  lossPlaceName: string;
  lossCountry: string;
  applicationCompleteDateTime: Date;
  applicationType: string;
  version: number;
};

export type ParentRequest = {
  parentId: string;
  parentReferenceNumber: string;
  parentReferenceType: number;
  parentReferenceNumberIssuingCountry: string;
  parentGivenName: string;
  parentFamilyName: string;
  parentRelationship: string;
  parentDateOfBirth: Date;
  parentEmailAddress: string;
  applicationCompleteDateTime: Date;
  applicationCreatedBy: string;
  applicationId: string;
};

export type IncompleteParentRequest = {
  parentId: string;
  parentRelationship: string;
  parentDateOfBirth: Date;
  parentEmailAddress: string;
  applicationCompleteDateTime: Date;
  applicationCreatedBy: string;
  applicationId: string;
};

export type SecondParentRequest = {
  parentId: string;
  parentReferenceNumber: string;
  parentReferenceType: number;
  parentReferenceNumberIssuingCountry: string;
  parentGivenName: string;
  parentFamilyName: string;
  applicationCreatedBy: string;
};

export type SecondParentApplicationRequest = {
  applicationUpdatedBy: string;
  applicationId: string;
};
